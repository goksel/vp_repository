# README #

Virtual Parts Repository a is a database of standard virtual parts (SVPs) which are reusable, modular and composable models of physical biological parts. These models can be joined together computationally in order to facilitate the model-driven design of large-scale biological systems. 

This project has been developed as an open-source, Maven project.

Please contact us if you have any questions regarding this project (http://www.virtualparts.org/about).


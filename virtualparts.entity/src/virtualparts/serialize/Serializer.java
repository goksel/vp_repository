/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package virtualparts.serialize;

import com.thoughtworks.xstream.XStream;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import virtualparts.entity.Formula;
import virtualparts.entity.Interaction;
import virtualparts.entity.InteractionPartDetail;
import virtualparts.entity.Interactions;
import virtualparts.entity.MolecularFormType;
import virtualparts.entity.Parameter;
import virtualparts.entity.Parts;
import virtualparts.entity.Property;
import virtualparts.entity.SearchEntity;
import virtualparts.entity.Summary;
import virtualparts.entity.Part;

/**
 * Provides methods to serialise and deserialise objects representing parts and interactions
 * @author a8901379
 */
public class Serializer {
    private XStream xstream;
    /**
     * Constructor
     */
	public Serializer()
	{
		

	}
    private void IntializePartSerialization()
    {
          xstream = new XStream();

        //Part object definitions
        xstream.alias("Part", Part.class);
        xstream.aliasField("Name", Part.class, "name");
        xstream.aliasField("Type", Part.class, "type");
        xstream.aliasField("Description", Part.class, "description");
        xstream.aliasField("MetaType", Part.class, "metaType");
        xstream.aliasField("Sequence", Part.class, "sequence");
        xstream.aliasField("SequenceURI", Part.class, "sequenceURI");
        xstream.aliasField("DisplayName", Part.class, "displayName");
        xstream.aliasField("Organism", Part.class, "organism");
        xstream.aliasField("Status", Part.class, "status");
        xstream.aliasField("DesignMethod", Part.class, "designMethod");
        
        //xstream.omitField(Vpart.class, null)
        //Part.Role
        //xstream.addImplicitCollection(Vpart.class, "roles", "Role", String.class);
        xstream.addImplicitCollection(Part.class, "properties",Property.class);

        //Property object definitions
        xstream.alias("Property", Property.class);
        xstream.aliasField("Name", Property.class, "name");
        xstream.aliasField("Value", Property.class, "value");
        xstream.aliasField("Description", Property.class, "description");


        //SerializableParts definitions
        xstream.addImplicitCollection(Parts.class, "parts", Part.class);
        xstream.alias("Parts", Parts.class);
        
		/*//MolecularFormType definitions
		xstream.alias("MolecularFormType", MolecularFormType.class);
		xstream.aliasField("Name", MolecularFormType.class, "name");
		xstream.aliasField("Extension", MolecularFormType.class, "extension");
		xstream.aliasField("Value", MolecularFormType.class, "value");*/
    }

    /**
     * Deserialises an object representing parts from a given String
     * @param partsData String containing part objects
     * @return Parts
     */
    public Parts GetParts(String partsData) {
      
        IntializePartSerialization();
        Parts parts = (Parts) xstream.fromXML(partsData);
        return parts;
    }

    /**
     * Serialises a Parts objects into a String
     * @param parts
     */
    public String SerializeParts(Parts parts)
    {
       IntializePartSerialization();
       String output=xstream.toXML(parts);
       output="<?xml version=\"1.0\" encoding=\"utf-8\"?>" + output;
       return output;        
    }
    
    /**
     * Serialises a Parts objects into a String
     * @param parts
     */
    public String SerializePart(Part part)
    {
       IntializePartSerialization();
       String output=xstream.toXML(part);
       output="<?xml version=\"1.0\" encoding=\"utf-8\"?>" + output;
       return output;        
    }
    
     /**
     * Deserialises a part object from a given String
     * @param partsData String containing data about the part
     * @return Vpart
     */
     public Part GetPart(String partData) {

        IntializePartSerialization();
        Part part = (Part) xstream.fromXML(partData);
        return part;
    }

     private void IntializeSummarySerialization()
     {
           xstream = new XStream();
           xstream.alias("Summary", Summary.class);
           xstream.aliasField("PageCount", Summary.class, "pageCount");
           
     }
     /**
      * Deserialises a summary object from a given String
      * @param summaryData String containing the data
      * @return Summary
      */
     public Summary GetSummary(String summaryData) {

    	 IntializeSummarySerialization();
        Summary summary = (Summary) xstream.fromXML(summaryData);
        return summary;
    }

     /**
      * Serialises a summary object into a XML string representation
      * @param summary Summary object
      * @return XML String representation of the summary object
      */
     public String SerializeSummary(Summary summary)
 	{		
    	IntializeSummarySerialization(); 		
 		String output=xstream.toXML(summary);
 		output="<?xml version=\"1.0\" encoding=\"utf-8\"?>" + output;
 		return output;
 	}
     
     private void IntializeSearchEntitySerialization()
     {
           xstream = new XStream();
           xstream.alias("SearchEntity", SearchEntity.class);
           xstream.aliasField("SearchText", SearchEntity.class, "searchText");
           xstream.aliasField("Organism", SearchEntity.class, "organism");
           xstream.aliasField("PartType", SearchEntity.class, "partType");
           xstream.aliasField("PropertyName", SearchEntity.class, "propertyName");
           xstream.aliasField("PropertyValue", SearchEntity.class, "propertyValue");
           xstream.aliasField("ExactPropertyValueMatch", SearchEntity.class, "exactPropertyValueMatch");
           xstream.aliasField("IncludeGeneticElements", SearchEntity.class, "includeGeneticElements");           
     }
     
     /**
      * Deserialises a search entity object from a given String
      * @param searchEntityData String containing the data
      * @return SearchEntity
      */
     public SearchEntity GetSearchEntity(String searchEntityData) {

    	IntializeSearchEntitySerialization();
        SearchEntity searchEntuty= (SearchEntity) xstream.fromXML(searchEntityData);
        return searchEntuty;
    }

     /**
      * Serialises a summary object into a XML string representation
      * @param summary Summary object
      * @return XML String representation of the summary object
      */
     public String SerializeSearchEntity(SearchEntity searchEntity)
 	{		
    	 IntializeSearchEntitySerialization(); 		
 		String output=xstream.toXML(searchEntity);
 		output="<?xml version=\"1.0\" encoding=\"utf-8\"?>" + output;
 		return output;
 	}
     
     
     private void IntializeInteractionSerialization()
     {
    	xstream = new XStream();
         //Interacttion object definitions
        xstream.alias("Interaction", Interaction.class);
        xstream.aliasField("Name", Interaction.class, "name");
        xstream.aliasField("Description", Interaction.class, "description");
        xstream.aliasField("InteractionType", Interaction.class, "interactionType");
        xstream.aliasField("MathName", Interaction.class, "mathName");
        xstream.aliasField("FreeTextMath", Interaction.class, "freeTextMath");
        xstream.aliasField("IsReaction", Interaction.class, "isReaction");
        xstream.aliasField("IsInternal", Interaction.class, "isInternal");
        xstream.aliasField("IsReversible", Interaction.class, "isReversible");
        
        //String[] Part definitions
        xstream.addImplicitCollection(Interaction.class, "parts", "Part", String.class);

        //Interactions definitions
        //Interactions colelction is named as "Interactions" in the xml
        xstream.alias("Interactions", Interactions.class);
        //Interactions contains a filed named interactions which is a list of Interaction class
        xstream.addImplicitCollection(Interactions.class, "interactions", Interaction.class);  
        
        xstream.alias("PartDetail", InteractionPartDetail.class);
        xstream.aliasField("PartDetails", Interaction.class, "partDetails");
        //xstream.addImplicitCollection(Interaction.class, "partDetails", InteractionPartDetail.class);          
        xstream.aliasField("NameInMath", InteractionPartDetail.class, "mathName");
        xstream.aliasField("Part", InteractionPartDetail.class, "partName");
        xstream.aliasField("MoleculerFormType", InteractionPartDetail.class, "partForm");
        xstream.aliasField("InteractionRole", InteractionPartDetail.class, "interactionRole");
        xstream.aliasField("Stoichiometry", InteractionPartDetail.class, "numberOfMolecules");
        
        //xstream.addImplicitCollection(Interaction.class, "parameters", Parameter.class);  
        xstream.alias("Parameter", Parameter.class);
        xstream.aliasField("Parameters", Interaction.class, "parameters");        
        xstream.aliasField("Scope", Parameter.class, "scope");
        xstream.aliasField("Name", Parameter.class, "name");
        xstream.aliasField("ParameterType", Parameter.class, "parameterType");
        xstream.aliasField("Value", Parameter.class, "value");
        xstream.aliasField("EvidenceType", Parameter.class, "evidenceType");
     }
     
      /**
      * Deserialises an Interactions object from a given String
      * @param summaryData String containing the data
      * @return Interactions
      */
    public Interactions GetInteractions(String interactionsData) {
        
    	IntializeInteractionSerialization();
        Interactions interactions = (Interactions) xstream.fromXML(interactionsData);
        return interactions;
    }
    
    /**
     * Serialises Interactions objects into XML strings
     * @param parts
     */
    public String SerializeInteractions(Interactions interactions)
    {
    	IntializeInteractionSerialization();
       String output=xstream.toXML(interactions);
       output="<?xml version=\"1.0\" encoding=\"utf-8\"?>" + output;
       return output;        
    }

    
    private void IntializeMolecularFormTypeSerialization()
    {
          xstream = new XStream();
          xstream.alias("MolecularFormTypes", List.class);
          xstream.alias("MolecularFormType", MolecularFormType.class);
          xstream.aliasField("Name", MolecularFormType.class, "name");
          xstream.aliasField("Accession", MolecularFormType.class, "accession");
          xstream.aliasField("Extension", MolecularFormType.class, "extension");
          
    }
    
    /**
     * Deserialises an Interactions object from a given String
     * @param summaryData String containing the data
     * @return Interactions
     */
   public List<MolecularFormType> GetMolecularFormTypes(String data) {
       
	   IntializeMolecularFormTypeSerialization();
       List<MolecularFormType> types = (List<MolecularFormType>) xstream.fromXML(data);
       return types;
   }
   
   /**
    * Serialises Interactions objects into XML strings
    * @param parts
    */
   public String SerializeMolecularFormType(List<MolecularFormType> types)
   {
	  IntializeMolecularFormTypeSerialization();
      String output=xstream.toXML(types);
      output="<?xml version=\"1.0\" encoding=\"utf-8\"?>" + output;
      return output;        
   }
   
   private void InitializeFormulaSerialization()
   {
         xstream = new XStream();
         xstream.alias("Maths", List.class);
         xstream.alias("Math", Formula.class);
         xstream.aliasField("Name", Formula.class, "name");
         xstream.aliasField("Formula", Formula.class, "formula");
         xstream.aliasField("ReactionFlux", Formula.class, "reactionFlux");
         xstream.aliasField("Function", Formula.class, "function");
         xstream.aliasField("Accession", Formula.class, "accession");
         xstream.aliasField("MetaType", Formula.class, "metaType");      
   }
   
   /**
    * Deserialises an Interactions object from a given String
    * @param summaryData String containing the data
    * @return Interactions
    */
  public List<Formula> GetFormulas(String data) {
      
	  InitializeFormulaSerialization();
      List<Formula> maths = (List<Formula>) xstream.fromXML(data);
      return maths;
  }
  
  /**
   * Serialises Interactions objects into XML strings
   * @param parts
   */
  public String SerializeFormulas(List<Formula> maths)
  {
	  InitializeFormulaSerialization();
     String output=xstream.toXML(maths);
     output="<?xml version=\"1.0\" encoding=\"utf-8\"?>" + output;
     return output;        
  }
  
    
    /**
     * Creates a file using a given String
     * @param filename The file path
     * @param content The content to create the file with
     * @throws Exception
     */
    public static void WriteToFile(String filename, String content) throws Exception
    {
        BufferedWriter bufferedWriter = null;
        try
        {
            //Construct the BufferedWriter object
            bufferedWriter = new BufferedWriter(new java.io.FileWriter(filename));

            //Start writing to the output stream
            bufferedWriter.write(content);


        }
        catch (FileNotFoundException ex)
        {
            ex.printStackTrace();
            throw ex;
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
            throw ex;
        }
        finally
        {
            //Close the BufferedWriter
            try
            {
                if (bufferedWriter != null)
                {
                    bufferedWriter.flush();
                    bufferedWriter.close();
                }
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
        }
    }

    //http://www.javapractices.com/topic/TopicAction.do?Id=42

      /**
       * Reads the content of a file into a String variable
       * @param aFile File object, referring to the location of the file
       * @return String variable including the file content
       * @throws Exception
       */
      public static String GetFileContent(File aFile) throws Exception
    {
        //...checks on aFile are elided
        StringBuilder contents = new StringBuilder();
        try
        {
            //use buffering, reading one line at a time
            //FileReader always assumes default encoding is OK!
            BufferedReader input = new BufferedReader(new java.io.FileReader(aFile));
            try
            {
                String line = null; //not declared within while loop
        /*
                 * readLine is a bit quirky :
                 * it returns the content of a line MINUS the newline.
                 * it returns null only for the END of the stream.
                 * it returns an empty String if two newlines appear in a row.
                 */
                while ((line = input.readLine()) != null)
                {
                    contents.append(line);
                    contents.append(System.getProperty("line.separator"));
                }
            }
            finally
            {
                input.close();
            }
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
            throw ex;
        }

        return contents.toString();
    }



}

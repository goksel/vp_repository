package virtualparts.serialize;

import static org.junit.Assert.*;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import virtualparts.entity.Formula;
import virtualparts.entity.Interaction;
import virtualparts.entity.InteractionPartDetail;
import virtualparts.entity.InteractionRole;
import virtualparts.entity.Interactions;
import virtualparts.entity.MolecularFormType;
import virtualparts.entity.Namespaces;
import virtualparts.entity.Parameter;
import virtualparts.entity.Part;
import virtualparts.entity.Parts;
import virtualparts.entity.Property;
import virtualparts.entity.Summary;

public class SerializerTest {

     public SerializerTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

	private Parts GetTestParts()
	{
		Parts parts=new Parts();
		Part part=new Part();
		part.setDescription("test description");
		part.setDesignMethod("Manual");
		part.setDisplayName("test display name");
		part.setMetaType("Part");
		part.setName("test_name");
		part.setOrganism("B. subtilis");
		Property property=new Property();
		property.setDescription("property1 description");
		property.setName("property1 name");
		property.setValue("property1 value");
		part.AddProperty(property);
		Property property2=new Property();
		property2.setDescription("property2 description");
		property2.setName("property2 name");
		property2.setValue("property2 value");
		part.AddProperty(property2);
		part.setSequence("AAAGGAGGTCACT");
		part.setSequenceURI("http://www.bsubtilis.org/test_name/fasta");
		part.setStatus("Works");
		part.setType("RBS");
		parts.AddPart(part);
		return parts;
	}
	@Test
	public void testGetParts() {
		Parts parts=GetTestParts();
		Serializer serializer=new Serializer();
		String partData=serializer.SerializeParts(parts);
		Parts parts2=serializer.GetParts(partData);
		assertTrue("Serialization failed",parts2!=null && parts2.getParts()!=null && parts2.getParts().size()>0);
		Part part1=parts.getParts().get(0);
		Part part2=parts2.getParts().get(0);
		assertTrue("Description serialization failed",part1.getDescription().equals(part2.getDescription()));
		assertTrue("DesignMethod Serialization failed",part1.getDesignMethod().equals(part2.getDesignMethod()));
		assertTrue("DisplayName Serialization failed",part1.getDisplayName().equals(part2.getDisplayName()));
		assertTrue("MetaType Serialization failed",part1.getMetaType().equals(part2.getMetaType()));
		assertTrue("Property Serialization failed",part1.getProperties().size()==part2.getProperties().size());
		
		//TODO Add other properties	
	}
	
	private Interactions GetTestInteractions()
	{
		Interactions interactions=new Interactions();
		Interaction interaction=new Interaction();
		interaction.setDescription("Int description");
		interaction.setFreeTextMath("Int free text");
		interaction.setInteractionType("Int type");
		interaction.setIsInternal(true);
		interaction.setIsReaction(false);
		interaction.setMathName("int Math name");
		interaction.setName("int name");		
		interaction.AddParameter(new Parameter("param type", "param name", 0.4, "inferred", "Input"));
		interaction.AddPart("SpaR");
		interaction.AddPartDetail(new InteractionPartDetail("SpaR", "Phosphorylated", InteractionRole.INPUT.toString(), 2, "SpaR_P"));
		interaction.AddPartDetail(new InteractionPartDetail("SpaR", "Default", InteractionRole.OUTPUT.toString(), 2, "SpaR"));
		
		interactions.AddInteraction(interaction);
		return interactions;
	}
	
	@Test 
	public void testGetInteractions()
	{
		Interactions interactions=GetTestInteractions();
		Serializer serializer=new Serializer();
		String data=serializer.SerializeInteractions(interactions);
		Interactions interactions2=serializer.GetInteractions(data);
		assertTrue("Serialization failed",interactions2!=null && interactions2.getInteractions()!=null && interactions2.getInteractions().size()>0);
		Interaction interaction1=interactions.getInteractions().get(0);
		Interaction interaction2=interactions2.getInteractions().get(0);
		assertTrue("Description serialization failed",interaction1.getDescription().equals(interaction2.getDescription()));
		assertTrue("Parameter serialization failed",interaction1.getParameters().size()==interaction2.getParameters().size());
		assertTrue("Part detail serialization failed",interaction1.getPartDetails().size()==interaction2.getPartDetails().size());
		assertTrue("Part serialization failed",interaction1.getParts().size()==interaction2.getParts().size());		
	}
	
	@Test 
	public void testGetInteractions2()
	{
		  String data="<Interactions><Interaction>" + 
		    "<Name>BO_32227_bi_to_BO_3778</Name>" + 
		    "<Description>BO_32227 binds to BO_3778 operator</Description>" + 
		    "<InteractionType>Transcriptional repression using an operator</InteractionType>" + 
		    "<MathName>OperatorRepression</MathName>" + 
		    "<FreeTextMath>Km / (BO_32227 + Km)</FreeTextMath>" + 
		    "<IsReaction>false</IsReaction>" + 
		    "<Part>BO_3778</Part>" + 
		    "<Part>BO_32227</Part>" + 
		    "<PartDetails>" + 
		      "<PartDetail>" + 
		        "<NameInMath>Repressor</NameInMath>" + 
		        "<Part>BO_32227</Part>" + 
		        "<MoleculerFormType>Default</MoleculerFormType>" + 
		        "<InteractionRole>Modifier</InteractionRole>" + 
		        "<Stoichiometry>1</Stoichiometry>" + 
		      "</PartDetail>" + 
		    "</PartDetails>" + 
		    "<Parameters>" + 
		      "<Parameter>" + 
		        "<Name>Km</Name>" + 
		        "<ParameterType>Km</ParameterType>" + 
		        "<Value>500.0</Value>" + 
		        "<EvidenceType>IA</EvidenceType>" + 
		      "</Parameter>" + 
		    "</Parameters>" + 
		    "<IsInternal>false</IsInternal>" + 
		  "</Interaction></Interactions>";
		  Serializer serializer=new Serializer();			
		  Interactions interactions2=serializer.GetInteractions(data);
		  String str="";
	}
	
	@Test 
	public void testGetSumary()
	{
		Summary summary=new Summary();
		summary.setPageCount(5);
		Serializer serializer=new Serializer();
		String data=serializer.SerializeSummary(summary);
		Summary summary2=serializer.GetSummary(data);
		assertTrue("Serialization failed",summary2!=null);
		assertTrue("Pagecount serialization failed", summary.getPageCount()==summary2.getPageCount());
	}
	
	@Test 
	public void testGetMolecularformTypes()
	{
		MolecularFormType type=new MolecularFormType();
		type.setAccession("type accession");
		type.setExtension("type extension");
		type.setName("type name");
		List<MolecularFormType> types=new ArrayList<MolecularFormType>();
		types.add(type);
		Serializer serializer=new Serializer();
		String data=serializer.SerializeMolecularFormType(types);
		List<MolecularFormType> types2=serializer.GetMolecularFormTypes(data);
		assertTrue("Serialization failed",types!=null && types.size()>0);
		assertTrue("Name serialization failed", types.get(0).getName().equals(types2.get(0).getName()));
	}
	
	@Test 
	public void testGetMaths()
	{
		Formula type=new Formula();
		type.setAccession("type accession");
		type.setFormula("fomula");
		type.setName("type name");
		type.setFunction("function");
		type.setMetaType("meta type");
		type.setReactionFlux("reaction flux");
		
		List<Formula> types=new ArrayList<Formula>();
		types.add(type);
		Serializer serializer=new Serializer();
		String data=serializer.SerializeFormulas(types);
		List<Formula> types2=serializer.GetFormulas(data);
		assertTrue("Serialization failed",types!=null && types.size()>0);
		assertTrue("Name serialization failed", types.get(0).getName().equals(types2.get(0).getName()));
	}
	
	@Test
	public void testJAXBPart() throws Exception
	{
		 StringWriter stringWriter = new StringWriter(); 
		JAXBContext jc = JAXBContext.newInstance(Parts.class);
		 Parts parts=GetTestParts();
	        //Write using JAXB
	        Marshaller marshaller = jc.createMarshaller();
	        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);	        
	        marshaller.marshal(parts, stringWriter);
	        String xml=stringWriter.toString();
	        System.out.print("Serialised using JAXB:");
	        System.out.println(xml);
	        	        
	        //Read using JAXB
	        StringReader reader=new StringReader(xml);
	        Unmarshaller unmarshaller = jc.createUnmarshaller();
	        Parts parts2= (Parts) unmarshaller.unmarshal(reader);
	        
	        //Read using Xstream
	        Serializer serializer =new Serializer();
	        Parts parts3=serializer.GetParts(xml);
	        
	        //Write using Xstream
	        String xml2=serializer.SerializeParts(parts3);
	        System.out.print("Serialised using XStream:");
	        System.out.print(xml2);
	        
       
	        /*
	        StringWriter stringWriter = new StringWriter();
	        XMLStreamWriter xmlStreamWriter = XMLOutputFactory.newInstance().createXMLStreamWriter(stringWriter);
	        xmlStreamWriter.setPrefix("gm", Namespaces.PARTS_NAMESPACE);
	        marshaller.marshal(part, stringWriter);
	        System.out.println(stringWriter.toString());
	        */
	}
	
	@Test
	public void testJAXBInteraction() throws Exception
	{
		 StringWriter stringWriter = new StringWriter(); 
		JAXBContext jc = JAXBContext.newInstance(Interactions.class);
		 Interactions interactions=GetTestInteractions();
	        //Write using JAXB
		 	Marshaller marshaller = jc.createMarshaller();
	        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);	        
	        marshaller.marshal(interactions, stringWriter);
	        String xml=stringWriter.toString();
	        System.out.print("Serialised using JAXB:");
	        System.out.println(xml);
	        	        
	        //Read using JAXB
	        StringReader reader=new StringReader(xml);
	        Unmarshaller unmarshaller = jc.createUnmarshaller();
	        Interactions interactions2= (Interactions) unmarshaller.unmarshal(reader);
	        
	        //Read using Xstream
	        Serializer serializer =new Serializer();
	        Interactions interactions3=serializer.GetInteractions(xml);
	        
	        //Write using Xstream
	        String xml2=serializer.SerializeInteractions(interactions3);
	        System.out.print("Serialised using XStream:");
	        System.out.print(xml2);	       
	}
	
	

}

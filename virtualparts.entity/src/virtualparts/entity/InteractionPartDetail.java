package virtualparts.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is used to represent the details of parts participating in an interaction. Only used the represent parts that can encode for a gene product
 * @author a8901379
 */
@XmlType(namespace = "http://www.virtualparts.org/xsd/interactions/1.0.0", name="PartDetail")
@XmlAccessorType(XmlAccessType.FIELD)
public class InteractionPartDetail {
	@XmlElement(required = false, name = "NameInMath")
	private String mathName;
	@XmlElement(required = true, name = "Part")
	private String partName;
	
	//Dimer, Tetramer, Phosphorylated, Default
	@XmlElement(required = false, name = "MoleculerFormType")
	private String partForm;
	
	//Input, Output, Modifier
	@XmlElement(required = false, name = "InteractionRole")
	private String interactionRole;
	
	@XmlElement(required = false, name = "Stoichiometry")
	private int numberOfMolecules;
	
	public InteractionPartDetail()
	{
		
	}
	
	//Interaction Role, number of molecules and partForm are the constraints
        /**
         * Constructor
         * @param partName Name of the part
         * @param partForm Molecular form of the part
         * @param interactionRole Whether the part is input, output or modifier
         * @param numberOfMolecules Number of molecules of the gene product of a part
         * @param mathName Name used to represent such a part in the associated mathematical formula
         */
	public InteractionPartDetail (String partName, String partForm, String interactionRole, int numberOfMolecules, String mathName)
	{
		this.partName=partName;
		this.partForm=partForm;
		this.interactionRole=interactionRole;
		this.numberOfMolecules=numberOfMolecules;
		this.mathName=mathName;
	}

        /**
         * Gets the name of the part
         * @return Part name
         */
	public String getPartName() {
		return partName;
	}

        /**
         * Sets the name of the part
         * @param partName Part name
         */
	public void setPartName(String partName) {
		this.partName = partName;
	}

        /**
         * Gets the molecular form of the part
         * @return Molecular form of the part
         */
	public String getPartForm() {
		return partForm;
	}

        /**
         * Sets the molecular form of the part
         * @param partForm Molecular form of the part
         */
	public void setPartForm(String partForm) {
		this.partForm = partForm;
	}

        /**
         * Gets the role of the part in the interaction: Input, Output, Modifier
         * @return Role of the part in the interaction
         */
	public String getInteractionRole() {
		return interactionRole;
	}

        /**
         * Sets the role of the part in the interaction
         * @param interactionRole Role of the part in the interaction
         */
	public void setInteractionRole(String interactionRole) {
		this.interactionRole = interactionRole;
	}

        /**
         * Gets the number of molecules of the gene product of a part
         * @return Number of molecules of the gene product of a part
         */
	public int getNumberOfMolecules() {
		return numberOfMolecules;
	}

        /**
         * sets the number of molecules of the gene product of a part
         * @param numberOfMolecules Number of molecules of the gene product of a part
         */
	public void setNumberOfMolecules(int numberOfMolecules) {
		this.numberOfMolecules = numberOfMolecules;
	}

        /**
         * Gets the name used to represent such a part in the associated mathematical formula
         * @return Name used to represent such a part in the associated mathematical formula
         */
	public String getMathName() {
		return mathName;
	}

        /**
         * Sets the name used to represent such a part in the associated mathematical formula
         * @param mathName Name used to represent such a part in the associated mathematical formula
         */
	public void setMathName(String mathName) {
		this.mathName = mathName;
	}	
}

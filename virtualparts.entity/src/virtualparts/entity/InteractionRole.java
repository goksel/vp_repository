package virtualparts.entity;

/**
 * Enum constants for the interaction role types
 * @author a8901379
 */
public enum InteractionRole {
    /**
     * Input
     */
	INPUT {
		public String toString() {
			return "Input";
		}
	},
        /**
         * Output
         */
	OUTPUT {
		public String toString() {
			return "Output";
		}
	},
        /**
         * Modifier
         */
	MODIFIER {
		public String toString() {
			return "Modifier";
		}
	}

}

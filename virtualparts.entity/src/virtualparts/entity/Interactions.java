package virtualparts.entity;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Used as a holder for a list of interactions
 * @author a8901379
 */
@XmlType(namespace = "http://www.virtualparts.org/xsd/interactions/1.0.0", name="Interactions")
//@XmlType(namespace = Namespaces.PARTS_NAMESPACE, name="Parts")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement (namespace = "http://www.virtualparts.org/xsd/interactions/1.0.0", name="Interactions")
public class Interactions {
	@XmlElement(required = true, name = "Interaction")
	private List<Interaction> interactions;


    /**
     * Sets the list of interactions
     * @param interactions
     */
	public void setInteractions(List<Interaction> interactions) {
		this.interactions = interactions;
	}

    /**
     * Gets the list of interactions
     * @return
     */
	public List<Interaction> getInteractions() {
		return interactions;
	}

        /**
         * Adds an interaction to the list
         * @param interaction
         */
	public void AddInteraction(Interaction interaction)
	{
		if (interactions==null)
		{
			interactions=new ArrayList<Interaction>();			
		}
		interactions.add(interaction);		
	}
}

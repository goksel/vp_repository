package virtualparts.entity;

/**
 * Class representing biological constraints. These constraints allow to write rules about interactions. As these rules can be written between specific parts, they can also be generalized to types of parts.
 * Constraints are specified between source and target objects with a qualifier. An example rule: "a specific operator should be used upstream of all promoters"
 * @author a8901379
 */
public class Constraint {
private String sourceID;
private String sourceType;
private String targetID;
private String targetType;
private String qualifier;

/**
 * Gets the ID of the source
 * @return ID of the source
 */
public String getSourceID() {
	return sourceID;
}

/**
 * Sets the ID of the source
 * @param sourceID ID of the source
 */
public void setSourceID(String sourceID) {
	this.sourceID = sourceID;
}

/**
 * Gets the type of the source
 * @return Type of the source
 */
public String getSourceType() {
	return sourceType;
}

/**
 * Sets the type of the source
 * @param sourceType Type of the source
 */
public void setSourceType(String sourceType) {
	this.sourceType = sourceType;
}

/**
 * Gets the ID of the target
 * @return ID of the target
 */
public String getTargetID() {
	return targetID;
}

/**
 * Sets the ID of the target
 * @param targetID ID of the target
 */
public void setTargetID(String targetID) {
	this.targetID = targetID;
}

/**
 * Gets the type of the target
 * @return Type of the target
 */
public String getTargetType() {
	return targetType;
}

/**
 * Sets the type of the target
 * @param targetType Type of the target
 */
public void setTargetType(String targetType) {
	this.targetType = targetType;
}

/**
 * Gets the qualifier describing the constraint
 * @return Qualifier describing the constraint
 */
public String getQualifier() {
	return qualifier;
}

/**
 * Sets the qualifier describing the constraint
 * @param qualifier Qualifier describing the constraint
 */
public void setQualifier(String qualifier) {
	this.qualifier = qualifier;
}



  
}

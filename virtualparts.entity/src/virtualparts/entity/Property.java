package virtualparts.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is used to represent properties of parts. Such properties are name value pairs with a description and can represent any property of a part.
 * @author a8901379
 */
/*@XmlType(namespace = Namespaces.PARTS_NAMESPACE, propOrder = {"name","value","description"})
@XmlAccessorType(XmlAccessType.FIELD)
*/
@XmlType(namespace = "http://www.virtualparts.org/xsd/parts/1.0.0",
propOrder = {"name","value","description"},name="Property")
@XmlAccessorType(XmlAccessType.FIELD)

public class Property {
	@XmlElement(required = true)
	private String name;
	 
	@XmlElement(required = true)
	private String value;
	 
	 @XmlElement(required = false)
	 private String description;

        /**
         * Gets the name of the property
         * @return Name of the property
         */
	public String getName() {
		return name;
	}

        /**
         * Sets the name of the property
         * @param name Name of the property
         */
	public void setName(String name) {
		this.name = name;
	}

        /**
         * Gets the value of the property
         * @return Value of the property
         */
	public String getValue() {
		return value;
	}

        /**
         * Sets the value of the property
         * @param value Value of the property
         */
	public void setValue(String value) {
		this.value = value;
	}

        /**
         * Gets the description of the property
         * @return Description of the property
         */
	public String getDescription() {
		return description;
	}

        /**
         * Sets the description of the property
         * @param description Description of the property
         */
	public void setDescription(String description) {
		this.description = description;
	}
}

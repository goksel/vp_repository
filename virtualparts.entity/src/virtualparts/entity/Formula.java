package virtualparts.entity;

/**
 * Class representing a mathematical formula
 * @author a8901379
 */
public class Formula {
private String name;
private String formula;
private String reactionFlux;
private String reactionRate;
private String function;
private String accession;
private String metaType;

/**
 * Gets the name of the formula
 * @return name
 */
public String getName() {
	return name;
}

/**
 * Sets the name of the formula
 * @param name Name of the formula
 */
public void setName(String name) {
	this.name = name;
}

/**
 * Gets the free text representation of the formula. E.g.:Protein1_P + Protein2 -> Protein1 + Protein2_P
 * @return Free text representation of the formula
 */
public String getFormula() {
	return formula;
}

/**
 * Sets the free text representation of the formula
 * @param formula Free text representation of the formula
 */
public void setFormula(String formula) {
	this.formula = formula;
}

/**
 * Gets the reaction flux for the formula. E.g.: kf * Protein1_P * Protein2
 * @return Reaction flux for the formula
 */
public String getReactionFlux() {
	return reactionFlux;
}

/**
 * Sets the reaction flux for the formula
 * @param reactionFlux Reaction flux for the formula
 */
public void setReactionFlux(String reactionFlux) {
	this.reactionFlux = reactionFlux;
}

/**
 * Gets the name of the mathematical function if defined. Such functions can also be used to provide the reaction fluxes
 * @return Name of the mathematical function
 */
public String getFunction() {
	return function;
}

/**
 * Sets the name of the mathematical function
 * @param function Name of the mathematical function
 */
public void setFunction(String function) {
	this.function = function;
}

/**
 * Gets the accession for the formula
 * @return Accession for the formula
 */
public String getAccession() {
	return accession;
}

/**
 * Sets the accession for the formula
 * @param accession Accession for the formula
 */
public void setAccession(String accession) {
	this.accession = accession;
}

/**
 * Gets the meta type. Used for the annotation of models.
 * @return Meta type
 */
public String getMetaType() {
	return metaType;
}

/**
 * Sets the meta type.
 * @param metaType Meta type
 */
public void setMetaType(String metaType) {
	this.metaType = metaType;
}

public String getReactionRate() {
	return reactionRate;
}

public void setReactionRate(String reactionRate) {
	this.reactionRate = reactionRate;
}

}

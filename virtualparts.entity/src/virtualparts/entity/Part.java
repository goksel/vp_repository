package virtualparts.entity;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * The Part class is used to represent biological parts. Properties include the name, sequence  and type of a biological part.
 * @author a8901379
 */
@XmlType(namespace = "http://www.virtualparts.org/xsd/parts/1.0.0", name="Part")
@XmlAccessorType(XmlAccessType.FIELD)
public class Part {
	@XmlElement(required = true, name = "Name")
	private String name;
	
	@XmlElement(required = false, name = "DisplayName")
	private String displayName;	
	
	@XmlElement(required = true, name = "Type")	
	private String type;
	
	@XmlElement(required = false, name = "Description")		
	private String description;
	
	@XmlElement(required = true, name = "MetaType")			
	private String metaType;	
	
	@XmlElement(required = false, name = "Sequence")				
	private String sequence;
	
	@XmlElement(required = false, name = "SequenceURI")					
	private String sequenceURI;
	
	//Which chassis in the parts are from	
	@XmlElement(required = false, name = "Organism")						
	private String organism;	
	
	@XmlElement(required = false, name = "Status")							
	private String status;
	
	@XmlElement(required = false, name = "DesignMethod")							
	private String designMethod;	
	
	@XmlElement(required = false, name = "Property")								
	private List<Property> properties;	
	
    public static final String FUNCTIONAL_PART="FunctionalPart";
    public static final String PROMOTER="Promoter";
    
	/**
     * Gets the name
     * @return the name
     */
	public String getName() {
		return name;
	}
	
	/**
     * Sets the name
     * @param name the name to set
     */
	public void setName(String name) {
		this.name = name;
	}

        /**
         * Gets the display name of the part
         * @return Display name of the part
         */
	public String getDisplayName() {
		return displayName;
	}

        /**
         * Sets the display name of the part
         * @param displayName  Display name of the part
         */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
    /**
     * Gets the type of the part
     * @return Type of the part
     */
	public String getType() {
		return type;
	}
	
    /**
     * Sets the type of the part
     * @param type Type of the part
     */
	public void setType(String type) {
		this.type = type;
	}
	
    /**
     * Gets the description of the part
     * @return Description of the part
     */
	public String getDescription() {
		return description;
	}
	
    /**
     * Sets the description of the part
     * @param description Description of the part
     */
	public void setDescription(String description) {
		this.description = description;
	}
	
    /**
     * Gets the metatype of the part. Example metatypes are: Part, EnvironmentConstant, mRNA
     * @return MetaType of the part
     */
	public String getMetaType() {
		return metaType;
	}
	

    /**
     * Sets the metatype  of the part
     * @param metaType MetaType of the part
     */
	public void setMetaType(String metaType) {
		this.metaType = metaType;
	}
	
    /**
     * Gets the nucleotide sequence of the part
     * @return Nucleotide sequence of the part
     */
	public String getSequence() {
		return sequence;
	}
	
    /**
     * Sets the nucleotide sequence of the part
     * @param sequence Nucleotide sequence of the part
     */
	public void setSequence(String sequence) {
		this.sequence = sequence;
	}
	
    /**
     * Gets the URI that points to the FASTA sequence of the part.
     * @return Nucleotide sequence URI of the part
     */

	public String getSequenceURI() {
		return sequenceURI;
	}
	
    /**
     * Sets the nucleotide sequence URI of the part
     * @param sequenceURI Nucleotide sequence URI of the part
     */
	public void setSequenceURI(String sequenceURI) {
		this.sequenceURI = sequenceURI;
	}

        /**
         * Gets the organism from which the part is derived
         * @return Organism from which the part is derived
         */
	public String getOrganism() {
		return organism;
	}

        /**
         * Sets the organism from which the part is derived
         * @param organism Organism from which the part is derived
         */
	public void setOrganism(String organism) {
		this.organism = organism;
	}

        /**
         * Gets the status of the part. E.g.: Works
         * @return Status of the part
         */
	public String getStatus() {
		return status;
	}

        /**
         * Sets the status of the part.
         * @param status Status of the part.
         */
	public void setStatus(String status) {
		this.status = status;
	}

        /**
         * Gets the design method of the part. Values: Manual, Computational
         * @return Design method of the part
         */
	public String getDesignMethod() {
		return designMethod;
	}

        /**
         * Sets the design method of the part
         * @param designMethod Design method of the part
         */
	public void setDesignMethod(String designMethod) {
		this.designMethod = designMethod;
	}
	
	/*GMGM 20130109 public List<Parameter> getParameters() {
		return parameters;
	}
	
	public void setParameters(List<Parameter> parameters) {
		this.parameters = parameters;
	}*/
	
	
    /**
     * Gets the properties of the part
     * @return the properties Properties of the part
     */
	public List<Property> getProperties() {
		return properties;
	}
	

    /**
     * Sets the properties of the part
     * @param properties Properties of the part
     */
	public void setProperties(List<Property> properties) {
		this.properties = properties;
	}
	
	/*public List<MolecularForm> getMolecularForms() {
		return molecularForms;
	}
	
	public void setMolecularForms(List<MolecularForm> molecularForms) {
		this.molecularForms = molecularForms;
	}*/
	
	/* GMGM 20120109 
	public void AddParameter(Parameter parameter)
	{
		if (parameters==null)
		{
			parameters=new ArrayList<Parameter>();
		}
		parameters.add(parameter);
	}*/

        /**
         * Adds a property to the list of properties of the part
         * @param property Property to be added
         */
	public void AddProperty(Property property)
	{
		if (properties==null)
		{
			properties=new ArrayList<Property>();
		}
		properties.add(property);
	}
	
	/*public void AddMolecularForm(MolecularForm molecularForm)
	{
		if (molecularForms==null)
		{
			molecularForms=new ArrayList<MolecularForm>();
		}
		molecularForms.add(molecularForm);
	}*/
}

package virtualparts.entity;

public class SearchEntity {
	
	public SearchEntity()
	{
		
	}
	public SearchEntity(String text, String type, String organism, String property, String propertyValue, boolean exactPropertyValueMatch, boolean includeGeneticElements) 
	{
		this.searchText=text;
		this.partType=type;
		this.organism=organism;
		this.propertyName=property;
		this.propertyValue=propertyValue;
		this.exactPropertyValueMatch=exactPropertyValueMatch;
		this.includeGeneticElements=includeGeneticElements;
	}

private String searchText;
public String getSearchText() {
	return searchText;
}
public void setSearchText(String searchText) {
	this.searchText = searchText;
}
public String getOrganism() {
	return organism;
}
public void setOrganism(String organism) {
	this.organism = organism;
}
public String getPartType() {
	return partType;
}
public void setPartType(String partType) {
	this.partType = partType;
}
public String getPropertyName() {
	return propertyName;
}
public void setPropertyName(String propertyName) {
	this.propertyName = propertyName;
}
public String getPropertyValue() {
	return propertyValue;
}
public void setPropertyValue(String propertyValue) {
	this.propertyValue = propertyValue;
}
public boolean getExactPropertyValueMatch() {
	return exactPropertyValueMatch;
}
public void setExactPropertyValueMatch(boolean exactPropertyValueMatch) {
	this.exactPropertyValueMatch = exactPropertyValueMatch;
}
public boolean getIncludeGeneticElements() {
	return includeGeneticElements;
}
public void setIncludeGeneticElements(boolean includeGeneticElements) {
	this.includeGeneticElements = includeGeneticElements;
}
private String organism;
private String partType;
private String propertyName;
private String propertyValue;
private boolean exactPropertyValueMatch;
private boolean includeGeneticElements;



}

package virtualparts.entity;

/**
 * Parts are retrieved from the repository using pagination. Each page provides 50 records at a time. In order to retrieve  all of the records, summary information about the number of parts is necessary.
 * This class includes a property for the number of pages .
 * @author a8901379
 */
public class Summary {
	private int pageCount;

        /**
         * Gets the page count
         * @return Page count
         */
	public int getPageCount() {
		return pageCount;
	}

        /**
         * Sets the page count
         * @param pageCount Page count
         */
	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}
}

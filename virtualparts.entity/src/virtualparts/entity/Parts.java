package virtualparts.entity;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Used as a holder for a list of parts
 * @author a8901379
 */
@XmlType(namespace = "http://www.virtualparts.org/xsd/parts/1.0.0", name="Parts")
//@XmlType(namespace = Namespaces.PARTS_NAMESPACE, name="Parts")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement (namespace = "http://www.virtualparts.org/xsd/parts/1.0.0", name="Parts") 
public class Parts {
	@XmlElement(required = false, name = "Part")
	private List<Part> parts=new ArrayList<Part>();

    /**
     * Gets the list of parts
     * @return List of parts
     */
	public List<Part> getParts() {
		return parts;
	}

    /**
     * Sets the list of parts
     * @param parts List of parts
     */
	public void setParts(List<Part> parts) {
		this.parts = parts;
	}

        /**
         * Adds a part to the list of parts
         * @param part
         */
	public void AddPart(Part part) {		
		parts.add(part);
	}
}

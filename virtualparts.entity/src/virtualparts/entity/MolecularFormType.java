package virtualparts.entity;

/**
 * Represent molecular form types
 * @author a8901379
 */
public class MolecularFormType
 {

	public String name;
	public String accession;
	private String extension="";

        /**
         * Gets the name of the molecular form
         * @return Name of the molecular form
         */
	public String getName() {
		return name;
	}

        /**
         * Sets the name of the molecular form
         * @param name Name of the molecular form
         */
	public void setName(String name) {
		this.name = name;
	}

        /**
         * Gets the accession of the molecular form
         * @return Accession of the molecular form
         */
	public String getAccession() {
		return accession;
	}

        /**
         * Sets the accession of the molecular form
         * @param accession Accession of the molecular form
         */
	public void setAccession(String accession) {
		this.accession = accession;
	}

        /**
         * Gets the label that is used to add to default name of parts. E.g.: Phosphorylated
         * @return Extension label
         */
	public String getExtension() {
		if (extension==null)
		{
			return "";
		}
		else
		{
			return extension;
		}
	}

        /**
         * Gets the label that is used to add to default name of parts
         * @param extension Extension label
         */
	public void setExtension(String extension) {
		this.extension = extension;
	}

	 
}
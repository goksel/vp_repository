package virtualparts.entity;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * Interaction class is used to represent biological interactions.
 * 
 * @author a8901379
 */
@XmlType(namespace = "http://www.virtualparts.org/xsd/interactions/1.0.0", name="Interaction")
@XmlAccessorType(XmlAccessType.FIELD)
public class Interaction {

	@XmlElement(required = true, name = "Name")
	private String name;
	
	@XmlElement(required = false, name = "Description")
	private String description;
	
	@XmlElement(required = true, name = "InteractionType")
	private String interactionType;
	
	@XmlElement(required = false, name = "MathName")
	private String mathName;
	
	@XmlElement(required = true, name = "Part")
	private List<String> parts;
	
	@XmlElement(required = false, name = "FreeTextMath")
	private String freeTextMath;
	
	@XmlElement(required = false, name = "IsReaction")
	private Boolean isReaction;
	
	@XmlElementWrapper(name="PartDetails")
	@XmlElement(required = false, name = "PartDetail")
	private List<InteractionPartDetail> partDetails;
	
	@XmlTransient
	private List<Constraint> constraints;
	
	@XmlElementWrapper(name="Parameters")
	@XmlElement(required = false, name = "Parameter")
	private List<Parameter> parameters;
	
	@XmlElement(required = false, name = "IsInternal")
	private Boolean isInternal = false;
	
	@XmlElement(required = false, name = "IsReversible")
	private Boolean isReversible = false;

	public static final String TRANSCRIPTIONAL_REPRESSION = "Transcriptional repression";
	public static final String TRANSCRIPTIONAL_ACTICATION = "Transcriptional activation";
	public static final String TRANSCRIPTIONAL_ACTIVATION_OPERATOR = "Transcriptional activation using an operator";
	public static final String TRANSCRIPTIONAL_REPRESSION_OPERATOR = "Transcriptional repression using an operator";

	/**
	 * Gets the name of the interaction
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name of the interaction
	 * 
	 * @param name
	 *            The name of the interaction
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the description of the interaction
	 * 
	 * @return Description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description of the interaction
	 * 
	 * @param description
	 *            The description of the interaction
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the type of the interaction
	 * 
	 * @return the interactionType
	 */
	public String getInteractionType() {
		return interactionType;
	}

	/**
	 * Sets the type of the interaction
	 * 
	 * @param interactionType
	 *            The interaction type
	 */
	public void setInteractionType(String interactionType) {
		this.interactionType = interactionType;
	}

	/**
	 * Gets the name of the formula used for the interaction
	 * 
	 * @return the mathName
	 */
	public String getMathName() {
		return mathName;
	}

	/**
	 * Sets the name of the formula used for the interaction
	 * 
	 * @param mathName
	 *            the mathName to set
	 */
	public void setMathName(String mathName) {
		this.mathName = mathName;
	}

	/**
	 * Gets the list of parts participating in the interaction
	 * 
	 * @return the parts
	 */
	public List<String> getParts() {
		return parts;
	}

	/**
	 * Sets the list of parts participating in the interaction
	 * 
	 * @param parts
	 *            the parts to set
	 */
	public void setParts(List<String> parts) {
		this.parts = parts;
	}

	/**
	 * Gets the free text representation of the interaction
	 * 
	 * @return
	 */
	public String getFreeTextMath() {
		return freeTextMath;
	}

	/**
	 * Sets the free text representation of the interaction
	 * 
	 * @param freeTextMath
	 */
	public void setFreeTextMath(String freeTextMath) {
		this.freeTextMath = freeTextMath;
	}

	/**
	 * True if the interaction represents a biological reaction that is part of
	 * an ODE, false if the interaction is simply a mathematical rule such as
	 * for those that represent kinetic parameters
	 * 
	 * @return
	 */
	public Boolean getIsReaction() {
		return isReaction;
	}

	/**
	 * Sets whether the interaction represents a reaction or not
	 * 
	 * @param isReaction
	 */
	public void setIsReaction(Boolean isReaction) {
		this.isReaction = isReaction;
	}

	/**
	 * Gets the list of details of parts participating in the interaction
	 * 
	 * @return List<InteractionPartDetail>
	 */
	public List<InteractionPartDetail> getPartDetails() {
		return partDetails;
	}

	/**
	 * Gets the list of details of parts participating in the interaction
	 * 
	 * @param partDetails
	 *            List<InteractionPartDetail>
	 */
	public void setPartDetails(List<InteractionPartDetail> partDetails) {
		this.partDetails = partDetails;
	}

	/**
	 * Gets the list of biological constraints
	 * 
	 * @return
	 */
	public List<Constraint> getConstraints() {
		return constraints;
	}

	/**
	 * Sets the list of biological constraints
	 * 
	 * @param constraints
	 */
	public void setConstraints(List<Constraint> constraints) {
		this.constraints = constraints;
	}

	/**
	 * Gets the list of biological parameters
	 * 
	 * @return
	 */
	public List<Parameter> getParameters() {
		return parameters;
	}

	/**
	 * Sets the list of biological parameters
	 * 
	 * @param parameters
	 */
	public void setParameters(List<Parameter> parameters) {
		this.parameters = parameters;
	}

	/**
	 * True if the interaction is between different parts, false if the
	 * interaction is specific to a part
	 * 
	 * @return
	 */
	public Boolean getIsInternal() {
		return isInternal;
	}

	/**
	 * Sets whether the interaction is internal or not
	 * 
	 * @param isInternal
	 */
	public void setIsInternal(Boolean isInternal) {
		this.isInternal = isInternal;
	}

	/**
	 * True if the interaction is revesible, otherwise return false.
	 * 
	 * @return
	 */
	public Boolean getIsReversible() {
		return isReversible;
	}

	/**
	 * Sets whether the interaction is revesible or nor
	 * 
	 * @param isReversible
	 */
	public void setIsReversible(Boolean isReversible) {
		this.isReversible = isReversible;
	}

	/*
	 * public String getMetaType() { return metaType; }
	 * 
	 * public void setMetaType(String metaType) { this.metaType = metaType; }
	 */

	/**
	 * Adds a InteractionPartDetail object to the interaction
	 * 
	 * @param partDetail
	 *            InteractionPartDetail
	 */
	public void AddPartDetail(InteractionPartDetail partDetail) {
		if (partDetails == null) {
			partDetails = new ArrayList<InteractionPartDetail>();
		}
		partDetails.add(partDetail);
	}

	/**
	 * Adds a participating part to the interaction
	 * 
	 * @param part
	 */
	public void AddPart(String part) {
		if (parts == null) {
			parts = new ArrayList<String>();
		}
		parts.add(part);
	}

	/**
	 * Adds a biological constraint to the interaction
	 * 
	 * @param constraint
	 */
	public void AddConstraint(Constraint constraint) {
		if (constraints == null) {
			constraints = new ArrayList<Constraint>();
		}
		constraints.add(constraint);
	}

	/**
	 * Adds a parameter to the interaction
	 * 
	 * @param parameter
	 */
	public void AddParameter(Parameter parameter) {
		if (parameters == null) {
			parameters = new ArrayList<Parameter>();
		}
		parameters.add(parameter);
	}
}

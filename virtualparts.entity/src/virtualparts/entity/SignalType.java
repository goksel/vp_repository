package virtualparts.entity;

/**
 * Enum constants for biological signal types that can be used for model composition
 * @author a8901379
 */
public enum SignalType {

        /**
         * Species
         */
	SPECIES {
		public String toString() {
			return "Species";
		}
	},
        /**
         * Environment constant
         */
	ENVIRONMENTCONSTANT {
		public String toString() {
			return "EnvironmentConstant";
		}
	},
        /**
         * Ribosomes per second
         */
	RiPS {
		public String toString() {
			return "RiPS";
		}
	},
        /**
         * Volume
         */
	Volume {
		public String toString() {
			return "Volume";
		}
	},
        /**
         * Polymerases per second
         */
	PoPS {
		public String toString() {
			return "PoPS";
		}
	},
        /**
         * mRNA
         */
	MRNA {
		public String toString() {
			return "mRNA";
		}
	}
}

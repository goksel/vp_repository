package virtualparts.entity;

public interface Namespaces {
	  public static final String PARTS_NAMESPACE = "http://www.virtualparts.org/xsd/parts/1.0.0";
	  public static final String INTERACTIONS_NAMESPACE = "http://www.virtualparts.org/xsd/interactions/1.0.0";
	  
}

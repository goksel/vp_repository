package virtualparts.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Represents biochemical parameters
 * @author a8901379
 */
@XmlType(namespace = "http://www.virtualparts.org/xsd/interactions/1.0.0", name="Parameter")
@XmlAccessorType(XmlAccessType.FIELD)
public class Parameter {
	@XmlElement(required = false, name = "Scope")
	private String scope="";
	
	@XmlElement(required = true, name = "Name")
	private String name;
	
	@XmlElement(required = false, name = "ParameterType")
	private String parameterType;
	
	@XmlElement(required = false, name = "Value")
	private Double value;
	//private String molecularForm;
	private String evidenceType;

        /**
         * Constructor
         */
	public Parameter ()
	{
	
	}

        /**
         * Constructor
         * @param type Parameter type
         * @param name Parameter name
         * @param value Parameter value
         */
	public Parameter (String type, String name, Double value)
	{
		this.parameterType=type;
		this.name=name;
		this.value=value;		
	}

        /**
         * Constructor
         * @param type Parameter type
         * @param name Parameter name
         * @param value Parameter value
         * @param evidenceType Evidence type for the parameter
         * @param scope Scope of the parameter
         */
	public Parameter (String type, String name, Double value,String evidenceType, String scope)
	{
		this.parameterType=type;
		this.name=name;
		this.value=value;		
		this.scope=scope;
		this.evidenceType=evidenceType;
	}

        /**
         * Gets the name of the parameter
         * @return Name of the parameter
         */
	public String getName() {
		return name;
	}

        /**
         * Sets the name of the parameter
         * @param name Name of the parameter
         */
	public void setName(String name) {
		this.name = name;
	}

        /**
         * Gets the type of the parameter
         * @return Type of the parameter
         */
	public String getParameterType() {
		return parameterType;
	}

        /**
         * Sets the type of the parameter
         * @param parameterType Type of the parameter
         */
	public void setParameterType(String parameterType) {
		this.parameterType = parameterType;
	}

        /**
         * Gets the value of the parameter
         * @return Value of the parameter
         */
	public Double getValue() {
		return value;
	}

        /**
         * Sets the value of the parameter
         * @param value Value of the parameter
         */
	public void setValue(Double value) {
		this.value = value;
	}

        /**
         * Gets the evidence type of the parameter
         * @return Evidence type of the parameter
         */
	public String getEvidenceType() {
		return evidenceType;
	}

        /**
         * Sets the evidence type of the parameter
         * @param evidenceType Evidence type of the parameter
         */
	public void setEvidenceType(String evidenceType) {
		this.evidenceType = evidenceType;
	}
	
	/*public String getMolecularForm() {
		if  (molecularForm==null || molecularForm.length()==0)
		{
			return "Default";
		}
		else
		{
			return molecularForm;
		}
	}
	public void setMolecularForm(String molecularForm) {
		this.molecularForm = molecularForm;
	}*/
	
	/*
	private ParameterType parameterTypeObject;
	public ParameterType GetParameterTypeObject() throws Exception
	{
		if (parameterType!=null)
		{
			ParameterTypeDataHandler handler=new ParameterTypeDataHandler();
			parameterTypeObject=handler.GetType(parameterType);						
		}
		return parameterTypeObject;		
	}
	*/

        /**
         * Gets the scope type of the parameter.
         * Values:
         * Input: For parameters that act as input,
         * Output: For parameters that act as output
         * Global: For parameters that should globally be defined once
         * @return Scope type of the parameter
         */
	public String getScope() {
		return scope;
	}

        /**
         * Sets the scope type of the parameter
         * @param scope Scope type of the parameter
         */
	public void setScope(String scope) {
		this.scope = scope;
	}

}

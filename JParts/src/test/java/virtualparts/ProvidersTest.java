/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package virtualparts;

import java.io.IOException;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import virtualparts.entity.Formula;
import virtualparts.entity.MolecularFormType;
import static org.junit.Assert.*;

/**
 *
 * @author a8901379
 */
public class ProvidersTest {
    //String serverURL="http://localhost:8888";
    //String serverURL="http://bacillobricks.appspot.com";
    //String serverURL="http://www.bacillobricks.co.uk";
     //String serverURL="http://128.240.143.7:8082";
    //   String serverURL="http://192.168.234.140:8080";
    //String serverURL="http://128.240.143.7:8081";
    String serverURL=TestConfiguration.ServerURL;//"http://192.168.113.130:8080";
    
    
     
    public ProvidersTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception
    {
    }

    @AfterClass
    public static void tearDownClass() throws Exception
    {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

   
     @Test
     public void testMolecularFormProvider() throws IOException {
        List<MolecularFormType> molecularForms=MolecularFormProvider.GetMolecularForms(this.serverURL);
        assertTrue("Could not receive molecular forms",molecularForms!=null && molecularForms.size()>0);
     }

      @Test
     public void testFormulaProvider() throws IOException {
        List<Formula> formulas=FormulaProvider.GetFormulas(this.serverURL);
        assertTrue("Could not receive formulas",formulas!=null && formulas.size()>0);
     }

}
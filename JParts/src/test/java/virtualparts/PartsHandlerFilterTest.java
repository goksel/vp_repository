/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package virtualparts;

import java.io.IOException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import virtualparts.entity.Parts;
import virtualparts.filter.ANDFilter;
import virtualparts.filter.Filter;
import virtualparts.filter.FilterOperator;
import virtualparts.filter.FilterParameter;
import static org.junit.Assert.*;

/**
 *
 * @author a8901379
 */
public class PartsHandlerFilterTest {
    //String serverURL="http://localhost:8888";
    //String serverURL="http://bacillobricks.appspot.com";
     //String serverURL="http://www.bacillobricks.co.uk";
     //String serverURL="http://128.240.143.7:8082";
    //String serverURL="http://128.240.143.7:8081";
	//String serverURL="http://192.168.113.130:8080";
	String serverURL=TestConfiguration.ServerURL;
    public PartsHandlerFilterTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception
    {
    }

    @AfterClass
    public static void tearDownClass() throws Exception
    {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}

     @Test
    public void GreaterThanTest() throws IOException
    {
        PartsHandler handler=new PartsHandler(this.serverURL);
        Filter filter=new Filter(FilterParameter.TRANSCRIPTION_RATE, FilterOperator.GREATER_THAN, "0.6");
        Parts parts=handler.GetParts(filter);
        assertTrue("No part has been returned", parts != null && parts.getParts()!=null && parts.getParts().size()>0);
    }
 
    @Test
    public void EqualTest() throws IOException
    {
        PartsHandler handler=new PartsHandler(this.serverURL);
        Filter filter=new Filter(FilterParameter.TRANSCRIPTION_RATE, FilterOperator.EQUAL_TO, "0.1263");
        Parts parts=handler.GetParts(filter);
        assertTrue("No part has been returned", parts != null && parts.getParts()!=null && parts.getParts().size()>0);
    }

    @Test
    public void LessThanTest() throws IOException
    {
        PartsHandler handler=new PartsHandler(this.serverURL);
        Filter filter=new Filter(FilterParameter.TRANSCRIPTION_RATE, FilterOperator.LESS_THAN, "0.001");
        Parts parts=handler.GetParts(filter);
        assertTrue("No part has been returned", parts != null && parts.getParts()!=null && parts.getParts().size()>0);
    }

    @Test
    public void LessThanOrEqualToTest() throws IOException
    {
        PartsHandler handler=new PartsHandler(this.serverURL);
        Filter filter=new Filter(FilterParameter.TRANSCRIPTION_RATE, FilterOperator.LESS_THAN_OR_EQUAL_TO, "0.001");
        Parts parts=handler.GetParts(filter);
        assertTrue("No part has been returned", parts != null && parts.getParts()!=null && parts.getParts().size()>0);
    }

   

     @Test
    public void GreaterThanOrEqualToTest() throws IOException
    {
        PartsHandler handler=new PartsHandler(this.serverURL);
        Filter filter=new Filter(FilterParameter.TRANSCRIPTION_RATE, FilterOperator.GREATER_THAN_OR_EQUAL_TO, "0.6");
        Parts parts=handler.GetParts(filter);
        assertTrue("No part has been returned", parts != null && parts.getParts()!=null && parts.getParts().size()>0);
    }

    @Test
    public void ANDTest() throws IOException
    {
        PartsHandler handler=new PartsHandler(this.serverURL);
        Filter filter1=new Filter(FilterParameter.TRANSCRIPTION_RATE, FilterOperator.GREATER_THAN_OR_EQUAL_TO, "0.5");
        Filter filter2=new Filter(FilterParameter.TRANSCRIPTION_RATE, FilterOperator.LESS_THAN_OR_EQUAL_TO, "0.6");
        ANDFilter andFilter=new ANDFilter(filter1, filter2);
        Parts parts=handler.GetParts(andFilter);
        assertTrue("No part has been returned", parts != null && parts.getParts()!=null && parts.getParts().size()>0);
    }
 
 
}
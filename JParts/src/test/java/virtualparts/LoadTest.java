/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package virtualparts;

import java.util.List;

import org.apache.log4j.Level;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.sbml.jsbml.SBMLDocument;

import virtualparts.SBML.SBMLHandler;
import virtualparts.entity.Part;
import virtualparts.serialize.Serializer;
import static org.junit.Assert.*;
import virtualparts.entity.Interaction;
import virtualparts.entity.Interactions;
import virtualparts.entity.Property;

/**
 *
 * @author ngm44
 */
public class LoadTest {
    //String serverURL="http://192.168.113.130:8080";
	//String serverURL="http://sbol.ncl.ac.uk:8081/";//"http://192.168.113.130:8080";
    //String serverURL="http://localhost:8888";
   //String serverURL="http://192.168.234.128:8888";
    String serverURL=TestConfiguration.ServerURL;
    public LoadTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception
    {
    }

    @AfterClass
    public static void tearDownClass() throws Exception
    {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    //partsHandler.GetInteractionModel(partsHandler.GetInteractions(spaKPart).getInteractions().get(0)
    
     //@Test
    public void LoadTest_GetInteractionModels() throws Exception
    {
        PartsHandler partsHandler = new PartsHandler(serverURL);
        Part part=partsHandler.GetPart("SpaK");
        Interactions interactions=partsHandler.GetInteractions(part);
        
        for (int i=1;i<1000;i++)
        {
            try
            {
            partsHandler.GetInteractionModel(interactions.getInteractions().get(0));          
            }
            catch(Exception ex){}
            System.out.println("Processing" + i);
        }
    }
     
    /*
    @Test
    public void LoadTest_GetModels() throws Exception
    {
        PartsHandler partsHandler = new PartsHandler(serverURL);
        Part part=partsHandler.GetPart("SpaK");
        for (int i=1;i<20000;i++)
        {
            try
            {
            partsHandler.GetModel(part);            
            }
            catch(Exception ex){}
            System.out.println("Processing" + i);
        }
    }
    */
   /* @Test
    public void LoadTest_GetParts() throws Exception
    {
        PartsHandler partsHandler = new PartsHandler(serverURL);
        for (int i=1;i<10000;i++)
        {
            try
            {
             Part part=partsHandler.GetPart("SpaK");
             * 
            }
            catch (Exception ex)
            {
            }
        }
    }
    */
    @Test
    public void GetPart() throws Exception
    {
    	PartsHandler partsHandler = new PartsHandler(serverURL);
        
        for (int i=1;i<100;i++)
        {
        	 System.out.print("Processing " + i);  
        	  Part PSpaRKPart=partsHandler.GetPart("PspaRK");
              List<Property> properties= PSpaRKPart.getProperties();
              if (properties==null || properties.size()<5)
              {
              	throw new Exception("Properties are lost");
              }
              else
              {
            	  System.out.println(" Properties count:" + properties.size());
              }
        }
    }

    //@Test
    public void ConstructSubtilinReceiverLoadTest() throws Exception
    {
        for (int i=1;i<100000;i++)
        {
            try
            {
            /*Logger.global.setLevel(Level.SEVERE);
            Logger.getGlobal().setLevel(Level.OFF);*/
            	//org.apache.log4j.Logger.getRootLogger().setLevel(Level.ERROR);
            System.out.println("Processing" + i);
            String filePath="loadtest.xml";
            ConstructSubtilinReceiver2(filePath);
            }
            catch(Exception ex)
            {
            ex.printStackTrace();
            throw ex;
            }
        }
    }
     
    public void ConstructSubtilinReceiver2(String filePath) throws Exception
    {
        SBMLHandler sbmlHandler = new SBMLHandler();
        SBMLDocument sbmlContainer = sbmlHandler.GetSBMLTemplateModel("SubtilinReceiver");
        ModelBuilder modelBuilder = new ModelBuilder(sbmlContainer);
        
        PartsHandler partsHandler = new PartsHandler(serverURL);
        Part PSpaRKPart=partsHandler.GetPart("PspaRK");
        List<Property> properties= PSpaRKPart.getProperties();
        if (properties==null || properties.size()<5)
        {
        	throw new Exception("Properties are lost");
        }
        SBMLDocument pspaRK = partsHandler.GetModel(PSpaRKPart);
        SBMLDocument rbsSpaK = partsHandler.GetModel(partsHandler.GetPart("RBS_SpaK"));
        SBMLDocument rbsSpaR = partsHandler.GetModel(partsHandler.GetPart("RBS_SpaR"));
        Part spaKPart = partsHandler.GetPart("SpaK");
        SBMLDocument proteinSpaK = partsHandler.GetModel(spaKPart);
        SBMLDocument proteinSpaR = partsHandler.GetModel(partsHandler.GetPart("SpaR"));
        SBMLDocument spaK_spaR_interaction = partsHandler.GetInteractionModel(partsHandler.GetInteractions(spaKPart).getInteractions().get(0));
        Part pspaSPart = partsHandler.GetPart("PspaS");
        SBMLDocument pspaS = partsHandler.GetModel(pspaSPart);
        SBMLDocument pspaS_SpaR_interaction = partsHandler.GetInteractionModel(partsHandler.GetInteractions(pspaSPart).getInteractions().get(0));
        SBMLDocument rbsGFP = partsHandler.GetModel(partsHandler.GetPart("RBS_SpaS"));
        SBMLDocument proteinGFP = partsHandler.GetModel(partsHandler.GetPart("GFP_rrnb"));
        //Get mRNAs.mRNA virtual part is a template. Each time it is given with different IDs
        SBMLDocument mRNASpaRK = partsHandler.GetModel(partsHandler.GetPart("mRNA"));
        SBMLDocument mRNAGFP = partsHandler.GetModel(partsHandler.GetPart("mRNA"));

        modelBuilder.Add(pspaRK);

        modelBuilder.Add(mRNASpaRK);
        modelBuilder.Link(pspaRK, mRNASpaRK);

        modelBuilder.Add(rbsSpaK);
        modelBuilder.Link(mRNASpaRK, rbsSpaK);

        modelBuilder.Add(rbsSpaR);
        modelBuilder.Link(mRNASpaRK, rbsSpaR);

        modelBuilder.Add(proteinSpaK);
        modelBuilder.Link(rbsSpaK, proteinSpaK);

        modelBuilder.Add(proteinSpaR);
        modelBuilder.Link(rbsSpaR, proteinSpaR);

        modelBuilder.Add(spaK_spaR_interaction);

        modelBuilder.Add(pspaS);

        modelBuilder.Add(pspaS_SpaR_interaction);
        modelBuilder.Link(pspaS, pspaS_SpaR_interaction);

        modelBuilder.Add(mRNAGFP);
        modelBuilder.Link(pspaS_SpaR_interaction, mRNAGFP);

        modelBuilder.Add(rbsGFP);
        modelBuilder.Link(mRNAGFP, rbsGFP);

        modelBuilder.Add(proteinGFP);
        modelBuilder.Link(rbsGFP, proteinGFP);

        String sbmlOutput = modelBuilder.GetModelString();
        Serializer.WriteToFile(PartsHandlerTest.GetBaseDirectory() + filePath, sbmlOutput);
        //System.out.println(sbmlOutput);
        //Serializer.WriteToFile(filePath, sbmlOutput);
    }

}
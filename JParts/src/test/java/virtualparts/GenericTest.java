/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package virtualparts;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

import org.sbml.jsbml.SBMLDocument;

import virtualparts.SBML.SBMLHandler;
import virtualparts.entity.Interaction;
import virtualparts.entity.Part;
import virtualparts.entity.Parts;
import virtualparts.entity.Property;
import virtualparts.entity.SearchEntity;
import virtualparts.entity.Summary;
import virtualparts.filter.ANDFilter;
import virtualparts.filter.Filter;
import virtualparts.filter.FilterOperator;
import virtualparts.filter.FilterParameter;
import virtualparts.serialize.Serializer;

/**
 *
 * @author ngm44
 */
public class GenericTest {
    
    public GenericTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    
 
    //String serverURL="http://192.168.234.140:8080";
     //String serverURL="http://128.240.143.7:8081";
     String serverURL=TestConfiguration.ServerURL;
    
     
     
     
     
     
     
    
     
     
     private String GetTF(Interaction interaction, String operatorName)
     {
    	 String tf=null;
    	 List<String> partNames=interaction.getParts();
         for (String partName:partNames)
         {
        	 if (!partName.equals(operatorName))
        	 {
        		 tf=partName;
        		 break;
        	 }
         }
         return tf;
     }
     
     private void promoterOperatorConstruct(ModelBuilder modelBuilder,Part promoter, Part operator, Part rbs, Part TF, Interaction operatorBinding) throws Exception
     {
    	 PartsHandler partsHandler = new PartsHandler(serverURL);
          
    	 SBMLDocument promoterModel=partsHandler.GetModel(promoter);
         SBMLDocument operatorModel=partsHandler.GetModel(operator);
         SBMLDocument TFModel=partsHandler.GetModel(TF);
         SBMLDocument rbsModel=partsHandler.GetModel(rbs);                  
         SBMLDocument mRNAModel = partsHandler.GetModel(partsHandler.GetPart("mRNA"));
         SBMLDocument operatorBindingModel=partsHandler.GetInteractionModel(operatorBinding);
         
         modelBuilder.Add(promoterModel);
         
         modelBuilder.Add(operatorModel);
         modelBuilder.Link(promoterModel, operatorModel);
         
         modelBuilder.Add(operatorBindingModel);
         modelBuilder.Link(operatorModel, operatorBindingModel);
         
         modelBuilder.Add(mRNAModel);
         modelBuilder.Link(operatorBindingModel, mRNAModel);
         
         modelBuilder.Add(rbsModel );
         modelBuilder.Link(mRNAModel, rbsModel );  
         
         modelBuilder.Add(TFModel);
         modelBuilder.Link(rbsModel, TFModel);
     }
     
     private void promoterTFConstructWithAReporter(ModelBuilder modelBuilder,Part promoter, Part rbs, Part TF, Interaction promoterBinding, Part rbsReporter, Part reporter) throws Exception
     {
    	 System.out.println(String.format("Construct: %s-%s-%s", promoter.getName(),rbs.getName(),TF.getName()));
         
    	 PartsHandler partsHandler = new PartsHandler(serverURL);
          
    	 SBMLDocument promoterModel=partsHandler.GetModel(promoter);
         SBMLDocument TFModel=partsHandler.GetModel(TF);
         SBMLDocument rbsModel=partsHandler.GetModel(rbs);                  
         SBMLDocument mRNAModel = partsHandler.GetModel(partsHandler.GetPart("mRNA"));
         SBMLDocument promoterBindingModel=partsHandler.GetInteractionModel(promoterBinding);
         
         modelBuilder.Add(promoterModel);
         
         modelBuilder.Add(promoterBindingModel);
         modelBuilder.Link(promoterModel, promoterBindingModel);
         
         modelBuilder.Add(mRNAModel);
         modelBuilder.Link(promoterBindingModel, mRNAModel);
         
         modelBuilder.Add(rbsModel );
         modelBuilder.Link(mRNAModel, rbsModel );  
         
         modelBuilder.Add(TFModel);
         modelBuilder.Link(rbsModel, TFModel);
         
         if (rbsReporter!=null && reporter!=null)
         {
        	 SBMLDocument rbsReporterModel=partsHandler.GetModel(rbsReporter);
        	 SBMLDocument reporterModel=partsHandler.GetModel(reporter);
        	 
        	 modelBuilder.Add(rbsReporterModel );
             modelBuilder.Link(mRNAModel, rbsReporterModel );  
             
             modelBuilder.Add(reporterModel);
             modelBuilder.Link(rbsReporterModel, reporterModel);        	 
         }
     }
     
    // @Test
     public void ToggleSwitchExample() throws Exception
     {
         SBMLHandler sbmlHandler = new SBMLHandler();
         SBMLDocument sbmlContainer = sbmlHandler.GetSBMLTemplateModel("ToggleSwitch");
         ModelBuilder modelBuilder = new ModelBuilder(sbmlContainer);
         PartsHandler partsHandler = new PartsHandler(serverURL);
         
         //Get the first binding sequence part and the repressor part for the sequence
         Part repressor1BindingSitePart=partsHandler.GetPart("BO_3964");//PyrR_pyrR, A PyrR binding site      
         Interaction repressor1Binding=partsHandler.GetInteractions(repressor1BindingSitePart).getInteractions().get(0);
         Part repressor1=partsHandler.GetPart(GetTF(repressor1Binding, "BO_3964"));//Get the PyrR part
         
         //Get the second binding sequence part and the repressor part for the sequence          
         Part repressor2BindingSitePart=partsHandler.GetPart("BO_3993");//RocR_rocR, A RocR binding site RocR:BP_32223
         Interaction repressor2Binding=partsHandler.GetInteractions(repressor2BindingSitePart).getInteractions().get(0);
         Part repressor2=partsHandler.GetPart(GetTF(repressor2Binding, "BO_3993"));//Get the RocR Part
                  
         //Get the promoter and RBS parts for the first operon
         Part constPromoter1=partsHandler.GetPart("BO_2770");//fur core promoter       
         //Part constPromoter1=partsHandler.GetPart("BO_3003");//pyrG core promoter                
         //Part rbs1=partsHandler.GetPart("BO_27783");//Get the nasD RBS 
         //Search for RBSs using the filters
         Filter filter1=new Filter(FilterParameter.TRANSLATION_RATE, FilterOperator.GREATER_THAN_OR_EQUAL_TO, "0.08");
         Filter filter2=new Filter(FilterParameter.TRANSLATION_RATE, FilterOperator.LESS_THAN_OR_EQUAL_TO, "0.1");
         ANDFilter andFilter=new ANDFilter(filter1, filter2);
         Parts rbsParts=partsHandler.GetParts(andFilter);
         Part rbs1=rbsParts.getParts().get(0);
                  
         Part constPromoter2=partsHandler.GetPart("BO_2747");//cysK core promoter
         Part rbs2=partsHandler.GetPart("BO_27785");//Get the mtnE RBS
         
         //Create the models and join them for the first operon
         promoterOperatorConstruct(modelBuilder, constPromoter1, repressor2BindingSitePart, rbs1, repressor1, repressor2Binding);
         
         promoterOperatorConstruct(modelBuilder, constPromoter2, repressor1BindingSitePart, rbs2, repressor2, repressor1Binding);
         
         String sbmlOutput = modelBuilder.GetModelString();
         System.out.println(sbmlOutput);
         Serializer.WriteToFile(GetBaseDirectory() + "ToggleSwitch.xml", sbmlOutput);
     }
     
     
     //@Test
     public void RepressilatorExample() throws Exception
     {
         SBMLHandler sbmlHandler = new SBMLHandler();
         SBMLDocument sbmlContainer = sbmlHandler.GetSBMLTemplateModel("Repressilator");
         ModelBuilder modelBuilder = new ModelBuilder(sbmlContainer);
         PartsHandler partsHandler = new PartsHandler(serverURL);
         
         
         Parts repressiblePromoters=partsHandler.GetParts(1, "Promoter", "type", "RepressiblePromoter");
         Part promoter1=repressiblePromoters.getParts().get(0);
         Part promoter2=repressiblePromoters.getParts().get(1);
         Part promoter3=repressiblePromoters.getParts().get(4);
         /*
         Filter filter3=new Filter(FilterParameter.TRANSCRIPTION_RATE, FilterOperator.GREATER_THAN_OR_EQUAL_TO, "0.01");
         Filter filter4=new Filter(FilterParameter.TRANSCRIPTION_RATE, FilterOperator.LESS_THAN_OR_EQUAL_TO, "0.05");
         ANDFilter andFilterForPromoters=new ANDFilter(filter3, filter4);
         Parts promoterParts=partsHandler.GetParts(andFilterForPromoters);
         for (Part part:promoterParts.getParts())
         {
        	 if (HasProperty("RepressiblePromoper", part.getProperties()))
        	 {
        		 promoter2=part;
        		 break;
        	 }
         }
         */
         
         Interaction promoter1Repression=partsHandler.GetInteractions(promoter1).getInteractions().get(0);
         Interaction promoter2Repression=partsHandler.GetInteractions(promoter2).getInteractions().get(0);
         Interaction promoter3Repression=partsHandler.GetInteractions(promoter3).getInteractions().get(0);
         
         Part TF1=partsHandler.GetPart(GetTF(promoter1Repression, promoter1.getName()));
         Part TF2=partsHandler.GetPart(GetTF(promoter2Repression, promoter2.getName()));
         Part TF3=partsHandler.GetPart(GetTF(promoter3Repression, promoter3.getName()));
         
                                   
         Filter filter1=new Filter(FilterParameter.TRANSLATION_RATE, FilterOperator.GREATER_THAN_OR_EQUAL_TO, "0.08");
         Filter filter2=new Filter(FilterParameter.TRANSLATION_RATE, FilterOperator.LESS_THAN_OR_EQUAL_TO, "0.2");
         ANDFilter andFilter=new ANDFilter(filter1, filter2);
         Parts rbsParts=partsHandler.GetParts(andFilter);
         Part rbs1=rbsParts.getParts().get(0);
         Part rbs2=rbsParts.getParts().get(1);
         Part rbs3=rbsParts.getParts().get(2);
         Part rbs4=rbsParts.getParts().get(3);         
         Part reporter=partsHandler.GetPart("GFP_rrnb");
         
         
         
         
         //Create the models and join them for the first operon
         promoterTFConstructWithAReporter(modelBuilder, promoter1, rbs1, TF2, promoter1Repression, null, null);
         promoterTFConstructWithAReporter(modelBuilder, promoter2, rbs2, TF3, promoter2Repression, null, null);
         promoterTFConstructWithAReporter(modelBuilder, promoter3, rbs3, TF1, promoter3Repression, rbs4, reporter);
         
         
         String sbmlOutput = modelBuilder.GetModelString();
         System.out.println(sbmlOutput);
         Serializer.WriteToFile(GetBaseDirectory() + "Repressilator.xml", sbmlOutput);
     }
     
     private Boolean HasProperty(String propertyName, List<Property> properties)
     {
    	 for (Property property:properties)
    	 {
    		 if (propertyName.equals(property.getValue()))
    		 {
    			 return true;
    		 }
    	 }
    	 return false;
     }
     
    // @Test
     public void ConstructSubtilinReceiver2() throws Exception
     {
         SBMLHandler sbmlHandler = new SBMLHandler();
         SBMLDocument sbmlContainer = sbmlHandler.GetSBMLTemplateModel("SubtilinReceiver");
         ModelBuilder modelBuilder = new ModelBuilder(sbmlContainer);
         PartsHandler partsHandler = new PartsHandler(serverURL);

         SBMLDocument pspaRK = partsHandler.GetModel(partsHandler.GetPart("PspaRK"));
         SBMLDocument rbsSpaK = partsHandler.GetModel(partsHandler.GetPart("RBS_SpaK"));
         SBMLDocument rbsSpaR = partsHandler.GetModel(partsHandler.GetPart("RBS_SpaR"));
         Part spaKPart = partsHandler.GetPart("SpaK");
         SBMLDocument proteinSpaK = partsHandler.GetModel(spaKPart);
         SBMLDocument proteinSpaR = partsHandler.GetModel(partsHandler.GetPart("SpaR"));
         SBMLDocument spaK_spaR_interaction = partsHandler.GetInteractionModel(partsHandler.GetInteractions(spaKPart).getInteractions().get(0));
         Part pspaSPart = partsHandler.GetPart("PspaS");
         SBMLDocument pspaS = partsHandler.GetModel(pspaSPart);
         SBMLDocument pspaS_SpaR_interaction = partsHandler.GetInteractionModel(partsHandler.GetInteractions(pspaSPart).getInteractions().get(0));
         SBMLDocument rbsGFP = partsHandler.GetModel(partsHandler.GetPart("RBS_SpaS"));
         SBMLDocument proteinGFP = partsHandler.GetModel(partsHandler.GetPart("GFP_rrnb"));
         //Get mRNAs.mRNA virtual part is a template. Each time it is given with different IDs
         SBMLDocument mRNASpaRK = partsHandler.GetModel(partsHandler.GetPart("mRNA"));
         SBMLDocument mRNAGFP = partsHandler.GetModel(partsHandler.GetPart("mRNA"));

         modelBuilder.Add(pspaRK);

         modelBuilder.Add(mRNASpaRK);
         modelBuilder.Link(pspaRK, mRNASpaRK);

         modelBuilder.Add(rbsSpaK);
         modelBuilder.Link(mRNASpaRK, rbsSpaK);

         modelBuilder.Add(rbsSpaR);
         modelBuilder.Link(mRNASpaRK, rbsSpaR);

         modelBuilder.Add(proteinSpaK);
         modelBuilder.Link(rbsSpaK, proteinSpaK);

         modelBuilder.Add(proteinSpaR);
         modelBuilder.Link(rbsSpaR, proteinSpaR);

         modelBuilder.Add(spaK_spaR_interaction);

         modelBuilder.Add(pspaS);

         modelBuilder.Add(pspaS_SpaR_interaction);
         modelBuilder.Link(pspaS, pspaS_SpaR_interaction);

         modelBuilder.Add(mRNAGFP);
         modelBuilder.Link(pspaS_SpaR_interaction, mRNAGFP);

         modelBuilder.Add(rbsGFP);
         modelBuilder.Link(mRNAGFP, rbsGFP);

         modelBuilder.Add(proteinGFP);
         modelBuilder.Link(rbsGFP, proteinGFP);

         String sbmlOutput = modelBuilder.GetModelString();
         System.out.println(sbmlOutput);
         Serializer.WriteToFile(GetBaseDirectory() + "ConstructSubtilinReceiver2.xml", sbmlOutput);
     }

  
  
     
    //@Test
    public void EcoliNullModel() throws Exception
    {
    	 PartsHandler partsHandler = new PartsHandler(serverURL);
    	 Part promoterPart=partsHandler.GetPart("BBa_C0012");
    	 SBMLDocument promoterModel = partsHandler.GetModel(promoterPart);
    	 assertTrue("There is no model for this part and the model should be null ", promoterModel==null );
    	  
    }
 //@Test
 public void NegativeFeedbackModel() throws Exception
    {
        SBMLHandler sbmlHandler = new SBMLHandler();
        SBMLDocument sbmlContainer = sbmlHandler.GetSBMLTemplateModel("SubtilinReceiver");
        ModelBuilder modelBuilder = new ModelBuilder(sbmlContainer);
        PartsHandler partsHandler = new PartsHandler(serverURL);
        Part promoterPart=partsHandler.GetPart("PspaRK");
        SBMLDocument promoterModel = partsHandler.GetModel(promoterPart);
  /*      SBMLDocument rbsModel = partsHandler.GetModel(partsHandler.GetPart("RBS_SpaK"));
        SBMLDocument cdsModel = partsHandler.GetModel(partsHandler.GetPart("BO_32077"));
        SBMLDocument promoterTFInteractionModel = partsHandler.GetInteractionModel(partsHandler.GetInteractions(promoterPart).getInteractions().get(0));
        SBMLDocument mRNAModel = partsHandler.GetModel(partsHandler.GetPart("mRNA"));
*/
        modelBuilder.Add(promoterModel);

        /*modelBuilder.Add(promoterTFInteractionModel);
        modelBuilder.Link(promoterModel, promoterTFInteractionModel);

        modelBuilder.Add(mRNAModel);
        modelBuilder.Link(promoterTFInteractionModel, mRNAModel);

        modelBuilder.Add(rbsModel);
        modelBuilder.Link(mRNAModel, rbsModel);

        modelBuilder.Add(cdsModel);
        modelBuilder.Link(rbsModel, cdsModel);
        String sbmlOutput = modelBuilder.GetModelString();
        System.out.println(sbmlOutput);
        Serializer.WriteToFile("C:/Temp/NegativeFeedBack.xml", sbmlOutput);*/
    }
 
 private String GetBaseDirectory()
 {
 	if (IsWindows())
 	{
 		return "c:/temp/";
 	}
 	else
 	{
 		return "/tmp/";
 	}
 }
 
 private boolean IsWindows()
 {
   String os=System.getProperty("os.name");
   return os.startsWith("Windows");
 }
 
 
}

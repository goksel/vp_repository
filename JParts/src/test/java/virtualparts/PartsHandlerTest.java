/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package virtualparts;

import java.io.File;
import java.util.HashSet;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.sbml.jsbml.SBMLDocument;

import virtualparts.SBML.SBMLHandler;
import virtualparts.entity.Interaction;
import virtualparts.entity.Interactions;
import virtualparts.entity.Part;
import virtualparts.entity.Property;
import virtualparts.entity.SearchEntity;
import static org.junit.Assert.*;
import virtualparts.entity.Parts;
import virtualparts.entity.Summary;
import virtualparts.serialize.Serializer;

/**
 *
 * @author a8901379
 */
public class PartsHandlerTest {
    //String serverURL="http://atgc-eidos-sys.appspot.com";
   // String serverURL="http://atgc-eidos.appspot.com";
   // String serverURL="http://localhost:8888";
    //String serverURL="http://bacillobricks.appspot.com";
    //String serverURL="http://www.bacillobricks.co.uk";
   //String serverURL="http://128.240.143.7:8082";
   //    String serverURL="http://192.168.234.140:8080";
   //String serverURL="http://128.240.143.7:8081";
   String serverURL=TestConfiguration.ServerURL;// "http://192.168.113.130:8080";
   


    public PartsHandlerTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void SearchEcoliFunctionalParts() throws Exception
    {
    	PartsHandler partsHandler = new PartsHandler(serverURL);                
        SearchEntity searchEntity=new SearchEntity(null, "FunctionalPart", "Escherichia coli", null, null, false, false);
        Parts parts=partsHandler.GetParts(1, searchEntity);        
        assertTrue("Parts should be returned",  parts!=null && parts.getParts()!=null && parts.getParts().size()>0);                     
    }
    
    @Test
    public void SearchEcoliParts() throws Exception
    {
    	PartsHandler partsHandler = new PartsHandler(serverURL);                
        SearchEntity searchEntity=new SearchEntity(null, null, "Escherichia coli", null, null, false, false);
        Parts parts=partsHandler.GetParts(1, searchEntity);        
        assertTrue("Parts should be returned",  parts!=null && parts.getParts()!=null && parts.getParts().size()>0);                     
    }
    
    @Test
    public void SearchBsubtilisSpo0ARepressiblePromoters() throws Exception
    {
    	PartsHandler partsHandler = new PartsHandler(serverURL);                
        SearchEntity searchEntity=new SearchEntity("spo0A", "Promoter", "Bacillus subtilis", "type", "Repressible", false, false);
        Parts parts=partsHandler.GetParts(1, searchEntity);        
        assertTrue("Parts should be returned",  parts!=null && parts.getParts()!=null && parts.getParts().size()>0);                     
    }
    
    @Test
    public void NBTPartsAccessible() throws Exception
    {
    	PartsHandler partsHandler = new PartsHandler(serverURL);                
        SearchEntity searchEntity=new SearchEntity(null, null, null, "publication", "sbol-nbt", true, true);
        Parts parts=partsHandler.GetParts(1, searchEntity);        
        assertTrue("NBT parts are not accessible", parts!=null && parts.getParts()!=null && parts.getParts().size()>0);                     
    }
    
    @Test
    public void GetNoTerminators() throws Exception
    {
    	PartsHandler partsHandler = new PartsHandler(serverURL);                
        SearchEntity searchEntity=new SearchEntity(null, "Terminator", null, null, null, false, false);
        Parts parts=partsHandler.GetParts(1, searchEntity);        
        assertTrue("No terminators should be returned", parts==null || parts.getParts()==null || parts.getParts().size()==0);                     
    }
    

    @Test
    public void GetTerminators() throws Exception
    {
    	PartsHandler partsHandler = new PartsHandler(serverURL);                
        SearchEntity searchEntity=new SearchEntity(null, "Terminator", null, null, null, false, true);
        Parts parts=partsHandler.GetParts(1, searchEntity);        
        assertTrue("Terminators should be returned",  parts!=null && parts.getParts()!=null && parts.getParts().size()>0);                     
    }
    
    @Test
    public void GetNoTerminatorsSummary() throws Exception
    {
    	PartsHandler partsHandler = new PartsHandler(serverURL);                
        SearchEntity searchEntity=new SearchEntity(null, "Terminator", null, null, null, false, false);
        Summary summary=partsHandler.GetPartsSummary(searchEntity);        
        assertTrue("Terminator count should be zero", summary.getPageCount()==0);                     
    }
    

    @Test
    public void GetTerminatorsSummary() throws Exception
    {
    	PartsHandler partsHandler = new PartsHandler(serverURL);                
        SearchEntity searchEntity=new SearchEntity(null, "Terminator", null, null, null, false, true);
        Summary summary=partsHandler.GetPartsSummary(searchEntity);           
        assertTrue("Terminator count should be bigger than zero",  summary.getPageCount()>0);                     
    }
        

    @Test
    public void PropertiesExist() throws Exception
    {
    	PartsHandler partsHandler = new PartsHandler(serverURL);                
        	  Part PSpaRKPart=partsHandler.GetPart("PspaRK");
              List<Property> properties= PSpaRKPart.getProperties();
              assertTrue("Properties are lost", properties!=null && properties.size()==4);            
        
    }
    
    @Test
    public void UpdateVisualName() throws Exception
    {
           PartsHandler handler=new PartsHandler(this.serverURL);
           Part part=handler.GetPart("BBa_E1010");
           SBMLDocument SBMLDoc=handler.GetModel(part);
           SBMLDocument newSBMLDoc=handler.UpdateVisualNameAnnotation(part.getDisplayName(), SBMLDoc);
           SBMLHandler SBMLHandler=new SBMLHandler();
           String newDoc=SBMLHandler.GetSBML(newSBMLDoc);
           boolean result=newDoc.contains("<mts:VisualName>" + part.getDisplayName() + "</mts:VisualName>");
           assertTrue("Could not update the visual name annotation", result==true);
    }

    @Test
    public void NegativeFeedbackModel() throws Exception
    {
        SBMLHandler sbmlHandler = new SBMLHandler();
        SBMLDocument sbmlContainer = sbmlHandler.GetSBMLTemplateModel("SubtilinReceiver");
        ModelBuilder modelBuilder = new ModelBuilder(sbmlContainer);
        PartsHandler partsHandler = new PartsHandler(serverURL);
        Part promoterPart=partsHandler.GetPart("BO_2685");
        SBMLDocument promoterModel = partsHandler.GetModel(promoterPart);
        SBMLDocument rbsModel = partsHandler.GetModel(partsHandler.GetPart("RBS_SpaK"));
        SBMLDocument cdsModel = partsHandler.GetModel(partsHandler.GetPart("BO_32077"));
        SBMLDocument promoterTFInteractionModel = partsHandler.GetInteractionModel(partsHandler.GetInteractions(promoterPart).getInteractions().get(0));
        SBMLDocument mRNAModel = partsHandler.GetModel(partsHandler.GetPart("mRNA"));

        modelBuilder.Add(promoterModel);

        modelBuilder.Add(promoterTFInteractionModel);
        modelBuilder.Link(promoterModel, promoterTFInteractionModel);

        modelBuilder.Add(mRNAModel);
        modelBuilder.Link(promoterTFInteractionModel, mRNAModel);

        modelBuilder.Add(rbsModel);
        modelBuilder.Link(mRNAModel, rbsModel);

        modelBuilder.Add(cdsModel);
        modelBuilder.Link(rbsModel, cdsModel);
        String sbmlOutput = modelBuilder.GetModelString();
        System.out.println(sbmlOutput);
        Serializer.WriteToFile(GetBaseDirectory() + "NegativeFeedBack.xml", sbmlOutput);
    }
    
    @Test
    public void NegativeFeedbackModelControl() throws Exception
    {
        SBMLHandler sbmlHandler = new SBMLHandler();
        SBMLDocument sbmlContainer = sbmlHandler.GetSBMLTemplateModel("SubtilinReceiver");
        ModelBuilder modelBuilder = new ModelBuilder(sbmlContainer);
        PartsHandler partsHandler = new PartsHandler(serverURL);
        Part promoterPart=partsHandler.GetPart("BO_2685");
        SBMLDocument promoterModel = partsHandler.GetModel(promoterPart);
        SBMLDocument rbsModel = partsHandler.GetModel(partsHandler.GetPart("RBS_SpaK"));
        SBMLDocument cdsModel = partsHandler.GetModel(partsHandler.GetPart("BO_32077"));
        SBMLDocument promoterTFInteractionModel = partsHandler.GetInteractionModel(partsHandler.GetInteractions(promoterPart).getInteractions().get(0));
        SBMLDocument mRNAModel = partsHandler.GetModel(partsHandler.GetPart("mRNA"));

        modelBuilder.Add(promoterModel);

       
        modelBuilder.Add(mRNAModel);
        modelBuilder.Link(promoterModel, mRNAModel);

        modelBuilder.Add(rbsModel);
        modelBuilder.Link(mRNAModel, rbsModel);

        modelBuilder.Add(cdsModel);
        modelBuilder.Link(rbsModel, cdsModel);
        String sbmlOutput = modelBuilder.GetModelString();
        System.out.println(sbmlOutput);
        Serializer.WriteToFile(GetBaseDirectory() + "NegativeFeedBackControl.xml", sbmlOutput);
    }
    
    @Test
    public void ConstructSubtilinReceiverUsingRunTimePartModels() throws Exception
    {
        SBMLHandler sbmlHandler = new SBMLHandler();
        SBMLDocument sbmlContainer = sbmlHandler.GetSBMLTemplateModel("SubtilinReceiver");
        ModelBuilder modelBuilder = new ModelBuilder(sbmlContainer);
        PartsHandler partsHandler = new PartsHandler(serverURL);

        SBMLDocument pspaRK = partsHandler.GetModel(partsHandler.GetPart("PspaRK"));
        SBMLDocument rbsSpaK = partsHandler.GetModel(partsHandler.GetPart("RBS_SpaK"));
        SBMLDocument rbsSpaR = partsHandler.GetModel(partsHandler.GetPart("RBS_SpaR"));
        Part spaKPart = partsHandler.GetPart("SpaK");

        List<Interaction> spaKInternalInteractions=partsHandler.GetInternalInteractions(spaKPart).getInteractions();
        SBMLDocument proteinSpaK=partsHandler.CreatePartModel(spaKPart, spaKInternalInteractions);
            // This line corresponds to the two lines above
            //SBMLDocument proteinSpaK = partsHandler.GetModel(spaKPart);

        SBMLDocument proteinSpaR = partsHandler.GetModel(partsHandler.GetPart("SpaR"));

        Interaction spaK_spaR_interactionObject=partsHandler.GetInteractions(spaKPart).getInteractions().get(0);
        SBMLDocument spaK_spaR_interaction=partsHandler.CreateInteractionModel(spaK_spaR_interactionObject);
            // This line corresponds to the two lines above
            //SBMLDocument spaK_spaR_interaction = partsHandler.GetInteractionModel(partsHandler.GetInteractions(spaKPart).getInteractions().get(0));


        Part pspaSPart = partsHandler.GetPart("PspaS");
        SBMLDocument pspaS = partsHandler.GetModel(pspaSPart);
        SBMLDocument pspaS_SpaR_interaction = partsHandler.GetInteractionModel(partsHandler.GetInteractions(pspaSPart).getInteractions().get(0));
        SBMLDocument rbsGFP = partsHandler.GetModel(partsHandler.GetPart("RBS_SpaS"));
        SBMLDocument proteinGFP = partsHandler.GetModel(partsHandler.GetPart("GFP_rrnb"));
        //Get mRNAs.mRNA virtual part is a template. Each time it is given with different IDs
        SBMLDocument mRNASpaRK = partsHandler.GetModel(partsHandler.GetPart("mRNA"));
        SBMLDocument mRNAGFP = partsHandler.GetModel(partsHandler.GetPart("mRNA"));

        modelBuilder.Add(pspaRK);

        modelBuilder.Add(mRNASpaRK);
        modelBuilder.Link(pspaRK, mRNASpaRK);

        modelBuilder.Add(rbsSpaK);
        modelBuilder.Link(mRNASpaRK, rbsSpaK);

        modelBuilder.Add(rbsSpaR);
        modelBuilder.Link(mRNASpaRK, rbsSpaR);

        modelBuilder.Add(proteinSpaK);
        modelBuilder.Link(rbsSpaK, proteinSpaK);

        modelBuilder.Add(proteinSpaR);
        modelBuilder.Link(rbsSpaR, proteinSpaR);

        modelBuilder.Add(spaK_spaR_interaction);

        modelBuilder.Add(pspaS);

        modelBuilder.Add(pspaS_SpaR_interaction);
        modelBuilder.Link(pspaS, pspaS_SpaR_interaction);

        modelBuilder.Add(mRNAGFP);
        modelBuilder.Link(pspaS_SpaR_interaction, mRNAGFP);

        modelBuilder.Add(rbsGFP);
        modelBuilder.Link(mRNAGFP, rbsGFP);

        modelBuilder.Add(proteinGFP);
        modelBuilder.Link(rbsGFP, proteinGFP);

        String sbmlOutput = modelBuilder.GetModelString();
        System.out.println(sbmlOutput);
        Serializer.WriteToFile(GetBaseDirectory() + "ConstructSubtilinReceiverUsingRunTimePartModels.xml", sbmlOutput);
    }

 @Test
    public void testGetPartsFromTwoPages() throws Exception
    {
        PartsHandler handler = new PartsHandler(this.serverURL);
        int pageCount = 2;

        Parts allParts = new Parts();
        for (int i = 1; i <= pageCount; i++)
        {
            System.out.print("Getting parts");
            Parts parts = handler.GetParts(i);
            System.out.print("Received parts");
            allParts.getParts().addAll(parts.getParts());
            assertTrue("No parts are returned", parts.getParts() != null);
            assertTrue("No parts are returned", parts.getParts().size() > 0);
        }
    }

    @Test
    public void testSerialiseParts() throws Exception
    {
        PartsHandler handler = new PartsHandler(this.serverURL);

        Parts parts = handler.GetParts(1);
        Serializer serializer = new Serializer();
        String content = serializer.SerializeParts(parts);
        Serializer.WriteToFile(GetBaseDirectory() + "vpartsSerialised.txt", content);
        File file = new File(GetBaseDirectory() + "vpartsSerialised.txt");
        String fileContent = Serializer.GetFileContent(file);
        assertTrue("Serialisation is not successful!", fileContent != null && !fileContent.isEmpty());

        Parts savedParts = serializer.GetParts(fileContent);
        assertTrue("Serialisation is not successful! Part numbers do not match", parts.getParts().size() == savedParts.getParts().size());

    }

   
    /*
    @Test
    public void testGetAllParts() throws Exception {
    PartsHandler handler=new PartsHandler(this.serverURL);
    Summary summary=handler.GetPartsSummary();
    int pageCount=summary.PageCount;
    assertTrue("No summary is returned",summary.PageCount!=-1);

    Parts allParts=new Parts();
    for (int i=1;i<=pageCount;i++)
    {
    Parts parts=handler.GetParts(i);
    allParts.getParts().addAll(parts.getParts());
    assertTrue("No parts are returned",parts.getParts()!=null);
    assertTrue("No parts are returned",parts.getParts().size()>0);
    }
    }
     */

    /**
     * Test of GetParts method, of class PartsHandler.
     */
    @Test
    public void testGetParts() throws Exception
    {
        PartsHandler handler = new PartsHandler(this.serverURL);
        Parts parts = handler.GetParts(1);
        assertTrue("No parts are returned", parts.getParts() != null);
        assertTrue("No parts are returned", parts.getParts().size() > 0);
    }

    @Test
    public void testGetPartsbyType() throws Exception
    {
        PartsHandler handler = new PartsHandler(this.serverURL);
        Parts parts = handler.GetParts(1, "Promoter");
        assertTrue("No parts are returned", parts.getParts() != null);
        assertTrue("No parts are returned", parts.getParts().size() > 0);
    }

    @Test
    public void testGetPartsByRole() throws Exception
    {
        PartsHandler handler = new PartsHandler(this.serverURL);
        Parts parts = handler.GetParts(1, "Promoter", "type", "SigAPromoter");
        assertTrue("No parts are returned", parts.getParts() != null);
        assertTrue("No parts are returned", parts.getParts().size() > 0);
    }

    @Test
    public void testGetPartsSummary() throws Exception
    {
        PartsHandler handler = new PartsHandler(this.serverURL);
        Summary summary = handler.GetPartsSummary();
        assertTrue("No summary is returned", summary.getPageCount() != -1);
    }

    @Test
    public void testGetPartsbyTypeSummary() throws Exception
    {
        PartsHandler handler = new PartsHandler(this.serverURL);
        Summary summary = handler.GetPartsSummary("Promoter");
        assertTrue("No summary is returned", summary.getPageCount() != -1);
    }

    @Test
    public void testGetPartsByRoleSummary() throws Exception
    {
        PartsHandler handler = new PartsHandler(this.serverURL);
        Summary summary = handler.GetPartsSummary("Promoter", "type", "SigAPromoter");
        assertTrue("No summary is returned", summary.getPageCount() != -1);
    }

    @Test
    public void testGetModel() throws Exception
    {
        PartsHandler handler = new PartsHandler(this.serverURL);
        Part part = new Part();
        part.setName("SpaK");
        SBMLDocument sbmlDocument = handler.GetModel(part);
        assertTrue("No species in the model", sbmlDocument.getModel().getNumSpecies() > 0);
        assertTrue("No reactions in the model", sbmlDocument.getModel().getNumReactions() > 0);
    }

    @Test
    public void testGetInteractions() throws Exception
    {
        PartsHandler handler = new PartsHandler(this.serverURL);
        Part part = new Part();
        part.setName("SpaK");
        Interactions interactions = handler.GetInteractions(part);
        assertTrue("No interactions are returned", interactions.getInteractions() != null);
        assertTrue("No interactions are returned", interactions.getInteractions().size() > 0);
    }

    @Test
    public void testGetInternalInteractions() throws Exception
    {
        PartsHandler handler = new PartsHandler(this.serverURL);
        Part part = new Part();
        part.setName("SpaK");
        Interactions interactions = handler.GetInternalInteractions(part);
        assertTrue("No interactions are returned", interactions.getInteractions() != null);
        assertTrue("No interactions are returned", interactions.getInteractions().size() > 0);
    }

    @Test
    public void testGetInteractionModel() throws Exception
    {
        PartsHandler handler = new PartsHandler(this.serverURL);
        Interaction interaction = new Interaction();
        interaction.setName("SpaK_SpaR");
        SBMLDocument sbmlDocument = handler.GetInteractionModel(interaction);
        assertTrue("No reactions in the model", sbmlDocument.getModel().getNumReactions() > 0);
    }

    @Test
    public void testModelOfFirstPart() throws Exception
    {
        PartsHandler handler = new PartsHandler(this.serverURL, true);
        List<Part> vparts = handler.GetParts(1).getParts();
        SBMLDocument sbmlDocument = handler.GetModel(vparts.get(0));
    }

    @Test
    public void testModelSerialization() throws Exception
    {
        PartsHandler handler = new PartsHandler(this.serverURL);
        Part part = new Part();
        part.setName("SpaK");
        SBMLDocument sbmlDocument = handler.GetModel(part);
        SBMLHandler sbmlHandler = new SBMLHandler();
        String modelAnnotation = sbmlDocument.getModel().getAnnotation().getNonRDFannotation();
        String sbmlOutput = sbmlHandler.GetSBML(sbmlDocument);
    }

    @Test
    public void GetSBMLTemplate() throws Exception
    {
        SBMLHandler sbmlHandler = new SBMLHandler();
        SBMLDocument sbmlDocument = sbmlHandler.GetSBMLTemplateModel("subtilinreceiver");
        String sbmlOutput = sbmlHandler.GetSBML(sbmlDocument);
        System.out.println(sbmlOutput);
    }

    /* DO NOT OPEN
    @Test
    public void CreateSubtilinReceiverModel() throws Exception {
    SBMLHandler sbmlHandler=new SBMLHandler();
    SBMLDocument sbmlDocument= sbmlHandler.GetSBMLTemplateModel("subtilinreceiver");
    // AddOperon(sbmlDocument,"PspaRK;Shim_RBS_SpaK_upstream;RBS_SpaK;Shim_RBS_SpaK_downstream;Shim_RBS_SpaR_upstream;RBS_SpaR;Shim_RBS_SpaR_downstream");

    String sbmlOutput=sbmlHandler.GetSBML(sbmlDocument);
    System.out.println(sbmlOutput);
    }

    private void AddOperon(SBMLDocument document, String parts)
    {

    }
     */
//   NOT USED anymore
//    @Test
//    public void AddmRNA() throws Exception
//    {
//        SBMLHandler sbmlHandler = new SBMLHandler();
//        SBMLDocument sbmlDocument = sbmlHandler.GetSBMLTemplateModel("subtilinreceiver");
//        PartsHandler partsHandler = new PartsHandler(serverURL);
//        Vpart part = partsHandler.GetPart("PspaRK");
//        SBMLDocument sbml = partsHandler.GetModel(part);
//        sbmlHandler.AddmRNA(part, sbml);
//        String sbmlOutput = sbmlHandler.GetSBML(sbml);
//        System.out.println(sbmlOutput);
//    }
//      @Test
//    public void RetrievemRNA() throws Exception
//    {
//        PartsHandler partsHandler = new PartsHandler(serverURL);
//        SBMLHandler sbmlHandler=new SBMLHandler();
//        Vpart mRNA = partsHandler.GetPart("mRNA");
//        SBMLDocument sbmlDocument = partsHandler.GetModel(mRNA);
//        ModelBuilder builder = new ModelBuilder(serverURL);
//        sbmlDocument=builder.UpdatemRNA(sbmlDocument, "GMGM");
//        String sbmlOutput = sbmlHandler.GetSBML(sbmlDocument);
//        System.out.println(sbmlOutput);
//    }

    /* //GM: Works iwth the beta2 version of jsbml
    @Test
    public void ConstructSubtilinRecveiver() throws Exception
    {
    SBMLHandler sbmlHandler = new SBMLHandler();
    ModelBuilder modelBuilder = new ModelBuilder(serverURL);
    SBMLDocument sbmlContainer = sbmlHandler.GetSBMLTemplateModel("subtilinreceiver");
    PartsHandler partsHandler = new PartsHandler(serverURL);

    SBMLDocument pspaRK = partsHandler.GetModel(partsHandler.GetPart("PspaRK"));
    SBMLDocument rbsSpaK=partsHandler.GetModel(partsHandler.GetPart("RBS_SpaK"));
    SBMLDocument rbsSpaR=partsHandler.GetModel(partsHandler.GetPart("RBS_SpaR"));
    Vpart spaKPart=partsHandler.GetPart("SpaK");
    SBMLDocument proteinSpaK=partsHandler.GetModel(spaKPart);
    SBMLDocument proteinSpaR=partsHandler.GetModel(partsHandler.GetPart("SpaR"));
    SBMLDocument spaK_spaR_interaction=partsHandler.GetInteractionModel(partsHandler.GetInteractions(spaKPart).getInteractions().get(0));
    Vpart pspaSPart=partsHandler.GetPart("PspaS");
    SBMLDocument pspaS=partsHandler.GetModel(pspaSPart);
    SBMLDocument pspaS_SpaR_interaction=partsHandler.GetInteractionModel(partsHandler.GetInteractions(pspaSPart).getInteractions().get(0));
    SBMLDocument rbsGFP=partsHandler.GetModel(partsHandler.GetPart("RBS_SpaS"));
    SBMLDocument proteinGFP=partsHandler.GetModel(partsHandler.GetPart("GFP_rrnb"));
    //Get mRNAs.mRNA virtual part is a template. Each time it is given with different IDs
    SBMLDocument mRNASpaRK = partsHandler.GetModel(partsHandler.GetPart("mRNA"));
    SBMLDocument mRNAGFP = partsHandler.GetModel(partsHandler.GetPart("mRNA"));

    modelBuilder.Add(pspaRK, sbmlContainer);

    modelBuilder.Link(pspaRK, mRNASpaRK, sbmlContainer);
    modelBuilder.Add(mRNASpaRK, sbmlContainer);

    modelBuilder.Link(mRNASpaRK,rbsSpaK,sbmlContainer);
    modelBuilder.Add(rbsSpaK,sbmlContainer);

    modelBuilder.Link(mRNASpaRK,rbsSpaR,sbmlContainer);
    modelBuilder.Add(rbsSpaR,sbmlContainer);

    modelBuilder.Link(rbsSpaK,proteinSpaK,sbmlContainer);
    modelBuilder.Add(proteinSpaK,sbmlContainer);

    modelBuilder.Link(rbsSpaR,proteinSpaR,sbmlContainer);
    modelBuilder.Add(proteinSpaR,sbmlContainer);

    modelBuilder.Add(spaK_spaR_interaction,sbmlContainer);

    modelBuilder.Add(pspaS,sbmlContainer);

    modelBuilder.Link(pspaS,pspaS_SpaR_interaction,sbmlContainer);
    modelBuilder.Add(pspaS_SpaR_interaction,sbmlContainer);

    modelBuilder.Link(pspaS_SpaR_interaction,mRNAGFP,sbmlContainer);
    modelBuilder.Add(mRNAGFP,sbmlContainer);

    modelBuilder.Link(mRNAGFP,rbsGFP,sbmlContainer);
    modelBuilder.Add(rbsGFP,sbmlContainer);

    modelBuilder.Link(rbsGFP,proteinGFP,sbmlContainer);
    modelBuilder.Add(proteinGFP,sbmlContainer);

    String sbmlOutput = sbmlHandler.GetSBML(sbmlContainer);
    System.out.println(sbmlOutput);
    Serializer.WriteToFile("C:/Temp/text.xml", sbmlOutput);
    }
     */
    @Test
    public void ConstructSubtilinReceiver() throws Exception
    {
        SBMLHandler sbmlHandler = new SBMLHandler();
        SBMLDocument sbmlContainer = sbmlHandler.GetSBMLTemplateModel("SubtilinReceiver");
        ModelBuilder modelBuilder = new ModelBuilder(sbmlContainer);
        PartsHandler partsHandler = new PartsHandler(serverURL);

        SBMLDocument pspaRK = partsHandler.GetModel(partsHandler.GetPart("PspaRK"));
        SBMLDocument rbsSpaK = partsHandler.GetModel(partsHandler.GetPart("RBS_SpaK"));
        SBMLDocument rbsSpaR = partsHandler.GetModel(partsHandler.GetPart("RBS_SpaR"));
        Part spaKPart = partsHandler.GetPart("SpaK");
        SBMLDocument proteinSpaK = partsHandler.GetModel(spaKPart);
        SBMLDocument proteinSpaR = partsHandler.GetModel(partsHandler.GetPart("SpaR"));
        SBMLDocument spaK_spaR_interaction = partsHandler.GetInteractionModel(partsHandler.GetInteractions(spaKPart).getInteractions().get(0));
        Part pspaSPart = partsHandler.GetPart("PspaS");
        SBMLDocument pspaS = partsHandler.GetModel(pspaSPart);
        SBMLDocument pspaS_SpaR_interaction = partsHandler.GetInteractionModel(partsHandler.GetInteractions(pspaSPart).getInteractions().get(0));
        SBMLDocument rbsGFP = partsHandler.GetModel(partsHandler.GetPart("RBS_SpaS"));
        SBMLDocument proteinGFP = partsHandler.GetModel(partsHandler.GetPart("GFP_rrnb"));
        //Get mRNAs.mRNA virtual part is a template. Each time it is given with different IDs
        SBMLDocument mRNASpaRK = partsHandler.GetModel(partsHandler.GetPart("mRNA"));
        SBMLDocument mRNAGFP = partsHandler.GetModel(partsHandler.GetPart("mRNA"));

        modelBuilder.Add(pspaRK);

        modelBuilder.Link(pspaRK, mRNASpaRK);
        modelBuilder.Add(mRNASpaRK);

        modelBuilder.Link(mRNASpaRK, rbsSpaK);
        modelBuilder.Add(rbsSpaK);

        modelBuilder.Link(mRNASpaRK, rbsSpaR);
        modelBuilder.Add(rbsSpaR);

        modelBuilder.Link(rbsSpaK, proteinSpaK);
        modelBuilder.Add(proteinSpaK);

        modelBuilder.Link(rbsSpaR, proteinSpaR);
        modelBuilder.Add(proteinSpaR);

        modelBuilder.Add(spaK_spaR_interaction);

        modelBuilder.Add(pspaS);

        modelBuilder.Link(pspaS, pspaS_SpaR_interaction);
        modelBuilder.Add(pspaS_SpaR_interaction);

        modelBuilder.Link(pspaS_SpaR_interaction, mRNAGFP);
        modelBuilder.Add(mRNAGFP);

        modelBuilder.Link(mRNAGFP, rbsGFP);
        modelBuilder.Add(rbsGFP);

        modelBuilder.Link(rbsGFP, proteinGFP);
        modelBuilder.Add(proteinGFP);

        String sbmlOutput = modelBuilder.GetModelString();
        System.out.println(sbmlOutput);
        Serializer.WriteToFile(GetBaseDirectory() + "ConstructSubtilinReceiver.xml", sbmlOutput);
    }

    @Test
    public void ConstructSubtilinReceiver2() throws Exception
    {
        SBMLHandler sbmlHandler = new SBMLHandler();
        SBMLDocument sbmlContainer = sbmlHandler.GetSBMLTemplateModel("SubtilinReceiver");
        ModelBuilder modelBuilder = new ModelBuilder(sbmlContainer);
        PartsHandler partsHandler = new PartsHandler(serverURL);

        SBMLDocument pspaRK = partsHandler.GetModel(partsHandler.GetPart("PspaRK"));
        SBMLDocument rbsSpaK = partsHandler.GetModel(partsHandler.GetPart("RBS_SpaK"));
        SBMLDocument rbsSpaR = partsHandler.GetModel(partsHandler.GetPart("RBS_SpaR"));
        Part spaKPart = partsHandler.GetPart("SpaK");
        SBMLDocument proteinSpaK = partsHandler.GetModel(spaKPart);
        SBMLDocument proteinSpaR = partsHandler.GetModel(partsHandler.GetPart("SpaR"));
        SBMLDocument spaK_spaR_interaction = partsHandler.GetInteractionModel(partsHandler.GetInteractions(spaKPart).getInteractions().get(0));
        Part pspaSPart = partsHandler.GetPart("PspaS");
        SBMLDocument pspaS = partsHandler.GetModel(pspaSPart);
        SBMLDocument pspaS_SpaR_interaction = partsHandler.GetInteractionModel(partsHandler.GetInteractions(pspaSPart).getInteractions().get(0));
        SBMLDocument rbsGFP = partsHandler.GetModel(partsHandler.GetPart("RBS_SpaS"));
        SBMLDocument proteinGFP = partsHandler.GetModel(partsHandler.GetPart("GFP_rrnb"));
        //Get mRNAs.mRNA virtual part is a template. Each time it is given with different IDs
        SBMLDocument mRNASpaRK = partsHandler.GetModel(partsHandler.GetPart("mRNA"));
        SBMLDocument mRNAGFP = partsHandler.GetModel(partsHandler.GetPart("mRNA"));

        modelBuilder.Add(pspaRK);

        modelBuilder.Add(mRNASpaRK);
        modelBuilder.Link(pspaRK, mRNASpaRK);

        modelBuilder.Add(rbsSpaK);
        modelBuilder.Link(mRNASpaRK, rbsSpaK);

        modelBuilder.Add(rbsSpaR);
        modelBuilder.Link(mRNASpaRK, rbsSpaR);

        modelBuilder.Add(proteinSpaK);
        modelBuilder.Link(rbsSpaK, proteinSpaK);

        modelBuilder.Add(proteinSpaR);
        modelBuilder.Link(rbsSpaR, proteinSpaR);

        modelBuilder.Add(spaK_spaR_interaction);

        modelBuilder.Add(pspaS);

        modelBuilder.Add(pspaS_SpaR_interaction);
        modelBuilder.Link(pspaS, pspaS_SpaR_interaction);

        modelBuilder.Add(mRNAGFP);
        modelBuilder.Link(pspaS_SpaR_interaction, mRNAGFP);

        modelBuilder.Add(rbsGFP);
        modelBuilder.Link(mRNAGFP, rbsGFP);

        modelBuilder.Add(proteinGFP);
        modelBuilder.Link(rbsGFP, proteinGFP);

        String sbmlOutput = modelBuilder.GetModelString();
        System.out.println(sbmlOutput);
        Serializer.WriteToFile(GetBaseDirectory() + "ConstructSubtilinReceiver2.xml", sbmlOutput);
    }




       @Test
    public void ConstructSubtilinReceiverPspaSTwice() throws Exception
    {
        //PspaS,RBS_SpaK,SpaK,RBS_SpaR, SpaR # PspaS, RBS_SpaS, GFP_rrnb
        SBMLHandler sbmlHandler = new SBMLHandler();
        SBMLDocument sbmlContainer=sbmlHandler.GetSBMLTemplateModel("SubtilinReceiver");
        ModelBuilder modelBuilder = new ModelBuilder(sbmlContainer);
        PartsHandler partsHandler = new PartsHandler(serverURL);

        Part pspaSPart=partsHandler.GetPart("PspaS");
        SBMLDocument pspaS1=partsHandler.GetModel(pspaSPart);
        Interaction pspaSInteraction=partsHandler.GetInteractions(pspaSPart).getInteractions().get(0);
        SBMLDocument pspaS_SpaR_interaction1=partsHandler.GetInteractionModel(pspaSInteraction);

        SBMLDocument rbsSpaK=partsHandler.GetModel(partsHandler.GetPart("RBS_SpaK"));
        SBMLDocument rbsSpaR=partsHandler.GetModel(partsHandler.GetPart("RBS_SpaR"));
        Part spaKPart=partsHandler.GetPart("SpaK");
        SBMLDocument proteinSpaK=partsHandler.GetModel(spaKPart);
        SBMLDocument proteinSpaR=partsHandler.GetModel(partsHandler.GetPart("SpaR"));
        SBMLDocument spaK_spaR_interaction=partsHandler.GetInteractionModel(partsHandler.GetInteractions(spaKPart).getInteractions().get(0));

        SBMLDocument pspaS2=modelBuilder.Clone(pspaS1, pspaSPart);
        SBMLDocument pspaS_SpaR_interaction2=modelBuilder.CloneInteraction(pspaS_SpaR_interaction1, pspaSInteraction);
        SBMLDocument rbsGFP=partsHandler.GetModel(partsHandler.GetPart("RBS_SpaS"));
        SBMLDocument proteinGFP=partsHandler.GetModel(partsHandler.GetPart("GFP_rrnb"));
        //Get mRNAs.mRNA virtual part is a template. Each time it is given with different IDs
        SBMLDocument mRNASpaRK = partsHandler.GetModel(partsHandler.GetPart("mRNA"));
        SBMLDocument mRNAGFP = partsHandler.GetModel(partsHandler.GetPart("mRNA"));

        modelBuilder.Add(pspaS1);

        modelBuilder.Add(pspaS_SpaR_interaction1);
        modelBuilder.Link(pspaS1,pspaS_SpaR_interaction1);

        modelBuilder.Add(mRNASpaRK);
        modelBuilder.Link(pspaS_SpaR_interaction1,mRNASpaRK);

        modelBuilder.Add(rbsSpaK);
        modelBuilder.Link(mRNASpaRK,rbsSpaK);

        modelBuilder.Add(rbsSpaR);
        modelBuilder.Link(mRNASpaRK,rbsSpaR);

        modelBuilder.Add(proteinSpaK);
        modelBuilder.Link(rbsSpaK,proteinSpaK);

        modelBuilder.Add(proteinSpaR);
        modelBuilder.Link(rbsSpaR,proteinSpaR);

        modelBuilder.Add(spaK_spaR_interaction);


        modelBuilder.Add(pspaS2);

        modelBuilder.Add(pspaS_SpaR_interaction2);
        modelBuilder.Link(pspaS2,pspaS_SpaR_interaction2);

        modelBuilder.Add(mRNAGFP);
        modelBuilder.Link(pspaS_SpaR_interaction2,mRNAGFP);

        modelBuilder.Add(rbsGFP);
        modelBuilder.Link(mRNAGFP,rbsGFP);

        modelBuilder.Add(proteinGFP);
        modelBuilder.Link(rbsGFP,proteinGFP);

        String sbmlOutput = modelBuilder.GetModelString();
        System.out.println(sbmlOutput);
        Serializer.WriteToFile(GetBaseDirectory() + "ConstructSubtilinReceiverPspaSTwice.xml", sbmlOutput);
    }

    @Test
    public void ConstructSubtilinReceiverPromoterRBSSpaRTwice() throws Exception
    {
        //PspaRK,RBS_SpaK,SpaK,RBS_SpaR, SpaR # PspaS, RBS_SpaS, GFP_rrnb # PspaRK,RBS_SpaR, SpaR
        SBMLHandler sbmlHandler = new SBMLHandler();
        SBMLDocument sbmlContainer = sbmlHandler.GetSBMLTemplateModel("SubtilinReceiver");
        ModelBuilder modelBuilder = new ModelBuilder(sbmlContainer);
        PartsHandler partsHandler = new PartsHandler(serverURL);

        Part pspaRKPart=partsHandler.GetPart("PspaRK");
        SBMLDocument pspaRK = partsHandler.GetModel(pspaRKPart);
        Part rbsSpaKPart=partsHandler.GetPart("RBS_SpaK");
        SBMLDocument rbsSpaK = partsHandler.GetModel(rbsSpaKPart);
        Part rbsSpaRPart=partsHandler.GetPart("RBS_SpaR");
        SBMLDocument rbsSpaR = partsHandler.GetModel(rbsSpaRPart);
        Part spaKPart = partsHandler.GetPart("SpaK");
        SBMLDocument proteinSpaK = partsHandler.GetModel(spaKPart);
        Part SpaRPart=partsHandler.GetPart("SpaR");
        SBMLDocument proteinSpaR = partsHandler.GetModel(SpaRPart);
        SBMLDocument spaK_spaR_interaction = partsHandler.GetInteractionModel(partsHandler.GetInteractions(spaKPart).getInteractions().get(0));
        Part pspaSPart = partsHandler.GetPart("PspaS");
        SBMLDocument pspaS = partsHandler.GetModel(pspaSPart);
        SBMLDocument pspaS_SpaR_interaction = partsHandler.GetInteractionModel(partsHandler.GetInteractions(pspaSPart).getInteractions().get(0));
        SBMLDocument rbsGFP = partsHandler.GetModel(partsHandler.GetPart("RBS_SpaS"));
        SBMLDocument proteinGFP = partsHandler.GetModel(partsHandler.GetPart("GFP_rrnb"));
        //Get mRNAs.mRNA virtual part is a template. Each time it is given with different IDs
        SBMLDocument mRNASpaRK = partsHandler.GetModel(partsHandler.GetPart("mRNA"));
        SBMLDocument mRNAGFP = partsHandler.GetModel(partsHandler.GetPart("mRNA"));

        modelBuilder.Add(pspaRK);

        modelBuilder.Add(mRNASpaRK);
        modelBuilder.Link(pspaRK, mRNASpaRK);

        modelBuilder.Add(rbsSpaK);
        modelBuilder.Link(mRNASpaRK, rbsSpaK);

        modelBuilder.Add(rbsSpaR);
        modelBuilder.Link(mRNASpaRK, rbsSpaR);

        modelBuilder.Add(proteinSpaK);
        modelBuilder.Link(rbsSpaK, proteinSpaK);

        modelBuilder.Add(proteinSpaR);
        modelBuilder.Link(rbsSpaR, proteinSpaR);

        modelBuilder.Add(spaK_spaR_interaction);

        modelBuilder.Add(pspaS);

        modelBuilder.Add(pspaS_SpaR_interaction);
        modelBuilder.Link(pspaS, pspaS_SpaR_interaction);

        modelBuilder.Add(mRNAGFP);
        modelBuilder.Link(pspaS_SpaR_interaction, mRNAGFP);

        modelBuilder.Add(rbsGFP);
        modelBuilder.Link(mRNAGFP, rbsGFP);

        modelBuilder.Add(proteinGFP);
        modelBuilder.Link(rbsGFP, proteinGFP);

        SBMLDocument pspaRK2=modelBuilder.Clone(pspaRK, pspaRKPart);
        SBMLDocument rbsSpaR2=modelBuilder.Clone(rbsSpaR, rbsSpaRPart);
        SBMLDocument proteinSpaR2=modelBuilder.Clone(proteinSpaR, SpaRPart);

        modelBuilder.Add(pspaRK2);
        SBMLDocument mRNASpaR2 = partsHandler.GetModel(partsHandler.GetPart("mRNA"));

        modelBuilder.Add(mRNASpaR2);
        modelBuilder.Link(pspaRK2, mRNASpaR2);

        modelBuilder.Add(rbsSpaR2);
        modelBuilder.Link(mRNASpaR2, rbsSpaR2);

        modelBuilder.Add(proteinSpaR2);
        modelBuilder.Link(rbsSpaR2, proteinSpaR2);

   
        String sbmlOutput = modelBuilder.GetModelString();
        System.out.println(sbmlOutput);
        Serializer.WriteToFile(GetBaseDirectory() + "ConstructSubtilinReceiverPromoterRBSSpaRTwice.xml", sbmlOutput);
    }

//    @Test
//    public void ConstructSubtilinReceiver4() throws Exception
//    {
//        String construct="Promoter:PspaRK,RBS:RBS_SpaR,CDS:SpaR,RBS:RBS_SpaK,CDS:SpaK#promoter:PspaS,RBS:RBS_SpaS;CDS:GFP_rrnb";
//        SBMLDocument sbmlDocument=GetSBML(construct);
//        SBMLHandler handler=new SBMLHandler();
//        String sbmlOutput=handler.GetSBML(sbmlDocument);
//        Serializer.WriteToFile("C:/Temp/text5.xml", sbmlOutput);
//    }

    @Test
    public void TwoPromoterTest() throws Exception
    {
        SBMLHandler sbmlHandler = new SBMLHandler();
        SBMLDocument sbmlContainer = sbmlHandler.GetSBMLTemplateModel("SubtilinReceiver");
        ModelBuilder modelBuilder = new ModelBuilder(sbmlContainer);
        PartsHandler partsHandler = new PartsHandler(serverURL);
        Part pspaRK=partsHandler.GetPart("PspaRK");
        Part pspaS=partsHandler.GetPart("PspaS");      
        Part spaKPart = partsHandler.GetPart("SpaK");                  
        
        SBMLDocument pspaRKModel = partsHandler.GetModel(pspaRK);
        SBMLDocument pspaSModel = partsHandler.GetModel(pspaS);         
        SBMLDocument rbsSpaKModel = partsHandler.GetModel(partsHandler.GetPart("RBS_SpaK"));
        SBMLDocument spaKModel = partsHandler.GetModel(spaKPart);
        SBMLDocument rbsSpaRModel = partsHandler.GetModel(partsHandler.GetPart("RBS_SpaR"));
        SBMLDocument spaRModel = partsHandler.GetModel(partsHandler.GetPart("SpaR"));
        
        SBMLDocument pspaS_SpaRInteractionModel = partsHandler.GetInteractionModel(partsHandler.GetInteractions(pspaS).getInteractions().get(0));
        SBMLDocument mRNAModel = partsHandler.GetModel(partsHandler.GetPart("mRNA"));

        modelBuilder.Add(pspaRKModel);
        modelBuilder.Add(mRNAModel);
        
        modelBuilder.Link(pspaRKModel, mRNAModel);
                 
        modelBuilder.Add(pspaSModel);         
        modelBuilder.Add(pspaS_SpaRInteractionModel);
        
        modelBuilder.Link(pspaSModel, pspaS_SpaRInteractionModel);
        
        modelBuilder.Link(pspaS_SpaRInteractionModel, mRNAModel);
        
        modelBuilder.Add(rbsSpaKModel );
        modelBuilder.Link(mRNAModel, rbsSpaKModel );
        
        modelBuilder.Add(spaKModel);
        modelBuilder.Link(rbsSpaKModel, spaKModel);
        
        modelBuilder.Add(rbsSpaRModel );
        modelBuilder.Link(mRNAModel, rbsSpaRModel );
        
        modelBuilder.Add(spaRModel);
        modelBuilder.Link(rbsSpaRModel, spaRModel);
        
        Interaction spaK_spaR_interactionObject=partsHandler.GetInteractions(spaKPart).getInteractions().get(0);
        SBMLDocument spaK_spaR_interaction=partsHandler.CreateInteractionModel(spaK_spaR_interactionObject);
        modelBuilder.Add(spaK_spaR_interaction);
        
        String sbmlOutput = modelBuilder.GetModelString();
        System.out.println(sbmlOutput);
        Serializer.WriteToFile(GetBaseDirectory() + "TwoPromoterTest.xml", sbmlOutput);
    }
    
    
    @Test
    public void OnePromoter() throws Exception
    {
        SBMLHandler sbmlHandler = new SBMLHandler();
        SBMLDocument sbmlContainer = sbmlHandler.GetSBMLTemplateModel("SubtilinReceiver");
        ModelBuilder modelBuilder = new ModelBuilder(sbmlContainer);
        PartsHandler partsHandler = new PartsHandler(serverURL);
        Part pspaRK=partsHandler.GetPart("PspaRK");
        SBMLDocument pspaRKModel = partsHandler.GetModel(pspaRK);
        SBMLDocument rbsSpaKModel = partsHandler.GetModel(partsHandler.GetPart("RBS_SpaK"));
        SBMLDocument cdsSpaKModel = partsHandler.GetModel(partsHandler.GetPart("SpaK"));
        
        SBMLDocument mRNAModel = partsHandler.GetModel(partsHandler.GetPart("mRNA"));

        modelBuilder.Add(pspaRKModel);
        
        
        
        modelBuilder.Add(mRNAModel);
        modelBuilder.Link(pspaRKModel, mRNAModel);
        
        modelBuilder.Add(rbsSpaKModel );
        modelBuilder.Link(mRNAModel, rbsSpaKModel );
        
        modelBuilder.Add(cdsSpaKModel);
        modelBuilder.Link(rbsSpaKModel, cdsSpaKModel);
        
        String sbmlOutput = modelBuilder.GetModelString();
        System.out.println(sbmlOutput);
        Serializer.WriteToFile(GetBaseDirectory() + "OnePromoter.xml", sbmlOutput);

    }
      
    private SBMLDocument GetSBML(String construct) throws Exception
    {
        SBMLHandler sbmlHandler = new SBMLHandler();
        SBMLDocument sbmlContainer=sbmlHandler.GetSBMLTemplateModel("SubtilinReceiver");
        ModelBuilder modelBuilder = new ModelBuilder(sbmlContainer);
        PartsHandler partsHandler = new PartsHandler(serverURL);
        HashSet<String> interactionsAdded=new HashSet<String>();
        HashSet<String> partsAdded=new HashSet<String>();

        String[] transcriptionalUnits=construct.split("#");
        for (int i=0;i<transcriptionalUnits.length;i++)
        {
            String transcriptionalUnit=transcriptionalUnits[i];
            String[] parts= transcriptionalUnit.split(",");
            SBMLDocument lastProcessedPartModel=null;
            for (int j=0;j<parts.length;i++)
            {
                String partInfo=parts[j];
                String[] partIDInfo=partInfo.split(":");
                String partType=partIDInfo[0];
                String partID=partIDInfo[1];
                Part part=partsHandler.GetPart(partID);
                SBMLDocument partModel=partsHandler.GetModel(part);

                if (partsAdded.contains(part.getName()))
                {
                    partModel=modelBuilder.Clone(partModel, part);
                }
                else
                {
                    partsAdded.add(partID);
                }
                modelBuilder.Add(partModel);
                
                if (lastProcessedPartModel!=null)
                {
                     modelBuilder.Link(lastProcessedPartModel,partModel);
                }

                Interactions interactions=partsHandler.GetInteractions(part);
                if (interactions!=null && interactions.getInteractions()!=null)
                {
                    for (int k=0;k<interactions.getInteractions().size();i++)
                    {
                        Interaction interaction=interactions.getInteractions().get(i);
                        if (!interactionsAdded.contains(interaction.getName()))
                        {
                            SBMLDocument interactionModel=partsHandler.GetInteractionModel(interaction);
                            modelBuilder.Add(interactionModel);
                            interactionsAdded.add(interaction.getName());
                            if (partType.equals(Part.PROMOTER))//Or the last PoPS provider
                            {
                                 modelBuilder.Link(partModel, interactionModel);
                                 SBMLDocument mRNA = partsHandler.GetModel(partsHandler.GetPart("mRNA"));
                                 modelBuilder.Add(mRNA);
                                 modelBuilder.Link(interactionModel, mRNA);
                                 lastProcessedPartModel=mRNA;
                            }
                        }
                        else
                        {

                        }
                    }
                }
                else if (partType.equals(Part.PROMOTER)) //Or the last PoPS provider
                {
                    SBMLDocument mRNA = partsHandler.GetModel(partsHandler.GetPart("mRNA"));
                    modelBuilder.Add(mRNA);
                    modelBuilder.Link(partModel, mRNA);
                    lastProcessedPartModel=mRNA;
                }
                else
                {
                    lastProcessedPartModel=partModel;
                }
            }
        }
        return null;
    }
    
    public static String GetBaseDirectory()
    {
    	if (IsWindows())
    	{
    		return "c:/temp/";
    	}
    	else
    	{
    		return "/tmp/";
    	}
    }
    
    private static boolean IsWindows()
    {
      String os=System.getProperty("os.name");
      return os.startsWith("Windows");
    }

}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package virtualparts;

import java.io.IOException;
import java.util.List;
import virtualparts.entity.Formula;
import virtualparts.http.HttpHandler;
import virtualparts.serialize.Serializer;

/**
 * Internal singleton class to retrieve the list of formulas from the parts repository
 * @author a8901379
 */
class FormulaProvider {
    private static List<Formula> formulas;

    private FormulaProvider() {
		
    }

    /**
     * The singleton method that retrieves the list of formulas
     * @param server The base URL of the parts repository
     * @return
     * @throws IOException
     */
    public static List<Formula> GetFormulas(String server) throws IOException {
		if (formulas == null) {
			formulas = FetchFormulas(server);
		}
		return formulas;
	}

    private static synchronized List<Formula> FetchFormulas(String server) throws IOException
    {
         //Get the parts in xml format
        String url = String.format("%s/formulas/xml", server);
        HttpHandler httpHandler = new HttpHandler();
        String data = httpHandler.Get(url);

        //Deserialize the xml data into Parts object
        Serializer serializer = new Serializer();
        List<Formula> formulaList = serializer.GetFormulas(data);
        return formulaList;
    }
}

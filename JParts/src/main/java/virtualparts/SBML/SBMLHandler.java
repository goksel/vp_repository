/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package virtualparts.SBML;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;

import javax.xml.stream.XMLStreamException;

import org.sbml.jsbml.ASTNode;
import org.sbml.jsbml.Annotation;
import org.sbml.jsbml.AssignmentRule;
import org.sbml.jsbml.Compartment;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.Parameter;
import org.sbml.jsbml.Reaction;
import org.sbml.jsbml.Rule;
import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.SBMLReader;
import org.sbml.jsbml.SBMLWriter;
import org.sbml.jsbml.Species;
import org.sbml.jsbml.Unit.Kind;

/**
 * Provides methods to work with SBML documents.
 * @author a8901379
 */
public class SBMLHandler {

    /**
     * Deserialises an SBML model from a given SVML String into an SBML object
     * @param sbmlXml XML representation of the model
     * @return An object that represents the given model
     * @throws XMLStreamException
     */
public SBMLDocument GetSBML(String sbmlXml) throws XMLStreamException
    {
        SBMLReader reader=new SBMLReader();
        SBMLDocument sbmlDocument=reader.readSBMLFromString(sbmlXml);
        return sbmlDocument;
    }

/**
 * Serialises an SBML model as an XML String
 * @param model An SBML model
 * @return  XML representation of the model
 * @throws Exception
 */
    public String GetSBML(SBMLDocument model) throws Exception {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        new SBMLWriter().write(model, new BufferedOutputStream(stream));
        String output = new String(stream.toByteArray());
        return output;
    }

    /**
     * Creates an empty container model to drop the models of parts into
     * @param modelName The name of the model to contruct
     * @return An object representing an empty SBML model
     */
    public SBMLDocument GetSBMLTemplateModel(String modelName)
    {
        SBMLHelper sbmlHelper=new SBMLHelper();
        SBMLDocument sbmlDocument = new SBMLDocument(SBMLHelper.LEVEL,SBMLHelper.VERSION);
        sbmlDocument.addNamespace("mts","xmlns", "http://purl.org/modeltosequence/1.0#");
        Model model = sbmlHelper.CreateSBMLModel(sbmlDocument,modelName);
        Parameter parameter=sbmlHelper.CreateParameter(model, "volume");
        parameter.setValue(1);
        sbmlHelper.CreateUnitDefinition(model, "volume", Kind.LITRE, -15);
        sbmlHelper.CreateUnitDefinition(model, "substance", Kind.MOLE, -9);
        Compartment compartment=sbmlHelper.CreateCompartment(model, "cell");
        compartment.setAnnotation(new Annotation(GetCellAnnotation()));
        return sbmlDocument;
    }

    /**
     * Returns true of a given SBML document includes a reaction with the given reaction ID, otherwise returns false
     * @param document SBMLDocument object to search for the reaction
     * @param reactionID ID of the reaction to search for
     * @return
     */
      public boolean ReactionExists(SBMLDocument document, String reactionID)
    {
        Boolean found=false;
        List<Reaction> reactions=document.getModel().getListOfReactions();
        for(Reaction reaction:reactions)
        {
            if (reaction.getId().equals(reactionID))
            {
                found=true;
                break;
            }
        }
        return found;
    }

      

    
        public AssignmentRule GetAssignmentRule(SBMLDocument document, String variable)
      {
          AssignmentRule foundRule=null;
          List<Rule> rules=document.getModel().getListOfRules();
          for(Rule rule:rules)
          {
              if (rule.isAssignment())
              {
            	  AssignmentRule aRule=(AssignmentRule) rule;
            	  if (aRule.getVariable().equals(variable))
            	  {
            		  foundRule=aRule;
            		  break;
            	  }
              }        	
          }
          return foundRule;
      }

      /**
       * Returns true of a given SBML document includes a species with the given species ID, otherwise returns false
       * @param document SBMLDocument object to search for the species
       * @param speciesID ID of the species to search for
       * @return
       */
     public boolean SpeciesExists(SBMLDocument document, String speciesID)
    {
        Boolean found=false;
        List<Species> speciesList=document.getModel().getListOfSpecies();
        for(Species species:speciesList)
        {
            if (species.getId().equals(speciesID))
            {
                found=true;
                break;
            }
        }
        return found;
    }

    private String GetCellAnnotation()
    {
    String annotation="<ModelToSequence xmlns=\"http://purl.org/modeltosequence/1.0#\">" + 
                          "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" xmlns:mts=\"http://purl.org/modeltosequence/1.0#\">" +
                              "<rdf:Description rdf:about=\"#cell_meta\">" +
                              "<rdf:type rdf:resource=\"mts:Chassis\" />" +
                              "<mts:TerminatorSequence>taaaggacgccgccaagccagcttaaacccagctcaatgagctgggttttttgtttgttaa</mts:TerminatorSequence>" +
                              "</rdf:Description>" +
                          "</rdf:RDF>" +
                      "</ModelToSequence>";
        return annotation;
    }

    public AssignmentRule CreateAssignmentRule(Model model, String name, ASTNode math)
    {
        SBMLHelper sbmlHelper=new SBMLHelper();
        return sbmlHelper.CreateAssignmentRule(model, name, math);
}


    /* Not used anymore
    public SBMLDocument AddmRNA(Vpart part, SBMLDocument sbmlDocument) throws Exception
    {
        SBMLHelper sbmlHelper=new SBMLHelper();
        String mRNAName=part.getName() + "_mRNA";
        String popsInputParameterName=mRNAName + "_PoPSInput";
        Species mRNA=sbmlHelper.CreateSpecies(sbmlDocument.getModel(), part.getName() + "_mRNA");
        mRNA.setCompartment(sbmlDocument.getModel().getCompartment(0));
        mRNA.setInitialConcentration(0);
        //TODO:Add Signal Carrier annotation
        //create the pops input variable parameter
        Parameter parameter=sbmlHelper.CreateParameter(sbmlDocument.getModel(), popsInputParameterName, Boolean.FALSE);
        parameter.setMetaId(null);
        //Create the PoPS input assignment rule.  Link the PoPs output from
        ASTNode math = ASTNode.parseFormula(String.format("1*%s_PoPSProductionPoPSOutput",part.getName()));//Get the PoPS output's name dynamically
        sbmlHelper.CreateAssignmentRule(sbmlDocument.getModel(), popsInputParameterName, math);

        //Add mRNA production
        Reaction mRNAProduction=sbmlHelper.CreateReaction(sbmlDocument.getModel(), mRNAName + "_PoPSProduction");
        SpeciesReference mRNARef=new SpeciesReference(mRNA);
        mRNAProduction.addProduct(mRNARef);
        sbmlHelper.AddKineticLaw(mRNAProduction, String.format("%s*10/6.0221415",popsInputParameterName));

        //Add mRNA degradation
        Reaction mRNADegradation=sbmlHelper.CreateReaction(sbmlDocument.getModel(), mRNAName + "_Degradation");
        mRNAProduction.addReactant(mRNARef);
        sbmlHelper.AddKineticLaw(mRNADegradation,String.format("0.0058*%s",mRNAName));
        return sbmlDocument;
    }
*/
}


package virtualparts.SBML;

import org.sbml.jsbml.ASTNode;
import org.sbml.jsbml.AssignmentRule;
import org.sbml.jsbml.Compartment;
import org.sbml.jsbml.KineticLaw;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.Parameter;
import org.sbml.jsbml.Reaction;
import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.Species;
import org.sbml.jsbml.Unit;
import org.sbml.jsbml.UnitDefinition;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Provides low-level methods to construct SBML models from units, compartments, parameters, reactions
 * @author a8901379
 */
class SBMLHelper {
    /**
     * LEVEL=2
     */
	public static int LEVEL=2;
        /**
         * VERSION=4
         */
	public static int VERSION=4;


/**
 * Creates a UnitDefinition object for a given model
 * @param model The SBML model.
 * @param definitionName The name of the unit definition
 * @param kind The kind of the unit
 * @param scale Scale
 * @return UnitDefinition
 */
public UnitDefinition CreateUnitDefinition(Model model, String definitionName,Unit.Kind kind, int scale)
{
    UnitDefinition unitDefinition=new UnitDefinition(definitionName,LEVEL,VERSION);
    Unit unit=unitDefinition.createUnit(kind);
    unit.setScale(scale);
    //unitDefinition.addUnit(unit);
    model.addUnitDefinition(unitDefinition);
    return unitDefinition;
}

/**
 * Creates a compartment for a given SBML model
 * @param model The SBML model.
 * @param name The name of the compartment
 * @return Compartment
 */
public Compartment CreateCompartment(Model model, String name)
{
    Compartment compartment=new Compartment(name, name, LEVEL, VERSION);
    compartment.setSize(1);
    compartment.setMetaId(name + "_meta");
    compartment.setId(name);
    model.addCompartment(compartment);
    return compartment;
}

/**
 * Creates an SBML parameter for a given SBML model
 * @param model The SBML model
 * @param name The name of the parameters
 * @param isConstant true to create a constant parameter.
 * @return Parameter
 */
public Parameter CreateParameter(Model model, String name, Boolean isConstant)
{
        Parameter parameter=model.createParameter(name);
        parameter.setId(name);
        parameter.setMetaId(name + "_meta");
        parameter.setConstant(isConstant);
        return parameter;
}

/**
 * Creates an SBML parameter for a given SBML model
 * @param model The SBML model
 * @param name The name of the parameters
 * @return Parameter
 */
public Parameter CreateParameter(Model model, String name)
{
        Parameter parameter=model.createParameter(name);
        parameter.setName(name);
        parameter.setMetaId(name + "_meta");
        parameter.setId(name);
        return parameter;
}

/**
 * Creates an SBML parameter for a given SBML model
 * @param model The SBML model
 * @return Parameter
 */
public Parameter CreateParameter(String name)
{
		Parameter parameter=new org.sbml.jsbml.Parameter(name,LEVEL,VERSION);
		parameter.setName(name);
		parameter.setMetaId(name + "_meta");
                parameter.setId(name);
        return parameter;
}

/**
 * Updates the name of a given parameter
 * @param parameter The parameter to be updated
 * @param name New name
 * @return Parameter
 */
public Parameter UpdateParameter(Parameter parameter, String name)
{
		parameter.setId(name);
		parameter.setName(name);
		parameter.setMetaId(name + "_meta");
        return parameter;
}

/**
 * Creates an assignment rule for a given SBML model.
 * @param model The SBML model
 * @param name The name of the rule
 * @param math The mathematical formula for the rule
 * @return AssignmentRule
 */
public AssignmentRule CreateAssignmentRule(Model model, String name, ASTNode math)
    {
        AssignmentRule rule=model.createAssignmentRule();
        rule.setVariable(name);
        rule.setMetaId(name + "_rule_meta");
        rule.setMath(math);
        return rule;
}

/*
    public String GetSBML(SBMLDocument doc) throws Exception {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        new SBMLWriter().write(doc, new BufferedOutputStream(stream));
        String output = new String(stream.toByteArray());
        //SBMLDocument doc2=new SBMLReader().readSBMLFromString(output);
        //output=FixAnnotation(output);
        return output;
    }
*/
    /**
	 * To be used for jsbml bug. (http://sourceforge.net/tracker/?func=detail&aid=3175833&group_id=279608&atid=1186776)
	 * Bug:Annotation tag is closed after the math tag in kinetic laws and assignent rules
	 * @param sbmlElement
	 */
	private String  FixAnnotation(String output)
	{
		output=output.replace("</math></annotation>", "</math>");
		int startIndex=0;
		//Add annotation close tag after ModelToSequence closing tag
		while (startIndex>-1)
		{
			int  mtsClosingIndex=output.indexOf("</ModelToSequence>",startIndex);
			if (mtsClosingIndex==-1)
			{
				break;
			}
			int  mathOpeninIndex=output.indexOf("<math",mtsClosingIndex);
			String space=output.substring(mtsClosingIndex, mathOpeninIndex);
			if (space.indexOf("</annotation>")==-1)
			{
				//TODO Add fix if the space includes rdf closing tags. Then annotation should be added after rdf closing tags

				output=output.substring(0,mtsClosingIndex) + "</ModelToSequence></annotation>" + output.substring(mathOpeninIndex);
			}
			startIndex=mtsClosingIndex + "</ModelToSequence>".length();

		}
		return output;
	}

        /**
         * Creates an SBML model for the given SBML document.
         * @param sbmlDocument The SBML document
         * @param name Name of the model to be included in the document
         * @return An SBML model
         */
    public Model CreateSBMLModel(SBMLDocument sbmlDocument, String name)
    {
    	Model sbmlModel = sbmlDocument.createModel(name);
        sbmlModel.setMetaId(name + "_meta");
        sbmlModel.setId(name);
        sbmlModel.setName(name);
        return sbmlModel;
    }

    /**
     * Creates an SBML species foe given SBML model.
     * @param sbmlModel The SBML model
     * @param name The name of the species.
     * @return An SBML species
     */
    public Species CreateSpecies(Model sbmlModel, String name)
    {
    	Species species=sbmlModel.createSpecies(name);
    	species.setMetaId(name + "_meta");
        species.setId(name);
    	species.setName(name);
        return species;
    }

    /**
     * Creates a SBML reaction for a given SBML model.
     * @param sbmlModel The SBML model
     * @param name The name of the reaction
     * @return Reaction
     */
    public Reaction CreateReaction(Model sbmlModel, String name )
    {
		Reaction reaction = sbmlModel.createReaction(name);
		reaction.setMetaId(name);
                reaction.setId(name + "_meta");
		reaction.setName(name);
		return reaction;
    }

    /**
     * Adds a kinetic law to a given SBML reaction
     * @param reaction The SBML reaction
     * @param formula The String representing the mathematical formula for the kinetic law
     * @return Updated reaction
     * @throws Exception
     */
    public Reaction AddKineticLaw(Reaction reaction, String formula ) throws Exception
    {
        KineticLaw kl=new KineticLaw(SBMLHelper.LEVEL, SBMLHelper.VERSION);
        ASTNode math = ASTNode.parseFormula(formula);
        kl.setMath(math);
        reaction.setKineticLaw(kl);
        return reaction;
    }
}

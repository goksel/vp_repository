/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package virtualparts;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.xml.stream.XMLStreamException;

import org.sbml.jsbml.SBMLDocument;

import uk.ac.ncl.intbio.util.HTTPHandler;
import virtualparts.SBML.SBMLHandler;
import virtualparts.entity.Formula;
import virtualparts.entity.Interaction;
import virtualparts.entity.Interactions;
import virtualparts.entity.MolecularFormType;
import virtualparts.entity.Part;
import virtualparts.entity.Parts;
import virtualparts.entity.SearchEntity;
import virtualparts.entity.Summary;
import virtualparts.filter.IFilter;
import virtualparts.http.HttpHandler;
import virtualparts.model.ModelFactory;
import virtualparts.model.ModelOutput;
import virtualparts.serialize.Serializer;

/**
 * Provides methods to retrieve metadata about, and models of, biological parts from a given BioBricks repository.
 * An example repository is at http://atgc-eidos.appspot.com
 */
public class PartsHandler {
    private String server;
    private Boolean debug=false;

    /**
     * Constructor.
     * @param server The URL of the BioBricks repository to retrieve data
     */
    public PartsHandler(String server)
    {
        this.server = server;
    }

    /**
     * Constructor.
     * @param server The URL of the BioBricks repository to retrieve data
     * @param debug Set to to true in order to write data retrieved from the server to the screen.
     */
    public PartsHandler(String server,Boolean debug)
    {
        this.server = server;
        this.debug=debug;
    }

    /**
     * Gets the list of all parts given with a page number
     * @param pageNumber Index of the page to retrieve metadata about parts. Each page contains 50 parts.
     * @return A list of parts
     * @throws IOException
     */
    public Parts GetParts(int pageNumber) throws IOException
    {
        //Get the parts in xml format
        String url = String.format("%s/parts/page/%d/xml", this.server,pageNumber);
        return GetPartsData(url);
    }
    /**
     * Gets the list of all parts given with a page number and type. 
     * @param pageNumber Index of the page to retrieve metadata about parts. Each page contains 50 parts.
     * @param partType   Type of the part. Valid types are: Promoter,FunctionalPart,RBS,Shim,Terminator, and Operator.
     * @return a List of parts
     * @throws IOException
     */
     public Parts GetParts(int pageNumber,String partType) throws IOException
    {
        //Get the parts in xml format
        String url = String.format("%s/parts/%s/page/%d/xml", this.server,partType,pageNumber);
        return GetPartsData(url);
    }
     
     /**
      * Gets the list of all parts given with a search criteria. 
      * @param pageNumber Index of the page to retrieve metadata about parts. Each page contains 50 parts.
      * @param searchEntity  SearchEntity object. Valid part types are: Promoter,FunctionalPart,RBS,Shim,Terminator, and Operator.
      * @return a List of parts
      * @throws IOException
      */
      public Parts GetParts(int pageNumber,SearchEntity searchEntity) throws IOException
     {
         //Get the parts in xml format
    	 Serializer serializer=new Serializer();
    	 String searchText=serializer.SerializeSearchEntity(searchEntity);
         
    	 String url = String.format("%s/postdata", this.server);
         HashMap<String, String > parameters=new HashMap<String, String>();
         parameters.put("type", "search");
         parameters.put("page", String.valueOf(pageNumber));         
         parameters.put("data", searchText);         
         
         String result=HTTPHandler.post(url, parameters);
         Parts parts=null;
         if (result!=null && result.length()>0)
         {
        	 parts = serializer.GetParts(result);
         }
         return parts;
     }

      /**
       * Gets the summary information about parts, including the number of pages, for a given part type. Each page contains 50 parts.
       * @param partType Type of the part. Valid types are: Promoter,FunctionalPart,RBS,Shim,Terminator, and Operator.
       * @return Summary
       * @throws IOException
       */
       public Summary GetPartsSummary(SearchEntity searchEntity) throws IOException
      {
    	  Serializer serializer=new Serializer();
     	  String searchText=serializer.SerializeSearchEntity(searchEntity);
          
     	  String url = String.format("%s/postdata", this.server);
          HashMap<String, String > parameters=new HashMap<String, String>();
          parameters.put("type", "searchsummary");
          parameters.put("data", searchText); 
          
          
          String result=HTTPHandler.post(url, parameters);
          Summary summary=null;
          if (result!=null && result.length()>0)
          {
         	 summary = serializer.GetSummary(result);
          }          
          return summary;
      }

     /**
      * Gets the list of all parts given with a page number and type, filtered by a role name and value pair
      * @param pageNumber Index of the page to retrieve metadata about parts. Each page contains 50 parts.
      * @param partType Type of the part. Valid types are: Promoter,FunctionalPart,RBS,Shim,Terminator, and Operator.
      * @param roleName Valid role names:type, located_in, has_function.
      * @param roleValue A value specified for the role name. Examples of role names and values([name]:[value]): type:BO_RepressiblePromoter, located_in:GO_0005886, has_function:GO_0046983
      * @return A list of parts
      * @throws IOException
      */
     public Parts GetParts(int pageNumber,String partType,String roleName, String roleValue) throws IOException
    {
        //Get the parts in xml format
        String url = String.format("%s/parts/%s/%s/%s/page/%d/xml", this.server,partType,roleName,roleValue, pageNumber);
        return GetPartsData(url);
    }

     /**
      * Gets the list of parts for a given filter
      * @param filter The filter object containing the restriction to retrieve parts.
      * @return
      * @throws IOException
      */
     public Parts GetParts(IFilter filter) throws IOException
     {
        String filterString=filter.GetFilterString();
        String url=String.format("%s/partsbyparameter/%s",server,filterString);
        //String url=String.format("%s/xmlviewbyparameter.jsp?type=parts&filter=%s",server,filterString);
        return GetPartsData(url);
     }

      /**
     * Gets the summary information about parts, including the number of pages. Each page contains 50 parts.
     * @return Summary
     * @throws IOException
     */
    public Summary GetPartsSummary() throws IOException
    {
        //Get the parts in xml format
        String url = String.format("%s/parts/summary/xml", this.server);
        return GetSummaryData(url);
    }
    /**
     * Gets the summary information about parts, including the number of pages, for a given part type. Each page contains 50 parts.
     * @param partType Type of the part. Valid types are: Promoter,FunctionalPart,RBS,Shim,Terminator, and Operator.
     * @return Summary
     * @throws IOException
     */
     public Summary GetPartsSummary(String partType) throws IOException
    {
        //Get the parts in xml format
        String url = String.format("%s/parts/%s/summary/xml", this.server,partType);
        return GetSummaryData(url);
    }         
      

     /**
      * Gets the summary information about parts, including the number of pages, for a given part type, filtered by a role name and value pair
      * @param partType Type of the part. Valid types are: Promoter,FunctionalPart,RBS,Shim,Terminator, and Operator.
      * @param roleName Valid role names:type, located_in, has_function.
      * @param roleValue A value specified for the role name. Examples of role names and values([name]:[value]): type:BO_RepressiblePromoter, located_in:GO_0005886, has_function:GO_0046983
      * @return Summary
      * @throws IOException
      */
     public Summary GetPartsSummary(String partType,String roleName, String roleValue) throws IOException
    {
        //Get the parts in xml format
        String url = String.format("%s/parts/%s/%s/%s/summary/xml", this.server,partType,roleName,roleValue);
        return GetSummaryData(url);
    }

    /**
     * Gets the metadata about a part given with an ID.
     * @param id The ID of the part
     * @return An object representing the metadata for a part.
     * @throws IOException
     */
    public Part GetPart(String id) throws IOException
    {
        //Get the parts in xml format
        String url = String.format("%s/part/%s/xml", this.server,id);
        String data = GetXml(url);

        //Deserialize the xml data into Parts object
        Serializer serializer = new Serializer();
        Part part = serializer.GetPart(data);

        return part;
    }

    /**
     * Gets the interactions of a given part
     * @param part The part for which the interactions are retrieved 
     * @return Interactions of the part
     * @throws IOException
     * @throws XMLStreamException
     */
    public Interactions GetInteractions(Part part) throws IOException, XMLStreamException
    {
        //Get the data in xml format
        String url = String.format("%s/part/%s/interactions/xml/jpart", this.server, part.getName());
        String data = GetXml(url);

        //Deserialize the xml data into Interactions object
        Serializer serializer = new Serializer();
        Interactions interactions = serializer.GetInteractions(data);

        return interactions;
    }

      /**
     * Gets the internal interactions of a given part. Such interactions are encapsulated as part of the models of parts
     * @param part The part for which the interactions are retrieved
     * @return Interactions of the part
     * @throws IOException
     * @throws XMLStreamException
     */
    public Interactions GetInternalInteractions(Part part) throws IOException, XMLStreamException
    {
        //Get the data in xml format
        String url = String.format("%s/part/%s/internalinteractions/xml", this.server, part.getName());
        String data = GetXml(url);

        //Deserialize the xml data into Interactions object
        Serializer serializer = new Serializer();
        Interactions interactions = serializer.GetInteractions(data);

        return interactions;
    }

    /**
     * Gets the SBML Model of a given part
     * @param part The part for which the SBML model is retrieved
     * @return The SBML model of the part
     * @throws IOException
     * @throws XMLStreamException
     */
    public SBMLDocument GetModel(Part part) throws IOException, XMLStreamException
    {
        String url = String.format("%s/part/%s/sbml/jpart", this.server, part.getName());
        return GetSBML(url);
    }

    /**
     * Gets the SBMLModel of the given interaction
     * @param interaction The interaction for which the SBML model is retrieved
     * @return The SBML model of the interaction.
     * @throws IOException
     * @throws XMLStreamException
     */
    public SBMLDocument GetInteractionModel(Interaction interaction) throws IOException, XMLStreamException
    {
        String url = String.format("%s/interaction/%s/sbml/jpart", this.server, interaction.getName());
        return GetSBML(url);
    }

    /**
     * Create an SBMLDocument using the given metadata about a part and its internal interactions which are encapsulated as part of the model.
     * @param part Part object to create the model for
     * @param internalInteractions List of interactions to build the part model
     * @return SBMLDocument object for the part
     * @throws Exception 
     */
    public SBMLDocument CreatePartModel(Part part, List<Interaction> internalInteractions) throws Exception
    {
        ModelFactory modelFactory=new ModelFactory();
        List<Part> interactionParts=GetInteractionParts(part, internalInteractions);
        List<MolecularFormType> molecularForms=MolecularFormProvider.GetMolecularForms(this.server);
        List<Formula> formulas=FormulaProvider.GetFormulas(this.server);
        ModelOutput modelOutput=modelFactory.GetModel(part, "sbml", internalInteractions, interactionParts,  formulas,molecularForms);
        SBMLHandler sbmlHandler=new SBMLHandler();
        SBMLDocument document=sbmlHandler.GetSBML(modelOutput.getMain());
        return document;
    }

    private List<Part> GetInteractionParts(Part part, List<Interaction> internalInteractions) throws IOException
    {
        List<Part> interactionParts=new ArrayList<Part>();
        interactionParts.add(part);
        HashSet<String> partNames=new HashSet<String>();
        partNames.add(part.getName());
        for (Interaction interaction:internalInteractions)
        {
            List<String> interactionPartNames=interaction.getParts();
            for (String partName:interactionPartNames)
            {
                if (!partNames.contains(partName))
                {
                    partNames.add(partName);
                    Part interactionPart=GetPart(partName);
                    interactionParts.add(interactionPart);
                }
            }
        }
        return  interactionParts;
    }

    private List<Part> GetInteractionParts(Interaction interaction) throws IOException
    {
        List<Part> interactionParts=new ArrayList<Part>();
        List<String> interactionPartNames=interaction.getParts();
        for (String partName:interactionPartNames)
        {
            Part interactionPart=GetPart(partName);
            interactionParts.add(interactionPart);               
        }
        return  interactionParts;
    }

    /**
     * Creates an SBML model for the given interaction
     * @param interaction The Interaction object to create the model for
     * @return SBLDoument representing the given interaction object
     * @throws Exception 
     */
    public SBMLDocument CreateInteractionModel(Interaction interaction) throws Exception
    {
        ModelFactory modelFactory=new ModelFactory();
        List<MolecularFormType> molecularForms=MolecularFormProvider.GetMolecularForms(this.server);
        List<Formula> formulas=FormulaProvider.GetFormulas(this.server);
        List<Part> interactionParts=GetInteractionParts(interaction);
        ModelOutput modelOutput=modelFactory.GetModel(interaction, "sbml", interactionParts, formulas, molecularForms);
        SBMLHandler sbmlHandler=new SBMLHandler();
        SBMLDocument document=sbmlHandler.GetSBML(modelOutput.getMain());
        return document;
    }

    /**
     * Creates an SBML model for the given interaction
     * @param interactionParts The Parts involved in the interaction
     * @param interaction The Interaction object to create the model for
     * @return SBMLDoument representing the given interaction object
     * @throws Exception
     */
    public SBMLDocument CreateInteractionModel(List<Part> interactionParts, Interaction interaction) throws Exception
    {
        // ========================================================
        // OG: Not sure if the CreateInteractionModel is used, so
        // have created a duplicate to fix the issue regarding
        // GetInteractionParts() searching for non-existent parts
        // ========================================================

        ModelFactory modelFactory=new ModelFactory();
        List<MolecularFormType> molecularForms=MolecularFormProvider.GetMolecularForms(this.server);
        List<Formula> formulas=FormulaProvider.GetFormulas(this.server);

        // ========================================================
        // Removed call to GetInteractionParts(), and instead use
        // provided List of Parts
        // ========================================================

        ModelOutput modelOutput=modelFactory.GetModel(interaction, "sbml", interactionParts, formulas, molecularForms);
        SBMLHandler sbmlHandler=new SBMLHandler();
        SBMLDocument document=sbmlHandler.GetSBML(modelOutput.getMain());
        return document;
    }

    /**
     * Updates the value of the VisualName property of the RDFDocument, which is included as annotation in the given SBML model
     * @param visualName The new VisualName property value
     * @param model SBMLDocument to be updated
     * @return SBMLDocument updated with the given VisualName value
     * @throws Exception 
     */
    public SBMLDocument UpdateVisualNameAnnotation(String visualName, SBMLDocument model) throws Exception
    {
        SBMLHandler sbmlHandler=new SBMLHandler();
        String sbmlOutput = sbmlHandler.GetSBML(model);
        String regExp="<mts:VisualName>(.*?)</mts:VisualName>";
        String updatedAnnotation="<mts:VisualName>" + visualName + "</mts:VisualName>";

        sbmlOutput=sbmlOutput.replaceAll(regExp,updatedAnnotation);
        model=sbmlHandler.GetSBML(sbmlOutput);
        return model;
    }

    private Parts GetPartsData(String URL)throws IOException
    {
        String data = GetXml(URL);
        //Deserialize the xml data into Parts object
        Serializer serializer = new Serializer();
        Parts parts = serializer.GetParts(data);

        return parts;
     }
      private Summary GetSummaryData(String URL)throws IOException
    {
        String data = GetXml(URL);
        //Deserialize the xml data into Parts object
        Serializer serializer = new Serializer();
        Summary summary = serializer.GetSummary(data);

        return summary;
     }

    /**
     * Get the data from the given URL
     * @param url
     * @return
     * @throws IOException
     */
    private String GetXml(String url) throws IOException
    {
        HttpHandler httpHandler = new HttpHandler();
        String data = httpHandler.Get(url);
        data = ProcessXmlData(data);       
        return data;
    }

    /**
     * Get the SBMLModel given with its URL
     * @param url
     * @return
     * @throws IOException
     * @throws XMLStreamException
     */
    private SBMLDocument GetSBML(String url) throws IOException, XMLStreamException
    {
        //Get the model in xml format
        HttpHandler httpHandler = new HttpHandler();

        String data = httpHandler.Get(url);
        if (data!=null && data.length()>0)
        {
	        data = ProcessXmlData(data);
	        //TODO This is a fix for now but need to be updated properly. There are some problems with the xml!
	        data=data.replace("xmlns:j.0=\"mts:\" ","");
	        if (this.debug)
	        {
	            System.out.println("The data from " + url + ":");
	            System.out.println(data);
	        }
	        SBMLHandler sbmlHandler = new SBMLHandler();
	        SBMLDocument sbmlDocument = sbmlHandler.GetSBML(data);

        	return sbmlDocument;
        }
        else
        {
        	return null;
        }
    }

    /**
     * REmoves any spaces before the XML start tag
     * @param xml
     * @return
     */
    private String ProcessXmlData(String xml)
    {
        int index = xml.indexOf("<?xml");
        if (index>-1)
        {
        	xml = xml.substring(index);
        }
        return xml;
    }

}


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package virtualparts.filter;

/**
 * AND filter which uses two IFilter objects to provide the boolean AND logic.
 * @author a8901379
 */
public class ANDFilter implements IFilter {

    private IFilter filter1;
    private IFilter filter2;
    /**
     * Contructor
     * @param filter1 The first IFilter object
     * @param filter2 The second IFilter object
     */
    public ANDFilter(IFilter filter1,IFilter filter2)
    {
        this.filter1=filter1;
        this.filter2=filter2;
    }

    
    /**
     * Overridden IFilter method
     */
    public String GetFilterString()
    {
        String result=String.format("(%s)_SPACE_AND_SPACE_(%s)",filter1.GetFilterString(),filter2.GetFilterString());
        return result;
    }
}

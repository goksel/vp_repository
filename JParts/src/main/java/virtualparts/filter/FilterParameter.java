/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package virtualparts.filter;

/**
 * The list of parameter types that can be used in filtering the parts
 * @author a8901379
 */
public enum FilterParameter {
    /**
     * The rate of transcription
     */
    TRANSCRIPTION_RATE,
    /**
     * The rate of translation
     */
    TRANSLATION_RATE;

    /**
     * 
     * @return The String representation of the enum constant
     */
    public String toString() {
        String result="";
        switch (this)
        {
            case TRANSCRIPTION_RATE:result="ktr";
                break;
            case TRANSLATION_RATE:result="ktl";
                break;
            default:break;
        }
        return result;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package virtualparts.filter;

/**
 * The list of filter operators
 * @author a8901379
 */
public enum FilterOperator {
    /**
     * Equal to operator
     */
    EQUAL_TO,
    /**
     * Less than operator
     */
    LESS_THAN,
    /**
     * Less than or equal to operator
     */
    LESS_THAN_OR_EQUAL_TO,
    /**
     * Greater than operator
     */
    GREATER_THAN,
    /**
     * Greater than or equal to operator
     */
    GREATER_THAN_OR_EQUAL_TO;

    /**
     * @return The String representation of the enum constant
     */
    public String toString() {
        String result="";
        switch (this)
        {
            case EQUAL_TO:result="EQ";//"==";
                break;
            case LESS_THAN:result="LT";//"<";
                break;
            case LESS_THAN_OR_EQUAL_TO:result="LTOE";//<=";
                break;
            case GREATER_THAN:result="GT";//>";
                break;
            case GREATER_THAN_OR_EQUAL_TO:result="GTOE";//">=";
                break;
            default:break;
        }
        return result;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package virtualparts.filter;

/**
 * Simple Filter class to create a restriction using parameter values
 * @author a8901379
 */
public class Filter implements IFilter {

    private FilterParameter parameter;
    private FilterOperator operator;
    private String value;

    /**
     * Constructor
     * @param parameter The type of the parameter to filter parts
     * @param operator The type of the filter operator
     * @param value Parameter value to filter parts
     */
    public Filter(FilterParameter parameter,FilterOperator operator, String value)
    {
        this.parameter=parameter;
        this.operator=operator;
        this.value=value;
    }

   

    
    /**
     * Overridden IFilter method
     */
    public String GetFilterString()
    {
        String result=String.format("parameterType=='%s'_SPACE_AND_SPACE_value%s%s",this.getParameter().toString(),this.getOperator().toString(), this.getValue());
        return result;
    }


    /**
     * Gets the type of the parameter
     * @return the parameter
     */
    public FilterParameter getParameter()
    {
        return parameter;
    }

    /**
     * Gets the type of the filter operator
     * @return the operator
     */
    public FilterOperator getOperator()
    {
        return operator;
    }

    /**
     * Gets the parameter value
     * @return the value
     */
    public String getValue()
    {
        return value;
    }
}

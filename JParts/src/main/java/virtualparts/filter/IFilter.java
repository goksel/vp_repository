/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package virtualparts.filter;

/**
 * In interface to define classes that can be used to restrict the list of retrieved from the repository
 * @author a8901379
 */
public interface IFilter {
    /**
     * Returns the filter provided by the IFilter object
     * @return
     */
    String GetFilterString();
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package virtualparts;

import java.io.IOException;
import java.util.List;
import virtualparts.entity.MolecularFormType;
import virtualparts.http.HttpHandler;
import virtualparts.serialize.Serializer;

/**
 * Internal singleton class to retrieve the list of molecular forms from the parts repository
 * @author a8901379
 */
class MolecularFormProvider {
    private static List<MolecularFormType> molecularForms;

    private MolecularFormProvider() {
		
    }

    public static List<MolecularFormType> GetMolecularForms(String server) throws IOException {
		if (molecularForms == null) {
			molecularForms = FetchMolecularForms(server);
		}
		return molecularForms;
	}

    private static synchronized List<MolecularFormType> FetchMolecularForms(String server) throws IOException
    {
         //Get the parts in xml format
        String url = String.format("%s/molecularforms/xml", server);
        HttpHandler httpHandler = new HttpHandler();
        String data = httpHandler.Get(url);

        //Deserialize the xml data into Parts object
        Serializer serializer = new Serializer();
        List<MolecularFormType> molecularFormList = serializer.GetMolecularFormTypes(data);
        return molecularFormList;
    }
}

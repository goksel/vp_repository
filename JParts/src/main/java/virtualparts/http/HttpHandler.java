/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package virtualparts.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Provides methods to retrieve data from a BacilloBricks repository given with an HTTP URL.
 * @author a8901379
 */
public class HttpHandler {

    /**
     * Gets the String data from given URI
     * @param URI The URI to access to the repository
     * @return String
     * @throws IOException
     */
public String Get(String URI) throws IOException
    {
      //URI=URLEncoder.encode(URI, "UTF-8");
     URI=URI.replace(" ", "%20");
      URI=URI.replace(">", "%3E");
      URL url=new URL(URI);
      int tried=0;
      InputStream stream=null;
      while (tried<5)
      {
        try
        {
          stream =url.openStream();
          tried=5;
        }
        catch (IOException exception)
        {
            tried++;  
            System.out.print("Could not access to the repository after attempt " + tried + ". ");   
            try
            {
            Thread.sleep(10000);//Sleep for 10 seconds
            }
            catch(Exception threadexception)
            {
                
            }
            if (tried==5)
            {
                tried=0;
                throw exception;
            }
            else
            {
                 System.out.println("Connection will be tried in 10 seconds");   
            }
        }
      }
      String output=ConvertStreamToString(stream);
      return output;
    }

    private String ConvertStreamToString(InputStream stream) throws IOException {
        if (stream != null) {
            Writer writer = new StringWriter();
            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            } finally {
                stream.close();
            }
            return writer.toString();

        } else {
            return "";
        }
    }

}

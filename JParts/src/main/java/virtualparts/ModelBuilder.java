/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package virtualparts;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.SimpleSelector;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import org.sbml.jsbml.ASTNode;
import org.sbml.jsbml.Annotation;
import org.sbml.jsbml.AssignmentRule;
import org.sbml.jsbml.ListOf;
import org.sbml.jsbml.ModifierSpeciesReference;
import org.sbml.jsbml.Parameter;
import org.sbml.jsbml.Reaction;
import org.sbml.jsbml.Rule;
import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.Species;
import virtualparts.SBML.SBMLHandler;
import virtualparts.entity.Interaction;
import virtualparts.entity.Part;

/**
 * Provides methods to add models of parts into a given SBML model and to connect these individual models in order to create simulateable models of biological systems.
 * @author a8901379
 */
public class ModelBuilder {
  
    //private String server;
    private SBMLDocument sbmlContainer;
    private HashSet<String> processedModelIDs=new HashSet<String>();
    /*public ModelBuilder(String server, String modelName)
    {
        this.server = server;
        SBMLHandler sbmlHandler = new SBMLHandler();
        sbmlContainer = sbmlHandler.GetSBMLTemplateModel(modelName);       
    }*/
    /**
     * Contructor
     * @param sbmlContainer An SBML container model to add the models of parts into
     */
    public ModelBuilder(SBMLDocument sbmlContainer)
    {
        this.sbmlContainer = sbmlContainer;
    }
/*    public void Add (Vpart part) throws Exception
    {
        PartsHandler partsHandler=new PartsHandler(server);
        SBMLDocument model=partsHandler.GetModel(part);
        AddModel(model);
    }
     public void Add (Interaction interaction) throws Exception
    {
        PartsHandler partsHandler=new PartsHandler(server);
        SBMLDocument model=partsHandler.GetInteractionModel(interaction);
        AddModel(model);
    }*/

    /**
     * Adds a given SBML model to the container model given in the constructor of ModelBuilder.
     * @param model
     * @throws Exception
     */
    public void Add (SBMLDocument model) throws Exception
    {
        /*String ID=model.getModel().getId();
        if (processedModelIDs.contains(ID))
        {
            throw new Exception ("This model has already been used. To continu")
        }*/
        AddModel(model);
    }

    /**
     * Gets the SBML model of the final constructed model
     * @return String representation of the constructed model
     * @throws Exception
     */
    public String GetModelString() throws Exception
    {
        SBMLHandler sbmlHandler=new SBMLHandler();
        String sbmlOutput = sbmlHandler.GetSBML(sbmlContainer);
        return sbmlOutput;
    }

    /**
     * Gets the SBML model of the final constructed model
     * @return SBML model of the constructed model
     * @throws Exception
     */
     public SBMLDocument GetModel() throws Exception
    {
        return sbmlContainer;
    }

   /* public SBMLDocument GetmRNAModel(SBMLDocument mRNADocument, String newName) throws Exception
    {
        SBMLHandler sbmlHandler=new SBMLHandler();
        String sbmlOutput=sbmlHandler.GetSBML(mRNADocument);
        sbmlOutput=sbmlOutput.replaceAll("mRNA", newName);
        mRNADocument =sbmlHandler.GetSBML(sbmlOutput);
        return mRNADocument;
    }*/

    /**
     * Clones an SBML document of a part in order to use it more than once in creating simulateable models.
     * @param document SBML model of a biological part
     * @param part Vpart object including metadata about the part
     * @return Cloned SBML document containing different entity IDs
     * @throws Exception
     */
    public SBMLDocument Clone(SBMLDocument document,Part part)throws Exception
    {
        Random randomGenerator = new Random();
        String randomKey=String.valueOf(randomGenerator.nextLong()).replace("-", "");
        String newID=part.getName() + randomKey;

        SBMLHandler sbmlHandler=new SBMLHandler();
        String sbmlOutput=sbmlHandler.GetSBML(document);
        if (part.getType().equals(Part.FUNCTIONAL_PART))
        {
            //Updated the RiPs input of the FunctionalPart SVP
            String oldRiPSInputParameter=part.getName().replace("_", "") + "ProductionRiPSInput";
            String newRiPSInputParameter=newID.replace("_", "") + "ProductionRiPSInput";
            sbmlOutput=sbmlOutput.replaceAll(oldRiPSInputParameter, newRiPSInputParameter);

            //Update the Protein production reaction since this reaction needs to be linked to a specific instance of an RBS and represents the CDS
            String oldProductionReaction=part.getName() + "_Production";
            String newProductionReaction=newID + "_Production";
            sbmlOutput=sbmlOutput.replaceAll(oldProductionReaction, newProductionReaction);
        }
        else//Promoter, TBS, Operator, Shim
        {
            sbmlOutput=sbmlOutput.replaceAll(part.getName(), newID);
            if (part.getName().contains("_"))
            {
                sbmlOutput=sbmlOutput.replaceAll(part.getName().replace("_", ""), newID.replace("_", ""));
            }
        }
        document=sbmlHandler.GetSBML(sbmlOutput);
        return document;
    }

    /**
     * Clones an SBML document of an interaction in order to use it more than once in creating simulateable models.
     * @param document SBML model of an interaction
     * @param interaction Interaction object including metadata about the interaction
     * @return Cloned SBML document containing different entity IDs 
     * @throws Exception
     */
     public SBMLDocument CloneInteraction(SBMLDocument document,Interaction interaction)throws Exception
    {
        if (IsClonableInteraction(interaction))
        {
            Random randomGenerator = new Random();
            String randomKey=String.valueOf(randomGenerator.nextLong()).replace("-", "");
            String newID=interaction.getName() + randomKey;
            SBMLHandler sbmlHandler=new SBMLHandler();
            String sbmlOutput=sbmlHandler.GetSBML(document);
            sbmlOutput=sbmlOutput.replaceAll(interaction.getName(), newID);
            if (interaction.getName().contains("_"))
            {
                sbmlOutput=sbmlOutput.replaceAll(interaction.getName().replace("_", ""), newID.replace("_", ""));
            }
            document =sbmlHandler.GetSBML(sbmlOutput);
        }
        return document;
    }

    private boolean IsClonableInteraction(Interaction interaction)
    {
        if (interaction.getInteractionType().equals(Interaction.TRANSCRIPTIONAL_ACTICATION)
                || interaction.getInteractionType().equals(Interaction.TRANSCRIPTIONAL_REPRESSION)
                || interaction.getInteractionType().equals(Interaction.TRANSCRIPTIONAL_ACTIVATION_OPERATOR)
                || interaction.getInteractionType().equals(Interaction.TRANSCRIPTIONAL_REPRESSION_OPERATOR))
        {
            return true;
        } else
        {
            return false;
        }
    }

     /**
      * Connects two given models, of parts or interactions, using their inputs and outputs. Examples of model types to connect are promoter-mRNA, mRNA-RBS, RBS-CDS, promoter-interaction, interaction-mRNA
      * @param from The SBML model whose output is connected to the input of another model
      * @param to The SBML model whose input is connected to the output from another model
      * @throws Exception
      */
    public void Link (SBMLDocument from, SBMLDocument to) throws Exception
    {
        String annotationStringFrom=from.getModel().getAnnotation().getNonRDFannotation();
        String annotationStringTo=to.getModel().getAnnotation().getNonRDFannotation();

        ArrayList<String> outputs=GetValuesByProperty(annotationStringFrom,"http://purl.org/modeltosequence/1.0#Output");
        ArrayList<String> inputs=GetValuesByProperty(annotationStringTo,"http://purl.org/modeltosequence/1.0#Input");

        //Pairs of signal type - modelling entity values
        HashMap<String,String> outputTypes=GetSignalEntityPairs(from, outputs);
        HashMap<String,String> inputTypes=GetSignalEntityPairs(to, inputs);
        for (Map.Entry<String,String> signal: outputTypes.entrySet())
        {
            String signalType=signal.getKey();
            String fromEntity=signal.getValue();
            String toEntity=inputTypes.get(signalType);
            AddLink(fromEntity,toEntity,signalType,sbmlContainer);
        }

        /*annotationString="<ModelToSequence xmlns=\"http://purl.org/modeltosequence/1.0#\">" +
"<rdf:RDF xmlns:mts=\"http://purl.org/modeltosequence/1.0#\" xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">" +
                "<rdf:Description rdf:about=\"#PspaRK\">" +  
                "<mts:Output>" +  
                "PspaRKPoPSProductionPoPSOutput" +  
                "</mts:Output>" +  
                "</rdf:Description>" +  
                "</rdf:RDF>"  +
                "</ModelToSequence> " ;*/
        /*InputStream stream=new ByteArrayInputStream( annotationString.getBytes() );
        Document XMLdocument=DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(stream);
        String rdf=XMLdocument.getFirstChild().getNodeValue();*/
    }

    //TODO This code is temporary, need to be constructed using Jena RDF
    private String GetLinkAnnotation(String signalType,String metadaId)
    {
        String linkAnnotation="<ModelToSequence xmlns=\"http://purl.org/modeltosequence/1.0#\">" +
        "<rdf:RDF xmlns:mts=\"http://purl.org/modeltosequence/1.0#\" xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">" +
                "<rdf:Description rdf:about=\"#%s\">" +
                "<rdf:type rdf:resource=\"mts:Signal_Carrier\" />" +
                "<mts:SignalType>%s</mts:SignalType>" +
                "<mts:VisualName>%s_link</mts:VisualName>" +
                "</rdf:Description>" +
                "</rdf:RDF>"  +
                "</ModelToSequence> " ;
        String annotation=String.format(linkAnnotation, metadaId,signalType,signalType);
        return annotation;
    }
    /* 20140318
     * private void AddLink (String from, String to, String signalType, SBMLDocument sbmlDocument) throws Exception
    {
          //Create the PoPS input assignment rule.  Link the PoPs output from
        ASTNode math = ASTNode.parseFormula(String.format("1*%s",from));//Get the PoPS output's name dynamically
        SBMLHandler sbmlHelper=new SBMLHandler();
        AssignmentRule rule=sbmlHelper.CreateAssignmentRule(sbmlDocument.getModel(), to, math);
        rule.setMetaId(from + to + "_meta");
        String annotation=GetLinkAnnotation(signalType,rule.getMetaId());
        rule.setAnnotation(new Annotation(annotation));
    }*/
    
    private void AddLink (String from, String to, String signalType, SBMLDocument sbmlDocument) throws Exception
    {
       SBMLHandler sbmlHelper=new SBMLHandler();
        AssignmentRule rule=sbmlHelper.GetAssignmentRule(sbmlDocument, to);
        if (rule==null)
        {
            //Create the PoPS input assignment rule.  Link the PoPs output from
            ASTNode math = ASTNode.parseFormula(String.format("1*%s",from));//Get the PoPS output's name dynamically         
        	rule=sbmlHelper.CreateAssignmentRule(sbmlDocument.getModel(), to, math);
        	rule.setMetaId(from + to + "_meta");
        	String annotation=GetLinkAnnotation(signalType,rule.getMetaId());
        	rule.setAnnotation(new Annotation(annotation));
        }
        else
        {
        	//Sum the linked inputs
        	String newFormula=String.format("%s+%s",rule.getMath().toFormula(),from);
        	ASTNode math = ASTNode.parseFormula(newFormula);//Get the PoPS output's name dynamically
        	rule.setMath(math);
        	AssignmentRule rule2=sbmlHelper.GetAssignmentRule(sbmlDocument, to);
        	System.out.print(rule2.getMath().toFormula()); 
        }
    }
    private HashMap<String,String> GetSignalEntityPairs(SBMLDocument sbmlDocument, ArrayList<String> modellingEntityNames) throws Exception
    {
        HashMap<String,String> signalTypes=new HashMap<String, String>();
        for (String entityName:modellingEntityNames)
        {
            ArrayList<String> signalType=new ArrayList<String>();
            ListOf<Rule> rules=sbmlDocument.getModel().getListOfRules();
            for (Rule rule:rules)
            {
                if (rule.isAssignment())
                {
                    AssignmentRule aRule=(AssignmentRule)rule;
                    if (aRule.getVariable().equals(entityName))
                    {
                      signalType=GetValuesByProperty(aRule.getAnnotation().getNonRDFannotation(),"http://purl.org/modeltosequence/1.0#SignalType");
                    }
                }
            }
            if (signalType.size()==0)
            {
                ListOf<Parameter> parameters=sbmlDocument.getModel().getListOfParameters();
                for (Parameter parameter:parameters)
                {
                    if (parameter.getName().equals(entityName))
                    {
                         signalType=GetValuesByProperty(parameter.getAnnotation().getNonRDFannotation(),"http://purl.org/modeltosequence/1.0#SignalType");
                    }
                }
            }
            if (signalType.size()==0)
            {
                ListOf<Species> speciesList=sbmlDocument.getModel().getListOfSpecies();
                for (Species species:speciesList)
                {
                    if (species.getName().equals(entityName))
                    {
                         signalType=GetValuesByProperty(species.getAnnotation().getNonRDFannotation(),"http://purl.org/modeltosequence/1.0#SignalType");
                    }
                }
            }
            if (signalType.size()==0)
            {
                throw new Exception ("Could not find output annotation for " + entityName);
            }
            signalTypes.put(signalType.get(0),entityName);
        }
        return signalTypes;
    }
    private ArrayList<String> GetValuesByProperty(String annotationString,String propertyURI)
    {
        ArrayList<String> outputs=new ArrayList<String>();

        int begin=annotationString.indexOf("<rdf:RDF");
        int end=annotationString.indexOf("</rdf:RDF>");
        if (begin>0 && end>0)
        {
            String rdfData=annotationString.substring(begin, end +"</rdf:RDF>".length());

            Model model = ModelFactory.createDefaultModel();
            InputStream stream=new ByteArrayInputStream( rdfData.getBytes());
            model.read(stream,"http://purl.org/modeltosequence/1.0");
            Property outputProperty=model.getProperty(propertyURI);
            StmtIterator stmtIterator=model.listStatements(new SimpleSelector(null, outputProperty, (RDFNode) null));
            while (stmtIterator.hasNext())
            {
                Statement stmt=stmtIterator.nextStatement();
                String output=stmt.getObject().toString();
                output=output.replaceAll(System.getProperty("line.separator"),"").trim();
                outputs.add(output);
            }
        }
        return outputs;
    }


    private void AddModel(SBMLDocument model)
    {
        ListOf<Parameter> parameters=model.getModel().getListOfParameters();
        for(Parameter parameter:parameters)
        {
            if (!IsGlobal(parameter))
            {
            parameter.setId(parameter.getName()) ;
            sbmlContainer.getModel().addParameter(parameter);
            }
        }
        ListOf<Rule> rules=model.getModel().getListOfRules();
        for(Rule rule:rules)
        {
            
                sbmlContainer.getModel().addRule(rule);
        }

        SBMLHandler sbmlHandler=new SBMLHandler();
        ListOf<Reaction> reactions=model.getModel().getListOfReactions();
        for(Reaction reaction:reactions)
        {
            if (!sbmlHandler.ReactionExists(sbmlContainer, reaction.getName()))
            {
                Reaction newReaction=reaction.clone();
                if (reaction.getId().isEmpty())
                {
                    newReaction.setId(reaction.getName());
                }
                if (reaction.getMetaId().isEmpty())
                {
                   newReaction.setMetaId(reaction.getName() + "_meta");
                }
                //newReaction.setName(reaction.getName());
                //newReaction.setMetaId(reaction.getName() + "_meta");

                /*ListOf<ModifierSpeciesReference> modifiers= reaction.getListOfModifiers();
                for (ModifierSpeciesReference speciesRef: modifiers)
                {
                    newReaction.addModifier(speciesRef);
                }
                ListOf<SpeciesReference> products= reaction.getListOfProducts();
                for (SpeciesReference speciesRef: products)
                {
                    newReaction.addProduct(speciesRef);
                }
                ListOf<SpeciesReference> reactants= reaction.getListOfReactants();
                for (SpeciesReference speciesRef: reactants)
                {
                    newReaction.addReactant(speciesRef);
                }*/
                //newReaction.setKineticLaw(reaction.getKineticLaw());
                newReaction.setAnnotation(reaction.getAnnotation());


                //newReaction.
                //reaction.setId(reaction.getName());
                sbmlContainer.getModel().addReaction(newReaction);
            }
        }

        ListOf<Species> speciesList=model.getModel().getListOfSpecies();
        for(Species species:speciesList)
        {
            if (!sbmlHandler.SpeciesExists(sbmlContainer, species.getName()))
            {
                Species newSpecies=species.clone();
                newSpecies.setId(species.getName());
                newSpecies.setMetaId(species.getName() + "_meta");
                newSpecies.setAnnotation(species.getAnnotation());
                sbmlContainer.getModel().addSpecies(newSpecies);
            }
        }
    }

   

    private boolean IsGlobal(Parameter parameter)
    {
        boolean isGlobal=false;
        ArrayList<String> values=GetValuesByProperty(parameter.getAnnotationString(),"http://purl.org/modeltosequence/1.0#IsGlobal");
        if (values.size()>0 && values.get(0).toLowerCase().equals("true"))
        {
            isGlobal=true;
        }
        return isGlobal;
    }

}

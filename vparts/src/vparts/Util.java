package vparts;


public class Util {
	public static String NEWLINE=System.getProperty("line.separator");
	  
	public static Boolean IsEmpty(String value)
	{
		return (value==null || value.length()==0);		
	}
	
	public static boolean IsNumeric(String value)
	{
		try
		{
			Double d=Double.parseDouble(value);
			return true;
		}
		catch(NumberFormatException e)
		{
			return false;
		}				
	}
	
	public static String Join(Iterable<?> elements, String delimiter) {   
		  StringBuilder sb = new StringBuilder();   
		  for (Object e : elements) {   
		    if (sb.length() > 0)   
		      sb.append(delimiter);    
		    sb.append(e);   
		  }   
		  return sb.toString();   
		} 
	
	public static int GetPageCount(int totalNumber, int pageSize)
	{
		int pageCount=0;
		if (totalNumber % pageSize==0)
		{
			pageCount=totalNumber/pageSize;
		}
		else
		{
			pageCount=totalNumber/pageSize +1;			
		}
		return pageCount;
	}

}

package vparts.dataimport;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


//import virtualparts.entity.Parameter;
import virtualparts.entity.Part;
import virtualparts.entity.Property;


public class PartReader extends DefaultHandler {
	// private static final Logger log =
	// Logger.getLogger(EmployeeHandler.class.getName());

	private Stack<Object> partStack;
	private ArrayList<Part> parts;
	private String characters;
	private StringBuffer characterBuffer=new StringBuffer();
	

/*	public PartReader(String filePath) throws Exception {

		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {

			SAXParser saxParser = factory.newSAXParser();
			File file=new File(filePath);
			saxParser.parse(file,this);
		} catch (Exception e) {
			throw e;
		} finally {
		}
	}*/
	public PartReader(String content,String type) throws Exception {

		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {

			SAXParser saxParser = factory.newSAXParser();
			InputStream inputStream= ConvertToStream(content);
			Reader reader = new InputStreamReader(inputStream,"UTF-8");			 
			InputSource is = new InputSource(reader);
			is.setEncoding("UTF-8");
	
			saxParser.parse(is,this);
		} catch (Exception e) {
			throw e;
		} finally {
		}
	}
	
	public PartReader(InputStream stream) throws Exception{

		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {

			SAXParser saxParser = factory.newSAXParser();
			saxParser.parse(stream,this);

		}  catch (Exception e) {
			throw e;
		} finally {
		}
	}
	
	public InputStream ConvertToStream(String str)throws Exception
	{
		byte[] bytes = str.getBytes("UTF-8");
		return new ByteArrayInputStream(bytes);
	}


	
	public PartReader(String filePath) throws Exception {

		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {

			SAXParser saxParser = factory.newSAXParser();
			File file=new File(filePath);
			InputStream inputStream= new FileInputStream(file);
			Reader reader = new InputStreamReader(inputStream,"UTF-8");
			 
			InputSource is = new InputSource(reader);
			is.setEncoding("UTF-8");
	
			saxParser.parse(is,this);
		} catch (Exception e) {
			throw e;
		} finally {
		}
	}
	
	public void startDocument() throws SAXException {

		partStack = new Stack<Object>();
		parts = new ArrayList<Part>();

	}

	public void startElement(String namespaceURI, String localName,
			String qualifiedName, Attributes attributes) throws SAXException {
		
		characterBuffer.setLength(0);
		
		if (qualifiedName.equals("Part")) {

			Part part = new Part();
			partStack.push(part);
		}
		/* GMGM 20130109
		else if (qualifiedName.equals("Parameter")) {

			Parameter parameter = new Parameter();
			partStack.push(parameter);
		}
		else if (qualifiedName.equals("MolecularForm")) {

			MolecularForm molecularForm = new MolecularForm();
			partStack.push(molecularForm);
		}*/
		else if (qualifiedName.equals("Property")) {

			Property role = new Property();
			partStack.push(role);
		}
	}

	public void endElement(String namespaceURI, String simpleName,
			String qualifiedName) throws SAXException {
		characterBuffer.setLength(0);
		if (!partStack.isEmpty()) {

			if (qualifiedName.equals("Part")) {

				parts.add((Part)partStack.pop());

			}			
			/* GMGM 20130109 
			else if (qualifiedName.equals("Parameter")) {			 
				Parameter parameter=(Parameter) partStack.pop();
				Part part=(Part)partStack.pop();
				part.AddParameter(parameter);
				partStack.push(part);
			}*/
			else if (qualifiedName.equals("Property")) {
				Property role=(Property)partStack.pop();
				Part part=(Part)partStack.pop();				
				part.AddProperty(role);
				partStack.push(part);
			}
			/* GMGM 20120109 
			else if (qualifiedName.equals("MolecularForm")) {
				MolecularForm molecularForm=(MolecularForm) partStack.pop();
				Part part=(Part)partStack.pop();
				part.AddMolecularForm(molecularForm);
				partStack.push(part);
			}*/
			//Part and Parameter property
			else if (qualifiedName.equals("Name")) {
				Object partObject=partStack.pop();
				if (partObject instanceof Part)
				{
					Part part = (Part)partObject;
					part.setName(characters);
					partStack.push(part);
				}
				/* GMGM 20130109
				else if (partObject instanceof Parameter)
				{
					Parameter parameter = (Parameter)partObject;
					parameter.setName(characters);
					partStack.push(parameter);
				}*/
				else if (partObject instanceof Property)
				{
					Property role = (Property)partObject;
					role.setName(characters);
					partStack.push(role);
				}
			}
			//Part
			 else if (qualifiedName.equals("Type")) {

					Part part = (Part)partStack.pop();
					part.setType(characters);
					partStack.push(part);
				}
			//Part
			 else if (qualifiedName.equals("Description")) {
				 	Object partObject=partStack.pop();
					if (partObject instanceof Part)
					{
						Part part = (Part)partObject;
						part.setDescription(characters);
						partStack.push(part);
					}					
					else if (partObject instanceof Property)
					{
						Property role = (Property)partObject;
						role.setDescription(characters);
						partStack.push(role);
					}					
				}
			//Part
			 else if (qualifiedName.equals("MetaType")) {

					Part part = (Part)partStack.pop();
					part.setMetaType(characters);
					partStack.push(part);
				}
			//Part
			 else if (qualifiedName.equals("Sequence")) {

					Part part = (Part)partStack.pop();
					part.setSequence(characters);
					partStack.push(part);
				}
			//Part
			 else if (qualifiedName.equals("SequenceURI")) {
					Part part = (Part)partStack.pop();
					part.setSequenceURI(characters);
					partStack.push(part);
				}
			//Part
			 else if (qualifiedName.equals("DisplayName")) {
					Part part = (Part)partStack.pop();
					part.setDisplayName(characters);
					partStack.push(part);
				}
			//Part
			 else if (qualifiedName.equals("Organism")) {
					Part part = (Part)partStack.pop();
					part.setOrganism(characters);
					partStack.push(part);
				}
			//Part
			 else if (qualifiedName.equals("Status")) {
					Part part = (Part)partStack.pop();
					part.setStatus(characters);
					partStack.push(part);
				}
			//Part
			 else if (qualifiedName.equals("DesignMethod")) {
					Part part = (Part)partStack.pop();
					part.setDesignMethod(characters);
					partStack.push(part);
				}
			/* GMGM 20130109
			//Parameter
			 else if (qualifiedName.equals("ParameterType")) {
				Parameter parameter = (Parameter)partStack.pop();
				parameter.setParameterType(characters);
				partStack.push(parameter);
			}
			 else if (qualifiedName.equals("EvidenceType")) {

				Parameter parameter = (Parameter)partStack.pop();
				parameter.setEvidenceType(characters);
				partStack.push(parameter);
			}*/
			//Parameter, Role
			 else if (qualifiedName.equals("Value")) {
				 Object partObject=partStack.pop();
				/* GMGM 20130109
				 if (partObject instanceof Parameter)
				 {
					 Parameter parameter = (Parameter)partObject;
					 parameter.setValue(Double.parseDouble(characters));
					 partStack.push(parameter);
				 }
				 else */if (partObject instanceof Property)
				 {
					 Property role = (Property)partObject;
					role.setValue(characters);
					partStack.push(role);
				 }
			}
			//Parameter
			/* GMGM 20120109 
			 else if (qualifiedName.equals("MolecularFormName")) {
				 Parameter parameter = (Parameter)partStack.pop();
				 parameter.setMolecularForm(characters);
				 partStack.push(parameter);
				}
			//MolecularForm
			 else if (qualifiedName.equals("FormName")) {
				 MolecularForm form = (MolecularForm)partStack.pop();
				 form.setName(characters);
				 partStack.push(form);
			 }*/							 			
		}		
	}

	public void characters(char buf[], int offset, int len) throws SAXException {

		//characters = new String(buf, offset, len);
		this.characterBuffer.append(buf,offset,len);
		this.characters = this.characterBuffer.toString().trim();
	}
	
	public List<Part> GetParts()
	{
		return parts;
	}

}

package vparts.dataimport;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import vparts.entity.VP_MolecularFormType;

public class MolecularTypeReader extends DefaultHandler {
	// private static final Logger log =
	// Logger.getLogger(EmployeeHandler.class.getName());

	private Stack<VP_MolecularFormType> typeStack;
	private ArrayList<VP_MolecularFormType> types;
	private String characters;
	private StringBuffer characterBuffer=new StringBuffer();


	public MolecularTypeReader(String filePath) throws Exception {

		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {
			SAXParser saxParser = factory.newSAXParser();
			File file=new File(filePath);
			saxParser.parse(file,this);
		}  catch (Exception e) {
			throw e;
		} finally {
		}
	}
	
	public MolecularTypeReader(InputStream stream) throws Exception{

		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {

			SAXParser saxParser = factory.newSAXParser();
			saxParser.parse(stream,this);

		}  catch (Exception e) {
			throw e;
		} finally {
		}
	}
	

	public void startDocument() throws SAXException {

		typeStack = new Stack<VP_MolecularFormType>();
		types = new ArrayList<VP_MolecularFormType>();

	}

	public void startElement(String namespaceURI, String localName,
			String qualifiedName, Attributes attributes) throws SAXException {

		characterBuffer.setLength(0);

		if (qualifiedName.equals("MolecularFormType")) {

			VP_MolecularFormType type = new VP_MolecularFormType();
			// role.setId(Long.parseLong(attributes.getValue("id")));
			typeStack.push(type);

		}
	}

	public void endElement(String namespaceURI, String simpleName,
			String qualifiedName) throws SAXException {

		characterBuffer.setLength(0);

		if (!typeStack.isEmpty()) {

			if (qualifiedName.equals("MolecularFormType")) {

				types.add(typeStack.pop());

			} else if (qualifiedName.equals("Name")) {

				VP_MolecularFormType type = typeStack.pop();
				type.setName(characters);
				typeStack.push(type);

			} else if (qualifiedName.equals("Accession")) {

				VP_MolecularFormType type = typeStack.pop();
				type.setAccession ( characters);
				typeStack.push(type);
			}
			else if (qualifiedName.equals("Extension")) {

				VP_MolecularFormType type = typeStack.pop();
				type.setExtension(characters);
				typeStack.push(type);
			}
		}

	}

	public void characters(char buf[], int offset, int len) throws SAXException {

		//characters = new String(buf, offset, len);
		this.characterBuffer.append(buf,offset,len);
		this.characters = this.characterBuffer.toString().trim();
	}
	
	public List<VP_MolecularFormType> GetMolecularFormTypes()
	{
		return types;
	}

}

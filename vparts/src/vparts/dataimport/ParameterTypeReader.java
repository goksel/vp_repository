package vparts.dataimport;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import vparts.entity.VP_ParameterType;

public class ParameterTypeReader extends DefaultHandler {
	// private static final Logger log =
	// Logger.getLogger(EmployeeHandler.class.getName());

	private Stack<VP_ParameterType> parameterTypeStack;
	private ArrayList<VP_ParameterType> parameterTypes;
	private String characters;
	private StringBuffer characterBuffer=new StringBuffer();


	public ParameterTypeReader(String filePath) throws Exception{

		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {

			SAXParser saxParser = factory.newSAXParser();
			File file=new File(filePath);
			saxParser.parse(file,this);

		}  catch (Exception e) {
			throw e;
		} finally {
		}
	}
	
	public ParameterTypeReader(InputStream stream) throws Exception{

		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {

			SAXParser saxParser = factory.newSAXParser();
			saxParser.parse(stream,this);

		}  catch (Exception e) {
			throw e;
		} finally {
		}
	}
	
	

	public void startDocument() throws SAXException {

		parameterTypeStack = new Stack<VP_ParameterType>();
		parameterTypes = new ArrayList<VP_ParameterType>();

	}

	public void startElement(String namespaceURI, String localName,
			String qualifiedName, Attributes attributes) throws SAXException {

		characterBuffer.setLength(0);
		
		if (qualifiedName.equals("ParameterType")) {

			VP_ParameterType parameterType = new VP_ParameterType();
			// role.setId(Long.parseLong(attributes.getValue("id")));
			parameterTypeStack.push(parameterType);

		}
	}

	public void endElement(String namespaceURI, String simpleName,
			String qualifiedName) throws SAXException {

		characterBuffer.setLength(0);

		if (!parameterTypeStack.isEmpty()) {

			if (qualifiedName.equals("ParameterType")) {

				parameterTypes.add(parameterTypeStack.pop());

			} else if (qualifiedName.equals("Name")) {

				VP_ParameterType type = parameterTypeStack.pop();
				type.Name = characters;
				parameterTypeStack.push(type);

			} else if (qualifiedName.equals("Accession")) {

				VP_ParameterType type = parameterTypeStack.pop();
				type.Accession = characters;
				parameterTypeStack.push(type);
			}
			else if (qualifiedName.equals("Description")) {

				VP_ParameterType type = parameterTypeStack.pop();
				type.Description = characters;
				parameterTypeStack.push(type);
			}
			else if (qualifiedName.equals("ModelName")) {

				VP_ParameterType type = parameterTypeStack.pop();
				type.ModelName = characters;
				parameterTypeStack.push(type);
			}
		} 		

	}

	public void characters(char buf[], int offset, int len) throws SAXException {

		//characters = new String(buf, offset, len);
		this.characterBuffer.append(buf,offset,len);
		this.characters = this.characterBuffer.toString().trim();
	}
	
	public List<VP_ParameterType> GetParameterTypes()
	{
		return parameterTypes;
	}

}

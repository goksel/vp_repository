package vparts.dataimport;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import vparts.entity.VP_Role;

public class PropertyReader extends DefaultHandler {
	// private static final Logger log =
	// Logger.getLogger(EmployeeHandler.class.getName());

	private Stack<VP_Role> roleStack;
	private ArrayList<VP_Role> roles;
	private String characters;
	private StringBuffer characterBuffer=new StringBuffer();


	public PropertyReader(String filePath) throws Exception {

		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {

			SAXParser saxParser = factory.newSAXParser();
			File file=new File(filePath);
			saxParser.parse(file,this);			
		} catch (Exception e) {
			throw e;
		} finally {
		}
	}

	public void startDocument() throws SAXException {

		roleStack = new Stack<VP_Role>();
		roles = new ArrayList<VP_Role>();

	}

	public void startElement(String namespaceURI, String localName,
			String qualifiedName, Attributes attributes) throws SAXException {

		characterBuffer.setLength(0);

		if (qualifiedName.equals("Role")) {

			VP_Role role = new VP_Role();
			// role.setId(Long.parseLong(attributes.getValue("id")));
			roleStack.push(role);

		}
	}

	public void endElement(String namespaceURI, String simpleName,
			String qualifiedName) throws SAXException {

		characterBuffer.setLength(0);

		if (!roleStack.isEmpty()) {

			if (qualifiedName.equals("Role")) {

				roles.add(roleStack.pop());

			} else if (qualifiedName.equals("Name")) {

				VP_Role role = roleStack.pop();
				role.setName(characters);
				roleStack.push(role);

			} else if (qualifiedName.equals("Accession")) {

				VP_Role role = roleStack.pop();
				role.setAccession(characters);
				roleStack.push(role);
			}
		}

	}

	public void characters(char buf[], int offset, int len) throws SAXException {

		//characters = new String(buf, offset, len);
		this.characterBuffer.append(buf,offset,len);
		this.characters = this.characterBuffer.toString().trim();
	}
	
	public List<VP_Role> GetRoles()
	{
		return roles;
	}

}

package vparts.dataimport;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import vparts.Util;
import virtualparts.entity.Constraint;
import virtualparts.entity.Interaction;
import virtualparts.entity.InteractionPartDetail;
import virtualparts.entity.Parameter;

public class InteractionReader extends DefaultHandler {
	// private static final Logger log =
	// Logger.getLogger(EmployeeHandler.class.getName());

	private Stack<Object> interactionStack;
	private ArrayList<Interaction> interactions;
	private String characters;
	private StringBuffer characterBuffer=new StringBuffer();


	public InteractionReader(String filePath) throws Exception{

		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {
			SAXParser saxParser = factory.newSAXParser();
			File file=new File(filePath);
			saxParser.parse(file,this);			
		}  catch (Exception e) {
			throw e;
		} finally {
		}
	}
	
	public InteractionReader(String content,String type) throws Exception {

		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {

			SAXParser saxParser = factory.newSAXParser();
			InputStream inputStream= ConvertToStream(content);
			Reader reader = new InputStreamReader(inputStream,"UTF-8");			 
			InputSource is = new InputSource(reader);
			is.setEncoding("UTF-8");
	
			saxParser.parse(is,this);
		} catch (Exception e) {
			throw e;
		} finally {
		}
	}
	
	public InteractionReader(InputStream stream) throws Exception{

		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {

			SAXParser saxParser = factory.newSAXParser();
			saxParser.parse(stream,this);

		}  catch (Exception e) {
			throw e;
		} finally {
		}
	}
	
	
	public InputStream ConvertToStream(String str)throws Exception
	{
		byte[] bytes = str.getBytes("UTF-8");
		return new ByteArrayInputStream(bytes);
	}

	

	public void startDocument() throws SAXException {

		interactionStack = new Stack<Object>();
		interactions = new ArrayList<Interaction>();

	}

	public void startElement(String namespaceURI, String localName,
			String qualifiedName, Attributes attributes) throws SAXException {

		characterBuffer.setLength(0);
		
		if (qualifiedName.equals("Interaction")) {

			Interaction interaction = new Interaction();
			interactionStack.push(interaction);
		}
		else if (qualifiedName.equals("PartDetail")) {

			InteractionPartDetail partDetail = new InteractionPartDetail();
			interactionStack.push(partDetail);
		}	
		else if (qualifiedName.equals("Constraint")) {

			Constraint constraint = new Constraint();
			interactionStack.push(constraint);
		}
		else if (qualifiedName.equals("Parameter")) {

			Parameter parameter = new Parameter ();
			interactionStack.push(parameter);
		}
		
	}

	public void endElement(String namespaceURI, String simpleName,
			String qualifiedName) throws SAXException {

		characterBuffer.setLength(0);

		if (!interactionStack.isEmpty()) {

			if (qualifiedName.equals("Interaction")) {

				interactions.add((Interaction)interactionStack.pop());

			}
			else if (qualifiedName.equals("PartDetail")) {

				InteractionPartDetail partDetail=(InteractionPartDetail) interactionStack.pop();
				Interaction interaction=(Interaction)interactionStack.pop();
				interaction.AddPartDetail(partDetail);
				interactionStack.push(interaction);
			}
			else if (qualifiedName.equals("Constraint")) {

				Constraint constraint=(Constraint) interactionStack.pop();
				Interaction interaction=(Interaction)interactionStack.pop();
				interaction.AddConstraint(constraint);
				interactionStack.push(interaction);
			}		
			else if (qualifiedName.equals("Parameter")) {
				Parameter parameter=(Parameter) interactionStack.pop();
				Interaction interaction=(Interaction)interactionStack.pop();
				interaction.AddParameter(parameter);
				interactionStack.push(interaction);
			}
			//Interaction and InteractionPartDetail property
			else if (qualifiedName.equals("Part")) {
				Object interactionObject=interactionStack.pop();				
				if (interactionObject instanceof Interaction)
				{
					Interaction interaction = (Interaction)interactionObject;
					interaction.AddPart(characters);
					interactionStack.push(interaction);
				}
				else if (interactionObject instanceof InteractionPartDetail)
				{
					InteractionPartDetail partDetail = (InteractionPartDetail)interactionObject;
					partDetail.setPartName(characters);
					interactionStack.push(partDetail);
				}					
			}									
			//Interaction or parameter property
			else if (qualifiedName.equals("Name")) {
				Object interactionObject=interactionStack.pop();				
				if (interactionObject instanceof Interaction)
				{
					Interaction interaction = (Interaction)interactionObject;
					interaction.setName(characters);
					interactionStack.push(interaction);
				}
				else if (interactionObject instanceof Parameter)
				{
					Parameter parameter = (Parameter)interactionObject;
					parameter.setName(characters);
					interactionStack.push(parameter);
				}						
			}
			//Interaction property
			else if (qualifiedName.equals("Description")) {

				Interaction interaction = (Interaction)interactionStack.pop();
				interaction.setDescription(characters);
				interactionStack.push(interaction);			
			}
			//Interaction property
			else if (qualifiedName.equals("InteractionType")) {

				Interaction interaction = (Interaction)interactionStack.pop();
				interaction.setInteractionType(characters);
				interactionStack.push(interaction);			
			}
			//Interaction property
			else if (qualifiedName.equals("MathName")) {

				Interaction interaction = (Interaction)interactionStack.pop();
				interaction.setMathName(characters);
				interactionStack.push(interaction);			
			}
			//Interaction property
			else if (qualifiedName.equals("FreeTextMath")) {

				Interaction interaction = (Interaction)interactionStack.pop();
				interaction.setFreeTextMath(characters);
				interactionStack.push(interaction);			
			}
			//Interaction property
			else if (qualifiedName.equals("IsReaction")) {

				Interaction interaction = (Interaction)interactionStack.pop();
				interaction.setIsReaction(Boolean.parseBoolean(characters));
				interactionStack.push(interaction);			
			}
			//Interaction property
			else if (qualifiedName.equals("IsReversible")) {

				Interaction interaction = (Interaction)interactionStack.pop();
				interaction.setIsReversible(Boolean.parseBoolean(characters));
				interactionStack.push(interaction);			
			}
			//Interaction property
			else if (qualifiedName.equals("IsInternal")) {

				Interaction interaction = (Interaction)interactionStack.pop();
				if (!Util.IsEmpty(characters))
				{					
					interaction.setIsInternal(Boolean.parseBoolean(characters));
				}
				interactionStack.push(interaction);			
			}
			//Part Detail property
			else if (qualifiedName.equals("MoleculerFormType")) {

				InteractionPartDetail partDetail = (InteractionPartDetail)interactionStack.pop();
				partDetail.setPartForm(characters);
				interactionStack.push(partDetail);					
			}
			//Part Detail property
			else if (qualifiedName.equals("InteractionRole")) {

				InteractionPartDetail partDetail = (InteractionPartDetail)interactionStack.pop();
				partDetail.setInteractionRole(characters);
				interactionStack.push(partDetail);					
			}
			//Part Detail property
			else if (qualifiedName.equals("Stoichiometry")) {

				InteractionPartDetail partDetail = (InteractionPartDetail)interactionStack.pop();
				partDetail.setNumberOfMolecules(Integer.parseInt(characters));
				interactionStack.push(partDetail);					
			}
			//Part Detail property
			else if (qualifiedName.equals("NameInMath")) {
				InteractionPartDetail partDetail = (InteractionPartDetail)interactionStack.pop();
				partDetail.setMathName(characters);
				interactionStack.push(partDetail);					
			}	
			//Constraint property
			else if (qualifiedName.equals("SourceName")) {

				Constraint constraint = (Constraint)interactionStack.pop();
				constraint.setSourceID(characters);
				interactionStack.push(constraint);					
			}	
			//Constraint property
			else if (qualifiedName.equals("SourceType")) {

				Constraint constraint = (Constraint)interactionStack.pop();
				constraint.setSourceType(characters);
				interactionStack.push(constraint);					
			}
			//Constraint property
			else if (qualifiedName.equals("Qualifier")) {

				Constraint constraint = (Constraint)interactionStack.pop();
				constraint.setQualifier(characters);
				interactionStack.push(constraint);					
			}
			//Constraint property
			else if (qualifiedName.equals("TargetType")) {

				Constraint constraint = (Constraint)interactionStack.pop();
				constraint.setTargetType(characters);
				interactionStack.push(constraint);					
			}
			//Constraint property
			else if (qualifiedName.equals("TargetName")) {

				Constraint constraint = (Constraint)interactionStack.pop();
				constraint.setTargetID(characters);
				interactionStack.push(constraint);					
			}
			//Parameter property
			else if (qualifiedName.equals("ParameterType")) {

				Parameter parameter= (Parameter)interactionStack.pop();
				parameter.setParameterType(characters);
				interactionStack.push(parameter);					
			}
			//Parameter property
			else if (qualifiedName.equals("Value")) {
				Parameter parameter= (Parameter)interactionStack.pop();
				if (Util.IsNumeric(characters))
				{
					parameter.setValue(Double.parseDouble(characters));
				}
				interactionStack.push(parameter);					
			}
			//Parameter property
			else if (qualifiedName.equals("Scope")) {
				Parameter parameter= (Parameter)interactionStack.pop();
				parameter.setScope(characters);
				interactionStack.push(parameter);					
			}
			else if (qualifiedName.equals("EvidenceType")) {
				 Parameter parameter = (Parameter)interactionStack.pop();
				 parameter.setEvidenceType(characters);
				 interactionStack.push(parameter);
			}
		}		
	}

	public void characters(char buf[], int offset, int len) throws SAXException {

		//characters = new String(buf, offset, len);
		this.characterBuffer.append(buf,offset,len);
		this.characters = this.characterBuffer.toString().trim();

	}
	
	public List<Interaction> GetInteractions()
	{
		return interactions;
	}

}

package vparts.dataimport;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import virtualparts.entity.Formula;

public class MathReader extends DefaultHandler {
	// private static final Logger log =
	// Logger.getLogger(EmployeeHandler.class.getName());

	private Stack<Formula> mathStack;
	private ArrayList<Formula> maths;
	private String characters;
	private StringBuffer characterBuffer=new StringBuffer();


	public MathReader(String filePath) throws Exception {

		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {
			SAXParser saxParser = factory.newSAXParser();
			File file=new File(filePath);
			saxParser.parse(file,this);
		}  catch (Exception e) {
			throw e;
		} finally {
		}
	}
	
	public MathReader(InputStream stream) throws Exception{

		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {

			SAXParser saxParser = factory.newSAXParser();
			saxParser.parse(stream,this);

		}  catch (Exception e) {
			throw e;
		} finally {
		}
	}
	

	public void startDocument() throws SAXException {

		mathStack = new Stack<Formula>();
		maths = new ArrayList<Formula>();

	}

	public void startElement(String namespaceURI, String localName,
			String qualifiedName, Attributes attributes) throws SAXException {

		characterBuffer.setLength(0);

		if (qualifiedName.equals("Math")) {

			Formula math = new Formula();
			// role.setId(Long.parseLong(attributes.getValue("id")));
			mathStack.push(math);

		}
	}

	public void endElement(String namespaceURI, String simpleName,
			String qualifiedName) throws SAXException {
	
		characterBuffer.setLength(0);

		if (!mathStack.isEmpty()) {

			if (qualifiedName.equals("Math")) {

				maths.add(mathStack.pop());

			} else if (qualifiedName.equals("Name")) {

				Formula math = mathStack.pop();
				math.setName(characters);
				mathStack.push(math);

			} else if (qualifiedName.equals("Formula")) {

				Formula math = mathStack.pop();
				math.setFormula(characters);
				mathStack.push(math);
			}
			else if (qualifiedName.equals("ReactionFlux")) {

				Formula math = mathStack.pop();
				math.setReactionFlux(characters);
				mathStack.push(math);
			}
			else if (qualifiedName.equals("ReactionRate")) {

				Formula math = mathStack.pop();
				math.setReactionRate(characters);
				mathStack.push(math);
			}
			else if (qualifiedName.equals("Function")) {

				Formula math = mathStack.pop();
				math.setFunction(characters);
				mathStack.push(math);
			}
			else if (qualifiedName.equals("Accession")) {

				Formula math = mathStack.pop();
				math.setAccession(characters);
				mathStack.push(math);
			}
			else if (qualifiedName.equals("MetaType")) {

				Formula math = mathStack.pop();
				math.setMetaType(characters);
				mathStack.push(math);
			}
		}

	}

	public void characters(char buf[], int offset, int len) throws SAXException {

		//characters = new String(buf, offset, len);
		this.characterBuffer.append(buf,offset,len);
		this.characters = this.characterBuffer.toString().trim();

	}
	
	public List<Formula> GetMaths()
	{
		return maths;
	}

}

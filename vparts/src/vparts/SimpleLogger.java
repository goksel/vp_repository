package vparts;

import java.util.logging.Level;
import java.util.logging.Logger;



public final class SimpleLogger {

	private static final Logger log = Logger.getLogger(PartsHandler.class.getName());
	

	private SimpleLogger() {
	
	}

	public static void Info(Class classToLog,  String message) {		
		log.info(classToLog.getName() + ":" + message);		
	}
	
	public static void Warning(Class classToLog,  String message) {		
		log.warning(classToLog.getName() + ":" + message);		
	}
	
}

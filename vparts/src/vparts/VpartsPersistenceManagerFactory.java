package vparts;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManagerFactory;


public final class VpartsPersistenceManagerFactory {

	private static final PersistenceManagerFactory pmfInstance = JDOHelper.getPersistenceManagerFactory("BacilloBricks");
	

	private VpartsPersistenceManagerFactory() {
	}

	public static PersistenceManagerFactory get() {
		return pmfInstance;
	}
}

package vparts.model;

import vparts.data.InteractionDataHandler;
import vparts.data.MathDataHandler;
import vparts.data.MolecularFormTypeDataHandler;
import vparts.data.PartDataHandler;
import virtualparts.entity.Formula;
import virtualparts.entity.Interactions;
import virtualparts.entity.Interaction;
import virtualparts.entity.MolecularFormType;
import virtualparts.entity.Part;
import virtualparts.entity.Parts;
import virtualparts.entity.Summary;
import virtualparts.serialize.Serializer;
import vparts.entity.VP_Formula;
import vparts.entity.VP_Interaction;
import vparts.entity.VP_MolecularFormType;
import vparts.entity.VP_Part;

import java.util.*;
public class XMLBuilder {
	public XMLBuilder()
	{							
	}
	/*public String ToXML(Object object)
	{
		String output=xstream.toXML(object);
		return output;
	}*/
	public String ToXML(Part part) throws Exception
	{					      
		PartDataHandler handler=new PartDataHandler();
		Serializer serializer=new Serializer();
		String output=serializer.SerializePart(part);
		return output;		
	}
	
	public String SummaryToXML(Summary summary) throws Exception
	{					      
		Serializer serializer=new Serializer();
		String output=serializer.SerializeSummary(summary);
		return output;
	}
				
	public String ToXML(List<Part> parts) throws Exception
	{					      
		String output="";
		if (parts==null)
		{
			parts=new ArrayList<Part>();
		}
		PartDataHandler handler=new PartDataHandler();
		
		Parts sPartsHolder=new Parts();
		sPartsHolder.setParts(parts);
		Serializer serializer=new Serializer();
		output=serializer.SerializeParts(sPartsHolder);
		return output;		
	}
	
	public String FormTypesToXML(List<MolecularFormType> formTypes) throws Exception
	{					      
		Serializer serializer=new Serializer();
		String output=serializer.SerializeMolecularFormType(formTypes);
		return output;		
	}
	
	public String FormulasToXML(List<Formula> formulas) throws Exception
	{					      
		
		Serializer serializer=new Serializer();
		String output=serializer.SerializeFormulas(formulas);
		return output;		
	}
	
	public String InteractionsToXML(List<Interaction> interactions)
	{
		List<Interaction> serializableInteractions=new ArrayList<Interaction>();
		InteractionDataHandler handler=new InteractionDataHandler();
		for (Interaction interaction: interactions)
		{
			if (!interaction.getIsInternal())
			{
				serializableInteractions.add(interaction);
			}
		}
		Interactions interactionsholder=new Interactions();
		interactionsholder.setInteractions(serializableInteractions);
		Serializer serializer=new Serializer();
		String output=serializer.SerializeInteractions(interactionsholder);
		return output;		
	}	
	
	public String InternalInteractionsToXML(List<Interaction> interactions)
	{
		List<Interaction> serializableInteractions=new ArrayList<Interaction>();
		for (Interaction interaction: interactions)
		{
			if (interaction.getIsInternal())
			{
				serializableInteractions.add(interaction);
			}
		}
		Interactions interactionsholder=new Interactions();
		interactionsholder.setInteractions(serializableInteractions);
		Serializer serializer=new Serializer();
		String output=serializer.SerializeInteractions(interactionsholder);
		return output;		
	}	
}

package vparts.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import virtualparts.entity.Interaction;
import virtualparts.entity.Formula;
import virtualparts.entity.MolecularFormType;
import virtualparts.entity.Part;
import virtualparts.model.ModelFactory;
import virtualparts.model.ModelOutput;
import vparts.data.InteractionDataHandler;
import vparts.data.MathDataHandler;
import vparts.data.MolecularFormTypeDataHandler;
import vparts.data.PartDataHandler;
import vparts.entity.VP_Formula;
import vparts.entity.VP_Interaction;
import vparts.entity.VP_MolecularFormType;
import vparts.entity.VP_Part;


public class ModelHandler {

	public ModelOutput GetPartModel (String partName, String modelType) throws Exception
	{
		PartDataHandler partHandler=new PartDataHandler();
		ModelOutput modelOutput=partHandler.GetModelOutput(partName, modelType);
		if (modelOutput==null)
		{
			modelOutput=GetPartModelFromData(partName,modelType);
		}
		//TODO Move this part to the API
		if (partName.toLowerCase().equals("mrna") && "sbml".equals(modelType))
		{			
			Random randomGenerator = new Random();
			String randomKey=String.valueOf(randomGenerator.nextLong()).replace("-", "");	
			String model=modelOutput.getMain();
			model=model.replace("<mts:SignalType>mRNA</mts:SignalType", "TEMP");
			model=model.replace("mRNA", "mRNA_" + randomKey);
			model=model.replace("TEMP", "<mts:SignalType>mRNA</mts:SignalType");	
			modelOutput.setMain(model);
		}
		return modelOutput;		
	}
	
	public ModelOutput GetPartModelFromData (String partName, String modelType) throws Exception
	{
		ModelFactory modelBuilder=new ModelFactory();
		
		PartDataHandler partHandler = new PartDataHandler();
		InteractionDataHandler iHandler = new InteractionDataHandler();
		
		//VP_Part vpart = partHandler.GetPart(partName);
		//Part part=partHandler.GetSerializablePart(vpart);
		Part part=partHandler.GetPart(partName);
		
		
		MathDataHandler formulaHandler=new MathDataHandler();		
		List<Formula> formulas=formulaHandler.GetFormulas();
		
		MolecularFormTypeDataHandler formTypeHandler=new MolecularFormTypeDataHandler();		
		List<MolecularFormType> formTypes=formTypeHandler.GetTypes();
		
		//Get interactions
		List<Interaction> interactions=iHandler.GetPartInteractionsDetailed(partName);
		
		//Get parts in these interactions
		List<Part> interactionParts=new ArrayList<Part>();
		interactionParts.add(part);
		PartDataHandler pHandler=new PartDataHandler();
		int partId=pHandler.GetPartId(partName);
		List<Part> interactionPartsTemp=iHandler.GetInteractionParts(partId);
		for (Part interactionPart:interactionPartsTemp)
		{
			if (!interactionPart.getName().equals(part.getName()))
			{
				interactionParts.add(interactionPart);
			}
		}	
		ModelOutput model=modelBuilder.GetModel(part, modelType, interactions, interactionParts, formulas, formTypes);
		//ModelOutput model=null;
		
		
		
		modelBuilder=null;
		partHandler=null;		
		part=null;
		interactions=null;
		formulaHandler=null;
		formTypeHandler=null;
		formulas=null;
		formTypes=null;
		return model;
		//return null;
		
	}
	
	public ModelOutput GetInteractionModel (String interactionName, String modelType) throws Exception
	{
		InteractionDataHandler handler=new InteractionDataHandler();
		ModelOutput modelOutput=handler.GetModelOutput(interactionName, modelType);
		if (modelOutput==null)
		{
			modelOutput=GetInteractionModelFromData(interactionName,modelType);
		}
		return modelOutput;	
	}
	
	public ModelOutput GetInteractionModelFromData (String interactionName, String modelType) throws Exception
	{
		InteractionDataHandler interactionHandler=new InteractionDataHandler();
		Interaction interaction=interactionHandler.GetInteractionDetailed(interactionName);
		
		//Get parts in these interactions
		PartDataHandler partHandler=new PartDataHandler();
		List<Part> interactionParts=new ArrayList<Part>();		
		List<Part> parts=interactionHandler.GetInteractionParts(interactionName);
		for (Part interactionPart:parts)
		{					
				interactionParts.add(interactionPart);
		}		
		
		MathDataHandler formulaHandler=new MathDataHandler();		
		List<Formula> formulas=formulaHandler.GetFormulas();
		
		MolecularFormTypeDataHandler formTypeHandler=new MolecularFormTypeDataHandler();		
		List<MolecularFormType> formTypes=formTypeHandler.GetTypes();
		
		ModelFactory modelBuilder=new ModelFactory();
		ModelOutput model= modelBuilder.GetModel (interaction, modelType, interactionParts, formulas,formTypes);
		
		interaction=null;
		interaction=null;
		interactionParts=null;
		formulas=null;
		formTypes=null;
		interactionHandler=null;
		formulaHandler=null;
		partHandler=null;
		formTypeHandler=null;
		modelBuilder=null;
		return model;
		//return null;
	}
}

package vparts.data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import vparts.VpartsPersistenceManagerFactory;
import vparts.cache.Cacher;
import virtualparts.entity.Formula;
import vparts.entity.VP_Formula;

public class MathDataHandler {
	private PersistenceManager pm = null;
	
@SuppressWarnings("unchecked")
public List<Formula> GetFormulas() throws Exception
	{
	//return GetTypesData();	
		List<Formula> formulas=null;
		Object formulaObjects=Cacher.Get(Cacher.FORMULA_KEY);
		if (formulaObjects==null)
		{			
			formulas=GetFormulasData();			
			//Cacher.Put(types.get(0), Cacher.MOLECULAR_FORM_TYPE_KEY);
			Cacher.Put(formulas, Cacher.FORMULA_KEY);
			
		}
		else
		{
			formulas=(List<Formula>) formulaObjects;
		}
		return formulas;
	}

public Formula GetFormula(String name) throws Exception
{

	return ( GetFormula(name, GetFormulas()));
	
}

private Formula GetFormula(String name,List<Formula> formulas)
{
	if (formulas!=null&& formulas.size()>0)
	{
		for (Formula formula : formulas)
		{
			if (formula.getName().equals(name))
			{
				return formula;
			}
		}
	}
	return null;
}

private List<Formula> GetFormulasData() throws Exception
{
	PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
	List<Formula> formulas=null;
	try
	{
		String sql = "select from " + VP_Formula.class.getName();		
		Query query=pm.newQuery(sql);
		List<VP_Formula> vFormulas = (List<VP_Formula>) query.execute();
		vFormulas.size();
		formulas=GetSerializableMath(vFormulas);
		query.closeAll();		
	}
	finally
	{
		pm.close();
	}
	return formulas;		
}

public Formula GetSerializableMath(VP_Formula formula) throws Exception
{
	Formula math=new Formula();	
		math.setName(formula.getName());
		math.setFormula(formula.getFormula());
		math.setReactionFlux(formula.getReactionFlux());
		math.setFunction(formula.getFunction());	
		math.setAccession(formula.getAccession());
		math.setMetaType(formula.getMetaType());
		math.setReactionRate(formula.getReactionRate());
	return math;
}

public List<Formula> GetSerializableMath(List<VP_Formula> maths) throws Exception
{
	List<Formula> serializableMaths=new ArrayList<Formula>(maths.size());
	for (VP_Formula math:maths)
	{
		Formula serializableMath=GetSerializableMath(math);
		serializableMaths.add(serializableMath);
	}	
	return serializableMaths;
}

public Boolean CreateMaths(List<Formula> maths) throws Exception
{
	Boolean result=false;
	try {
		List<VP_Formula> formulas=new ArrayList<VP_Formula>();
		pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();			
		for(Formula math:maths)
		{
			VP_Formula formula=new VP_Formula(math.getName(),math.getFormula(),math.getReactionFlux(),math.getFunction());
			formula.setAccession(math.getAccession());
			formula.setMetaType(math.getMetaType());	
			formula.setReactionRate(math.getReactionRate());				
			formulas.add(formula);
		}
		pm.makePersistentAll(formulas);
		result= true;
	} catch (Exception e) {

		e.printStackTrace();			
		result= false;
		throw e;
	} finally {

		pm.close();
	}	
	return result;
}	

public String GetInteractionFormula(Formula formula)
{
	String interactionFormula=formula.getReactionFlux().replace(" ", "");//Spaces are not allowed in the math in jsbml		
	//If the function is not empty, use the function instead of the reaction flux
	/*if (!vparts.Util.IsEmpty(formula.getFunction()) && !formula.getFunction().contains("\n"))
	{
		interactionFormula=formula.getFunction();
	}*/
	return interactionFormula;
}


}


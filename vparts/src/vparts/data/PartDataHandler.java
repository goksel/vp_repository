package vparts.data;

import java.lang.annotation.Annotation;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import org.sbolstandard.core.DnaComponent;
import org.sbolstandard.core.DnaSequence;
import org.sbolstandard.core.SBOLFactory;
import org.sbolstandard.core.SequenceAnnotation;
import org.sbolstandard.core.StrandType;
import org.tuckey.web.filters.urlrewrite.utils.Log;

import virtualparts.entity.Formula;
import virtualparts.entity.Interaction;
import virtualparts.entity.InteractionPartDetail;
import virtualparts.entity.Part;
import virtualparts.entity.Parts;
import virtualparts.entity.Property;
import virtualparts.entity.SearchEntity;
import virtualparts.entity.Summary;
import virtualparts.model.ModelOutput;
import virtualparts.serialize.Serializer;
import vpart.exchange.SBOLConverter;
import vparts.PartsHandler;
import vparts.SimpleLogger;
import vparts.Util;
import vparts.VpartsPersistenceManagerFactory;
import vparts.cache.Cacher;
import vparts.dataimport.PartReader;
import vparts.entity.VP_Annotation;
import vparts.entity.VP_Formula;
import vparts.entity.VP_Interaction;
import vparts.entity.VP_InteractionPartDetail;
import vparts.entity.VP_Model;
import vparts.entity.VP_MolecularForm;
import vparts.entity.VP_Role;
import vparts.entity.VP_Part;
import vparts.model.ModelHandler;

public class PartDataHandler {	
	
public  Boolean HasRole(Part part, String roleName) throws Exception
{
	Boolean hasRole=false;
	List<Property> roles=GetPartRoles(part.getName());
	if (roles!=null && roles.size()>0)
	{
		for (Property role :roles)
		{
			if (role.getName().equals(roleName))
			{
				return true;
			}
		}
	}
	return hasRole;
}

/* TODO 20140515: Delete
  public List<Part> GetVParts() throws Exception
{
	PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
	Query query=null;
	List<Part> parts=null;
	try
	{
		String sql = "select from " + VP_Part.class.getName() + " where metaType==\"Part\" order by organismCode desc, type,name asc";
		query=pm.newQuery(sql);
		List<VP_Part> vparts = (List<VP_Part>) query.execute();
		vparts.size();
		parts=GetSerializableParts(vparts);
		query.closeAll();
	}
	finally
	{		
		pm.close();
	}	
	return parts;		
}
*/

public List<Part> GetVParts(int pageNumber) throws Exception
{
	return GetVParts(null, null, null, null, null, false, pageNumber);
	
}

public int GetVPartsCount() throws Exception
{	
	int count=-1;
	Object countObject=Cacher.Get("VpartCount");
	if (countObject!=null)
	{
		count=(Integer) countObject;
	}
	else
	{
		count=GetVPartsCount(null, null, null, null, null, false);
		Cacher.Put(count,"VpartCount");
		SimpleLogger.Info(PartDataHandler.class, "Number of parts is" + count);		
	}
	return count;
}

public int GetVPartsCount(String partType) throws Exception
{
	String cacheKey="VpartCount_" + partType;
	int count=-1;
	Object countObject=Cacher.Get(cacheKey);
	if (countObject!=null)
	{
		count=(Integer) countObject;
	}
	else
	{
		count=GetVPartsCount(null, partType, null, null, null, false);
		Cacher.Put(count,cacheKey);
	}
	return count;
}

public int GetVPartsCount(String partType,String roleName, String roleValue) throws Exception
{
	int count=-1;
	if (roleName==null|| roleName.equals("") || roleValue==null || roleValue.equals(""))
	{
		count=GetVPartsCount(partType);
	}
	else
	{
		String cacheKey="VpartCount_" + partType + "_" + roleName + "_" + roleValue;		
		Object countObject=Cacher.Get(cacheKey);
		if (countObject!=null)
		{
			count=(Integer) countObject;
		}
		else
		{
			count=GetVPartsCount(null, partType, null, roleName, roleValue,true, false);
			Cacher.Put(count,cacheKey);
		}		
	}	
	return count;
}

public int GetVPartsCount(String roleName, String roleValue) throws Exception
{
	int count=-1;
	
		String cacheKey="VpartCount_" + "Property" + "_" + roleName + "_" + roleValue;		
		Object countObject=Cacher.Get(cacheKey);
		if (countObject!=null)
		{
			count=(Integer) countObject;
		}
		else
		{
			count=GetVPartsCount(null, null, null, roleName, roleValue,true, false);
			Cacher.Put(count,cacheKey);
		}		
		
	return count;
}


public List<Part> GetVParts(String partType,int pageNumber) throws Exception
{
	List<Part> parts=null;
	parts=GetVParts(null, partType, null, null, null, false, pageNumber);	
	return parts;		
}

public List<Part> GetVParts(String partType,String roleName,String roleValue,int pageNumber) throws Exception
{
	if (roleName==null || roleName.equals(""))
	{
		return GetVParts(partType,pageNumber);
	}
	else
	{
		return GetVParts(null, partType, null, roleName, roleValue,true, false, pageNumber);			
	}
}

public int GetVPartsCount(String text, String type, String organism, String property, String propertyValue, boolean includeAll) throws Exception
{
	return GetVPartsCount(text, type, organism, property, propertyValue, false, includeAll);
}

public int GetVPartsCount(String text, String type, String organism, String property, String propertyValue, boolean exactPropertyValueMatch, boolean includeAll) throws Exception
{
	PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
	int count=-1;
	try
	{			
		String sql=GetSearchQuery(text, type, organism, property, propertyValue,exactPropertyValueMatch, includeAll,true);
		
		Query query=pm.newQuery("javax.jdo.query.SQL",sql);	
		
		List result=(List)query.execute();
		if (result!=null && result.size()>0)
		{
			count= Integer.parseInt(result.get(0).toString());					
		}
		query.closeAll();			
	}
	catch (Exception exception)
	{
		throw exception;
	}
	finally
	{
		pm.close();
	}
	return count;	
}

public String SearchPartsString(String searchEntityData, int pageNumber) throws Exception
{
	String result="";
	if (searchEntityData!=null && searchEntityData.length()>0)
	{
		Serializer serializer=new Serializer();
		SearchEntity searchEntity=serializer.GetSearchEntity(searchEntityData);
		List<Part> parts=GetVParts(searchEntity.getSearchText(), searchEntity.getPartType(), searchEntity.getOrganism(), 
				searchEntity.getPropertyName(), searchEntity.getPropertyValue(), searchEntity.getExactPropertyValueMatch(), 
				searchEntity.getIncludeGeneticElements(), pageNumber);
		if (parts!=null && parts.size()>0)
		{
			Parts partsContainer=new Parts();
			partsContainer.setParts(parts);
			result=serializer.SerializeParts(partsContainer);
		}
	}
	return result;
}

public String SearchPartsSummaryString(String searchEntityData) throws Exception
{
	String result="";
	if (searchEntityData!=null && searchEntityData.length()>0)
	{
		Serializer serializer=new Serializer();
		SearchEntity searchEntity=serializer.GetSearchEntity(searchEntityData);
		int partCount=GetVPartsCount(searchEntity.getSearchText(), searchEntity.getPartType(), searchEntity.getOrganism(), 
				searchEntity.getPropertyName(), searchEntity.getPropertyValue(), searchEntity.getExactPropertyValueMatch(), 
				searchEntity.getIncludeGeneticElements());
		
		Summary summary=new Summary();
		int pageCount=Util.GetPageCount(partCount,50);	
		summary.setPageCount(pageCount);
		result=serializer.SerializeSummary(summary);		
	}
	return result;
}

public List<Part> GetVParts(String text, String type, String organism, String property, String propertyValue, boolean includeAll, int page) throws Exception
{
	return GetVParts(text, type, organism, property, propertyValue, false, includeAll, page);
}

public List<Part> GetVParts(String text, String type, String organism, String property, String propertyValue, boolean exactPropertyValueMatch, boolean includeAll, int page) throws Exception
{
	List<Part> parts=new ArrayList<Part>();
	PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
	
	try
	{			
		String sql=GetSearchQuery(text, type, organism, property, propertyValue, exactPropertyValueMatch, includeAll,false);
		sql=AddPageQuery(sql, page);
		
		Query query=pm.newQuery("javax.jdo.query.SQL",sql);
		query.setClass(VP_Part.class);
		List<VP_Part> vparts=   (List<VP_Part>) query.execute(query);
		vparts.size();
		parts=GetSerializableParts(vparts);		
		query.closeAll();		
	}
	finally
	{
		pm.close();
	}
	return parts;	
}

private int GetStart(int pageNumber)
{
	int pageSize=50;
	int start = pageNumber * pageSize - pageSize;
	return start;
}

private int GetEnd(int pageNumber)
{
	int pageSize=50;
	int start = GetStart(pageNumber);
	int end=start + pageSize;
	return end;
}

private String AddPageQuery(String sql, int page)
{
	int start=GetStart(page);
	int end=GetEnd(page);
	sql=String.format("%s LIMIT %d OFFSET %d",sql,end-start,start); 	
	return sql;
}

private String GetSearchQuery(String text, String type, String organism, String property, String propertyValue, boolean exactPropertyValueMatch, boolean includeAll, boolean countOnly)
{
	//Construct the select statement
	String sql="select distinct p.*, p.ID as ID";
	if (countOnly)
	{
		sql="select count(distinct p.ID)";
	}
	
	sql=sql + " from VP_PART p";
	if (!includeAll)
	{
		sql=sql +  " INNER JOIN VP_INTERACTION_PARTS ip ON p.ID=ip.PART_ID";
	}

	if (property!=null && propertyValue!=null && propertyValue.length()>0)
	{
		sql= sql  +  " INNER JOIN VP_ROLE r ON p.ID=r.PART_ID";
	}
	
	//Construct the filtering
	sql=sql + " where METATYPE='Part'";	
	if (property!=null && propertyValue!=null && propertyValue.length()>0)
	{
		if (exactPropertyValueMatch)
		{
			sql=sql + String.format(" AND r.NAME='%s' AND r.VALUE='%s'",property,propertyValue);

		}
		else
		{
			String anyValue="%" + propertyValue.toLowerCase() + "%";
			sql=sql + String.format(" AND r.NAME='%s' AND (LOWER(r.DESCRIPTION) LIKE '%s' OR LOWER(r.VALUE) LIKE '%s')",property,anyValue,anyValue);
		}
	}
			
	if (text!=null && text.length()>0)
	{
		String anyText="%" + text.toLowerCase() + "%";
		sql=sql + String.format(" AND (LOWER(p.NAME) LIKE '%s' OR LOWER(p.DESCRIPTION) LIKE '%s' OR LOWER(p.DISPLAYNAME) LIKE '%s')",anyText,anyText,anyText);		
	}
	
	if (type!=null && type.length()>0)
	{
		sql=sql + String.format(" AND p.TYPE='%s'", type);
	}
	if (organism!=null && organism.length()>0)
	{
		String organismStartingWith=organism + "%";
		sql=sql + String.format(" AND p.ORGANISM LIKE '%s'",organismStartingWith);			
	}
	if (!countOnly)
	{
		sql= sql + " order by p.organismCode desc, p.type,p.name asc";
	}
	return sql;
}

public List<Part> GetVParts(String propertyName,String propertyValue,int pageNumber) throws Exception
{
	return GetVParts(null, null, null, propertyName, propertyValue, false, pageNumber);			
}


public Part GetPartDetailed(String name) throws Exception
{
	PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
	Part part=null;
	/*FetchGroup fg= pm.getFetchGroup(VP_Part.class, "PartDetailed");
	fg.addMember("partRoles");
	pm.getFetchPlan().addGroup("PartDetailed");*/
	// pm.getFetchPlan().addGroup("fetchpartroles");
	try
	{
		String sql = "select from " + VP_Part.class.getName() + " where name==\"" + name + "\"";
		Query query=pm.newQuery(sql);
		query.setClass(VP_Part.class);
		query.setUnique(true);
		VP_Part vpart= (VP_Part) query.execute();
		part=GetSerializablePartDetailed(vpart);
		query.closeAll();	
		
		/*String sqlQuery = "select * from VP_ROLE where part_id =" + vpart.getId() ;
		Query roleQuery=pm.newQuery("javax.jdo.query.SQL",sqlQuery);
		roleQuery.setClass(VP_Role.class);
		List<VP_Role> roles= (List<VP_Role>) roleQuery.execute();
		roles.size();
		//vpart.setPartRoles(roles);				
		if (roles!=null && roles.size()>0)
		{
			List<Property> properties=new ArrayList<Property>();
			for (VP_Role vProperty:roles)
			{
				Property property=new Property();
				property.setName(vProperty.getName());
				property.setValue(vProperty.getValue());
				property.setDescription(vProperty.getDescription());
				properties.add(property);				
			}
			part.setProperties(properties);
		
		roleQuery.closeAll();
		query.closeAll();	*/
	}
	finally
	{	
		pm.close();
	}
	return part;
}

public Part GetPart(String name) throws Exception
{
	PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
	Part part=null;
	try
	{
		String dbQuery = "select from " + VP_Part.class.getName() + " where name==\"" + name + "\"";
		Query query=pm.newQuery(dbQuery);
		//query.setClass(VP_Part.class);
		//query.setUnique(true);
		//VP_Part vpart= (VP_Part) query.execute();
		
		List result=(List)query.execute();
		if (result!=null && result.size()>0)
		{
			VP_Part vpart= (VP_Part) result.get(0);
			part=GetSerializablePart(vpart);	
		}
		query.closeAll();
	}
	catch (Exception exception)
	{
		exception.printStackTrace();
		throw exception;
	}
	finally
	{
		pm.close();
	}
	return part;		
}

public VP_Part GetVPart(String name) throws Exception
{
	PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
	VP_Part vpart=null;
	try
	{
		String dbQuery = "select from " + VP_Part.class.getName() + " where name==\"" + name + "\"";
		Query query=pm.newQuery(dbQuery);
		
		List result=(List)query.execute();
		if (result!=null && result.size()>0)
		{
			vpart= (VP_Part) result.get(0);
		}
		query.closeAll();
	}
	catch (Exception exception)
	{
		exception.printStackTrace();
		throw exception;
	}
	finally
	{
		pm.close();
	}
	return vpart;		
}

public int GetPartId(String name) throws Exception
{
	PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
	int id=-1;
	try
	{
		String dbQuery = "select from " + VP_Part.class.getName() + " where name==\"" + name + "\"";
		Query query=pm.newQuery(dbQuery);
		//query.setClass(VP_Part.class);
		//query.setUnique(true);
		//VP_Part vpart= (VP_Part) query.execute();
		
		List result=(List)query.execute();
		VP_Part vpart= (VP_Part) result.get(0);
		id=vpart.getId();
		query.closeAll();
	}
	finally
	{
		pm.close();
	}
	return id;		
}

public VP_Part GetVPart(String name, PersistenceManager pm) throws Exception
{	
	VP_Part vpart=null;
	try
	{
		String dbQuery = "select from " + VP_Part.class.getName() + " where name==\"" + name + "\"";
		Query query=pm.newQuery(dbQuery);
		query.setClass(VP_Part.class);
		query.setUnique(true);
		vpart= (VP_Part) query.execute();
		query.closeAll();	
	}
	catch (Exception exception)
	{
		exception.printStackTrace();
		throw new Exception ("Part with name " + name + " could not be retrieved",exception);
	}
	return vpart;		
}


public List<Property> GetPartRoles(String partName) throws Exception
{	
	PropertyDataHandler roleHandler=new PropertyDataHandler();
	int partID=GetPartId(partName);	
	List<Property> roles=roleHandler.GetRolesByPartID(partID);			
	return roles;		
}





public List<VP_MolecularForm> GetPartMolecularForms(VP_Part part)
{		
	List<VP_MolecularForm> molecularForms=new ArrayList<VP_MolecularForm>();		
	PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
	Iterator<Integer> iterator=part.getMolecularForms().iterator();
	while (iterator.hasNext())
	{
		int id=iterator.next();
		VP_MolecularForm form= pm.getObjectById(VP_MolecularForm.class, id);
		molecularForms.add(form);
	}				
	return molecularForms;
}

public Part GetSerializablePartDetailed(VP_Part part) throws Exception
{
	Part sPart=null;
	try {
		sPart=GetSerializablePart(part);
		List<VP_Role> vProperties=part.getPartRoles();
		if (vProperties!=null && vProperties.size()>0)
		{
			List<Property> properties=new ArrayList<Property>();
			for (VP_Role vProperty:vProperties)
			{
				Property property=new Property();
				property.setName(vProperty.getName());
				property.setValue(vProperty.getValue());
				property.setDescription(vProperty.getDescription());
				properties.add(property);				
			}
			sPart.setProperties(properties);
		}
	} 
	catch (Exception ex) {
		throw new Exception ("Could not convert part " + sPart.getName() +  "to its serializable format." + ex.getMessage(),ex);		
	}
	return sPart;
}

public Part GetSerializablePart(VP_Part part) throws Exception
{
	Part sPart=null;
	try {
		sPart=new Part();
		sPart.setName(part.getName());
		sPart.setDescription(part.getDescription());
		sPart.setMetaType(part.getMetaType());
		sPart.setSequence(part.getSequence());
		sPart.setSequenceURI(part.getSequenceURI());
		sPart.setType(part.getType());
		sPart.setDisplayName(part.getDisplayName());	
		sPart.setOrganism(part.getOrganism());
		sPart.setDesignMethod(part.getDesignMethod());
	} 
	catch (Exception ex) {
		throw new Exception ("Could not convert part " + sPart.getName() +  "to its serializable format." + ex.getMessage(),ex);		
	}
	return sPart;
}


public List<Part>  GetSerializableParts(List<VP_Part> vparts) throws Exception
{
	List<Part> parts=new ArrayList<Part>(vparts.size());
	for (VP_Part vpart:vparts)
	{
		parts.add(GetSerializablePart(vpart));
	}
	return parts;
}

public void ImportParts(String partsContent)throws Exception
{
	PartReader reader = new PartReader(partsContent,"data");
	List<Part> parts = reader.GetParts();		
	PartsHandler handler=new PartsHandler();
	handler.CreateParts(parts);
	Cacher.Clear();
}

public List<Part> GetVPartsByParameter(String filter) throws Exception
{
	List<Part> parts=null;			
	PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
	try
	{
		/*String selectQuery="SELECT part.id,part.name,part.description,part.metaType,part.displayName,part.sequence,part.sequenceURI,part.organism,part.status,part.designMethod from VP_INTERACTION i" +
							" inner join VP_INTERACTION_PARAMETERS ip ON i.KEY=ip.KEY_OID" +
							" inner join VP_PARAMETER p ON ip.Element=p.Key" +
							" inner join VP_INTERACTION_PARTS ipart ON i.`KEY`=ipart.INTERACTION_ID" +
							" inner join VP_PART part ON ipart.PART_ID=part.`KEY`";*/
				
		String selectQuery="SELECT part.id as id,part.name,part.description,part.metaType,part.displayName,part.sequence,part.sequenceURI,part.organism,part.status,part.designMethod from VP_INTERACTION i" +
				" inner join VP_PARAMETER p ON i.id=p.parameterHolderId" +
				" inner join VP_INTERACTION_PARTS ipart ON i.id=ipart.INTERACTION_ID" +
				" inner join VP_PART part ON ipart.PART_ID=part.id";
		String filterQuery=" where " + FormatFilter(filter);
		String sqlQuery=selectQuery +  filterQuery;
		
		Query query=pm.newQuery("javax.jdo.query.SQL",sqlQuery);
		query.setClass(VP_Part.class);
		List<VP_Part> vparts=   (List<VP_Part>) query.execute(query);
		vparts.size();	
		parts=GetSerializableParts(vparts);
		query.closeAll();
	}
	catch(Exception exception)
	{
		exception.printStackTrace();
		throw exception;
	}
	finally
	{
		pm.close();
	}
	return parts;
}

/**
 * Creates the models for interactions that do not have models stored in the database. Models for internal interactions are not
 * included since those interactions are part of part models. 
 * @throws Exception
 */
public void CreatePartModels() throws Exception
{
	PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();	 
	try
	{					
		//Select parts that have Part as the meta type.Select only the parts that do not have any models (using the pm.PART_ID is NULL clause)
		String sqlQuery="SELECT distinct part.id as id, part.* from VP_PART part" +
				" left outer join VP_PART_MODELS pm ON pm.PART_ID=part.id" +
				" WHERE part.metaType='Part' AND pm.PART_ID is NULL";
		
		Query query=pm.newQuery("javax.jdo.query.SQL",sqlQuery);
		query.setClass(VP_Part.class);
		List<VP_Part> parts=(List<VP_Part>)query.execute();
		
		CreateModels(parts);
		
		query.closeAll();
	}
	catch(Exception exception)
	{
		exception.printStackTrace();
		throw exception;
	}
	finally
	{
		pm.close();
	}	
}

/**
 * Creates the models for interactions that do not have models stored in the database. Models for internal interactions are not
 * included since those interactions are part of part models. 
 * @throws Exception
 */
public void CreatePartModel(String partName) throws Exception
{
	PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();	 
	try
	{					
		VP_Part vPart=GetVPart(partName, pm);
		
		if (partName!=null)
		{
			List<VP_Part> vparts=new ArrayList<VP_Part>(Arrays.asList(vPart));
			CreateModels(vparts);
		}
	}
	catch(Exception exception)
	{
		exception.printStackTrace();
		throw exception;
	}
	finally
	{
		pm.close();
	}	
}
private void CreateModels(List<VP_Part> parts) throws Exception
{
		int i=0;
		String str="";
		for (VP_Part part:parts)
		{
			if ("Part".equals(part.getMetaType()))
			{
				try{
					VP_Model model=GetModel(part.getName(),"sbml");		
					if (model!=null && model.getText()!=null && model.getText().length()>0)
					{
						part.AddModel(model);	
					}

					VP_Model modelKappa=GetModel(part.getName(),"kappa");		
					if (modelKappa!=null && modelKappa.getText()!=null && modelKappa.getText().length()>0)
					{
						part.AddModel(modelKappa);	
					}

					i++;
					if (i%100==0)
					{
						Logger.getLogger(PartDataHandler.class.getName()).log(Level.INFO, "Creating the model for part  "+ i);
					}
				}
				catch(Exception exception)
				{
					throw new Exception ("Could not create the model for part:" + part.getName() + "." + exception.getMessage(), exception);
				}
			}
		}	
}

public ModelOutput GetModelOutput(String partName,String modelType) throws Exception
{
	ModelOutput modelOutput=null;
	List<Part> parts=null;			
	PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
	try
	{					
		String sqlQuery="SELECT model.text from VP_MODEL model" +
				" inner join VP_PART_MODELS pm ON pm.MODEL_ID=model.id" +
				" inner join VP_PART part ON pm.PART_ID=part.id" +
				" WHERE part.name='" + partName + "'" +
				" and model.type='" + modelType + "'";
		
		Query query=pm.newQuery("javax.jdo.query.SQL",sqlQuery);
		List result=(List)query.execute();
		if (result!=null && result.size()>0)
		{
			String text= (String)result.get(0);
			modelOutput=new ModelOutput();
			modelOutput.setMain(text);		
		}			
		query.closeAll();
	}
	catch(Exception exception)
	{
		exception.printStackTrace();
		throw exception;
	}
	finally
	{
		pm.close();
	}
	return modelOutput;
}

public void AddModel(String partName,String modelText, String modelType) throws Exception
{
	PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
	try
	{
		VP_Model model=new VP_Model();
		model.setType(modelType);
		model.setText(modelText);
		VP_Part part=GetVPart(partName, pm);
		part.AddModel(model);
	}
	catch (Exception exception)
	{
		exception.printStackTrace();
		throw new Exception ("Could not add the model for part" + partName + "." + exception.getMessage(),exception);
	}
	finally
	{
		pm.close();
	}
}

private VP_Model GetModel(String partName,String modelType) throws Exception
{	
	ModelHandler modelHandler=new ModelHandler();
	ModelOutput modelOutput=modelHandler.GetPartModelFromData(partName, modelType);
	VP_Model model=new VP_Model();
	model.setType(modelType);
	model.setText(modelOutput.getMain());
	return model;
}


public List<SequenceAnnotation> GetAnnotations(String partName) throws Exception
{		
	Query query=null;
	PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
	List<SequenceAnnotation> sequenceAnnotations=new ArrayList<SequenceAnnotation>();
	int partID=GetPartId(partName);
	try
	{
		String sqlQuery = "select * from " + VP_Annotation.class.getSimpleName().toUpperCase() +  " where PART_ID=" + partID ;
		query=pm.newQuery("javax.jdo.query.SQL",sqlQuery);
		query.setClass(VP_Annotation.class);
		List<VP_Annotation> annotations= (List<VP_Annotation>) query.execute();		
		annotations.size();
		
		for (VP_Annotation annotation:annotations)
		{
			SequenceAnnotation sequenceAnnotation=GetSequenceAnnotation(annotation);
			String uri=String.format("/part/%s/annotation/%d_%d_%s", partName,annotation.getStart(),annotation.getEnd(),annotation.getSubPart().getName());
			sequenceAnnotation.setURI(new URI(uri));
			sequenceAnnotations.add(sequenceAnnotation);
		}				
		query.closeAll();	
	}
	finally
	{
		pm.close();
	}
	return sequenceAnnotations;		
}

public List<SequenceAnnotation> GetAnnotationsDetailed(String partName,String baseURL) throws Exception
{		
	Query query=null;
	PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
	List<SequenceAnnotation> sequenceAnnotations=new ArrayList<SequenceAnnotation>();
	int partID=GetPartId(partName);
	try
	{
		String sqlQuery = "select * from " + VP_Annotation.class.getSimpleName().toUpperCase() +  " where PART_ID=" + partID ;
		query=pm.newQuery("javax.jdo.query.SQL",sqlQuery);
		query.setClass(VP_Annotation.class);
		List<VP_Annotation> annotations= (List<VP_Annotation>) query.execute();		
		annotations.size();
		
		for (VP_Annotation annotation:annotations)
		{
			SequenceAnnotation sequenceAnnotation=GetSequenceAnnotation(annotation);
			String subPartName=annotation.getSubPart().getName();
			String uri=String.format("/part/%s/annotation/%d_%d_%s", partName,annotation.getStart(),annotation.getEnd(),subPartName);
			sequenceAnnotation.setURI(new URI(uri));
			
			Part subPart=GetPart(subPartName);
			DnaComponent subComponent=SBOLConverter.GetSBOLDnaComponent(subPart, baseURL);
			List<SequenceAnnotation>  subComponentAnnotations=GetAnnotationsDetailed(subPartName, baseURL);
			if (subComponentAnnotations!=null && subComponentAnnotations.size()>0)
			{
				for(SequenceAnnotation subComponentAnnotation:subComponentAnnotations)
				{
					subComponent.addAnnotation(subComponentAnnotation);
				}
			}
			sequenceAnnotation.setSubComponent(subComponent);
			sequenceAnnotations.add(sequenceAnnotation);
			
		}				
		query.closeAll();	
	}
	finally
	{
		pm.close();
	}
	return sequenceAnnotations;		
}


public boolean AnnotationExists(VP_Part part,SequenceAnnotation annotation) throws Exception
{
	boolean result=false;
	PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();	
	try
	{
		String dbQuery = String.format("select from %s where part==p && p.id==%d && start==%d && end==%d", VP_Annotation.class.getName(),part.getId(),annotation.getBioStart(),annotation.getBioEnd());
		if (annotation.getStrand()!=null)
		{
			dbQuery = dbQuery + " && strand==\"" + annotation.getStrand().getSymbol() + "\"";
		}
		
		Query query=pm.newQuery(dbQuery);
		query.declareVariables(VP_Part.class.getName() + " p");
		
		//query.setClass(VP_Annotation.class);
		Object queryResult=query.execute();
		List<VP_Annotation> annotations= (List<VP_Annotation>) queryResult;
		if (annotations.size()>0)
		{
			result=true;
		}
		query.closeAll();	
	}	
	catch (Exception exception)
	{
		exception.printStackTrace();
		throw exception;	
	}
	finally
	{
		pm.close();
	}
	return result;
}

private SequenceAnnotation GetSequenceAnnotation(VP_Annotation annotation) throws Exception
{
	SequenceAnnotation sequenceAnnotation=SBOLFactory.createSequenceAnnotation();
	sequenceAnnotation.setBioStart(annotation.getStart());
	sequenceAnnotation.setBioEnd(annotation.getEnd());
	if (annotation.getStrand()!=null)
	{					
		sequenceAnnotation.setStrand(GetStrandType(annotation.getStrand()));
	}
	//VP_Part part= pm.getObjectById(VP_Part.class, annotation.getSubPart().getId());
	DnaComponent component=SBOLFactory.createDnaComponent();
	component.setDisplayId(annotation.getSubPart().getName());
	component.addType(new URI(SBOLConverter.GetTypeURI(annotation.getSubPart().getType())));
	component.setURI(new URI("/part/" + annotation.getSubPart().getName()));
	sequenceAnnotation.setSubComponent(component);
	return sequenceAnnotation;
}


/**
 * Returns a set of string in the form of partname_modeltype
 * @param parts
 * @return
 */
public Set<String> GetModelTypes(List<Part> parts) throws Exception
{
	Set<String> modelsOfParts=new HashSet<String>();
	if (parts!=null && parts.size()>0)
	{	
		PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();	
		StringBuilder builder=new StringBuilder();	
		for (Part part:parts)
		{
			builder.append("\"" + part.getName() + "\"," );
		}
		String inQuery=builder.toString();
		//Remove the last comma
		inQuery=inQuery.substring(0,inQuery.length()-1);
		inQuery=String.format("IN (%s)", inQuery);
		try
		{
			//String dbQuery = String.format("select from %s model where part.contains(model) && part.name %s", VP_Model.class.getName(),inQuery);				
			String sqlQuery = "select part.id,part.name,model.type from VP_PART" +
								" part inner join VP_PART_MODELS pmodel ON part.id=pmodel.PART_ID" +
								" inner join VP_MODEL model ON pmodel.MODEL_ID=model.ID" + 
								" where part.name " + inQuery;
			
			Query query=pm.newQuery("javax.jdo.query.SQL",sqlQuery);
					
			List queryResult= (List)query.execute();
			if (queryResult.size()>0)
			{
				for (Object result:queryResult)
				{
					Object[] listResult=(Object[]) result;
					String key=listResult[1] + "_" + listResult[2];
					modelsOfParts.add(key);
				}
			}
			query.closeAll();	
		}	
		catch (Exception exception)
		{
			exception.printStackTrace();
			throw exception;	
		}
		finally
		{
			pm.close();
		}
	}
	return modelsOfParts;
}

public Set<String> GetModelTypes(String partName) throws Exception
{
	Set<String> modelsOfParts=new HashSet<String>();
	PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();	
	try
	{
		//String dbQuery = String.format("select from %s model where part.contains(model) && part.name %s", VP_Model.class.getName(),inQuery);				
		String sqlQuery = "select part.id,part.name,model.type from VP_PART" +
							" part inner join VP_PART_MODELS pmodel ON part.id=pmodel.PART_ID" +
							" inner join VP_MODEL model ON pmodel.MODEL_ID=model.ID" + 
							" where part.name=\"" + partName + "\"";
		
		Query query=pm.newQuery("javax.jdo.query.SQL",sqlQuery);
				
		List queryResult= (List)query.execute();
		if (queryResult.size()>0)
		{
			for (Object result:queryResult)
			{
				Object[] listResult=(Object[]) result;
				String key=listResult[1] + "_" + listResult[2];
				modelsOfParts.add(key);
			}
		}
		query.closeAll();	
	}	
	catch (Exception exception)
	{
		exception.printStackTrace();
		throw exception;	
	}
	finally
	{
		pm.close();
	}
	return modelsOfParts;
}


private StrandType GetStrandType(String type)
{
	StrandType strandType=null;
	if ("+".equals(type))
	{
		strandType=StrandType.POSITIVE;
	}
	else if ("-".equals(type))
	{
		strandType=StrandType.NEGATIVE;
	}
	return strandType;	
}

private String FormatFilter(String filter)
{
	//filter=filter.replace("AND", "&&");
	//filter=filter.replace("OR", "||");
	filter=filter.replace("_SPACE_"," ");
	filter=filter.replace("GTOE",">=");
	filter=filter.replace("LTOE","<=");	
	filter=filter.replace("GT",">");
	filter=filter.replace("LT","<");
	filter=filter.replace("EQ","==");
	filter=filter.replace("==","=");		
	return filter;
}

public static String GetOrganismCode(String sourceOrganism)
{
	String code=null;
	if (sourceOrganism!=null)
	{
		sourceOrganism=sourceOrganism.toLowerCase();
		if (sourceOrganism.startsWith("bacillus subtilis"))
		{
			code="B. subtilis";
		}
		else if (sourceOrganism.startsWith("escherichia coli"))
		{
			code="E. coli";
		}			
	}
	return code;		
}
}

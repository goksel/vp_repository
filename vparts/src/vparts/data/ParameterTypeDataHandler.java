package vparts.data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import virtualparts.entity.MolecularFormType;
import vparts.VpartsPersistenceManagerFactory;
import vparts.cache.Cacher;
import vparts.entity.ParameterType;
import vparts.entity.VP_MolecularFormType;
import vparts.entity.VP_ParameterType;

public class ParameterTypeDataHandler {

	@SuppressWarnings("unchecked")
	public List<ParameterType> GetTypes() throws Exception
		{
		//return GetTypesData();	
			List<ParameterType> types=null;
			Object typesObject=Cacher.Get(Cacher.PARAMETER_TYPE_KEY);	
			if (typesObject==null)
			{			
				types=GetParameterTypesData();												
				Cacher.Put(types, Cacher.PARAMETER_TYPE_KEY);				
			}
			else
			{
				types=(List<ParameterType>) typesObject;
			}
			return types;
		}

	public ParameterType GetType(String name) throws Exception
	{

		return ( GetType(name, GetTypes()));
		
	}

	private ParameterType GetType (String name,List<ParameterType> types)
	{
		if (types!=null&& types.size()>0)
		{
			for (ParameterType type : types)
			{
				if (type.Name.equals(name))
				{
					return type;
				}
			}
		}
		return null;
	}

	public List<ParameterType> GetParameterTypesData()
	{
		PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
		List<ParameterType> types=null;
		try
		{
		String sql = "select from " + VP_ParameterType.class.getName();
		Query query=pm.newQuery(sql);	 		
		List<VP_ParameterType> vTypes = (List<VP_ParameterType>) query.execute();
		vTypes.size();
		types=GetSerializableParameterType(vTypes);
		query.closeAll();
		}
		finally
		{
			pm.close();
		}		
		return types;		
	}

	public ParameterType GetSerializableParameterType(VP_ParameterType type) 
	{
		ParameterType sType=new ParameterType();
		sType.Name=type.Name;
		sType.Description=type.Description;
		sType.ModelName=type.ModelName;
		sType.Accession=type.Accession;	
		return sType;
	}

	public List<ParameterType> GetSerializableParameterType(List<VP_ParameterType> types)
	{
		List<ParameterType> serializableTypes=new ArrayList<ParameterType>();
		for (VP_ParameterType type:types)
		{
			ParameterType serializableType=GetSerializableParameterType(type);
			serializableTypes.add(serializableType);
		}	
		return serializableTypes;
	}
}

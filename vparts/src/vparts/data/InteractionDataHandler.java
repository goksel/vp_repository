package vparts.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import vpart.exception.GenericException;
import vparts.entity.VP_Constraint;
import vparts.PartsHandler;
import vparts.Util;
import vparts.VpartsPersistenceManagerFactory;
import vparts.cache.Cacher;
import vparts.dataimport.InteractionReader;
import virtualparts.entity.Constraint;
import virtualparts.entity.Formula;
import virtualparts.entity.Interaction;
import virtualparts.entity.InteractionPartDetail;
import virtualparts.entity.Parameter;
import virtualparts.entity.Part;
import virtualparts.model.ModelOutput;
import vparts.entity.VP_Formula;
import vparts.entity.VP_Interaction;
import vparts.entity.VP_InteractionPartDetail;
import vparts.entity.VP_Model;
import vparts.entity.VP_Parameter;
import vparts.entity.VP_Part;
import vparts.model.ModelHandler;

public class InteractionDataHandler {

	public Boolean CreateInteractions(List<Interaction> interactions) throws Exception {
		PersistenceManager pm = null;		
		PartDataHandler handler=new PartDataHandler();
		Boolean result = false;
		try {

			for (Interaction interaction : interactions) {
				pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
				VP_Interaction vpartInteraction = GetInteraction(interaction);
				if (interaction.getPartDetails()!=null)
				{
					for (InteractionPartDetail sPartDetail : interaction.getPartDetails()) {
						VP_InteractionPartDetail partDetail=GetInteractionPartDetail(sPartDetail);																		
						vpartInteraction.AddInteractionPartDetail(partDetail);
					}
				}							
				//Also serialize part names for easy access
				String partNames=Util.Join(interaction.getParts(), ",");
				vpartInteraction.setPartNames(partNames);
				
				if (interaction.getConstraints()!=null && interaction.getConstraints().size()>0)
				{
					for (Constraint sConstraint: interaction.getConstraints()) {
						VP_Constraint constraint=GetConstraint(sConstraint);
						pm.makePersistent(constraint);
						vpartInteraction.AddConstraint(constraint.getId());						
					}
				}
				
				//Add the interaction parameters
				if (interaction.getParameters()!=null && interaction.getParameters().size()>0)
				{
					for (Parameter parameter: interaction.getParameters()) {
						VP_Parameter newP=new VP_Parameter(parameter.getParameterType(), parameter.getName(), parameter.getValue(),parameter.getScope(),parameter.getEvidenceType());
						vpartInteraction.AddParameter(newP);			
					}
				}				
				pm.makePersistent(vpartInteraction);
				
				for (String part: interaction.getParts()) {					
					VP_Part vpart=handler.GetVPart(part,pm);
					vpartInteraction.AddPart(vpart);
					vpart.AddInteraction(vpartInteraction.getId());//TODO GMGMGM Do we need this anymore					
				}
				//break;
			}
			result = true;
		} catch (Exception ex) {
			ex.printStackTrace();
			result = false;
			throw ex;
		} finally {

			pm.close();
		}
		return result;
	}	
	
	public VP_InteractionPartDetail GetInteractionPartDetail(InteractionPartDetail sPartDetail)
	{
		VP_InteractionPartDetail partDetail=new VP_InteractionPartDetail();
		partDetail.setInteractionRole(sPartDetail.getInteractionRole());
		partDetail.setMathName(sPartDetail.getMathName());
		partDetail.setNumberOfMolecules(sPartDetail.getNumberOfMolecules());
		partDetail.setPartForm(sPartDetail.getPartForm());
		partDetail.setPartName(sPartDetail.getPartName());	
		return partDetail;
	}
	public VP_Interaction GetInteraction(Interaction interaction)
	{
		VP_Interaction vpartInteraction = new VP_Interaction();
		vpartInteraction.setName(interaction.getName());
		vpartInteraction.setDescription(interaction.getDescription());
		vpartInteraction.setFreeTextMath(interaction.getFreeTextMath());
		vpartInteraction.setInteractionType(interaction.getInteractionType());
		vpartInteraction.setMath(interaction.getMathName());
		vpartInteraction.setReaction(interaction.getIsReaction());	
		vpartInteraction.setInternal(interaction.getIsInternal());
		vpartInteraction.setReversible(interaction.getIsReversible());
		return vpartInteraction;
	}
	public Interaction GetSerializableInteraction(VP_Interaction interaction)
	{
		Interaction serializableInteraction=new Interaction();
		serializableInteraction.setDescription(interaction.getDescription());
		serializableInteraction.setInteractionType(interaction.getInteractionType());
		serializableInteraction.setFreeTextMath(interaction.getFreeTextMath());
		serializableInteraction.setIsReaction(interaction.isReaction());
		serializableInteraction.setIsReversible(interaction.isReversible());		
		serializableInteraction.setName(interaction.getName());
		serializableInteraction.setIsInternal(interaction.isInternal());
		serializableInteraction.setMathName(interaction.getMath());
		String[] partNames=interaction.getPartNames().split(",");
		serializableInteraction.setParts(Arrays.asList(partNames));
		//TODO Parameters, part details and constraints are not added. Since the SerializabaleInteraction is simply used to represent the metyadata during teh serialization.
		//If need be,replace SerializableInteraction in this method with a small version of SerializableInetraction and just use that class 		
		return serializableInteraction;
	}
	
	public Parameter GetSerializableParameter(VP_Parameter parameter)
	{
		Parameter serializableParameter=new Parameter();
		serializableParameter.setEvidenceType(parameter.getEvidenceType());
		// GMGM 20130109 serializableParameter.setMolecularForm(parameter.getMolecularForm());
		serializableParameter.setName(parameter.getName());
		serializableParameter.setParameterType(parameter.getParameterType());
		serializableParameter.setScope(parameter.getScope());
		serializableParameter.setValue(parameter.getValue());
		return serializableParameter;
	}
	
	public InteractionPartDetail GetSerializableInteractionPartDetail(VP_InteractionPartDetail partDetail)
	{
		InteractionPartDetail serializablePartDetail=new InteractionPartDetail();
		serializablePartDetail.setInteractionRole(partDetail.getInteractionRole());
		serializablePartDetail.setMathName(partDetail.getMathName());
		serializablePartDetail.setNumberOfMolecules(partDetail.getNumberOfMolecules());
		serializablePartDetail.setPartForm(partDetail.getPartForm());
		serializablePartDetail.setPartName(partDetail.getPartName());
		return serializablePartDetail;
	}

	public Interaction GetInteractionDetailed(String interactionName)
	{
		PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();		
		Interaction interaction=null;
		try
		{
		Query query=null;
		
		String queryString = "select from " + VP_Interaction.class.getName() + " where name==\"" + interactionName + "\"";
		query=pm.newQuery(queryString);
		query.setClass(VP_Interaction.class);
		query.setUnique(true);
		VP_Interaction vInteraction= (VP_Interaction) query.execute();	
		interaction=GetSerializableInteraction(vInteraction);
		
		List<Parameter> serializableParameters=GetInteractionParameters(pm,vInteraction);				
		List<InteractionPartDetail> serializablePartDetails=GetInteractionPartDetails(pm, vInteraction);
				
		interaction.setParameters(serializableParameters);
		interaction.setPartDetails(serializablePartDetails);	
		query.closeAll();
		}
		finally
		{
			pm.close();
		}
		return interaction;
	}
	
	public List<Interaction> GetPartInteractionsDetailed(String partName) throws Exception
	{	
		PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
		List<Interaction> interactions=null;
		try
		{
			String queryString=String.format("select from %s where parts.contains(p) && p.name == '%s'",VP_Interaction.class.getName(),partName);
			Query query=pm.newQuery(queryString);
			query.declareVariables(VP_Part.class.getName() + " p");
			List<VP_Interaction> vInteractions=   (List<VP_Interaction>) query.execute();
			vInteractions.size();
			interactions=new ArrayList<Interaction>(vInteractions.size());
			for (VP_Interaction vInteraction:vInteractions)
			{
				Interaction interaction=GetSerializableInteraction(vInteraction);
				List<Parameter> serializableParameters=GetInteractionParameters(pm, vInteraction);					
				List<InteractionPartDetail> serializablePartDetails=GetInteractionPartDetails(pm, vInteraction);
						
				interaction.setParameters(serializableParameters);
				interaction.setPartDetails(serializablePartDetails);	
				interactions.add(interaction);
			}
			
			query.closeAll();
		}
		finally
		{
			pm.close();		
		}
		return interactions;
	}
		

	public List<Interaction> GetPartInteractions(int id) throws Exception
	{	
		PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
		List<Interaction> interactions=null;
		try
		{
			String queryString=String.format("select from %s where parts.contains(p) && p.id == %d",VP_Interaction.class.getName(),id);
			Query query=pm.newQuery(queryString);
			query.declareVariables(VP_Part.class.getName() + " p");
			List<VP_Interaction> vInteractions=   (List<VP_Interaction>) query.execute();
			vInteractions.size();
			InteractionDataHandler iHandler=new InteractionDataHandler();
			interactions=iHandler.GetSerializableInteractions(vInteractions);
			query.closeAll();
		}
		finally
		{
			pm.close();		
		}
		return interactions;
	}
	
public List<Interaction> GetPartInteractions(String name) throws Exception
{	
	PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
	List<Interaction> interactions=null;
	try
	{
		interactions=GetPartInteractions(pm,name);
	}
	finally
	{
		pm.close();		
	}
	return interactions;
}

public List<Interaction> GetPartInteractions(PersistenceManager pm, String name) throws Exception
{	
	List<Interaction> interactions=null;
	String queryString=String.format("select from %s where parts.contains(p) && p.name == '%s'",VP_Interaction.class.getName(),name);
	Query query=pm.newQuery(queryString);
	query.declareVariables(VP_Part.class.getName() + " p");
	List<VP_Interaction> vInteractions=   (List<VP_Interaction>) query.execute();
	vInteractions.size();
	InteractionDataHandler iHandler=new InteractionDataHandler();
	interactions=iHandler.GetSerializableInteractions(vInteractions);
	query.closeAll();	
	return interactions;
}
	
	public List<Interaction>  GetSerializableInteractions(List<VP_Interaction> vInteractions) throws Exception
	{
		List<Interaction> interactions=new ArrayList<Interaction>(vInteractions.size());
		for (VP_Interaction vInteraction:vInteractions)
		{
			interactions.add(GetSerializableInteraction(vInteraction));
		}
		return interactions;
	}
	
	public VP_Constraint GetConstraint (Constraint sConstraint)
	{
		VP_Constraint constraint=new VP_Constraint();
		constraint.setQualifier(sConstraint.getQualifier());
		constraint.setSourceID(sConstraint.getSourceID());
		constraint.setSourceType(sConstraint.getSourceType());
		constraint.setTargetID(sConstraint.getTargetID());
		constraint.setTargetType(sConstraint.getTargetType());		
		return constraint;		
	}
	
	public List<Interaction> GetInteractions() throws Exception
	{		
		List<Interaction> interactions=null;
		PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
		try
		{
			String sql = "select from " + VP_Interaction.class.getName() +  " where isInternal!=\"true\" order by isInternal,interactionType";
			Query query=pm.newQuery(sql);
			List<VP_Interaction> vInteractions= (List<VP_Interaction>) query.execute();		
			vInteractions.size();	
			interactions=GetSerializableInteractions(vInteractions);
			query.closeAll();
		}		
		finally
		{
			pm.close();
		}
		return interactions;		
	}
	
	public List<VP_Interaction> GetInteractions(int pageNumber, String interactionTypeString)
	{
		List<VP_Interaction> interactions=null;
		PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
		Query query=null;
		try
		{
			int pageSize=50;
			int start = pageNumber * pageSize - pageSize;
			int end=start + pageSize;
			
			String sql = "select from " + VP_Interaction.class.getName() +  " where isInternal!=\"true\" && interactionType == '" + interactionTypeString + "' RANGE " + start + ","  + end;
			query=pm.newQuery(sql);
			interactions = (List<VP_Interaction>) query.execute();
			interactions.size();
		}
		finally
		{
			pm.close();
		}		
		return interactions;		
	}
	
	public List<VP_Interaction> GetInteractions(int pageNumber) 
	{
		List<VP_Interaction> interactions=null;
		Query query=null;
		PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
		try
		{
			int pageSize=50;
			int start = pageNumber * pageSize - pageSize;
			int end=start + pageSize;
			String sql = "select from " + VP_Interaction.class.getName() +  " where isInternal!=\"true\" order by isInternal,interactionType RANGE " + start + ","  + end;
			query=pm.newQuery(sql);
			interactions = (List<VP_Interaction>) query.execute();
			interactions.size();		
			}
		finally
		{
			//GMGMGM query.closeAll();
			pm.close();
		}
		return interactions;	
	}

	public int GetInteractionsCount() throws Exception
	{		
		int count=-1;
		Object countObject=Cacher.Get("InteractionCount");
		if (countObject!=null)
		{
			count=(Integer) countObject;
		}
		else
		{
			PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
			Query query=null;
			try
			{
				query = pm.newQuery("select from " + VP_Interaction.class.getName() +  " where isInternal!=\"true\""); 
				query.setResult("count(id)"); 
				Object result=(Object)query.execute();
				count= Integer.parseInt(result.toString());
				Cacher.Put(count,"InteractionCount");
			}
			finally
			{
				pm.close();
			}
		}
		return count;
	}
	
	public int GetInteractionsCount(String interactionTypeString) throws GenericException
	{
		int count=-1;
		Object countObject=Cacher.Get("InteractionCount_" + interactionTypeString);
		if (countObject!=null)
		{
			count=(Integer) countObject;
		}
		else
		{
			PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
			Query query=null;
			try
			{
				query = pm.newQuery("select from " + VP_Interaction.class.getName() +  " where isInternal!=\"true\" && interactionType == '" + interactionTypeString + "'"); 
				query.setResult("count(id)"); 
				Object result=(Object)query.execute();
				count= Integer.parseInt(result.toString());
				Cacher.Put(count,"InteractionCount_" + interactionTypeString);
			}
			finally
			{
				pm.close();
			}
		}
		return count;
		
	}
	
	public List<Part> GetInteractionParts(String interactionName) throws Exception
	{
		int interactionId=GetInteractionId(interactionName);
		PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
		List<Part> parts=null;
		Query query=null;
		try
		{
			String sqlQuery = "select * from " + VP_Part.class.getSimpleName().toUpperCase() +  " p inner join VP_INTERACTION_PARTS ip ON p.ID=ip.PART_ID where ip.INTERACTION_ID=" + interactionId ;		
			query=pm.newQuery("javax.jdo.query.SQL",sqlQuery);
			query.setClass(VP_Part.class);		
			List<VP_Part> vparts= (List<VP_Part>) query.execute();		
			vparts.size();
			PartDataHandler partsHandler=new PartDataHandler();			
			parts=partsHandler.GetSerializableParts(vparts);
			query.closeAll();
		}
		finally
		{
			pm.close();
		}
		return parts;			
	}
	
	public List<Part> GetInteractionParts(int partId) throws Exception
	{
		PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
		List<Part> parts=null;
		Query query=null;
		try
		{
			String sqlQuery = "select distinct otherparts.id as id, otherparts.* from VP_INTERACTION_PARTS ip inner join VP_INTERACTION_PARTS iotherparts ON ip.interaction_id=iotherparts.interaction_id inner join VP_PART otherparts ON  iotherparts.part_id=otherparts.id where ip.part_id=" + partId ;		
			query=pm.newQuery("javax.jdo.query.SQL",sqlQuery);
			query.setClass(VP_Part.class);		
			List<VP_Part> vparts= (List<VP_Part>) query.execute();		
			vparts.size();
			PartDataHandler pHandler=new PartDataHandler();
			parts=pHandler.GetSerializableParts(vparts);
			query.closeAll();			
		}
		finally
		{
			pm.close();
		}
		return parts;			
	}
	
	public int GetInteractionId(String name)
	{
		PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
		int id=-1;
		
		try
		{
			String queryString = "select from " + VP_Interaction.class.getName() + " where name==\"" + name + "\"";
			Query query=pm.newQuery(queryString);
			query.setResult("id");
			List result=(List)query.execute();
			Object idObject=result.get(0);
			id=Integer.parseInt(idObject.toString());			
			query.closeAll();			
		}
		finally
		{
			pm.close();		
		}
		return id;		
	}
	
	public Interaction GetInteraction(String name)
	{
		PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
		Interaction interaction=null;
		try
		{
			interaction=GetInteraction(pm,name);
		}
		finally
		{
			pm.close();		
		}
		return interaction;		
	}
		
	
	public Interaction GetInteraction(PersistenceManager pm, String name)
	{
		Query query=null;
		Interaction interaction=null;
		String queryString = "select from " + VP_Interaction.class.getName() + " where name==\"" + name + "\"";
		query=pm.newQuery(queryString);
		query.setClass(VP_Interaction.class);
		query.setUnique(true);
		VP_Interaction vInteraction= (VP_Interaction) query.execute();	
		interaction=GetSerializableInteraction(vInteraction);
		query.closeAll();
		return interaction;		
	}
	
	
	public VP_Interaction GetVInteraction(PersistenceManager pm, String name)
	{
		Query query=null;
		String queryString = "select from " + VP_Interaction.class.getName() + " where name==\"" + name + "\"";
		query=pm.newQuery(queryString);
		query.setClass(VP_Interaction.class);
		query.setUnique(true);
		VP_Interaction vInteraction= (VP_Interaction) query.execute();	
		query.closeAll();
		return vInteraction;		
	}
		
	private List<InteractionPartDetail> GetInteractionPartDetails(PersistenceManager pm, VP_Interaction interaction)
	{		
		Query query=null;
		
			String sqlQuery = "select * from " + VP_InteractionPartDetail.class.getSimpleName().toUpperCase() +  " where INTERACTION_ID=" + interaction.getId() ;
			query=pm.newQuery("javax.jdo.query.SQL",sqlQuery);
			query.setClass(VP_InteractionPartDetail.class);
			List<VP_InteractionPartDetail> vPartDetails= (List<VP_InteractionPartDetail>) query.execute();		
			vPartDetails.size();
			
			List<InteractionPartDetail> partDetails=new ArrayList<InteractionPartDetail>(vPartDetails.size());		
			for (VP_InteractionPartDetail partDetail:vPartDetails)
			{
				partDetails.add(GetSerializableInteractionPartDetail(partDetail));
			}			
			query.closeAll();
		
		return partDetails;		
	}

	public List<VP_Constraint> GetInteractionConstraints (VP_Interaction interaction)
	{
		List<VP_Constraint> constraints=new ArrayList<VP_Constraint>();		
		if (interaction!=null && interaction.getConstraints()!=null && interaction.getConstraints().size()>0)
		{
			PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
			try
			{
				Iterator<Integer> iterator=interaction.getConstraints().iterator();
				while (iterator.hasNext())
				{
					int id=iterator.next();
					VP_Constraint constraint= pm.getObjectById(VP_Constraint.class, id);
					constraints.add(constraint);
				}		
			}
			finally
			{
				pm.close();
			}
		}
		return constraints;			
	}
	
	public List<Parameter> GetInteractionParameters(PersistenceManager pm, VP_Interaction interaction)
	{		
		Query query=null;
		
			String sqlQuery = "select * from " + VP_Parameter.class.getSimpleName().toUpperCase() +  " where parameterHolderId=" + interaction.getId() ;
			query=pm.newQuery("javax.jdo.query.SQL",sqlQuery);
			query.setClass(VP_Parameter.class);
			List<VP_Parameter> vParameters= (List<VP_Parameter>) query.execute();		
			vParameters.size();
			
			List<Parameter> parameters=new ArrayList<Parameter>();		
			for (VP_Parameter vParameter:vParameters)
			{
				parameters.add(GetSerializableParameter(vParameter));
			}
			
		query.closeAll();
		return parameters;				
	}
	
	

	public Formula GetFormula(VP_Interaction interaction) throws Exception
	{
		MathDataHandler mathDataHandler=new MathDataHandler();
		
		//Get the reaction formula
		Formula formula=mathDataHandler.GetFormula(interaction.getMath());
		return formula;		
	}
	
	public void ImportInteractions(String content)throws Exception
	{
		InteractionReader reader = new InteractionReader(content,"data");
		List<Interaction> interactions= reader.GetInteractions();		
		CreateInteractions(interactions);
		Cacher.Clear();
	}
	
	/**
	 * Clones interactions of a part given with the templatePartID to the given part objects without committing anything to the database
	 * @param part
	 * @param templatePartID
	 * @return
	 */
	/*public List<Interaction> CloneInteractionsFrom(VP_Part part, String templatePartID) throws Exception
	{
		PartDataHandler handler=new PartDataHandler();
		List<Interaction> partInteractions=handler.GetPartInteractions(Integer.parseInt(templatePartID));
		for (int i=0;i<partInteractions.size();i++)
		{
			Interaction interaction=partInteractions.get(i);
			String interactionName=interaction.getName().replace(templatePartID, part.getName());
			interaction.setName(interactionName);
			partInteractions.set(i, interaction);
		}
		return partInteractions;
	}
	*/
	private HashSet<String> GetInteractionTypeList() 
	{
		HashSet<String> types=new HashSet<String>();
		PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
		Query query=null;
		try
		{
			String sql = "select from " + VP_Interaction.class.getName() +  " where isInternal!=\"true\"";
			query=pm.newQuery(sql);
			query.setResult("distinct interactionType");
			List result = (List) query.execute();		
			result.size();		
			types.addAll(result);
		}
		finally
		{
			query.closeAll();
			pm.close();
		}
		return types;
	}	
	
	public HashSet<String> GetInteractionTypes() throws GenericException
	{		
		HashSet<String> uniqueTypes=new HashSet<String>();
		Object uniqueTypesObject=Cacher.Get("InteractionTypes");
		if (uniqueTypesObject!=null)
		{
			uniqueTypes=(HashSet<String>) uniqueTypesObject;
		}
		else
		{
			uniqueTypes= GetInteractionTypeList();			
			Cacher.Put(uniqueTypes, "InteractionTypes");
		}
		return uniqueTypes;
	}			
	
	
	/**
	 * Creates the models for parts that do not have models stored in the database
	 * @throws Exception
	 */
	public void CreateInteractionModels() throws Exception
	{
		PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();	 
		try
		{					
			String sqlQuery="SELECT i.id as id, i.* from VP_INTERACTION i" +
					" left outer join VP_INTERACTION_MODELS im ON im.INTERACTION_ID=i.id" +
					" WHERE i.isInternal='false' AND im.INTERACTION_ID is NULL";
			
			Query query=pm.newQuery("javax.jdo.query.SQL",sqlQuery);
			query.setClass(VP_Interaction.class);
			List<VP_Interaction> interactions=(List<VP_Interaction>)query.execute();
			
			CreateModels(interactions);
			
			query.closeAll();
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
			throw exception;
		}
		finally
		{
			pm.close();
		}	
	}
	
	private void CreateModels(List<VP_Interaction> interactions) throws Exception
	{
			int i=0;
			for (VP_Interaction interaction:interactions)
			{
				if (!interaction.isInternal())
				{
					try
					{
						VP_Model model=GetModel(interaction.getName(),"sbml");		
						interaction.AddModel(model);
						
						VP_Model modelKappa=GetModel(interaction.getName(),"kappa");		
						interaction.AddModel(modelKappa);
						
						i++;
						if (i%100==0)
						{
							Logger.getLogger(InteractionDataHandler.class.getName()).log(Level.INFO, "Creating the model for interaction  "+ i);
						}
					}
					catch(Exception exception)
					{
						throw new Exception ("Could not create the model for interaction:" + interaction.getName() + "." + exception.getMessage() , exception);
					}
				}
			}	
		}
	
	

	public ModelOutput GetModelOutput(String partName,String modelType) throws Exception
	{
		ModelOutput modelOutput=null;
		PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
		try
		{					
			String sqlQuery="SELECT model.text from VP_MODEL model" +
					" inner join VP_INTERACTION_MODELS im ON im.MODEL_ID=model.id" +
					" inner join VP_INTERACTION interaction ON im.INTERACTION_ID=interaction.id" +
					" WHERE interaction.name='" + partName + "'" +
					" and model.type='" + modelType + "'";
			
			Query query=pm.newQuery("javax.jdo.query.SQL",sqlQuery);
			List result=(List)query.execute();
			if (result!=null && result.size()>0)
			{
				String text= (String)result.get(0);
				modelOutput=new ModelOutput();
				modelOutput.setMain(text);		
			}			
			query.closeAll();
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
			throw exception;
		}
		finally
		{
			pm.close();
		}
		return modelOutput;
	}
	
	
	private VP_Model GetModel(String interactionName, String modelType) throws Exception
	{	
		ModelHandler modelHandler=new ModelHandler();
		ModelOutput modelOutput=modelHandler.GetInteractionModelFromData(interactionName, modelType);
		VP_Model model=new VP_Model();
		model.setType(modelType);
		model.setText(modelOutput.getMain());
		return model;
	}
}

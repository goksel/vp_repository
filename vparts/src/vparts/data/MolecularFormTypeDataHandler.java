package vparts.data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import vparts.VpartsPersistenceManagerFactory;
import vparts.cache.Cacher;
import virtualparts.entity.MolecularFormType;
import vparts.entity.VP_MolecularFormType;

public class MolecularFormTypeDataHandler {
	private static String DEFAULT_FORM_NAME="Default";
@SuppressWarnings("unchecked")
public List<MolecularFormType> GetTypes() throws Exception
	{
		List<MolecularFormType> types=null;
		Object molecularForms=Cacher.Get(Cacher.MOLECULAR_FORM_TYPE_KEY);	
		if (molecularForms==null)
		{			
			types=GetTypesData();						
			//Cacher.Put(types.get(0), Cacher.MOLECULAR_FORM_TYPE_KEY);
			Cacher.Put(types, Cacher.MOLECULAR_FORM_TYPE_KEY);			
		}
		else
		{
			types=(List<MolecularFormType>) molecularForms;
		}
		return types;
	}

public MolecularFormType GetType(String name) throws Exception
{

	return ( GetType(name, GetTypes()));
	
}

private MolecularFormType GetType (String name,List<MolecularFormType> types)
{
	if (types!=null&& types.size()>0)
	{
		for (MolecularFormType type : types)
		{
			if (type.getName().equals(name))
			{
				return type;
			}
		}
	}
	return null;
}

private List<MolecularFormType> GetTypesData() throws Exception
{
	PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
	List<MolecularFormType> types=null;
	try
	{
		String sql = "select from " + VP_MolecularFormType.class.getName();
		Query query=pm.newQuery(sql);	 
		List<VP_MolecularFormType> vTypes = (List<VP_MolecularFormType>) pm.newQuery(query).execute();
		vTypes.size();
		types=GetSerializableMolecularFormType(vTypes);
		query.closeAll();
	}
	finally
	{
		pm.close();
	}
	return types;		
}

public MolecularFormType GetSerializableMolecularFormType(VP_MolecularFormType type) throws Exception
{
	MolecularFormType sType=new MolecularFormType();
		sType.setName(type.getName());
		sType.setExtension(type.getExtension());
		sType.setAccession(type.getAccession());
		
	return sType;
}

public List<MolecularFormType> GetSerializableMolecularFormType(List<VP_MolecularFormType> types) throws Exception
{
	List<MolecularFormType> serializableTypes=new ArrayList<MolecularFormType>();
	for (VP_MolecularFormType type:types)
	{
		MolecularFormType serializableType=GetSerializableMolecularFormType(type);
		serializableTypes.add(serializableType);
	}	
	return serializableTypes;
}

public MolecularFormType GetDefaultType() throws Exception
{
	return ( GetType("Default", GetTypes()));	
}

public static String GetVisualFormName(VP_MolecularFormType formType)
{
	String formName="";
	if (!formType.getName().equals(DEFAULT_FORM_NAME))
	{
		formName=formType.getName();					
	}
	return formName;
}

}


package vparts.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import com.hp.hpl.jena.graph.impl.Fragments.GetSlot;

import virtualparts.entity.Property;
import vparts.VpartsPersistenceManagerFactory;
import vparts.entity.VP_Role;
import vparts.entity.VP_Part;

public class PropertyDataHandler {
public static String ROLE_SHIM_RBS_UPSTREAM="RBS_upstream";
public static String ROLE_SHIM_RBS_DOWNSTREAM="RBS_downstream";

public HashMap<String,List<Property>> GetRoleIDsByPartType(String partType)
{	
	PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
	String sqlQuery = "select distinct r.name, r.value,r.description from VP_ROLE r inner join VP_PART p on r.part_id= p.id where p.type ='" + partType + "'";
	Query roleQuery=pm.newQuery("javax.jdo.query.SQL",sqlQuery);
	List roles= (List)roleQuery.execute();		
	roles.size();
	pm.close();
	
	HashMap<String,List<Property>> groupedRoleMap=new HashMap<String,List<Property>>();
	Iterator iterator=roles.iterator();
	while(iterator.hasNext())
	{
		Object[] roleDetails=(Object[])iterator.next();
		String roleName=(String)roleDetails[0];
		String roleValue=(String)roleDetails[1];
		String roleDescription=(String)roleDetails[2];
		
		List<Property> groupedRoles=groupedRoleMap.get(roleName);
		if (groupedRoles==null)
		{
			groupedRoles=new ArrayList<Property>();
		}
		Property property=new Property();
		property.setName(roleName);
		property.setValue(roleValue);
		property.setDescription(roleDescription);		
		groupedRoles.add(property);
		groupedRoleMap.put(roleName, groupedRoles);		
	}		
	return groupedRoleMap;
				
}

public List<Property> GetRolesByPartID(int partID) throws Exception
{
	PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
	List<Property> properties=null;
	try
	{
		
	String sqlQuery = "select * from VP_ROLE where PART_ID =" + partID;
	Query roleQuery=pm.newQuery("javax.jdo.query.SQL",sqlQuery);
	roleQuery.setClass(VP_Role.class);
	List<VP_Role> roles= (List<VP_Role>) roleQuery.execute();
	roles.size();
	properties=GetSerialisedProperties(roles);
	roleQuery.closeAll();
	}
	finally
	{
		pm.close();
	}
	return properties;
}

public Property GetRole(String roleName, String roleValue) throws Exception
{
	Property property=new Property();
	//TODO 20130807 From Cache maybe!
	PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
	//String dbQuery = "select from " + VP_Role.class.getName() + " where name==\"" + role.getName() + "\" && value==\"" + role.getValue() + "\"";
	String dbQuery = "select name, value, description from VP_ROLE where name='" + roleName + "' and value='" + roleValue + "' limit 1";
	
	try	
	{		
		Query query = pm.newQuery("javax.jdo.query.SQL",dbQuery);
		query.setUnique(true);
		Object[] result = (Object[]) query.execute();	
		if (result!=null)
		{
			property.setName(result[0].toString());
			property.setValue(result[1].toString());
			if (result[2]!=null)
			{
				property.setDescription(result[2].toString());	
			}
		}
		query.closeAll();
	}
	catch (Exception exception)
	{
		exception.printStackTrace();
		throw exception;		
	}
	finally
	{
		pm.close();	
	}
	
	return property;
}

public List<Property> GetSerialisedProperties(List<VP_Role> roles)
{
	List<Property> properties =new ArrayList<Property>(roles.size());
	for (VP_Role role:roles)
	{		
		Property sProperty=new Property();
		sProperty.setName(role.getName());
		sProperty.setValue(role.getValue());
		sProperty.setDescription(role.getDescription());
		properties.add(sProperty);
	}
	return properties;
}

}

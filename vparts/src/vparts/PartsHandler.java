package vparts;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import vparts.data.PartDataHandler;
import vparts.data.PropertyDataHandler;
import virtualparts.entity.Part;
import virtualparts.entity.Property;
import vparts.entity.VP_Annotation;
import vparts.entity.VP_Formula;
import vparts.entity.VP_Interaction;
import vparts.entity.VP_InteractionPartDetail;
import vparts.entity.VP_Model;
import vparts.entity.VP_MolecularForm;
import vparts.entity.VP_MolecularFormType;
import vparts.entity.VP_Parameter;
import vparts.entity.VP_ParameterType;
import vparts.entity.VP_Role;
import vparts.entity.VP_Constraint;
import vparts.entity.VP_Part;

public class PartsHandler {
	private PersistenceManager pm = null;
	private static final Logger log = Logger.getLogger(PartsHandler.class.getName());
		
	
	public List<VP_Role> GetRoles()
	{
		PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();
		String query = "select from " + VP_Role.class.getName();
		List<VP_Role> roles = (List<VP_Role>) pm.newQuery(query).execute();
		return roles;		
	}
	
	public Boolean CreateParameterTypes(List<VP_ParameterType> parameterTypes)
	{
		Boolean result=false;
		try {

			pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();		
			pm.makePersistentAll(parameterTypes);
			result= true;
		} catch (Throwable t) {

			t.printStackTrace();			
			result= false;
		} finally {

			pm.close();
		}	
		return result;
	}
	
	
	public Boolean CreateMolecularFormTypes(List<VP_MolecularFormType> types)
	{
		Boolean result=false;
		try {
			pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();					
			pm.makePersistentAll(types);
			result= true;
		} catch (Throwable t) {

			t.printStackTrace();			
			result= false;
		} finally {

			pm.close();
		}	
		return result;
	}	
	
	private VP_Part GetVPart(Part part) throws Exception
	{
		VP_Part vpart = new VP_Part();
		vpart.setName(GetName(part.getName()));
		vpart.setDescription(part.getDescription());
		vpart.setType(part.getType());
		vpart.setMetaType(part.getMetaType());
		vpart.setSequence(part.getSequence());
		vpart.setSequenceURI(part.getSequenceURI());
		vpart.setDisplayName(GetName(part.getDisplayName()));
		vpart.setStatus(part.getStatus());
		vpart.setOrganism(part.getOrganism());
		vpart.setDesignMethod(part.getDesignMethod());

		try
		{
			if (part.getProperties()!=null)
			{
				for (Property property: part.getProperties()) {
					VP_Role vpartRole=new VP_Role();
					vpartRole.setName(property.getName());
					vpartRole.setValue(property.getValue());
					vpartRole.setDescription(property.getDescription());									
					vpart.AddPartRole(vpartRole);								
				}		
			}
		}
		catch(Exception e)
		{
			throw new Exception ("Could not add the roles",e);
		}	
		return vpart;
	}
	
	private List<VP_Part> GetVParts(List<Part> parts) throws Exception
	{
		List<VP_Part> vparts=new ArrayList<VP_Part>();
		for (Part part:parts)
		{
			VP_Part vpart=GetVPart(part);
			vparts.add(vpart);
		}
		return vparts;				
	}
	private String GetName(String name)
	{
		if (name!=null && name.length()>255)
		{
			return name.substring(0,254);
		}
		else
		{
			return name;
		}
	}
	public boolean CreateParts(List<Part> parts) throws Exception{
		Boolean result = false;
		try {
			log.log(Level.INFO, "Starting to create the parts");
			pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();		
			PropertyDataHandler roleHandler=new PropertyDataHandler();
			log.log(Level.INFO, "Getting the VP list");
			List<VP_Part> vparts=GetVParts(parts);
			log.log(Level.INFO, "Created the VP list. Persisting the SVPs");			
			//pm.makePersistentAll(vparts);
			
			for (VP_Part vpart:vparts)
			{
				try
				{
					pm.makePersistent(vpart);			
				}
				catch (Exception e)
				{
					PartDataHandler dataHandler=new PartDataHandler();
					Part duplicatePart=dataHandler.GetPart(vpart.getName());
					if (duplicatePart!=null)
					{
						log.log(Level.INFO, vpart.getName() +  " is ignored since it exists in the database");
										
					}
					else
					{
						e.printStackTrace();
						throw new Exception ("Could not import part " + vpart.getName() + "." + e.getMessage(),e);
					}
				}
			}
			result = true;
			log.log(Level.INFO, "Added the parts");
		} 
		catch (Exception t) {
			t.printStackTrace();
			result = false;
			throw t;
		} 
		finally {

			if (!pm.isClosed())
			{
				pm.close();
			}
		}
		return result;
	}	
	
	/*public Boolean CreateParts(List<Part> parts) throws Exception{
		Boolean result = false;
		try {
			log.log(Level.INFO, "Starting to create the parts");
			pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();		
			PropertyDataHandler roleHandler=new PropertyDataHandler();
			for (Part part : parts) {
				try
				{
					VP_Part vpart = new VP_Part();
					vpart.setName(part.getName());
					vpart.setDescription(part.getDescription());
					vpart.setType(part.getType());
					vpart.setMetaType(part.getMetaType());
					vpart.setSequence(part.getSequence());
					vpart.setSequenceURI(part.getSequenceURI());
					vpart.setDisplayName(part.getDisplayName());
					vpart.setStatus(part.getStatus());
					vpart.setOrganism(part.getOrganism());
					vpart.setDesignMethod(part.getDesignMethod());

					try
					{
						if (part.getProperties()!=null)
						{
							for (Property property: part.getProperties()) {
								VP_Role vpartRole=new VP_Role();
								vpartRole.setName(property.getName());
								vpartRole.setValue(property.getValue());
								vpartRole.setDescription(property.getDescription());									
								vpart.AddPartRole(vpartRole);								
							}		
						}
					}
					catch(Exception e)
					{
						throw new Exception ("Could not add the roles",e);
					}				
					pm.makePersistent(vpart);
					result=true;
				}
				catch (Exception e)
				{
					PartDataHandler dataHandler=new PartDataHandler();
					Part duplicatePart=dataHandler.GetPart(part.getName());
					if (duplicatePart!=null)
					{
						log.log(Level.INFO, part.getName() +  " is ignored since it exists in the database");
										
					}
					else
					{
						e.printStackTrace();
						throw new Exception ("Could not import part " + part.getName() + "." + e.getMessage(),e);
					}
				}
			}
			result = true;
		} catch (Exception t) {
			t.printStackTrace();
			result = false;
			throw t;
		} finally {

			if (!pm.isClosed())
			{
				pm.close();
			}
		}
		return result;
	}	*/
	
	/*public VP_MolecularForm GetMolecularForm (MolecularForm sMolecularForm)
	{
		VP_MolecularForm form=new VP_MolecularForm();
		form.setName(sMolecularForm.getName());
		return form;		
	}*/
	
	private void DeleteTableContent(String tableName, PersistenceManager pm)
	{
		try
		{
			//String sqlQuery = "drop table VP_INTERACTION_PARAMETERS";
			String sqlQuery = "delete from " + tableName;			
			Query dbQuery=pm.newQuery("javax.jdo.query.SQL",sqlQuery);
			dbQuery.execute();	
		}
		catch(Exception exception)
		{
			log.log(Level.INFO, tableName + " has not been created yet and it is ignored for deletion");
		}
	}
	
	public void ClearData()
	{
		PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();	
		try
		{
			//DeleteTableContent("VP_INTERACTION_PARAMETERS", pm);
			DeleteTableContent("VP_PART_MODELS", pm);
			DeleteTableContent("VP_INTERACTION_MODELS", pm);
			DeleteTableContent(VP_Model.class.getSimpleName().toUpperCase(), pm);
			DeleteTableContent(VP_Annotation.class.getSimpleName().toUpperCase(), pm);
			DeleteTableContent(VP_Constraint.class.getSimpleName().toUpperCase(), pm);
			DeleteTableContent(VP_InteractionPartDetail.class.getSimpleName().toUpperCase(), pm);
			DeleteTableContent(VP_Parameter.class.getSimpleName().toUpperCase(), pm);
			DeleteTableContent("VP_INTERACTION_PARTS", pm);			
			DeleteTableContent(VP_Interaction.class.getSimpleName().toUpperCase(), pm);
 			DeleteTableContent(VP_Role.class.getSimpleName().toUpperCase(), pm);
			DeleteTableContent(VP_Part.class.getSimpleName().toUpperCase(), pm);
			DeleteTableContent(VP_MolecularForm.class.getSimpleName().toUpperCase(), pm);
			DeleteTableContent(VP_MolecularFormType.class.getSimpleName().toUpperCase(), pm);
			
			DeleteTableContent(VP_ParameterType.class.getSimpleName().toUpperCase(), pm);			
			DeleteTableContent(VP_Formula.class.getSimpleName().toUpperCase(), pm);
		}
		finally
		{
			pm.close();		
		}					
	}
	
	
	public void ClearData2Del()
	{
		PersistenceManager pm = VpartsPersistenceManagerFactory.get().getPersistenceManager();			
		String query="";
		int i=0;
		try
		{
			//String sqlQuery = "drop table VP_INTERACTION_PARAMETERS";
			String sqlQuery = "delete from VP_INTERACTION_PARAMETERS";			
			Query dbQuery=pm.newQuery("javax.jdo.query.SQL",sqlQuery);
			dbQuery.execute();	
		}
		catch(Exception exception)
		{
			log.log(Level.INFO, "VP_INTERACTION_PARAMETERS has not been created yet and it is ignored for deletion");
		}
		try
		{
			//String sqlQuery = "drop table VP_INTERACTION_PARAMETERS";
			String sqlQuery = "delete from VP_PART_MODELS";			
			Query dbQuery=pm.newQuery("javax.jdo.query.SQL",sqlQuery);
			dbQuery.execute();	
		}
		catch(Exception exception)
		{
			log.log(Level.INFO, "VP_PART_MODELS has not been created yet and it is ignored for deletion");
		}
		try
		{
			//String sqlQuery = "drop table VP_INTERACTION_PARAMETERS";
			String sqlQuery = "delete from VP_INTERACTION_MODELS";			
			Query dbQuery=pm.newQuery("javax.jdo.query.SQL",sqlQuery);
			dbQuery.execute();	
		}
		catch(Exception exception)
		{
			log.log(Level.INFO, "VP_INTERACTION_MODELS has not been created yet and it is ignored for deletion");
		}
		
		try
		{
			query = "delete from " + VP_InteractionPartDetail.class.getName();
			List<VP_InteractionPartDetail> interactionPartDetails = (List<VP_InteractionPartDetail>) pm.newQuery(query).execute();
			i=interactionPartDetails.size();		
			pm.deletePersistentAll(interactionPartDetails);			
		}
		catch(Exception exception)
		{
			log.log(Level.INFO, "VP_INTERACTIONPARTDETAIL has not been created yet and it is ignored for deletion");
		}
		
		
		query = "select from " + VP_Model.class.getName();
		List<VP_Model> models= (List<VP_Model>) pm.newQuery(query).execute();
		i=models.size();		
		pm.deletePersistentAll(models);
		
		query = "select from " + VP_Annotation.class.getName();
		List<VP_Annotation> annotations = (List<VP_Annotation>) pm.newQuery(query).execute();
		i=annotations.size();		
		pm.deletePersistentAll(annotations);
		
		query = "select from " + VP_Interaction.class.getName();
		List<VP_Interaction> interactions = (List<VP_Interaction>) pm.newQuery(query).execute();
		i=interactions.size();		
		pm.deletePersistentAll(interactions);
				
		query = "select from " + VP_Role.class.getName();
		List<VP_Role> roles = (List<VP_Role>) pm.newQuery(query).execute();
		i=roles.size();		
		pm.deletePersistentAll(roles);
		
		query = "select from " + VP_Part.class.getName();
		List<VP_Part> parts = (List<VP_Part>) pm.newQuery(query).execute();
		i=parts.size();		
		pm.deletePersistentAll(parts);
		
		query = "select from " + VP_MolecularForm.class.getName();
		List<VP_MolecularForm> forms = (List<VP_MolecularForm>) pm.newQuery(query).execute();
		i=forms.size();		
		pm.deletePersistentAll(forms);
		
		query = "select from " + VP_MolecularFormType.class.getName();
		List<VP_MolecularFormType> formTypes = (List<VP_MolecularFormType>) pm.newQuery(query).execute();
		i=formTypes.size();		
		pm.deletePersistentAll(formTypes);
		
		query = "select from " + VP_Parameter.class.getName();
		List<VP_Parameter> parameters = (List<VP_Parameter>) pm.newQuery(query).execute();
		i=parameters.size();	
		if (i>0)
		{
			pm.deletePersistentAll(parameters);
		}
		
		query = "select from " + VP_ParameterType.class.getName();
		List<VP_ParameterType> parameterTypes = (List<VP_ParameterType>) pm.newQuery(query).execute();
		i=parameterTypes.size();		
		pm.deletePersistentAll(parameterTypes);
		
		query = "select from " + VP_Constraint.class.getName();
		List<VP_Constraint> constraints = (List<VP_Constraint>) pm.newQuery(query).execute();
		i=constraints.size();		
		pm.deletePersistentAll(constraints);
		
		query = "select from " + VP_Formula.class.getName();
		List<VP_Formula> formulas = (List<VP_Formula>) pm.newQuery(query).execute();
		i=formulas.size();		
		pm.deletePersistentAll(formulas);
	
		pm.close();								
	}
	
}

package vparts;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

import javax.jdo.JDOObjectNotFoundException;

public class ErrorHandler {
	public static String GetErrorMesage(Throwable exception)
	{		
		String message="";
		if (exception instanceof JDOObjectNotFoundException)
		{
			message="The resource does not exists!";
		}
		else
		{
			message=exception.getMessage();	
		}
		return message;
	}
	public static String GetStackTrace(Throwable exception)
	{		
		String message="";		
		Writer writer=new StringWriter();
		PrintWriter pWriter=new PrintWriter(writer);
		exception.printStackTrace(pWriter);
		message=writer.toString();					
		return message;
	}
}

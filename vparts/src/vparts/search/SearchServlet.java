package vparts.search;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import virtualparts.entity.Part;
import virtualparts.entity.Property;
import virtualparts.serialize.Serializer;
import vpart.exception.ExceptionLogger;
import vparts.PartsHandler;
import vparts.data.PartDataHandler;
import vparts.entity.MetaType;

/**
 * Servlet implementation class SearchServlet
 */
public class SearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//The first time request
		String text=request.getParameter("text");
		String type=request.getParameter("type");
		String organism=request.getParameter("organism");
		String property=request.getParameter("property");
		String propertyValue=request.getParameter("propertyvalue");
		boolean includeAll=false;
		String includeAllText=request.getParameter("allgeneticelements");
		if (includeAllText!=null)
		{
			includeAll=true;
		}
		
		//If the request is through the paging
		String searchobject=request.getParameter("searchobject");
		String page=request.getParameter("page");
		String partCountString=request.getParameter("partcount");
		int pageNumber=1;
		int partCount=0;
		Serializer serializer=new Serializer();
		PartDataHandler partsHandler=new PartDataHandler();
		
		if (page==null || page.length()==0)
		{
			if ("NONE".equals(property))
			{
				property=null;
			}
			if ("NONE".equals(type))
			{
				type=null;
			}
			if ("NONE".equals(organism))
			{
				organism=null;
			}
			if (propertyValue!=null && propertyValue.length()>0)
			{
				propertyValue=propertyValue.trim();
			}
			if (text!=null && text.length()>0)
			{
				text=text.trim();
			}
			Part searchPart=new Part();				
			searchPart.setName(text);
			searchPart.setOrganism(organism);
			searchPart.setType(type);
			Property searchProperty=new Property();
			searchProperty.setName(property);
			searchProperty.setDescription(property);
			searchProperty.setValue(propertyValue);				
			searchPart.AddProperty(searchProperty);	
			
			if (includeAll==false)
			{
				searchPart.setMetaType(MetaType.PART);
			}
			else
			{
				searchPart.setMetaType(null);
			}
			
			searchobject=serializer.SerializePart(searchPart);	
			try
			{
				partCount=partsHandler.GetVPartsCount(text, type, organism, property, propertyValue, includeAll);
			}
			catch (Exception exception)
			{
				String message=String.format("Could not retrieve the search results. Search parameters:Text:%s, Type:%s, Organism:%s, Property:%s, Value:%s, Search Object:%s",text,type,organism,property,propertyValue,searchobject);
				IOException ioException=new IOException(message,exception);
				ExceptionLogger.Log(ioException);
				throw ioException;
			}
		}
		else
		{
			pageNumber=Integer.parseInt(page);
			partCount=Integer.parseInt(partCountString);
			Part searchPart=null;
			String decodedSearchString=null;
			try
			{
				searchobject=URLDecoder.decode(searchobject,"UTF-8");
				searchPart=serializer.GetPart(searchobject);
			}
			catch(Exception exception)
			{				
				String message=String.format("Could not retrieve the search results. Search parameters:Text:%s, Type:%s, Organism:%s, Property:%s, Value:%s, Search Object:%s",text,type,organism,property,propertyValue,decodedSearchString);
				IOException ioException=new IOException(message,exception);
				ExceptionLogger.Log(ioException);
				throw ioException;
			}
			text=searchPart.getName();
			type=searchPart.getType();
			organism=searchPart.getOrganism();
			if (searchPart.getProperties()!=null && searchPart.getProperties().size()>0)
			{
				property=searchPart.getProperties().get(0).getName();
				propertyValue=searchPart.getProperties().get(0).getValue();				
			}
			
			if (MetaType.PART.equals(searchPart.getMetaType()))
			{
				includeAll=false;
			}
			else
			{
				includeAll=true;
			}
		}
		
		
		List<Part> parts=null;
		try
		{			
			parts=partsHandler.GetVParts(text, type, organism, property, propertyValue,includeAll,pageNumber);
			
		}
		catch (Exception exception)
		{
			String message=String.format("Could not retrieve the search results. Search parameters:Text:%s, Type:%s, Organism:%s, Property:%s, Value:%s, Search Object:%s",text,type,organism,property,propertyValue,searchobject);
			IOException ioException=new IOException(message,exception);
			ExceptionLogger.Log(ioException);
			throw ioException;
		}
		request.setAttribute("searchobject", searchobject);
		request.setAttribute("page", pageNumber);
		request.setAttribute("partcount", partCount);
		request.setAttribute("parts", parts);				
		
		request.getRequestDispatcher("index.jsp").forward(request,response);
		
		//System.out.println(String.format ("Text:%s, Type:%s, Organism:%s, Property:%s, Value:%s",text,type, organism, property,propertyValue));
		
	}

}

package vparts.entity;

import java.io.Serializable;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(identityType = IdentityType.APPLICATION,  detachable = "true")
public class VP_Formula implements Serializable
{
	public VP_Formula(String name, String formula, String reactionFlux, String function)	
	{
		this.name=name;
		this.formula=formula;
		this.reactionFlux=reactionFlux;
		this.function=function;
	}
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
	
	@Persistent
	private String name;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Persistent
	private String formula;
	
	public String getFormula() {
		return formula;
	}

	public void setFormula(String formula) {
		this.formula = formula;
	}

	public String getReactionFlux() {
		return reactionFlux;
	}

	public void setReactionFlux(String reactionFlux) {
		this.reactionFlux = reactionFlux;
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	@Persistent	
	private String reactionFlux;
	
	@Persistent
	private String function;
	
	@Persistent
	private String accession;

	@Persistent
	private String reactionRate;
	
	@Persistent
	private String metaType;
	

	public String getMetaType() {
		return metaType;
	}

	public void setMetaType(String metaType) {
		this.metaType = metaType;
	}
	
	public String getAccession() {
		return accession;
	}

	public void setAccession(String accession) {
		this.accession = accession;
	}
	
	public String getReactionRate() {
		return reactionRate;
	}

	public void setReactionRate(String reactionRate) {
		this.reactionRate = reactionRate;
	}

}

package vparts.entity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.jdo.annotations.Column;
import javax.jdo.annotations.Element;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.Index;
import javax.jdo.annotations.Join;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import javax.jdo.annotations.Unique;

import vparts.Util;
import vparts.data.PartDataHandler;

/*Delete 20130807
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Text;
*/
@PersistenceCapable(identityType = IdentityType.APPLICATION,  detachable = "true")
//@FetchGroups({@FetchGroup(name="fetchpartroles",members={@Persistent(name="partRoles")})})
//@FetchGroup(name="fetchpartroles", members={@Persistent(name="partRoles")})
public class VP_Part{
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	@Persistent
	@Unique(name="VP_PART.NAME_UniqueIDX")
	private String name;
	
	@Persistent
	@Index(name="VP_PART_TYPE_IDX")
	private String type;
	 
	@Persistent
	@Column(jdbcType="CLOB")	
	private String description;
	
	@Persistent
	@Index(name="VP_PART_METATYPE_IDX")
	private String metaType;
	
	@Persistent
	private String displayName;
	
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	public String getMetaType() {
		return metaType;
	}

	public void setMetaType(String metaType) {
		this.metaType = metaType;
	}

	@Persistent(defaultFetchGroup="false")
	private Set<Integer> parameters;
	
	public Set<Integer> getParameters() {
		return parameters;
	}

	public void setParameters(Set<Integer> parameters) {
		this.parameters = parameters;
	}
	
	public void AddParameter(int parameter) {
		if (this.parameters==null)
		{
			this.parameters=new HashSet<Integer>();						
		}
		this.parameters.add(parameter);
	}
	
	/*
	@Persistent(defaultFetchGroup="true")
	private Set<Integer> roles;
	public Set<Integer> getRoles() {
		return roles;
	}

	public void setRoles(Set<Integer> roles) {
		this.roles = roles;
	}
	
	public void AddRole(int role) {
		if (this.roles==null)
		{
			this.roles=new HashSet<Integer>();						
		}
		this.roles.add(role);
	}
	*/
	
	@Persistent(defaultFetchGroup="false")
	private Set<Integer> interactions;
	
	public Set<Integer> getInteractions() {
		return interactions;
	}

	public void setInteractions(Set<Integer> interactions) {
		this.interactions = interactions;
	}
	
	public void AddInteraction(int interaction) {
		if (this.interactions==null)
		{
			this.interactions=new HashSet<Integer>();						
		}
		this.interactions.add(interaction);
	}
	
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		if (description!=null && description.length()>=500)
		{
			description=description.substring(0, 500);
		}
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@Persistent(defaultFetchGroup="false")
	Set<Integer> molecularForms;
	public Set<Integer> getMolecularForms() {
		return molecularForms;
	}

	public void setMolecularForms(Set<Integer> molecularForms) {
		this.molecularForms = molecularForms;
	}
	public void AddMolecularForm(Integer form) {
		if (this.molecularForms==null)
		{
			this.molecularForms=new HashSet<Integer>();						
		}
		this.molecularForms.add(form);
	}
	
	public String getSequence() {
		if (sequence!=null)
		{
			String data=sequence;//GMGM
			//String data=sequence;			
			data=data.replaceAll("(.{60})", "$0" + Util.NEWLINE );
			data=data.toUpperCase();
			return data;
		}
		else
		{
			return "";
		}
	}

	public void setSequence(String sequenceData) {
		if (sequenceData==null)
		{
			sequenceData="";
		}
		/*Delete 20130807
		this.sequence = new Text(sequenceData);//GMGM
		*/
		this.sequence=sequenceData;		
		//sequence=sequenceData;
	}

	public String getSequenceURI() {
		if (sequenceURI!=null)
		{
			return sequenceURI;
		}
		else
		{
			return "";
		}
	}

	public void setSequenceURI(String sequenceURI) {
		this.sequenceURI = sequenceURI;
	}

	@Persistent(defaultFetchGroup = "true") //GMGM
	//@Column(length=Integer.MAX_VALUE)
	@Column(jdbcType="CLOB")	
	private String sequence;
	
	@Persistent
	private String sequenceURI;
	
	@Persistent
	@Index(name="VP_PART_SOURCE_ORGANISM_IDX")
	private String organism;
	public String getOrganism() {
		return organism;
	}

	public void setOrganism(String organism) {
		this.organism = organism;
		this.organismCode=PartDataHandler.GetOrganismCode(organism);
	}
		
	@Persistent
	@Index(name="VP_PART_ORGANISM_CODE_IDX")
	private String organismCode;
	public String getOrganismCode() {
		return organism;
	}
	
	@Persistent
	private String status;
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	@Persistent
	private String designMethod;
	public String getDesignMethod() {
		return designMethod;
	}

	public void setDesignMethod(String designMethod) {
		this.designMethod = designMethod;
	}
	
	/* When creating a join table @Persistent(table="VP_PART_ROLES")
	@Join(column="PART_ID")
    @Element(column="ROLE_ID")*/
	/*When adding the Id of the part to the role table as a foreign key
	@Persistent(defaultFetchGroup = "true")
	@Element(column="PART_ID")*/
	@Persistent(defaultFetchGroup="false")
	@Element(column="PART_ID")
	private List<VP_Role> partRoles;

	public List<VP_Role> getPartRoles() {
		return partRoles;
	}

	public void setPartRoles(List<VP_Role> partRoles) {
		this.partRoles = partRoles;
	}
	public void AddPartRole(VP_Role role) {
		if (this.partRoles==null)
		{
			this.partRoles=new ArrayList<VP_Role>();						
		}
		this.partRoles.add(role);
	}
	
	@Persistent(defaultFetchGroup="false",table="VP_PART_MODELS")
	@Join(column="PART_ID")
    @Element(column="MODEL_ID")
	private List<VP_Model> models;

	public List<VP_Model> getModels() {
		return models;
	}

	public void setModels(List<VP_Model> models) {
		this.models = models;
	}
	public void AddModel(VP_Model model) {
		if (this.models==null)
		{
			this.models=new ArrayList<VP_Model>();						
		}
		this.models.add(model);
	}
	
	public List<VP_Annotation> getAnnotations() {
		return annotations;
	}

	public void setAnnotations(List<VP_Annotation> annotations) {
		this.annotations = annotations;
	}

	public void AddAnnotations(VP_Annotation annotation) {
		if (this.annotations==null)
		{
			this.annotations=new ArrayList<VP_Annotation>();						
		}
		this.annotations.add(annotation);
	}
	@Persistent(defaultFetchGroup="false", mappedBy="part")
	private List<VP_Annotation> annotations;
	
	/*
	private Collection<VP_Role> partRoles;

	public Collection<VP_Role> getPartRoles() {
		return partRoles;
	}

	public void setPartRoles(Collection<VP_Role> partRoles) {
		this.partRoles = partRoles;
	}
	public void AddPartRole(VP_Role role) {
		if (this.partRoles==null)
		{
			this.partRoles=new HashSet<VP_Role>();						
		}
		this.partRoles.add(role);
	}
*/
}

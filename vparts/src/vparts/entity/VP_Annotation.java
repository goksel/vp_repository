package vparts.entity;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class VP_Annotation {

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getEnd() {
		return end;
	}
	public void setEnd(int end) {
		this.end = end;
	}
	public VP_Part getSubPart() {
		return subPart;
	}
	public void setSubPart(VP_Part subPart) {
		this.subPart = subPart;
	}
	public VP_Part getPart() {
		return part;
	}
	public void setPart(VP_Part part) {
		this.part = part;
	}
	@Persistent
	private int start;
	
	@Persistent
	private int end;
	
	@Persistent(column="SUBPART_ID")
	private VP_Part subPart;
	
	@Persistent(column="PART_ID")
	private VP_Part part;
	
	@Persistent
	private String strand;
	public String getStrand() {
		return strand;
	}
	public void setStrand(String strand) {
		this.strand = strand;
	}
	
	
}

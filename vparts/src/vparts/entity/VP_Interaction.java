package vparts.entity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.jdo.annotations.Element;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.Index;
import javax.jdo.annotations.Join;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;


@PersistenceCapable (identityType = IdentityType.APPLICATION, detachable = "true")
public class VP_Interaction {
	
	public VP_Interaction()
	{
		
	}
	
	public  VP_Interaction(String name, String description, String type, String math, String freetextMath, Boolean isReaction)
	{
		this.name=name;
		this.description=description;
		this.interactionType=type;
		this.math=math;
		this.freeTextMath=freetextMath;
		this.isReaction=isReaction;		
	}
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getInteractionType() {
		return interactionType;
	}

	public void setInteractionType(String interactionType) {
		this.interactionType = interactionType;
	}

	public String getMath() {
		return math;
	}

	public void setMath(String math) {
		this.math = math;
	}

	public String getFreeTextMath() {
		return freeTextMath;
	}

	public void setFreeTextMath(String freeTextMath) {
		this.freeTextMath = freeTextMath;
	}

	public boolean isReaction() {
		return isReaction;
	}

	public void setReaction(boolean isReaction) {
		this.isReaction = isReaction;
	}
		
	public Set<VP_Part> getParts() {
		return parts;
	}

	public void setParts(Set<VP_Part> parts) {
		this.parts = parts;
	}
	
	public void AddPart(VP_Part partKey) {
		if (this.parts==null)
		{
			this.parts=new HashSet<VP_Part>();						
		}
		this.parts.add(partKey);
	}	
	
	public Set<Integer> getConstraints() {
		return constraints;
	}

	public void setConstraints(Set<Integer> constraints) {
		this.constraints = constraints;
	}
	
	public void AddConstraint(int constraint) {
		if (this.constraints==null)
		{
			this.constraints=new HashSet<Integer>();						
		}
		this.constraints.add(constraint);
	}
		
	public Set<VP_Parameter> getParameters() {
		return parameters;
	}

	public void setParameters(Set<VP_Parameter> parameters) {
		this.parameters = parameters;
	}
	
	public void AddParameter(VP_Parameter parameter) {
		if (this.parameters==null)
		{
			this.parameters=new HashSet<VP_Parameter>();						
		}
		this.parameters.add(parameter);
	}
	

/*	@Persistent(defaultFetchGroup="true",table="VP_INTERACTION_PARAMETERS")
	@Join(column="INTERACTION_ID")
    @Element(column="PARAMETER_ID")	*/
	@Persistent(defaultFetchGroup="false")
	@Element(column="parameterHolderId")
	private Set<VP_Parameter> parameters;
			
	@Persistent(defaultFetchGroup="false")
	private Set<Integer> constraints;
		
	@Persistent(defaultFetchGroup="true", mappedBy="interaction")
	private List<VP_InteractionPartDetail> interactionPartDetails;

	public List<VP_InteractionPartDetail> getInteractionPartDetails() {
		return interactionPartDetails;
	}
	public void setInteractionPartDetails(List<VP_InteractionPartDetail> interactionPartDetails) {
		this.interactionPartDetails = interactionPartDetails;
	}
	public void AddInteractionPartDetail(VP_InteractionPartDetail interactionPartDetail) {
		if (this.interactionPartDetails==null)
		{
			this.interactionPartDetails=new ArrayList<VP_InteractionPartDetail>();						
		}
		this.interactionPartDetails.add(interactionPartDetail);
	}
	
	
	@Persistent(defaultFetchGroup="false",table="VP_INTERACTION_PARTS")
	@Join(column="INTERACTION_ID")
    @Element(column="PART_ID")
	private Set<VP_Part> parts;
	
	@Persistent
	@Index(name="VP_INTERACTION_NAME_IDX",unique="true")
	private String name;
	
	@Persistent
	private String description;
	
	@Persistent
	@Index(name="VP_INTERACTION_INTERACTIONTYPE_IDX")
	private String interactionType;
	
	@Persistent
	private String math;
	
	@Persistent
	private String freeTextMath;
	
	@Persistent
	private boolean isReaction;
	
	@Persistent
	@Index(name="VP_INTERACTION_ISINTERNAL_IDX")	
	private String isInternal;		

	@Persistent
	private String partNames;	
	
	public String getPartNames() {
		return partNames;
	}

	public void setPartNames(String partNames) {
		this.partNames = partNames;
	}

	public boolean isInternal() {
		return (isInternal!=null && isInternal.toLowerCase().equals("true"));
	}

	public void setInternal(boolean isInternal) {
		this.isInternal = Boolean.toString(isInternal);
		
	}
	
	@Persistent(defaultFetchGroup="false",table="VP_INTERACTION_MODELS")
	@Join(column="INTERACTION_ID")
    @Element(column="MODEL_ID")
	private List<VP_Model> models;

	public List<VP_Model> getModels() {
		return models;
	}

	public void setModels(List<VP_Model> models) {
		this.models = models;
	}
	public void AddModel(VP_Model model) {
		if (this.models==null)
		{
			this.models=new ArrayList<VP_Model>();						
		}
		this.models.add(model);
	}
	
	@Persistent
	private boolean isReversible;

	public boolean isReversible() {
		return isReversible;
	}

	public void setReversible(boolean isReversible) {
		this.isReversible = isReversible;
	}
	
	  
}

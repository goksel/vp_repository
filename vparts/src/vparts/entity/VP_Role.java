package vparts.entity;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.Index;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(identityType = IdentityType.APPLICATION,  detachable = "true")
public class VP_Role {
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private int id; 
 
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	@Persistent
	@Index(name="VP_ROLE_NAME_IDX")
	private String name;
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Persistent
	@Index(name="VP_ROLE_VALUE_IDX")
	private String value;
	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	@Persistent
	private String accession;
	public String getAccession() {
		return this.accession;
	}

	public void setAccession(String accession) {
		this.accession = accession;
	}
	
	@Persistent
	private String description;
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}	  
	/*
	@Persistent
	private Collection<VP_Part> parts;*/
	/*@Persistent
	private VP_Part part;

	public VP_Part getPart() {
		return part;
	}

	public void setPart(VP_Part part) {
		this.part = part;
	}*/
	
}

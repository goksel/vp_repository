package vparts.entity;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import vparts.data.ParameterTypeDataHandler;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class VP_Parameter {
	 
	public VP_Parameter ()
	{
	
	}
	public VP_Parameter (String type, String name, Double value,String scope,String evidenceType)
	{
		this.parameterType=type;
		this.name=name;
		this.value=value;
		this.scope=scope;
		this.evidenceType=evidenceType;
	}
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParameterType() {
		return parameterType;
	}

	public void setParameterType(String parameterType) {
		this.parameterType = parameterType;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	@Persistent
	private String name;
	
	@Persistent
	private String parameterType;
	
	@Persistent
	private Double value;
	
	@Persistent
	private String molecularForm;
	
	@Persistent
	private String scope;
	
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	@Persistent
	private String evidenceType;
	
	public String getEvidenceType() {
		return evidenceType;
	}
	public void setEvidenceType(String evidenceType) {
		this.evidenceType = evidenceType;
	}
	
	public String getMolecularForm() {
		if  (molecularForm==null || molecularForm.length()==0)
		{
			return "Default";
		}
		else
		{
			return molecularForm;
		}
	}
	public void setMolecularForm(String molecularForm) {
		this.molecularForm = molecularForm;
	}
	
	/*
	private VP_ParameterType parameterTypeObject;
	public VP_ParameterType GetParameterTypeObject() throws Exception
	{
		if (parameterType!=null)
		{
			ParameterTypeDataHandler handler=new ParameterTypeDataHandler();
			parameterTypeObject=handler.GetType(parameterType);						
		}
		return parameterTypeObject;		 
	}
	*/
	//The Id of the interaction. In future, parts can have parameters too with the addition of parameterFor ('Interaction' or 'Part') field
	//http://www.datanucleus.org/products/datanucleus/jdo/orm/one_to_many_collection.html
	/*@Persistent
	private int parameterHolderID;
	
	public int getparameterHolder() {
		return parameterHolderID;
	}
	public void setparameterHolder(int parameterHolderID) {
		this.parameterHolderID = parameterHolderID;
	}*/
}

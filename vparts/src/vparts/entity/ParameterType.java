package vparts.entity;

import java.io.Serializable;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

public class ParameterType implements Serializable {
	
	public String Name;
	
	public String Description;
	
	public String Accession;
	
	public String ModelName;   
}

package vparts.entity;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class VP_InteractionPartDetail {
	public VP_InteractionPartDetail()
	{		
	}
	
	//Interaction Role, number of molecules and partForm are the constraints
	public VP_InteractionPartDetail (String partName, String partForm, String interactionRole, int numberOfMolecules, String mathName)
	{
		this.partName=partName;
		this.partForm=partForm;
		this.interactionRole=interactionRole;
		this.numberOfMolecules=numberOfMolecules;
		this.mathName=mathName;
	}
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
		

	public String getPartName() {
		return partName;
	}

	public void setPartName(String partName) {
		this.partName = partName;
	}

	public String getPartForm() {
		return partForm;
	}

	public void setPartForm(String partForm) {
		this.partForm = partForm;
	}

	public String getInteractionRole() {
		return interactionRole;
	}

	public void setInteractionRole(String interactionRole) {
		this.interactionRole = interactionRole;
	}
	
	public int getNumberOfMolecules() {
		return numberOfMolecules;
	}

	public void setNumberOfMolecules(int numberOfMolecules) {
		this.numberOfMolecules = numberOfMolecules;
	}
	
	public String getMathName() {
		return mathName;
	}

	public void setMathName(String mathName) {
		this.mathName = mathName;
	}
	
	public VP_Interaction getInteraction() {
		return interaction;
	}

	public void setInteraction(VP_Interaction interaction) {
		this.interaction = interaction;
	}
	
	@Persistent
	private String mathName;
	
	@Persistent
	private String partName;
	
	//Dimer, Tetramer, Phosphorylated, Default
	@Persistent
	private String partForm;
	
	//Input, Output, Modifier
	@Persistent
	private String interactionRole;
	
	@Persistent
	private int numberOfMolecules;
	
	
	@Persistent(column="INTERACTION_ID")
	private VP_Interaction interaction;
			
}

package vparts.entity;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class VP_Constraint {
private String sourceID;
public String getSourceID() {
	return sourceID;
}

public void setSourceID(String sourceID) {
	this.sourceID = sourceID;
}

public String getSourceType() {
	return sourceType;
}

public void setSourceType(String sourceType) {
	this.sourceType = sourceType;
}

public String getTargetID() {
	return targetID;
}

public void setTargetID(String targetID) {
	this.targetID = targetID;
}

public String getTargetType() {
	return targetType;
}

public void setTargetType(String targetType) {
	this.targetType = targetType;
}

public String getQualifier() {
	return qualifier;
}

public void setQualifier(String qualifier) {
	this.qualifier = qualifier;
}

private String sourceType;
private String targetID;
private String targetType;
private String qualifier;

@PrimaryKey
@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
private int id;

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}


  
}

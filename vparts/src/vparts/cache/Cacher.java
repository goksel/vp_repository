package vparts.cache;

import java.util.Collections;

//import org.apache.jcs.JCS;




import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import vpart.exception.GenericException;
import vparts.SimpleLogger;


/*
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
*/
/*
import javax.cache.Cache;
import javax.cache.CacheException;
//import javax.cache.CacheFactory;
import javax.cache.CacheManager;
import javax.cache.Caching;
import javax.cache.spi.CachingProvider;
*/

public class Cacher {
public static String MOLECULAR_FORM_TYPE_KEY="Molecular_Form_Type";
public static String PARAMETER_TYPE_KEY="Parameter_Type_Type";
public static String FORMULA_KEY="Formula";



private static Cache cache;

//private static JCS cache;

private  Cacher() throws GenericException
 {
		
			 
	 }
 
 public static void Put (Object value, String key) throws GenericException
 {
	 cache=GetCache(); 
	 try
	 {
	 //cache.put(key, value); 
		 cache.put(new Element(key, value)); 
	 }
	 catch(Exception exception)
	 {
		 throw new GenericException("Could not store data in the cache", exception);
		 
	 }
 }
 public static Object Get( String key) throws GenericException
 {
	 Object value=null;
	 cache=GetCache(); 
	 Element element= cache.get(key);
	 if (element!=null)
	 {
		 value=element.getObjectValue();
	 }
	 return value;
 }
private static Cache GetCache() throws GenericException
{
	if (cache==null)
	{
		try
		{
			CacheManager cacheManager = CacheManager.create();
			//cacheManager.addCache("BacilloBricksCache");
			cache = cacheManager.getCache("BacilloBricksCache");
			if (cache==null)
			{	
				cacheManager.addCache("BacilloBricksCache");
				cache = cacheManager.getCache("BacilloBricksCache");			
			}
		}
		catch(Exception e)
		{ 
			e.printStackTrace();
			throw new GenericException("Could not create the cache", e);
		}
	}
	return cache;
}
// private static JCS GetCache() throws GenericException
// {
//	 if (cache==null)
//	 {
//		 try {
//			 	//JCS.setConfigFilename("/WEB-INF/cache.ccf");
//			   cache=JCS.getInstance("BacilloBricksCache");
//			 	/*CachingProvider provider=Caching.getCachingProvider();
//			 	CacheManager manager=provider.getCacheManager();
//			 	int i=0;
//			 	cache=manager.getCache("BacilloBricksCache");
//			 	*/
//			 	/*CacheManager manager=CacheManager.getInstance();
//				CacheFactory cacheFactory = manager.getCacheFactory();
//				cache = cacheFactory.createCache(Collections.emptyMap());*/
//			} catch(Exception e)
//			{ 
//				e.printStackTrace();
//				throw new GenericException("Could not create the cache", e);
//			}
//	 }
//	 return cache;
// }
 
 public static void Clear() throws GenericException
 {	 
	
	 try
	 {
		 CacheManager.getInstance().removeCache("BacilloBricksCache");
		 CacheManager.getInstance().shutdown();
		 cache=null; 
		
		 /*cache=GetCache();
	 	 cache.clear();	*/
	 }
	 catch(Exception ex)
	 {
		 String message="Cannot clear the cache";
		 SimpleLogger.Warning(Cacher.class, message);
		 System.out.println(message);
	 }
	 
 }
 
}

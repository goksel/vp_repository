package vpart.exchange;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import vparts.Configuration;



/**
 * Servlet implementation class ImageServlet
 */
public class ImageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ImageServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String partName=request.getParameter("part");
		try
		{
			BufferedImage buffer=ImageIO.read(new File(Configuration.IMAGE_DIRECTORY + "/" + partName + ".png"));
			response.setContentType("image/png");
			OutputStream os = response.getOutputStream();
			ImageIO.write(buffer, "png", os);
			os.flush();
			os.close();
		}
		catch (Exception exception)
		{
			Logger.getLogger(ImageServlet.class.getName()).log(Level.SEVERE, ImageServlet.class.getName() + ":" + exception.getMessage());					
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}

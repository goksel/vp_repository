package vpart.exchange;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.sbolstandard.core.SBOLDocument;
import org.sbolstandard.core.SBOLFactory;

import vparts.Configuration;
import vparts.ErrorHandler;



/**
 * Servlet implementation class ImageServlet
 */
public class SBOLValidatorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SBOLValidatorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().print("The validator is running");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String message="The SBOL document was successfully validated!";
		SBOLDocument document=null;
		try
		{
			String sbolData=request.getParameter("data");		
			document= SBOLFactory.read(new ByteArrayInputStream(sbolData.getBytes()));				
		}
		catch (Exception exception)
		{
			message=exception.getMessage();//ErrorHandler.GetStackTrace(exception);// exception.getMessage();
			exception.printStackTrace();
			
		}
		finally
		{
			//response.sendRedirect("sbolvalidator.jsp?message=" + message);
			request.setAttribute("message", message);
			request.getRequestDispatcher("sbolvalidator.jsp").forward(request,response);
		}
	}

}

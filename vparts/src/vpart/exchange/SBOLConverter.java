package vpart.exchange;

import java.awt.Component;
import java.io.ByteArrayOutputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.sbolstandard.core.Collection;
import org.sbolstandard.core.DnaComponent;
import org.sbolstandard.core.DnaSequence;
import org.sbolstandard.core.SBOLDocument;
import org.sbolstandard.core.SBOLFactory;







import org.sbolstandard.core.SequenceAnnotation;
import org.sbolstandard.core.StrandType;

import virtualparts.entity.Part;
import virtualparts.entity.Property;
import vparts.PartsHandler;
import vparts.data.PartDataHandler;
import vparts.entity.VP_Part;
/*import org.sbolstandard.core.Collection;
import org.sbolstandard.core.DnaComponent;
import org.sbolstandard.xml.CollectionImpl;
import org.sbolstandard.xml.DnaComponentImpl;
import org.sbolstandard.xml.DnaSequenceImpl;
import org.sbolstandard.xml.Parser;
import org.sbolstandard.xml.SequenceAnnotationImpl;
import org.sbolstandard.xml.UtilURI;
*/
import vparts.entity.VP_Role;

public class SBOLConverter {
	/*public static String GetSBOLTest() throws Exception
	{		
		String baseURL="http://fakeuri.org";
		SBOLDocument document=SBOLFactory.createDocument();
		Collection collection=SBOLFactory.createCollection();
		collection.setURI(new URI(baseURL + "/genericparts"));
		collection.setDisplayId("GenericParts");
		
		DnaComponent dnaComponent = SBOLFactory.createDnaComponent();
		dnaComponent.setName("test part");
		dnaComponent.setDescription("test description");
		dnaComponent.setDisplayId("testid");
		dnaComponent.setURI(new URI(baseURL + "/part/test" ));
		dnaComponent.addType(new URI("http://purl.org/obo/owl/SO#SO_0000167"));
		
		DnaSequence sequence=SBOLFactory.createDnaSequence();		
		sequence.setNucleotides("acccggaaagggccc");		
		sequence.setURI(new URI(baseURL + "/part/test/NA"));			
		dnaComponent.setDnaSequence(sequence);	
		
		collection.addComponent(dnaComponent);
		document.addContent(collection);
		String output=Serialize(document);
		return output;
	}*/
	
	/*public static SBOLDocument GetSBOL2(Part part,String baseURL) throws Exception
	{		
		SBOLDocument document=SBOLFactory.createDocument();
		//Collection collection=SBOLFactory.createCollection();
		//collection.setURI(new URI(baseURL + "/part/" + part.getName()));
		//collection.setDisplayId(part.getName());
		DnaComponent dnaComponent = GetSBOLDnaComponent(part,baseURL);
		//collection.addComponent(dnaComponent);
		document.addContent(dnaComponent);		
		//document.addContent(collection);		
		return document;
	}*/
	public  List<SequenceAnnotation> TestAnnotations() throws Exception
	{
		String baseURL="http//www.fakeuri.org/part";
		String baseAnnURL="http//www.fakeuri.org/annotation/";
		PartDataHandler handler=new PartDataHandler();
		
		DnaComponent promoter1=GetSBOLDnaComponent(handler.GetPart("PspaRK"),baseURL);
		DnaComponent promoter2=GetSBOLDnaComponent(handler.GetPart("PspaS"),baseURL);
		DnaComponent rbs1=GetSBOLDnaComponent(handler.GetPart("RBS_SpaK"),baseURL);
		DnaComponent rbs2=GetSBOLDnaComponent(handler.GetPart("RBS_SpaR"),baseURL);
		DnaComponent cds1=GetSBOLDnaComponent(handler.GetPart("SpaK"),baseURL);
		DnaComponent cds2=GetSBOLDnaComponent(handler.GetPart("SpaR"),baseURL);
		DnaComponent ter1=GetSBOLDnaComponent(handler.GetPart("ECK120010818"),baseURL);
		DnaComponent ter2=GetSBOLDnaComponent(handler.GetPart("ECK120015440"),baseURL);
		
		
		
		
		DnaComponent device1=GetDnaComponent(baseURL + "device1", "device1", "device1", "", "SO_000001");
		
		SequenceAnnotation annotation1=GetSequenceAnnotation(baseAnnURL + "1", 1, 3, StrandType.POSITIVE, promoter1);
		SequenceAnnotation annotation2=GetSequenceAnnotation(baseAnnURL + "2", 4, 6, StrandType.POSITIVE, rbs1);
		SequenceAnnotation annotation3=GetSequenceAnnotation(baseAnnURL + "3", 7, 9, StrandType.POSITIVE, cds1);
		SequenceAnnotation annotation4=GetSequenceAnnotation(baseAnnURL + "4", 10, 12, StrandType.POSITIVE, ter1);
		
		
		List<SequenceAnnotation> annotations=new ArrayList<SequenceAnnotation>();
		device1.addAnnotation(annotation1);
		device1.addAnnotation(annotation2);
		device1.addAnnotation(annotation3);
		device1.addAnnotation(annotation4);
		
		DnaComponent device2=GetDnaComponent(baseURL + "device2", "device2", "device2", "", "SO_000001");
		SequenceAnnotation annotation5=GetSequenceAnnotation(baseAnnURL + "5", 1, 3, StrandType.POSITIVE, promoter2);
		SequenceAnnotation annotation6=GetSequenceAnnotation(baseAnnURL + "6", 4, 6, StrandType.POSITIVE, rbs1);
		SequenceAnnotation annotation7=GetSequenceAnnotation(baseAnnURL + "7", 7, 9, StrandType.POSITIVE, cds1);
		SequenceAnnotation annotation8=GetSequenceAnnotation(baseAnnURL + "8", 10, 12, StrandType.POSITIVE, rbs2);
		SequenceAnnotation annotation9=GetSequenceAnnotation(baseAnnURL + "9", 13, 15, StrandType.POSITIVE, cds2);		
		SequenceAnnotation annotation10=GetSequenceAnnotation(baseAnnURL + "10", 16, 18, StrandType.POSITIVE, ter2);
		device2.addAnnotation(annotation5);
		device2.addAnnotation(annotation6);
		device2.addAnnotation(annotation7);
		device2.addAnnotation(annotation8);
		device2.addAnnotation(annotation9);
		device2.addAnnotation(annotation10);
		
		DnaComponent device3=GetDnaComponent(baseURL + "device3", "device3", "device3", "", "SO_000001");
		SequenceAnnotation annotation11=GetSequenceAnnotation(baseAnnURL + "11", 1, 12, StrandType.POSITIVE, device1);
		SequenceAnnotation annotation12=GetSequenceAnnotation(baseAnnURL + "12", 13, 30, StrandType.NEGATIVE, device2);
		device3.addAnnotation(annotation11);
		device3.addAnnotation(annotation12);
		
		DnaComponent device4=GetDnaComponent(baseURL + "device4", "device4", "device4", "", "SO_000001");
		SequenceAnnotation annotation13=GetSequenceAnnotation(baseAnnURL + "13", 1, 18, StrandType.POSITIVE, device2);
		SequenceAnnotation annotation14=GetSequenceAnnotation(baseAnnURL + "14", 19, 30, StrandType.NEGATIVE, device1);
		SequenceAnnotation annotation15=GetSequenceAnnotation(baseAnnURL + "15", 31, 48, StrandType.POSITIVE, device2);
		
		device4.addAnnotation(annotation13);
		device4.addAnnotation(annotation14);
		device4.addAnnotation(annotation15);
		
		
		DnaComponent device5=GetDnaComponent(baseURL + "device5", "device5", "device5", "", "SO_000001");
		SequenceAnnotation annotation16=GetSequenceAnnotation(baseAnnURL + "16", 1, 48, StrandType.NEGATIVE, device4);
		SequenceAnnotation annotation17=GetSequenceAnnotation(baseAnnURL + "17", 49, 68, StrandType.POSITIVE, device3);
		
		device5.addAnnotation(annotation16);
		device5.addAnnotation(annotation17);
		
		
		
		
		return device5.getAnnotations();
		
	}
	private SequenceAnnotation GetSequenceAnnotation(String uri,int start,int end, StrandType strandType,DnaComponent dnaComponent) throws Exception
	{
		SequenceAnnotation annotation=SBOLFactory.createSequenceAnnotation();
		annotation.setURI(new URI(uri));
		annotation.setBioStart(start);
		annotation.setBioEnd(end);
		annotation.setStrand(strandType);
		annotation.setSubComponent(dnaComponent);
		return annotation;		
	}
	private DnaComponent GetDnaComponent(String uri, String displayId, String name,String description, String type) throws Exception
	{
		DnaComponent dnaComponent = SBOLFactory.createDnaComponent();
		dnaComponent.setName(name);
		dnaComponent.setDescription(description);
		dnaComponent.setDisplayId(displayId);
		dnaComponent.setURI(new URI(uri));
		dnaComponent.addType(new URI(GetTypeURI(type)));
		return dnaComponent;
	}
	public static SBOLDocument GetSBOL(Part part,List<SequenceAnnotation> annotations,String baseURL) throws Exception
	{		
		SBOLDocument document=SBOLFactory.createDocument();
		Collection collection=SBOLFactory.createCollection();
		collection.setURI(new URI(baseURL + "/collection/" + part.getName()));		
		collection.setDisplayId(part.getName() + " collection");
		DnaComponent dnaComponent = GetSBOLDnaComponent(part,baseURL);
		if (annotations!=null && annotations.size()>0)
		{
			for (SequenceAnnotation annotation:annotations)
			{
				if (annotation.getURI().toString().startsWith("/"))
				{
					annotation.setURI(new URI(baseURL + annotation.getURI().toString()));
				}
				String subComponentURI=annotation.getSubComponent().getURI().toString();
				if (subComponentURI.startsWith("/"))
				{
					annotation.getSubComponent().setURI(new URI(baseURL + subComponentURI));
				}
				dnaComponent.addAnnotation(annotation);
			}
		}
		collection.addComponent(dnaComponent);
		document.addContent(collection);		
		return document;
	}
	public static DnaComponent GetSBOLDnaComponent(Part part,String baseURL) throws Exception
	{
		DnaComponent dnaComponent = SBOLFactory.createDnaComponent();
		dnaComponent.setName(part.getDisplayName());
		dnaComponent.setDescription(part.getDescription());
		dnaComponent.setDisplayId(part.getName());
		dnaComponent.setURI(new URI(baseURL + "/part/" + part.getName()));
		dnaComponent.addType(new URI(GetTypeURI(part.getType())));
		AddTypes(dnaComponent,part);
		if (part.getSequence()!=null && part.getSequence().length()>0)
		{
			DnaSequence sequence=SBOLFactory.createDnaSequence();		
			sequence.setNucleotides(part.getSequence().toLowerCase().replace(System.getProperty("line.separator"), ""));		
			sequence.setURI(new URI(dnaComponent.getURI().toString() + "/" + "NA"));			
			dnaComponent.setDnaSequence(sequence);	
		}		            
		return dnaComponent;		
	}
	
	public static SBOLDocument GetSBOL(List<Part>  parts,String baseURL,String collectionURL) throws Exception
	{
		SBOLDocument document=SBOLFactory.createDocument();
		Collection collection=SBOLFactory.createCollection();
		collection.setURI(new URI(collectionURL));		
		collection.setDisplayId(collectionURL.replace(baseURL + "/", ""));
	
		for (Part part : parts)
		{
			DnaComponent dnaComponent = GetSBOLDnaComponent(part,baseURL);						
			collection.addComponent(dnaComponent);			
		}
		document.addContent(collection);
		return document;
	}
	
	private static void AddTypes(DnaComponent dnaComponent,Part part) throws Exception
	{
		PartDataHandler handler=new PartDataHandler();
		List<Property> properties=handler.GetPartRoles(part.getName());
		for (Property property:properties)
		{
			if (property!=null)
			{
				if ("type".equals(property.getName()))
				{
					String newType=property.getValue();
					int index=newType.toLowerCase().indexOf("so_");
					if (index>-1)
					{
						String SOTerm=newType.substring(index);
						if (!Pigeon.HasType(dnaComponent.getTypes(), SOTerm))
						{
							dnaComponent.addType(new URI(property.getValue()));
						}						
					}				
				}
			}
		}
	}
	public static String Serialize(SBOLDocument document) throws Exception
	{
		ByteArrayOutputStream buffer=new ByteArrayOutputStream();  
		SBOLFactory.write(document, buffer);
		String xml=buffer.toString(); 
		return xml;		
	}
	
	public static String GetTypeURI(String type)
	{
		String URI=type;
		if (type.equals("Promoter"))
		{
			URI="SO_0000167";
		}
		else if (type.equals("FunctionalPart"))
		{
			URI="SO_0000316";
		}
		else if (type.equals("Terminator"))
		{
			URI="SO_0000614";
		}
		else if (type.equals("RNA"))
		{
			URI="SO_0000356";
		}
		else if (type.equals("RBS"))
		{
			URI="SO_0000139";
		}
		else if (type.equals("Shim"))
		{
			URI="SO_0000997";
		}
		else if (type.equals("Operator"))
		{
			URI="SO_0000057";
		}
		else if (type.equals("mRNA"))
		{
			URI="SO_0000234";
		}
		
		URI="http://purl.org/obo/owl/SO#" + URI;
		return URI;		
	}
	
}


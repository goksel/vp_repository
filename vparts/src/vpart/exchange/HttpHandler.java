package vpart.exchange;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

/**
 * Provides methods to retrieve data from a BacilloBricks repository given with an HTTP URL.
 * @author a8901379
 */
public class HttpHandler {

    /**
     * Gets the String data from given URI
     * @param URI The URI to access to the repository
     * @return String
     * @throws IOException
     */
public String Get(String URI) throws IOException
    {
      //URI=URLEncoder.encode(URI, "UTF-8");
     URI=URI.replace(" ", "%20");
      URI=URI.replace(">", "%3E");
      URL url=new URL(URI);
      int tried=0;
      InputStream stream=null;
      while (tried<5)
      {
        try
        {
          stream =url.openStream();
          tried=5;
        }
        catch (IOException exception)
        {
            tried++;  
            System.out.print("Could not access to the repository after attempt " + tried + ". ");   
            try
            {
            Thread.sleep(10000);//Sleep for 10 seconds
            }
            catch(Exception threadexception)
            {
                
            }
            if (tried==5)
            {
                tried=0;
                throw exception;
            }
            else
            {
                 System.out.println("Connection will be tried in 10 seconds");   
            }
        }
      }
      String output=ConvertStreamToString(stream);
      return output;
    }

public static void saveImage(String imageUrl, String destinationFile) throws IOException {
	URL url = new URL(imageUrl);
	InputStream is = url.openStream();
	OutputStream os = new FileOutputStream(destinationFile);

	byte[] b = new byte[2048];
	int length;

	while ((length = is.read(b)) != -1) {
		os.write(b, 0, length);
	}

	is.close();
	os.close();
}

public static String Post(String urlString, String data, String parameterName) throws Exception
{

    URL url;
    URLConnection urlConn;
    DataOutputStream printout;
    DataInputStream input;
    // URL of CGI-Bin script.
    url = new URL(urlString);
    // URL connection channel.
    urlConn = url.openConnection();
    // Let the run-time system (RTS) know that we want input.
    urlConn.setDoInput(true);
    // Let the RTS know that we want to do output.
    urlConn.setDoOutput(true);
    // No caching, we want the real thing.
    urlConn.setUseCaches(false);
    // Specify the content type.
    urlConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
    // Send POST output.
    printout = new DataOutputStream(urlConn.getOutputStream());
    String content = parameterName + "=" + URLEncoder.encode(data);
    printout.writeBytes(content);
    printout.flush();
    printout.close();
    // Get response data.
    input = new DataInputStream(urlConn.getInputStream());
    String str;
    String output="";
    while (null != ((str = input.readLine())))
    {
        output=output + str;
    }
    input.close();
    return output;
}

    private String ConvertStreamToString(InputStream stream) throws IOException {
        if (stream != null) {
            Writer writer = new StringWriter();
            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            } finally {
                stream.close();
            }
            return writer.toString();

        } else {
            return "";
        }
    }

}

package vpart.exchange;


import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.sbolstandard.core.DnaComponent;
import org.sbolstandard.core.SequenceAnnotation;
import org.sbolstandard.core.StrandType;

import vpart.exchange.HttpHandler;
import vparts.Configuration;
import vparts.ErrorHandler;
import vparts.PartsHandler;
import vparts.data.PartDataHandler;

public class Pigeon {
	public Pigeon()
	{
	}
	
	private boolean PigeonExists(String partName)
	{
		boolean  result=false;
		File file=new File(Configuration.IMAGE_DIRECTORY + "/"  + partName + ".png");
		
		if (file.exists())
		{
			result=true;
		}
		return result;
	}


	public String GetData (String partName,List<SequenceAnnotation> annotations) throws Exception
	{
		String imagePath="";
		try
		{
			if (!PigeonExists(partName))
			{
				String data="";
				data=GetPigeonData(annotations,data);
				data=data + "# Arcs"; 
				
				String pigeonServer="http://cidar1.bu.edu:5801";
				//data="p p";
				//data=data.replace(" ", System.getProperty("line.separator"));
				//data=data +  System.getProperty("line.separator") + "# Arcs"; 
				
				HttpHandler handler=new HttpHandler();
				String result=handler.Post(pigeonServer + "/pigeon.php", data, "desc");
				imagePath=GetImagePath(result,pigeonServer);
				if (imagePath!=null)
				{
					//handler.saveImage(imagePath, "/tmp/pigeon/test.png");
					handler.saveImage(imagePath, Configuration.IMAGE_DIRECTORY + "/" + partName + ".png");
					
				}
				/*String image=handler.Get(imagePath);
				File file=new File("/tmp/pigeon/test.png");
				BufferedWriter bw=new BufferedWriter(new FileWriter(file));
				bw.write(image);
				bw.flush();
				bw.close();*/
			}
		}
		catch (Exception exception)
		{
			Logger.getLogger(Pigeon.class.getName()).log(Level.SEVERE,ErrorHandler.GetStackTrace(exception));
		}
		return imagePath;
	}
	
	public String GetDataDetailed (String partName,String baseURL) throws Exception
	{
		String imagePath="";
		try
		{
			if (!PigeonExists(partName))
			{
				List<String> data=new ArrayList<String>();
				PartDataHandler partHandler=new PartDataHandler ();
				List<SequenceAnnotation> annotations=partHandler.GetAnnotationsDetailed(partName, baseURL);
				/*SBOLConverter converter=new SBOLConverter();
				List<SequenceAnnotation> annotations=converter.TestAnnotations();*/
				
				data=GetPigeonDataDetailed(annotations,data,StrandType.POSITIVE);
				String text="";
				for (String annotationData:data)
				{
					text=text + annotationData;
				}
				text=text + "# Arcs"; 
				
				String pigeonServer="http://cidar1.bu.edu:5801";
				//data="p p";
				//data=data.replace(" ", System.getProperty("line.separator"));
				//data=data +  System.getProperty("line.separator") + "# Arcs"; 
				
				HttpHandler handler=new HttpHandler();
				String result=handler.Post(pigeonServer + "/pigeon.php", text, "desc");
				imagePath=GetImagePath(result,pigeonServer);
				if (imagePath!=null)
				{
					//handler.saveImage(imagePath, "/tmp/pigeon/test.png");
					handler.saveImage(imagePath, Configuration.IMAGE_DIRECTORY + "/" + partName + ".png");
					
				}
				/*String image=handler.Get(imagePath);
				File file=new File("/tmp/pigeon/test.png");
				BufferedWriter bw=new BufferedWriter(new FileWriter(file));
				bw.write(image);
				bw.flush();
				bw.close();*/
			}
		}
		catch (Exception exception)
		{
			Logger.getLogger(Pigeon.class.getName()).log(Level.SEVERE,ErrorHandler.GetStackTrace(exception));
		}
		return imagePath;
	}
	
	private String GetImagePath(String htmlOutput,String pigeonServer)
	{
		int index=htmlOutput.indexOf("alt = \"Weyekin");
		String imagePath=null;
		if (index>-1)
		{
			htmlOutput=htmlOutput.substring(0,index);
			int index1=htmlOutput.lastIndexOf("scratch");
			int index2=htmlOutput.lastIndexOf("png");
			imagePath=pigeonServer + "/" + htmlOutput.substring(index1,index2) + "png";			
		}
		return imagePath;
	}
	private String GetPigeonData(List<SequenceAnnotation> annotations, String data)
	{
		//String data="p p r c t t t";
		if (annotations!=null && annotations.size()>0)
		{
			for (SequenceAnnotation annotation:annotations)
			{
				DnaComponent component=annotation.getSubComponent();
				if (component.getAnnotations()!=null && component.getAnnotations().size()>0)
				{
					GetPigeonData(component.getAnnotations(), data);
				}
				else
				{
					String code=GetPigeonCode(component.getTypes());
					if (annotation.getStrand().equals(StrandType.NEGATIVE) && (!code.startsWith("?")))
					{						
						code="<" + code;
					}
					String name=component.getDisplayId();
					if (name.length()>12)
					{
						name=name.substring(0,11) + "...";
					}
					code=String.format(code, name);
					data=data + code + System.getProperty("line.separator");
				}
			}
		}
		return data;
	}
	
	private List<String> GetPigeonDataDetailed(List<SequenceAnnotation> annotations, List<String> data,StrandType parentCalculatedStrandType)
	{
		//String data="p p r c t t t";
		if (annotations!=null && annotations.size()>0)
		{
			for (SequenceAnnotation annotation:annotations)
			{				
				StrandType innerComponentStrand=parentCalculatedStrandType;
				if (annotation.getStrand().equals(StrandType.NEGATIVE) && parentCalculatedStrandType.equals(StrandType.POSITIVE))
				{
					innerComponentStrand=StrandType.NEGATIVE;
				}
				else if (annotation.getStrand().equals(StrandType.NEGATIVE) && parentCalculatedStrandType.equals(StrandType.NEGATIVE))
				{
					innerComponentStrand=StrandType.POSITIVE;
				}
				
				DnaComponent component=annotation.getSubComponent();
				if (component.getAnnotations()!=null && component.getAnnotations().size()>0)
				{
															
					List<String> annotationsData=new ArrayList<String>();
					annotationsData=GetPigeonDataDetailed(component.getAnnotations(), annotationsData,innerComponentStrand);
					String text="";
					if (innerComponentStrand.equals(StrandType.NEGATIVE))
					{
						for (int i=annotationsData.size()-1;i!=-1;i--)
						{
							text=text + annotationsData.get(i);
						}						
					}	
					else
					{
						for (String annotationData:annotationsData)
						{
							text=text + annotationData;
						}							
					}
					data.add(text);
				}
				else
				{
					String code=GetPigeonCode(component.getTypes());
					if (annotation.getStrand().equals(StrandType.NEGATIVE) && parentCalculatedStrandType.equals(StrandType.POSITIVE) && (!code.startsWith("?")))
					{						
						code="<" + code;
					}
					else if (annotation.getStrand().equals(StrandType.POSITIVE) && parentCalculatedStrandType.equals(StrandType.NEGATIVE) && (!code.startsWith("?")))
					{						
						code="<" + code;
					}
					String name=component.getDisplayId();
					if (name.length()>12)
					{
						name=name.substring(0,11) + "...";
					}
					code=String.format(code, name);
					data.add(code + System.getProperty("line.separator"));
				}
			}
		}
		return data;
	}
	
	
	public static  boolean HasType(Collection<URI> types,String type)
	{
		boolean result=false;
		if (types!=null)
		{
			for (URI uri:types)
			{
				if (uri.toString().endsWith(type))
				{
					result=true;
					break;
				}
			}
		}
		return result;
	}
	private static String GetPigeonCode(Collection<URI> types)
	{
		//String code="?";
		String code="? %s 12";		
		if (HasType(types,"0000167"))
		{
			code="p %s 4";
		}
		else if (HasType(types,"0000316"))
		{
			code="c %s 2";
		}
		else if (HasType(types,"0000614") || HasType(types,"0000141"))
		{
			code="t %s 6";
		}
		else if (HasType(types,"0000139"))
		{
			code="r %s 8";
		}		
		else if (HasType(types,"0000057"))
		{
			code="o %s 1";
		}
		return code;		
	}
}

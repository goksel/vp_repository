package vpart.exchange;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.sbolstandard.core.SBOLDocument;
import org.sbolstandard.core.SequenceAnnotation;

import virtualparts.entity.Formula;
import virtualparts.entity.Interaction;
import virtualparts.entity.Interactions;
import virtualparts.entity.MolecularFormType;
import virtualparts.entity.Part;
import vparts.Util;
import vparts.data.InteractionDataHandler;
import vparts.data.MathDataHandler;
import vparts.data.MolecularFormTypeDataHandler;
import vparts.data.PartDataHandler;
import vparts.entity.VP_Formula;
import vparts.entity.VP_Interaction;
import vparts.entity.VP_MolecularFormType;
import vparts.entity.VP_Part;
import vparts.model.XMLBuilder;

public class Serializer {

public static String GetBaseURL(HttpServletRequest request)
{
	String applicationbaseURL="http://" + request.getServerName();
	if (request.getServerPort()!=80)
	{
		applicationbaseURL=applicationbaseURL + ":" + request.getServerPort();
	}
	return applicationbaseURL;
}
public String Serialize(String dataType,String informationType,String format,String partName,HttpServletRequest request) throws Exception
{
	String applicationbaseURL=GetBaseURL(request);	
	String output="";
	XMLBuilder xmlBuilder=new XMLBuilder();
	if (dataType!=null)		
	{		
		if (dataType.equals("part"))
		{			
				PartDataHandler handler=new PartDataHandler();					
				InteractionDataHandler iHandler=new InteractionDataHandler();	
				Part part=handler.GetPartDetailed(partName);	
				
				if (informationType==null)
				{			
					if (format=="xml")
					{
						part.setSequence("![CDATA[" + part.getSequence() + "]]");
						output=xmlBuilder.ToXML(part);
					}
					else if (format.equals("sbol"))
					{
						List<SequenceAnnotation> annotations=handler.GetAnnotations(part.getName());
						
						SBOLDocument document =SBOLConverter.GetSBOL(part,annotations,applicationbaseURL);
						
						output=SBOLConverter.Serialize(document);
						
						
					}
				}
				else if (informationType.toLowerCase().equals("interactions"))
				{
					List<Interaction> interactions=iHandler.GetPartInteractionsDetailed(partName);
					output=xmlBuilder.InteractionsToXML(interactions);		
				}
				else if (informationType.toLowerCase().equals("internalinteractions"))
				{
					
					List<Interaction> interactions=iHandler.GetPartInteractionsDetailed(partName);
					output=xmlBuilder.InternalInteractionsToXML(interactions);		
				}
		}		
		else if (dataType.equals("molecularforms"))	
		{	
			MolecularFormTypeDataHandler handler=new MolecularFormTypeDataHandler();
			List<MolecularFormType> formTypes=handler.GetTypes();		
			output=xmlBuilder.FormTypesToXML(formTypes);		
		}
		
		else if (dataType.equals("formulas"))	
		{	
			MathDataHandler handler=new MathDataHandler();
			List<Formula> formulas=handler.GetFormulas();		
			output=xmlBuilder.FormulasToXML(formulas);		
		}
	}	
	return output;
}

public String SerializeParts(String format,String partType, String roleName, String roleValue, String page,String summary, HttpServletRequest request) throws Exception
{
	String applicationbaseURL="http://" + request.getServerName();
	if (request.getServerPort()!=80)
	{
		applicationbaseURL=applicationbaseURL + ":" + request.getServerPort();
	}
	String output="";
	XMLBuilder xmlBuilder=new XMLBuilder();
	int pageNumber=1;
	if (page!=null)
	{
		pageNumber=Integer.parseInt(page);
	}
	PartDataHandler handler=new PartDataHandler();
	if ("true".equals(summary))
	{
		int partCount=0;
		if (partType==null)
		{
			partCount=handler.GetVPartsCount();
		}
		else
		{
			if (roleName!=null && roleValue!=null)
			{
				partCount=handler.GetVPartsCount(partType, roleName, roleValue);
			}
			else
			{
				partCount=handler.GetVPartsCount(partType);
			}
		}
		//int pageCount=(int)Math.ceil(partCount/50.0);
		int pageCount=Util.GetPageCount(partCount,50);
		virtualparts.entity.Summary summaryObject=new virtualparts.entity.Summary();
		summaryObject.setPageCount(pageCount);
		output=xmlBuilder.SummaryToXML(summaryObject);		
	}
	else
	{
		List<Part> parts=null;
		if (partType==null)
		{
			parts=handler.GetVParts(pageNumber);
		}
		else
		{
			if (roleName!=null && roleValue!=null)
			{
				parts=handler.GetVParts(partType, roleName, roleValue, pageNumber);
			}
			else
			{
				parts=handler.GetVParts(partType, pageNumber);
			}
		}
		if (format=="xml")
		{
			for (Part part : parts)
			{
				part.setSequence("![CDATA[" + part.getSequence() + "]]");
			}		
			output=xmlBuilder.ToXML(parts);		
		}
		else if (format.equals("sbol"))
		{
			
			String collectionURL=AddUrlPart(applicationbaseURL,partType);
			collectionURL=AddUrlPart(collectionURL,roleName);
			collectionURL=AddUrlPart(collectionURL,roleValue);
			collectionURL=AddUrlPart(collectionURL,"page/" + pageNumber);
			
			
			SBOLDocument document=SBOLConverter.GetSBOL(parts,applicationbaseURL,collectionURL);
			output=SBOLConverter.Serialize(document);				
		}
	}
	return output;
}

private String AddUrlPart(String baseURL, String toAdd)
{
	if (toAdd!=null && toAdd.length()>0)
	{
		baseURL= baseURL + "/" + toAdd;
		
	}
	return baseURL;
}
}

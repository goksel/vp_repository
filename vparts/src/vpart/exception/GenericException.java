package vpart.exception;

public class GenericException extends Exception {
	  private String message; // a detailed message 
	  private Exception exception = null;
	  private String separator = "\n"; // line separator
	  
	  public GenericException(String message, Exception exception) {
		  super(message, exception);		 	    
	  } 
	  public GenericException(String message) {
		  super(message);		 	    
	  } 
	}


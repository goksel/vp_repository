package vpart.exception;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ExceptionLogger {
	public static void Log(Throwable exception)
	{		
		String error=GetExceptionString(exception);
		Logger logger=Logger.getLogger(ExceptionLogger.class.getSimpleName());		
		logger.log(Level.SEVERE,error);		
	}
	
	public static String GetExceptionString(Throwable exception)
	{
		Writer writer=new StringWriter();
		PrintWriter pWriter=new PrintWriter(writer);
		exception.printStackTrace(pWriter);
		return writer.toString();		
	}
}

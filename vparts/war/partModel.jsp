<%@page import="vparts.model.ModelHandler"%>
<%@page import="virtualparts.model.ModelOutput"%>
<%@page import="vparts.data.PartDataHandler"%>
<%@ page language="java" import="java.util.*"%>
<%-- 
    Document   : index2.jsp
    Created on : 03-Dec-2008, 16:08:19
    Author     : A8901379
--%>

<%@page contentType="text/xml" pageEncoding="UTF-8"%>
<%@page import="vparts.entity.VP_Part"%>
<%@page import="vparts.PartsHandler"%>
<%@page import="vparts.entity.VP_Interaction"%>
<%@page import="vparts.entity.VP_Parameter"%>
<%@page import="vparts.entity.VP_Role"%>
<%
	String partName = request.getParameter("name");	
	String modelType= request.getParameter("model");	
	PartDataHandler handler=new PartDataHandler();
	ModelHandler modelBuilder=new ModelHandler();
	ModelOutput model=modelBuilder.GetPartModel(partName,modelType);

	
%>
<?xml version="1.0" encoding="utf-8"?>


<cellmlModel>
<main>

    
    <%
    	out.print(model.getMain());
    %>

</main>
<%
if (model.getImports()!=null && model.getImports().size()>0){
	for (String importModel: model.getImports()){
%>

<importFiles>
<%=importModel%>
</importFiles>
<%}}%>
</cellmlModel>


<%@ page language="java" import="java.util.*" %>
<%-- 
    Document   : index2.jsp
    Created on : 03-Dec-2008, 16:08:19
    Author     : A8901379
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Virtual Parts</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="container">

<jsp:include page ="header.jsp"/>

<div id="container2">

<div id="content">
<h2>About Virtual Parts</h2>
<p>"Virtual Parts" is a catalogue of biological parts. Parts are provided in the form of mathematical models that can be composed to create genetic circuits.
<br/><br/>
This project is supported by EPSRC.
</p>
<br/>
<i>Developed by <a href="mailto:goksel.misirli@ncl.ac.uk">Dr. Goksel Misirli</a>, <a href="mailto:jennifer.hallinan@ncl.ac.uk">Dr. Jennifer Hallinan</a> and <a href="mailto:anil.wipat@ncl.ac.uk">Prof. Neil Wipat</a> in the School of Computing Science, Newcastle University, UK. </i>
<br/><br/><img src="/images/kappa-logo.png" height="14px" style="padding-top:-10px" alt="Kappa"/><i> support developed with Prof. Vincent Danos, Edinburgh University, UK.</i>
<br/><br/>
</div>

<jsp:include page ="footer.jsp"/>

</div>

</div>
</body>
</html>


<%@page import="virtualparts.entity.Part"%>
<%@page import="vparts.Util"%>
<%@page import="vparts.data.PartDataHandler"%>
<%@ page language="java" import="java.util.*" %>
<%-- 
    Document   : index2.jsp
    Created on : 03-Dec-2008, 16:08:19
    Author     : A8901379
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">
<%@page import="vparts.entity.VP_Part"%>
<%@page import="vparts.PartsHandler"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<%
	PartDataHandler handler=new PartDataHandler();

String pageNumberString = request.getParameter("page");
int pageNumber=1;
if (pageNumberString!=null)
{
	pageNumber=Integer.parseInt(pageNumberString);
}
 List<Part> parts= handler.GetVParts(pageNumber);  
 Set<String> modelTypes=handler.GetModelTypes(parts);
 
 int partCount= handler.GetVPartsCount();
//int pageCount=Integer.parseInt(String.valueOf(partCount/50));
int pageCount=Util.GetPageCount(partCount,50);
%>
<head>
<title>Virtual Parts</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="container">
<jsp:include page ="header.jsp"/>
<div id="container2">
<div id="content">
<h2>Standard Virtual Parts</h2>
<p> This page displays the list of standard virtual parts available in the database. The SBOL export is now supported for the DNA feature level information.
<br/><br/>
<font style="color:#D00000">The database is still under construction and being manually curated.</font> 
</p>
<%
	for (int i=1;i<=pageCount;i++)
{
	if (i==pageNumber)
	{
%>
		<bold><a id="pageNumber" href="/parts/page/<%=i%>"><%=i%></a></bold>&nbsp;
		<%
			}
			else
			{
		%>
		<a href="/parts/page/<%=i%>"><%=i%></a>&nbsp;
		<%
			}
				}
		%>
    <table>
        <caption>List of parts</caption>
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Type</th>  
            <th scope="col">Name</th>
            <th scope="col" class="centercell">Organism</th>  
            <th scope="col" class="centercell">Repository format</th>
            <th scope="col" class="centercell">SBML Export</th>  
            <th scope="col" class="centercell">Kappa Export</th>              
            <th scope="col" class="centercell">SBOL Export</th>            
          </tr>
        </thead>       
        <tbody>
            <%
            	Iterator<Part> iterator= parts.iterator();
                                                                         while (iterator.hasNext())
                                                                         {
                                                                             Part part=(Part)iterator.next();
            %>
          <tr <%if (part.getDesignMethod()!=null && part.getDesignMethod().equals("Manual")){%>class="manual"<%}%>>
              <th scope="row" ><a href="/part/<%=part.getName()%>"><% if (part.getName().length()>=22){out.print(part.getName().substring(0,19) + "...");}else{out.print(part.getName());}%></a></th>          
            <td><%=part.getType()%></td>
            <td><% if (part.getDisplayName()!=null) out.print(part.getDisplayName().replace("||",", "));%></td>  
            <td class="centercell"><% if (part.getOrganism()!=null){%><span title="<%=part.getOrganism()%>"><%=PartDataHandler.GetOrganismCode(part.getOrganism())%></span><%} %></td>
            <td class="centercell"><a target="new" href="/part/<%=part.getName()%>/xml">VPR</a></td>  
            <td class="centercell"><% if (modelTypes.contains(part.getName() + "_sbml")){%><a target="new" href="/part/<%=part.getName()%>/sbml">SBML</a><%} %></td>  
            <td class="centercell"><% if (modelTypes.contains(part.getName() + "_kappa")){%><a target="new" href="/part/<%=part.getName()%>/kappa">Kappa</a><%} %></td>              
            <td class="centercell"><a target="new" href="/part/<%=part.getName()%>/sbol">SBOL</a></td>                                    
          </tr>	            
         <%}%>
         
        </tbody>
      </table>
</div>

<div id="sidebar">
<h2>Filter virtual parts </h2>
<p>Filter virtual parts by choosing one of the options below</p>
	<div class="sidebaritem">
          <h3>By Type</h3>
          <div class="sbilinks">           
            <ul>
             <li><a href="/parts/Promoter">Promoter</a></li>
             <li><a href="/parts/FunctionalPart">FunctionalPart</a></li>
             <li><a href="/parts/RBS">RBS</a></li>
             <li><a href="/parts/Shim">Shim</a></li>
             <li><a href="/parts/Terminator">Terminator</a></li>
             <li><a href="/parts/Operator">Operator</a></li>             
            </ul>
          </div>
        </div>
		<br style="clear:both"/> 
		<br style="clear:both"/> 
		<br style="clear:both"/> 		
	    <img src="/images/fp_cvi_logo.gif" style="margin-left:auto;margin-right:auto;display:block"/>
	    <br style="clear:both"/> 
		<br style="clear:both"/> 
	    <img src="/images/flowers_small.jpg" style="margin-left:auto;margin-right:auto;display:block" /> 
	        
	
</div>

<jsp:include page ="footer.jsp"/>

</div>

</div>
</body>
</html>


<%@page import="virtualparts.entity.MolecularFormType"%>
<%@page import="vparts.data.MolecularFormTypeDataHandler"%>
<%@ page language="java" import="java.util.*" %>
<%-- 
    Document   : index2.jsp
    Created on : 03-Dec-2008, 16:08:19
    Author     : A8901379
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">

<%@page import="vparts.entity.VP_Part"%>
<%@page import="vparts.PartsHandler"%>
<%@page import="vparts.entity.VP_Role"%>
<%@page import="vparts.entity.VP_MolecularFormType"%><html xmlns="http://www.w3.org/1999/xhtml">
<%
	MolecularFormTypeDataHandler handler=new MolecularFormTypeDataHandler();
 List<MolecularFormType> types= handler.GetTypes();
%>
<head>
<title>V parts</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="container">

<jsp:include page ="header.jsp"/>

<div id="container2">

<div id="content">
<h2>Molecular Forms</h2>
<p> This page displays the list of molecular forms that parts may have. Each molecular form is handled as a different species and concentrations are calculated accordingly.
<!-- <br/><br/><a href="/molecularforms/xml" target="_blank">View as xml</a> -->
</p>


    <table>
        <caption>List of molecular forms</caption>
        <thead>
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Accession</th>        
          </tr>
        </thead>       
        <tbody>
            <%
            	Iterator<MolecularFormType> iterator= types.iterator();
                         while (iterator.hasNext())
                         {
                        	 MolecularFormType type=(MolecularFormType)iterator.next();
            %>
          <tr>
              <th scope="row"><%=type.getName()%></th>          
            <td><%=type.getAccession()%></td>
          </tr>	            
         <%}%>
         
        </tbody>
      </table>
</div>


<jsp:include page ="footer.jsp"/>

</div>

</div>
</body>
</html>


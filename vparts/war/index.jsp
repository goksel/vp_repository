<%@page import="java.net.URLEncoder"%>
<%@page import="virtualparts.serialize.Serializer"%>
<%@page import="virtualparts.entity.Part"%>
<%@page import="vparts.Util"%>
<%@page import="vparts.data.PartDataHandler"%>
<%@ page language="java" import="java.util.*" %>
<%-- 
    Document   : index2.jsp
    Created on : 03-Dec-2008, 16:08:19
    Author     : A8901379
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">
<%@page import="vparts.entity.VP_Part"%>
<%@page import="vparts.PartsHandler"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<%
PartDataHandler handler=new PartDataHandler();

String searchObject = null;
List<Part> parts=null;
Set<String> modelTypes=null;
int pageCount=-1;
int partCount=-1;
int pageNumber=-1;

if (request.getAttribute("page")!=null)
{
	pageNumber = Integer.parseInt(request.getAttribute("page").toString());
}
if (request.getAttribute("searchobject")!=null)
{
	searchObject = request.getAttribute("searchobject").toString();
}
if (request.getAttribute("partcount")!=null)
{
	partCount = Integer.parseInt(request.getAttribute("partcount").toString());	
	pageCount=Util.GetPageCount(partCount,50);
}
if (request.getAttribute("parts")!=null)
{
	parts = (List<Part>)request.getAttribute("parts");
	 modelTypes=handler.GetModelTypes(parts);	 
}
%>
<head>
<title>Virtual Parts</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="container">
<jsp:include page ="header.jsp"/>
<div id="container2">
<div id="content">
<%if (parts==null || parts.size()==0) {%>

<h2>Standard Virtual Parts</h2>

Virtual Parts is a repository of standard virtual parts (SVPs) which are reusable, modular and composable models of physical biological parts.
These models can be joined together computationally in order to facilitate the model-driven design of large-scale biological systems. Models are available in SBML and Kappa format. Genetic descriptions of parts can be retrieved using SBOL.
The database is still under construction and being manually curated.<br/><br/>
<%} %>	
Please use the form below to manually search for SVPs. For computational access to the repository, please use the <a href="/repositorydocumentation#computationalaccess">Web service</a> interface or the <a href="/repositorydocumentation#jparts">Java API</a>.  
<br/><br/>
 
<div id="formsection">
<form action="svpsearch" method="post" enctype="application/x-www-form-urlencoded" id="searchform">
 
 <span class="spanwithinput">Search text</span><input class="input" type="text" name="text"><br>
 <span>Type</span>
 <select name="type">
  <option value="NONE">Choose a type</option>
  <option value="Promoter">Promoter</option>
  <option value="FunctionalPart">Functional Part</option>
  <option value="RBS">RBS</option>
  <option value="Shim">Shim</option>
  <option value="Terminator">Terminator</option>
  <option value="Operator">Operator</option>
</select>
<br/>
 <span>Source organism</span>
 <select name="organism">
  <option value="NONE">Choose an organism</option>
  <option value="Bacillus subtilis">Bacillus subtilis</option>
  <option value="Escherichia coli">Escherichia coli</option>
</select>
<br/>
 <select name="property">
  <option value="NONE">Filter by property</option>
  <option value="type">Type</option>
  <option value="has_function">Has Function</option>
  <option value="located_in">Located In</option>
  <option value="participates_in">Participates In</option>
</select>
<input class="input" type="text" name="propertyvalue"><br>
(Examples: Has Function "catalytic", Has Function "GO_0000156", Type "C0G0436", Type "Inducible Promoter", Type "Enzyme")	
<br/>
<span class="spanwithinput">Include all genetic elements</span>
<input type="checkbox" name="allgeneticelements" value="All Genetic Elements"/>
<br/> 	  
 <span class="spanwithinput"></span><input type="submit" value="Search" id="submit" class="submit">
</form>
</div>
<br/>

<% if (parts!=null && parts.size()>0){%>
<form action="svpsearch" method="post" enctype="application/x-www-form-urlencoded" id="pagingform">
<%
	for (int i=1;i<=pageCount;i++)
{
	if (i==pageNumber)
	{
%>
		<input class="boldpage" type="submit" name="page" value="<%=i%>"/>&nbsp;
		<%
			}
			else
			{
		%>
		<input class="page" type="submit" name="page" value="<%=i%>"/>&nbsp;
		<%
			}
				}
		%>
		
<input type="hidden" name="partcount" value="<%=partCount%>">
<input type="hidden" name="searchobject" value="<%=URLEncoder.encode(searchObject,"UTF-8")%>">		
</form>

<table>
        <caption>List of parts</caption>
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Type</th>  
            <th scope="col">Name</th>
            <th scope="col" class="centercell">Organism</th>  
            <th scope="col" class="centercell">Repository format</th>
            <th scope="col" class="centercell">SBML Export</th>  
            <th scope="col" class="centercell">Kappa Export</th>              
            <th scope="col" class="centercell">SBOL Export</th>                 
          </tr>
        </thead>       
        <tbody>
            <%
            	Iterator<Part> iterator= parts.iterator();
                                                                         while (iterator.hasNext())
                                                                         {
                                                                             Part part=(Part)iterator.next();
            %>
          <tr <%if (part.getDesignMethod()!=null && part.getDesignMethod().equals("Manual")){%>class="manual"<%}%>>
              <th scope="row" ><a href="/part/<%=part.getName()%>"><% if (part.getName().length()>=22){out.print(part.getName().substring(0,19) + "...");}else{out.print(part.getName());}%></a></th>          
            <td><%=part.getType()%></td>
            <td><% if (part.getDisplayName()!=null) out.print(part.getDisplayName().replace("||",", "));%></td>  
            <td class="centercell"><% if (part.getOrganism()!=null){%><span title="<%=part.getOrganism()%>"><%=PartDataHandler.GetOrganismCode(part.getOrganism())%></span><%} %></td>
            <td class="centercell"><a target="new" href="/part/<%=part.getName()%>/xml">VPR</a></td>  
            <td class="centercell"><% if (modelTypes.contains(part.getName() + "_sbml")){%><a target="new" href="/part/<%=part.getName()%>/sbml">SBML</a><%} %></td>  
            <td class="centercell"><% if (modelTypes.contains(part.getName() + "_kappa")){%><a target="new" href="/part/<%=part.getName()%>/kappa">Kappa</a><%} %></td>                          
            <td class="centercell"><a target="new" href="/part/<%=part.getName()%>/sbol">SBOL</a></td>                                    
          </tr>	            
         <%}%>
         
        </tbody>
      </table>
  <%} %>    
</div>
<div id="sidebar">	
		<br style="clear:both"/> 
		<br style="clear:both"/> 
		<br style="clear:both"/> 		
	    <img src="/images/fp_cvi_logo.gif" style="margin-left:auto;margin-right:auto;display:block"/>
	    <br style="clear:both"/> 
		<br style="clear:both"/> 
	    <img src="/images/flowers_small.jpg" style="margin-left:auto;margin-right:auto;display:block" /> 	        
</div>
	    
<jsp:include page ="footer.jsp"/>

</div>

</div>
</body>
</html>


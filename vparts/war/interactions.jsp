<%@page import="vparts.Util"%>
<%@page import="vparts.data.InteractionDataHandler"%>
<%@ page language="java" import="java.util.*" %>
<%-- 
    Document   : index2.jsp
    Created on : 03-Dec-2008, 16:08:19
    Author     : A8901379
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">

<%@page import="vparts.entity.VP_Part"%>
<%@page import="vparts.PartsHandler"%>
<%@page import="vparts.entity.VP_Interaction"%><html xmlns="http://www.w3.org/1999/xhtml">
<%
	InteractionDataHandler handler = new InteractionDataHandler();
	//List<Interaction> interactions = handler.GetInteractions();
	
	
	String pageNumberString = request.getParameter("page");
	String interactionTypeString=request.getParameter("type");
	int pageNumber=1;
	if (pageNumberString!=null)
	{
		pageNumber=Integer.parseInt(pageNumberString);
	}
	int partCount=-1;
	List<VP_Interaction> interactions=null;
	if (interactionTypeString==null)
	{
	 	interactions= handler.GetInteractions(pageNumber);   
	 	partCount= handler.GetInteractionsCount();
	}
	else
	{
		interactions= handler.GetInteractions(pageNumber,interactionTypeString);   
	 	partCount= handler.GetInteractionsCount(interactionTypeString);
	}
	int pageCount=Util.GetPageCount(partCount,50);
	HashSet<String> interactionTypes=handler.GetInteractionTypes();
%>
<head>
<title>Virtual Parts</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="container">

<jsp:include page ="header.jsp"/>

<div id="container2">

<div id="content">
<h2>Interaction of Parts</h2>
<%
	if (interactionTypeString==null){
%>
<p> This page displays the list of parts' interactions</p>
<%
	}
else
{
%>
<p> This page displays <%=interactionTypeString%> interactions</p>
<%
	}
%>
<!-- <br/><br/><a href="/xml" target="_blank">View as xml</a>-->


<%
	if (interactionTypeString==null)
{
	for (int i=1;i<=pageCount;i++)
	{
		if (i==pageNumber)
		{
%>
			<bold><a id="pageNumber" href="/interactions/page/<%=i%>"><%=i%></a></bold>&nbsp;
			<%
				}
					else
					{
			%>
			<a href="/interactions/page/<%=i%>"><%=i%></a>&nbsp;
			<%
				}
				}
			}
			else
			{
				for (int i=1;i<=pageCount;i++)
				{
					if (i==pageNumber)
					{
			%>
			<bold><a id="pageNumber" href="/interactions/<%=interactionTypeString%>/page/<%=i%>"><%=i%></a></bold>&nbsp;
			<%
				}
					else
					{
			%>
			<a href="/interactions/<%=interactionTypeString%>/page/<%=i%>"><%=i%></a>&nbsp;
			<%
				}
				}
			}
			%>

    <table>
        <caption>List of interactions</caption>
        <thead>
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Parts</th>        
            <th scope="col">Type</th>                    
            <th scope="col">SBML Link</th>                                            
          </tr>
        </thead>       
        <tbody>
            <%
            	Iterator<VP_Interaction> iterator = interactions.iterator();
                        	while (iterator.hasNext()) {
                        		VP_Interaction interaction = (VP_Interaction) iterator.next();
            %>
					<tr>
						<th scope="row"><a href="/interaction/<%=interaction.getName()%>"><%=interaction.getName()%></a></th>
						<td><%=interaction.getPartNames().replace(",",", ")%></td>
						<td><%=interaction.getInteractionType()%></td>
						<td><a target="new" href="/interaction/<%=interaction.getName()%>/sbml">SBML</a></td>										
					</tr>
			<%
				}
			%>
         
        </tbody>
      </table>
</div>

<div id="sidebar">
<h2>Filter interactions</h2>
<p>Filter the interactions by choosing one of the options below</p>
	<div class="sidebaritem">
          <h3>By Type</h3>
          <div class="sbilinks">           
            <ul>
             <% Iterator<String> iteratorInteractionType=interactionTypes.iterator();
             while (iteratorInteractionType.hasNext())
             {
            	 String interactionType=iteratorInteractionType.next();
             %>
             	<li><a href="/interactions/<%=interactionType%>"><%=interactionType%></a></li>
             <%
             }
             %>
            </ul>
          </div>
        </div>

	
</div>

<jsp:include page ="footer.jsp"/>

</div>

</div>
</body>
</html>


<%@page import="java.net.URLEncoder"%>
<%@page import="virtualparts.entity.Part"%>
<%@page import="virtualparts.entity.Property"%>
<%@page import="vparts.Util"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="vparts.entity.VP_Role"%>
<%@page import="vparts.data.PropertyDataHandler"%>
<%@page import="vparts.data.PartDataHandler"%>
<%@ page language="java" import="java.util.*" %>
<%-- 
    Document   : index2.jsp
    Created on : 03-Dec-2008, 16:08:19
    Author     : A8901379
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">
<%@page import="vparts.entity.VP_Part"%>
<%@page import="vparts.PartsHandler"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<%
	String partType = request.getParameter("type");
String partRole = request.getParameter("role");
String roleValue = request.getParameter("roleValue");


PartDataHandler handler=new PartDataHandler();

String pageNumberString = request.getParameter("page");
int pageNumber=1;
if (pageNumberString!=null)
{
	pageNumber=Integer.parseInt(pageNumberString);
}  
int partCount=1;
List<Part> parts=null;
String pagingLink="";
if (partType!=null)
{
	partCount= handler.GetVPartsCount(partType,partRole,roleValue);
	parts= handler.GetVParts(partType,partRole,roleValue,pageNumber); 
	pagingLink="/parts/" + partType + "/page/";
}
else
{
	partType="property";
	boolean includeAll=false;
	if ("sbol-nbt".equals(roleValue))
	{
		includeAll=true;
	}
	partCount= handler.GetVPartsCount(null, null, null, partRole, roleValue, true, includeAll);

	parts= handler.GetVParts(null, null, null, partRole, roleValue, true, includeAll,pageNumber);	
}

//int pageCount=(int)Math.ceil(partCount/50.0);
int pageCount=Util.GetPageCount(partCount,50);
 
 Set<String> modelTypes=handler.GetModelTypes(parts);
 PropertyDataHandler roleHandler=new PropertyDataHandler();
 String roleDescription=roleValue;
 

 HashMap<String,List<Property>>  groupedRolesMap=roleHandler.GetRoleIDsByPartType(partType);
 

 if (partRole!=null && !partRole.equals(""))
 {
	 Property currentRole=roleHandler.GetRole(partRole,roleValue);
	 if (currentRole.getDescription()!=null && currentRole.getDescription().length()>0)
	 {
		 roleDescription=currentRole.getDescription();
	 }
	 pagingLink="/parts/" + partType + "/" + partRole + "/" + roleValue +  "/page/";
 }
%>
<head>
<title>Virtual Parts</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="container">
<jsp:include page ="header.jsp"/>
<div id="container2">
<div id="content">
<h2>Standard Virtual Parts</h2>
<p> This page displays the list of standard virtual parts available in the database.
<br/><br/>
<a href="/parts/xml" target="_blank">View as xml</a>
</p>

<%
	if (partType!=null && !partType.equals("") && !partType.equals("property"))
{
%>
<h2><%=partType%> Parts</h2>
<%
	}
%>

<%
	if (partRole!=null && !partRole.equals(""))
{
%>

<h3><%=partRole%>: <%=roleDescription%></h3>
<%
	}
%>

<%
	for (int i=1;i<=pageCount;i++)
{
	if (i==pageNumber)
	{
%>
		<a id="pageNumber" href="<%=pagingLink + i%>"><%=i%></a>&nbsp;
		<%
			}
			else
			{
		%>
		<a href="<%=pagingLink + i%>"><%=i%></a>&nbsp;
		<%
			}
				}
		%>

    <table>
        <caption>List of parts</caption>
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Type</th>  
            <th scope="col">Name</th>  
            <th scope="col" class="centercell">Organism</th>  
            <th scope="col" class="centercell">Repository format</th>
            <th scope="col" class="centercell">SBML Export</th>  
            <th scope="col" class="centercell">SBOL Export</th>  
          </tr>
        </thead>       
        <tbody>
            <%
            	Iterator<Part> iterator= parts.iterator();
                                                                                     while (iterator.hasNext())
                                                                                     {
                                                                                         Part part=(Part)iterator.next();
            %>
           <tr <%if (part.getDesignMethod()!=null && part.getDesignMethod().equals("Manual")){%>class="manual"<%}%>>
              <th scope="row"><a href="/part/<%=part.getName()%>"><%=part.getName()%></a></th>          
            <td><%=part.getType()%></td>
            <td><%
            	if (part.getDisplayName()!=null) {out.print(part.getDisplayName().replace("||",", "));}
            %></td>  
            <td class="centercell"><% if (part.getOrganism()!=null){%><span title="<%=part.getOrganism()%>"><%=PartDataHandler.GetOrganismCode(part.getOrganism())%></span><%} %></td>
            <td class="centercell"><a target="new" href="/part/<%=part.getName()%>/xml">VPR</a></td>  
            <td class="centercell"><% if (modelTypes.contains(part.getName() + "_sbml")){%><a target="new" href="/part/<%=part.getName()%>/sbml">SBML</a><%} %></td>     
            <td class="centercell"><a target="new" href="/part/<%=part.getName()%>/sbol">SBOL</a></td>                         
          </tr>	            
         <%
	                     	}
	                     %>
         
        </tbody>
      </table>
</div>

<div id="sidebar">
<% if (partType!=null && !partType.equals("") && !partType.equals("property")){ %>	
	<h2>Filter Virtual Parts</h2>
	<p>Filter virtual parts by choosing one of the options below</p>
	<%
		for (Iterator<Map.Entry<String,List<Property>>> iteratorGroupedRolesMap=groupedRolesMap.entrySet().iterator();iteratorGroupedRolesMap.hasNext();)
	{ 
		Map.Entry<String,List<Property>> entry=iteratorGroupedRolesMap.next();
		List<Property> roles=entry.getValue();
		if (entry.getKey().equals("has_function") || entry.getKey().equals("type") || entry.getKey().equals("located_in") || entry.getKey().equals("participates_in"))
		{
	%>
			<div class="sidebaritem">
		          <div id="formsection">
<form action="/svpsearch" method="post" enctype="application/x-www-form-urlencoded" id="searchform"> 
<span style=" font-weight: bold"><bold>By <%=entry.getKey()%></bold></span>
<input type="hidden" name="type" value="<%=partType%>">
<input type="hidden" name="property" value="<%=entry.getKey()%>">
 <select name="propertyvalue">
 <option value="NONE">Choose a term to search</option>
  <%
	             	for(Property property:roles) {
	             		if (property.getValue()!=null && !property.getValue().contains("/")){
	             %>
	             	<option value="<%=property.getValue()%>"><%if (property.getDescription()==null)out.print(property.getValue()); else out.print(property.getDescription());%></option>
	             <%
	             		}
	             	}
	             %>	             
</select>
<br/>

<input type="submit" value="Search" id="submit" class="submit">
</form>
</div>
</div>
		
		<%}
		}
	}
%>

	
</div>

<jsp:include page ="footer.jsp"/>

</div>

</div>
</body>
</html>


<%@page import="virtualparts.entity.Part"%>
<%@page
	import="vparts.model.XMLBuilder"%><%@page
	import="vparts.data.PartDataHandler"%><%@ page language="java"
	import="java.util.*"%><%@page contentType="text/xml"
	pageEncoding="UTF-8"%><%
	String output="";
	String dataType= request.getParameter("type");
	String filter=request.getParameter("filter");
	PartDataHandler handler=new PartDataHandler ();
	if ("parts".equals(dataType))
	{
		List<Part> parts=handler.GetVPartsByParameter(filter);
		XMLBuilder xmlBuilder=new XMLBuilder();
		output=xmlBuilder.ToXML(parts);	
	}
%><%=output%>
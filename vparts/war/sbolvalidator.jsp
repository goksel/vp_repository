<%@page import="vparts.security.Auth"%>
<%@ page language="java" import="java.util.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<title>Virtual Parts</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="container">

<jsp:include page ="header.jsp"/>

<div id="container2">

<div id="content">
<%
Object att=request.getAttribute("message");
if (att!=null)
{
	out.println(att.toString());
}
%>

<h3>SBOL Validator</h3>

<form action="sbolvalidator" method="post" enctype="application/x-www-form-urlencoded">
 Paste the SBOL data to the text area below:<br/>
<textarea rows="30" cols="100" name="data"></textarea>
<br />
<input type="submit" value="Validate SBOL"/>
</form>


 <br/><br/><br/>
</div>

<jsp:include page ="footer.jsp"/>

</div>

</div>
</body>
</html>


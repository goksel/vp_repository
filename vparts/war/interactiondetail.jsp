<%@page import="virtualparts.entity.Constraint"%>
<%@page import="virtualparts.entity.InteractionPartDetail"%>
<%@page import="virtualparts.entity.Part"%>
<%@page import="virtualparts.entity.Interaction"%>
<%@page import="virtualparts.entity.Parameter"%>
<%@page import="vparts.data.InteractionDataHandler"%>
<%@ page language="java" import="java.util.*"%>
<%-- 
    Document   : index2.jsp
    Created on : 03-Dec-2008, 16:08:19
    Author     : A8901379
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">

<%@page import="vparts.entity.VP_Part"%>
<%@page import="vparts.PartsHandler"%>
<%@page import="vparts.entity.VP_Interaction"%>
<%@page import="vparts.entity.VP_InteractionPartDetail"%>
<%@page import="vparts.entity.VP_Constraint"%><html
	xmlns="http://www.w3.org/1999/xhtml">
<%
	String name = request.getParameter("name");

	InteractionDataHandler handler = new InteractionDataHandler();
	Boolean update=false;
	String partName="";	
	
	Interaction interaction = handler.GetInteractionDetailed(name);
	List<Part> parts = handler.GetInteractionParts(name);
	List<InteractionPartDetail> partDetails=interaction.getPartDetails();
	List<Parameter> parameters=interaction.getParameters();
	List<Constraint> constraints=interaction.getConstraints();	
%>
<head>
<title>Virtual Parts</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="container"><jsp:include page="header.jsp" />

<div id="container2">

<div id="content">
<h2><%=interaction.getName()%></h2>
<p><%=interaction.getDescription()%> <br />
<!-- <br /><a href="/xml" target="_blank">View as xml</a></p> -->
<table>
	<caption>Interaction Details:</caption>
	<tbody>
		<tr>
			<th scope="row">Type</th>
			<td><%=interaction.getInteractionType()%></td>
		</tr>
		<tr>
			<th scope="row">Is Reaction?</th>
			<td><%=interaction.getIsReaction()%></td>
		</tr>
		<tr>
			<th scope="row">Freetext Math</th>
			<td><%=interaction.getFreeTextMath()%></td>
		</tr>
		<tr>
			<th scope="row">Math Type</th>
			<td><%=interaction.getMathName()%></td>
		</tr>
	</tbody>
</table>

<table>
	<caption>Parts:</caption>
	<thead>
		<tr>
			<th scope="col">Name</th>
			<th scope="col">Type</th>
		</tr>
	</thead>
	<tbody>
		<%
			Iterator<Part> iterator = parts.iterator();
			while (iterator.hasNext()) {
				Part part = iterator.next();
		%>
		<tr>
			<th scope="row"><a href="/part/<%=part.getName()%>"><%=part.getName()%></a></th>
			<td><%=part.getType()%></td>
		</tr>
		<%
			}
		%>
	</tbody>
</table>
<%
	if (partDetails!=null && partDetails.size()>0){
%>
<table>
	<caption>Interaction Constraints:</caption>
	<thead>
		<tr>
			<th scope="col">Name</th>
			<th scope="col">Molecular Form</th>
			<th scope="col">Stoichiometry</th>
			<th scope="col">Role</th>
			<th scope="col">Name in Math</th>
		</tr>
	</thead>
	<tbody>
		<%
			Iterator<InteractionPartDetail> partDetailIterator = partDetails
					.iterator();
			while (partDetailIterator.hasNext()) {
				InteractionPartDetail partDetail = partDetailIterator.next();
		%>
		<tr>
			<th scope="row"><a href="/part/<%=partDetail.getPartName()%>"><%=partDetail.getPartName()%></a></th>
			<td><%=partDetail.getPartForm()%></td>
			<td><%=partDetail.getNumberOfMolecules()%></td>
			<td><%=partDetail.getInteractionRole()%></td>
			<td><%=partDetail.getMathName()%></td>
		</tr>
		<%
			}
		%>
	</tbody>
</table>
<%
	}
%>

<%
	if (constraints!=null && constraints.size()>0) {
%>
<table>
	<caption>Additional Constraints:</caption>
	<thead>
		<tr>
			<th scope="col">Source Type</th>
			<th scope="col">Source Name</th>
			<th scope="col">Target Type</th>
			<th scope="col">TargetName</th>
			<th scope="col">Qualifier</th>
		</tr>
	</thead>
	<tbody>
		<%
			Iterator<Constraint> constraintIterator = constraints.iterator();
			while (constraintIterator.hasNext()) {
				Constraint constraint= constraintIterator.next();
		%>
		<tr>
			<td><%=constraint.getSourceType()%></td>
			<td><%=constraint.getSourceID()%></td>			
			<td><%=constraint.getTargetType()%></td>
			<td><%=constraint.getTargetID()%></td>
			<td><%=constraint.getQualifier()%></td>
		</tr>
		<%
			}
		%>
	</tbody>
</table>
<%
	}
%>


<%
	if (parameters!=null && parameters.size()>0) {
%>
<table>
	<caption>Parameters:</caption>
	<thead>
		<tr>
			<th scope="col">Name</th>
			<th scope="col">Type</th>
			<th scope="col">Value</th>
			<th scope="col">Evidence Code</th>			
		</tr>
	</thead>
	<tbody>
		<%
			Iterator<Parameter> parameterIterator = parameters.iterator();
			while (parameterIterator.hasNext()) {
				Parameter parameter= parameterIterator.next();
		%>
		<tr>
			<td><%=parameter.getName()%></td>
			<td><%=parameter.getParameterType()%></td>			
			<td><% if (parameter.getValue()!=null) out.print(parameter.getValue());%></td>
			<td><% if(parameter.getEvidenceType()!=null) out.print(parameter.getEvidenceType());%></td>			
		</tr>
		<%
			}
		%>
	</tbody>
</table>
<%} %>

<!-- 
<h2>Accessions:</h2>
<div class="links">
<ul>
	<li><a href="NCBI:<%=interaction.getName()%>" target="_blank">Go
	to <%=interaction.getName()%>'s page in NCBI</a></li>
</ul>
</div>
</div>
 -->


<jsp:include page="footer.jsp" /></div>

</div>
</body>
</html>

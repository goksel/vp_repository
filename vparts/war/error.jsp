<%@page import="vpart.exception.ExceptionLogger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.util.logging.LogRecord"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.io.StringWriter"%>
<%@page import="java.io.Writer"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="vparts.ErrorHandler"%>
<%@page import="javax.jdo.JDOObjectNotFoundException"%>
<%@ page isErrorPage="true" %>
<%@ page language="java" import="java.util.*" %>
<%-- 
    Document   : index2.jsp
    Created on : 03-Dec-2008, 16:08:19
    Author     : A8901379
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>V parts</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="container">

<jsp:include page ="header.jsp"/>

<div id="container2">

<div id="content">
<h2>ERROR:</h2>
<h3> <% if (exception!=null) 
	{

	ExceptionLogger.Log(exception);
	exception.printStackTrace();
	out.println("Your request could not be processed!");	
	}
	
	%>
</h3>


</div>

<jsp:include page ="footer.jsp"/>

</div>

</div>
</body>
</html>


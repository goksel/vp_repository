<%@page import="vpart.exchange.Serializer"%><%@ page language="java"
	import="java.util.*"%><%@page contentType="text/xml"
	pageEncoding="UTF-8"%><%
    String output="";
	String partName = request.getParameter("name");	
	String dataType= request.getParameter("type");
	String informationType= request.getParameter("info");
	String format=request.getParameter("format");
	
	//Parts parameters
	String pageNumber=request.getParameter("page");
	String partType=request.getParameter("parttype");
	String role=request.getParameter("role");
	String roleValue=request.getParameter("rolevalue");
	String summary=request.getParameter("summary");
	if (format==null)
	{
		format="xml";
	}
	
	Serializer serializer=new Serializer();
	if ("parts".equals(dataType))
	{
		output=serializer.SerializeParts(format,partType, role,roleValue, pageNumber,summary, request);		
	}
	else
	{
		output=serializer.Serialize(dataType,informationType,format,partName,request);
	}
%><%=output%>
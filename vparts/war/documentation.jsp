<%@ page language="java" import="java.util.*" %>
<%-- 
    Document   : index2.jsp
    Created on : 03-Dec-2008, 16:08:19
    Author     : A8901379
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Virtual Parts</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="container">

<jsp:include page ="header.jsp"/>

<div id="container2">

<div id="contentLargeText">

<h1>Overview</h1>

Virtual parts has a REST-based Web service interface to access information about, and models of, biological parts and their interactions computationally. The metadata is available in an XML format. SBML models representing these parts and interactions can also be retrieved computationally and can be joined together in order to create models of large-scale biological systems.
<br/><br/>

Moreover, we developed an API, called JParts, to get information about parts and interactions, and their models as Java objects. The API has also methods to join models of parts and interactions to create simulatable models. 

<h1>The repository</h1>
Virtual parts is a repository of standard virtual parts (SVPs) which are reusable, modular models of physical biological parts. Currently, it includes around 3000 virtual parts and 700 models of interactions between them. SVPs are provided for promoters, operators, RBSs, terminators, shims and CDSs . The CDSs and their encoded products are modelled as functional parts. Currently, the SVPs in the repository are provided in the SBML exchange format.
<br/><br/>
The menu provides access to parts and interactions with each page displaying 50 items. Parts are listed with their IDs, types and names. In the part view, the side bar includes links which enable the filtering of parts by type. The ID of a part is linked to the detail page for the part, providing additional information, such as the name, description, type, sequence, source organism and whether the SVP is manually or computationally created.
<br/><br/>
SVPs are suitable for biological system design. They represent biological parts and biochemical reactions associated with those parts. These models have standard inputs and outputs that allow the construction of models of larger systems to be built from basic biological parts. Inputs and outputs are based on widely-accepted biological signals such as polymerases per second (PoPS) and ribosomes per second (RiPS), and sharing these common signals makes the models composable.
<br/><br/>
Although, SVPs can be joined together using these signals in order to construct models with the desired behaviour, trying combinations of SVPs from the repository to build larger models may not be time- or cost-effective. SVPs should therefore be selected according to their functions in order to plug them into a larger model. SVPs have additional types in addition to basic types such as promoters and RBSs. These subtypes are suitable for the filtering of parts.
<br/><br/>
SVPs can have more than one subtype. For example, an SVP may be both a SigAPromoter and a InduciblePromoter, which indicates that the part modelled is an inducible SigA promoter. Similarly operators can be filtered according to the type of regulation of their TFs. FunctionalPart SVPs are of types defined by COG classification. SVPs also have additional properties for accession, PMIDs, and the data sources from which they are retrieved. For FunctionalPart SVPs, properties also exist for their cellular locations and molecular functions in the form of GO terms. Links to these terms from the repository provide access to all parts having the same terms.
<br/><br/>
SVPs include biochemical reactions, such as degradation, that are not dependent on other parts. Such reactions are listed as internal events and are encapsulated in the models of the SVPs. Interactions of parts with other parts are also stored in the repository. Such interactions, however, must be added when interacting parts are included, in order to create simulatable models. The interactions of a part are displayed with details such as the participating parts, interaction types and descriptions, and are linked to their details pages.
<br/><br/>
The details page of an interaction includes information about the interaction’s unique ID, description, a mathematical representation of the reaction and its type. Additional information includes links to the participating parts, reaction stoichiometries, the molecular forms of the parts involved and kinetic parameters. 
<br/><br/>
Types of interactions in the repository currently include transcriptional activation, transcriptional repression, phosphorylation, dephosphorylation, transcriptional activation by an operator, and transcriptional repression by an operator.

<a name="computationalaccess"></a>
<h1>Computational access to the repository</h1>
The website is also able to act as a REST-based Web service. The flattened URLs may be used to serve XML documents that can be accessed computationally. The URLs can be constructed by adding ‘/xml’ to the end of human-accessible URLs. In addition, computational models of parts and their interactions are available as SBML documents. The parts are also available in the SBOL format to conform to the standardisation efforts in synthetic biology. The Web service interface is explained below:
<ul>
<li>/parts/page/[PAGE_NUMBER]/xml: Retrieves parts for a given page number. 
	<br/>Example: /parts/page/1/xml.</li>
<li>/parts/[PART_TYPE]/page/[PAGE_NUMBER]/xml: Retrieves parts for a given page number and part type.
Example: /parts/Promoter/page/1/xml.</li>
<li>/parts/[PART_TYPE]/[PROPERTY]/[PROPERTY_VALUE]/page/[PAGE_NUMBER]/xml: Retrieves parts for a given property name and value pair. Part type and page number are also passed parameters.
<br/>Examples:
<br/>/parts/Promoter/type/RepressiblePromoter/page/1/xml: Retrieves repressible promoters.
<br/>/parts/FunctionalPart/located_in/GO_0005886/page/1/xml: Retrieves parts that are located in the cell membrane.
 <br/>/parts/FunctionalPart/has_function/GO_0046983/page/1/xml: Retrieves parts that have dimerisation activity.</li>
<li>/parts/summary/xml: Retrieves summary information such as the page count.</li>
<li>/parts/[PART_TYPE]/summary/xml: Retrieves summary information for a given part type.</li>
<li>/parts/[PART_TYPE]/[PROPERTY]/[PROPERTY_VALUE]/summary/xml: Retrieves summary information for a given property name, value, and part type.</li>
<li>/part/[PART_ID]/xml: Retrieves the details of the part given with an ID.
<br/>Example: /part/BO_28564/xml.</li>
<li>/part/[PART_ID]/interactions/xml: Retrieves the interactions of a part given with an ID.
<br/>Example: /part/BO_28770/interactions/xml.</li>
<li>/part/[PART_ID]/sbml: Retrieves the SBML model associated with the part. 
<br/>Example: /part/BO_28770/sbml.</li>
<li>/interaction/[INTERACTION_ID] /sbml: Retrieves the SBML model of the interaction given with an ID. 
<br/>Example: /interaction/BO_28770_bi_to_BO_4242/sbml.</li>
<li>/part/[PART_ID]/sbol: Retrieves the sequence information of a part given with an ID in SBOL format.</li>
</ul>

<br/>Parts and interactions can also be filtered by the use of flattened URLs. These REST-based URLs provide identifiers for resources in the system and can easily be indexed by search engines. Basic types can be used to filter SVPs. For example, the /parts/Promoter relative URL lists the promoter parts. Properties of parts can also be used in a similar manner to filter parts. For example, the SigAPromoter  subtype can be used to retrieve a list of SigA promoters. Similarly, parts can be filtered by COG number and GO term properties. For example, the GO_0000155  and GO_0000166  terms can be used to list kinase and response regulator SVPs respectively. The website can also be accessed computationally in a similar manner.
<h4>Examples:</h4>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="/parts/Promoter/type/SigAPromoter">/parts/Promoter/type/SigAPromoter</a>
<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="/parts/FunctionalPart/has_function/GO_0000155">/parts/FunctionalPart/has_function/GO_0000155</a>
<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="/parts/FunctionalPart/has_function/GO_0000166">/parts/FunctionalPart/has_function/GO_0000166</a>
<br/>

<a name="xmlschema"></a>
<h3>XML Schema for the Web Service Access</h3>
The repository use existing standards such as SBOL and SBML to exchange data. When these standards are not suitable, data are presented using the repository's custom XML format.
Although the repository API, discussed in the next section, provides programmatic access to such data, the relavent XML schemas for parts and interactions can also be used by tools to access data in repository's custom format.
<br/><br/>
<a target="blank" href="/schema/virtualparts.entity.parts.xsd">Click here</a> to see the XML schema for parts data.
<br/>
<a target="blank" href="/schema/virtualparts.entity.interactions.xsd">Click here</a> to see the XML schema for interactions data.

<a name="jparts"></a>
<h1>The Virtual Parts API (JParts)</h1>
An API that enables programmatic access to Virtual Parts via a Web service was also developed. The API, called JParts, returns Java objects in response to Web service calls. Due to the huge number of SVPs in the database, it is not possible to retrieve all of the records for a particular request at once. Therefore, similar to the approach used in the website and Web service, all of the SVPs can be retrieved 50 records at a time using the API. SVPs are returned with their details such as name, type, and DNA sequence. SVPs can be filtered using their types and subtypes. For example, in order to retrieve the first 50 SVPs that represent SigA promoters, ‘GetParts(1,"Promoter","type" ,"SigAPromoter")’ method call can be used, in which Promoter is the type of a part and SigAPromoter is the type of a promoter. Moreover, SVPs can also be searched for according to their functional roles. For example, protein SVPs with associated GO classes can be used to find parts based on their molecular functions and cellular locations. Parts can also be retrieved by their IDs using the API.
<br/><br/>
A list of interactions for a part can be retrieved in a different method call. Additionally, SBML models of parts and interactions can be retrieved to construct models of biological systems. Models are returned using the SBMLDocument objects of the SBML’s jSBML library which provides Java objects for SBML entities. JParts also provides utility methods to create a container model into which SVPs can be placed and their interactions, to join SVPs, and to add mRNA SVPs. 
<br/><br/>
Two main classes in this API are PartsHandler and ModelBuilder. PartsHandler is used to retrieve data from the Virtual Parts repository while the latter is used to create simulatable models using models retrieved from the repository. 
The example below shows the use of the API. The PartHandler class is used to access the models of parts and interactions. The ModelBuilder class is used to add these SVPs into a given SBML model and to join them using the defined inputs and outputs of SVPs. In the example a genetic circuit is constructed using a promoter, an RBS and the spaK CDS. SVPs of these parts are joined computationally in order to construct the simulatable model of this circuit.
<br/><br/>
<h4>A simple promoter-RBS-CDS genetic circuit, using the ‘PspaRK’, ‘RBS_SpaK’ and ‘SpaK’ SVPs:</h4>
<table class="codingstyletable">
<tbody>
<tr>
<td class="codingstyletablefirsttd">
PartsHandler partsHandler = new PartsHandler(serverURL);
<br/>//Create an SBML container model
<br/>SBMLDocument sbmlContainer = sbmlHandler.GetSBMLTemplateModel("SpaKProtein");
<br/>ModelBuilder modelBuilder = new ModelBuilder(sbmlContainer);
<br/>//Retrieve the models for the promoter, RBS, SpaK and mRNA SVPs
<br/>SBMLDocument pspaRK = partsHandler.GetModel(partsHandler.GetPart("PspaRK"));
<br/>SBMLDocument rbsSpaK=partsHandler.GetModel(partsHandler.GetPart("RBS_SpaK"));
<br/>SBMLDocument proteinSpaK=partsHandler.GetModel(partsHandler.GetPart("SpaK"));
<br/>SBMLDocument mRNASpaRK = partsHandler.GetModel(partsHandler.GetPart("mRNA")); 
<br/>//Add the promoter SVP
<br/>modelBuilder.Add(pspaRK);
<br/>//Add the mRNA SVP and connect to the promoter SVP
<br/>modelBuilder.Link(pspaRK, mRNASpaRK);
<br/>modelBuilder.Add(mRNASpaRK);
<br/>//Add the RBS SVP and connect to the mRNA SVP
<br/>modelBuilder.Link(mRNASpaRK,rbsSpaK);
<br/>modelBuilder.Add(rbsSpaK);
<br/>//Add the SpaK SVP and connect to the RBS SVP
<br/>modelBuilder.Link(rbsSpaK,proteinSpaK);
<br/>modelBuilder.Add(proteinSpaK);
<br/>//Get the XML content of the SBML model
<br/>String sbmlOutput = modelBuilder.GetSBMLOutput();

</td>
</tr>
</tbody>
</table>
<!-- 
<div style="overflow: hidden; margin-top: 0px; margin-left: -25px;width:1000px">

<iframe src="http://intbio.ncl.ac.uk/?projects=jparts-api" scrolling="no" style="height: 5100px; border: 0px none; width: 1050px; margin-top: -400px; margin-left: -240px; margin-right: -1000px;">
</iframe>
</div> -->

<h2 id="partshandler">PartsHandler</h2>
<h3>Retrieving Parts</h3>
Metadata about a part is represented with the Part class. PartsHandler provides methods to get a Part object based on its ID using the GetPart method:
<table class="codingstyletable">
<tbody>
<tr>
<td class="codingstyletablefirsttd">PartsHandler partsHandler = new PartsHandler(“http://www.virtualparts.org”);
<br/>Part part= partsHandler.GetPart("PspaRK"));</td>
</tr>
</tbody>
</table>
<br/>
List of parts can also be retrieved using the PartsHandler’s GetParts method. Due to the large number of parts, it is not possible to get information about all the parts at once, and therefore a method called paging is used. Only 50 records can be retrieved at each method call. For example, the code below retrieves the first 50 parts from the repository:
<table class="codingstyletable">
<tbody>
<tr>
<td class="codingstyletablefirsttd">Parts parts=partsHandler.GetParts(1);</td>
</tr>
</tbody>
</table>
<br/>
The GetPartsSummary method of the PartsHandler can be used to get the number of total pages in the repository in order to iteratively retrieve all records. The method returns a Summary object including the total number of pages, with each page having 50 records:
<table class="codingstyletable">
<tbody>
<tr>
<td class="codingstyletablefirsttd">Summary summary=partsHandler.GetPartsSummary();</td>
</tr>
</tbody>
</table>
<br/>
The number of pages can then be used to iteratively access all of the parts from the repository:
<table class="codingstyletable">
<tbody>
<tr>
<td class="codingstyletablefirsttd">Summary summary=handler.GetPartsSummary();
<br/>int pageCount=summary.PageCount;
<br/>Parts allParts=new Parts();
<br/>for (int i=1;i&lt;=pageCount;i++)
<br/>{
<br/>&nbsp;&nbsp;&nbsp;Parts parts=handler.GetParts(i);
<br/>&nbsp;&nbsp;&nbsp;allParts.getParts().addAll(parts.getParts());
<br/>}
</td>
</tr>
</tbody>
</table>
<br/>
The repository includes hundreds of parts and therefore retrieving all parts may not be efficient. Once the parts are retrieved, they can easily be saved locally using the Serializer class. This class provides methods to serialize/deserialize a list of parts into/from a String variable. The example below demonstrates how to save information about parts to a local file and use this file to create a List&lt;Part&gt; object, which includes the list Part objects.
<table class="codingstyletable">
<tbody>
<tr>
<td class="codingstyletablefirsttd">Serializer serializer = new Serializer();
<br/>String content = serializer.SerializeParts(parts);
<br/>Serializer.WriteToFile("c:/temp/parts.txt", content);
<br/>File file = new File("c:/temp/parts.txt");
<br/>String savedContent = Serializer.GetFileContent(file);
<br/>Parts savedParts = serializer.GetParts(savedcontent);</td>
</tr>
</tbody>
</table>
<br/>
Parts can also be retrieved based on their type and other additional features. Similarly the summary information can be returned using the same criteria being used. For example, to get information about promoters the following lines can be used in order to initially get the number of pages for promoters and filter the parts to retrieve information only about promoter parts:
<table class="codingstyletable">
<tbody>
<tr>
<td class="codingstyletablefirsttd">Summary summary=handler.GetPartsSummary("Promoter");
Parts parts=handler.GetParts(1,"Promoter");</td>
</tr>
</tbody>
</table>
<br/>
Furthermore, parts have subtypes. For example, a promoter virtual part can have subtypes to indicate the promoter's sigma factor, and whether or not the promoter is inducible. These subtypes are listed on the left-hand side of the Virtual Parts website for pages relevant to part types. For promoter type of parts, http://www.virtualparts.org/parts/Promoter link can be used to see these subtypes. SigAPromoter and InduciblePromoter are respectively used to find SigA and inducible promoters. These subtypes are stored as key/value pairs in the metadata of a part and can also be used to search for parts. The following example retrieves the list of SigA promoters.
<table class="codingstyletable">
<tbody>
<tr>
<td class="codingstyletablefirsttd">Parts parts = handler.GetParts(1, "Promoter", "type", "SigAPromoter");</td>
</tr>
</tbody>
</table>
<br/>
Summary information for SigA promoters can similarly be retrieved as described above using a different method call.
<table class="codingstyletable">
<tbody>
<tr>
<td class="codingstyletablefirsttd">Summary summary = handler.GetPartsSummary("Promoter", "type", "SigAPromoter");</td>
</tr>
</tbody>
</table>
<br/>
While key/value pairs can be 'type' and corrresponding type values, they can also be GO terms in the form of 'located_in'/GO_TERM_ID or 'has_function'/GO_TERM_ID key/value pairs.
<h3>Retrieving Parts by Parameters</h3>
Parts can further be retrieved based on parameter values. The searching of parts using parameter values requires the use of Filter objects which can be constructed using the following filter operators:
<ul>
	<li>EQUAL</li>
	<li>LESS_THAN</li>
	<li>LESS_THAN_OR_EQUAL_TO</li>
	<li>GREATER_THAN</li>
	<li>GREATER_THAN_OR_EQUAL_TO</li>
</ul>
Filters are constructed using the type of parameter to search for, the filter operator and the value specified for the parameter. For example, in order to retrieve all the parts whose transcription rates are greater than 0.5, the following Filter object can be constructed:
<table class="codingstyletable">
<tbody>
<tr>
<td class="codingstyletablefirsttd">Filter greaterThanFilter=new Filter(FilterParameter.TRANSCRIPTION_RATE, FilterOperator.GREATER_THAN, "0.5");</td>
</tr>
</tbody>
</table>
<br/>
Any two Filter obejcts can be combined in a query by constructing an ANDFilter which implements the logical conjuction specified in the given Filter objects. For example, ANDFilter can be used to search for a range of parameter values. The example below is used to construct a filter which can then be used to search for promoters with transcription rates greater than 0.5 and smaller than 0.6.
<table class="codingstyletable">
<tbody>
<tr>
<td class="codingstyletablefirsttd">Filter greaterThanFilter=new Filter(FilterParameter.TRANSCRIPTION_RATE, FilterOperator.GREATER_THAN_OR_EQUAL_TO, "0.5");
<br/>Filter lessThanFilter=new Filter(FilterParameter.TRANSCRIPTION_RATE, FilterOperator.LESS_THAN_OR_EQUAL_TO, "0.6");
<br/>ANDFilter andFilter=new ANDFilter(filter1, filter2);</td>
</tr>
</tbody>
</table>
<br/>
Filter and ANDFilter objects implements the IFilter interface and can be used to call GetParts method of the PartsHandler to retrieve parts.
<table class="codingstyletable">
<tbody>
<tr>
<td class="codingstyletablefirsttd">Parts parts=handler.GetParts(andFilter);</td>
</tr>
</tbody>
</table>
&nbsp;
<h3>Retrieving Interactions</h3>
The repository also stores metadata about interactions, including the types of interactions, participating parts and reaction stoichiometries. Information about these interactions can be accessed using the GetInteractions method:
<table class="codingstyletable">
<tbody>
<tr>
<td class="codingstyletablefirsttd">Interactions interactions=handler.GetInteractions(part);</td>
</tr>
</tbody>
</table>
<br/>
<h3>Retrieving models of parts and interactions</h3>
In addition to metadata, parts and interactions are also represented using SBML models. Each part or interaction has a single model with defined inputs and outputs. PartsHandler's GetModel and GetInteractionModel methods are respectively used to retrieve models of parts and interactions as Java objects:
<table class="codingstyletable">
<tbody>
<tr>
<td class="codingstyletablefirsttd">SBMLDocument sbmlDocument=handler.GetModel(part);
SBMLDocument sbmlDocument=handler.GetInteractionModel(interaction);</td>
</tr>
</tbody>
</table>
<br/>
In addition, to retrieving models of parts and interactions from the repository, the API also allows constructing these models at runtime using object representation of Parts and Interactions. Models of parts encapsulate part specific interactions that are called internal interactions. These interactions are not dependent on other parts are usually represent biochemical reactions such as degradation and autodephosphorylation. The values of stoichometries and biochemical parameters of biological molecules can be respectively updated using the InteractionPartDetail and Parameter objects of a corresponding interaction object. Therefore, models of biological parts that do not exist in the repository can also be created.
<table class="codingstyletable">
<tbody>
<tr>
<td class="codingstyletablefirsttd">
<div style="overflow: auto; width: 650px; display: block;">
List&lt;Interaction&gt; spaKInternalInteractions= partsHandler.GetInternalInteractions(spaKPart).getInteractions();
<br/>// Now, change the properties in Part and Interaction objects. Interaction object has a list of parameters and details of parts included in that interaction.
<br/>SBMLDocument proteinSpaK=partsHandler.CreatePartModel(spaKPart, spaKInternalInteractions);
</div>
</td>
</tr>
</tbody>
</table>
<br/>
Similarly, models of interactions can be constructed at runtime:
<table class="codingstyletable">
<tbody>
<tr>
<td class="codingstyletablefirsttd">
<div style="overflow: auto; width: 650px; display: block;">
Interaction spaK_spaR_interactionObject=partsHandler.GetInteractions(spaKPart).getInteractions().get(0);
//Change the properties in the Interaction object
SBMLDocument spaK_spaR_interaction=partsHandler.CreateInteractionModel(spaK_spaR_interactionObject);
</div>
</td>
</tr>
</tbody>
</table>
<br/>
<h2>ModelBuilder</h2>
Models of parts and interactions are not simulateable on their own. However, these models can be joined together using their inputs and outputs to create models of systems. ModelBuilder provides methods to add the models of parts and interactions into an empty container model and to connect them in order to construct simulateable models. This class is initialized using an SBML container model, which can be created using the SBMLHandler classes’s GetSBMLTemplateModel method:
<table class="codingstyletable">
<tbody>
<tr>
<td class="codingstyletablefirsttd">SBMLHandler sbmlHandler = new SBMLHandler();
SBMLDocument sbmlContainer=sbmlHandler.GetSBMLTemplateModel("SubtilinReceiver");</td>
</tr>
</tbody>
</table>
<br/>
The ModelBuilder class uses model annotations in order to connect models of parts and interactions using their inputs and outputs. Only the models that have the same type of input and output can be connected. For example promoter-mRNA, mRNA-RBS, RBS-CDS connections would be valid since models of these parts would have compatible inputs and outputs such as mRNA, PoPS and RiPS. The following example shows how to add and connect ‘RBS_SpaK’ RBS and ‘SpaK’ and CDS parts to a container model using the Add and Link methods.
<table class="codingstyletable">
<tbody>
<tr>
<td class="codingstyletablefirsttd" >ModelBuilder modelBuilder = new ModelBuilder(sbmlContainer);
<br/>SBMLDocument rbsSpaK=partsHandler.GetModel(partsHandler.GetPart("RBS_SpaK"));
<br/>SBMLDocument proteinSpaK=partsHandler.GetModel(partsHandler.GetPart("SpaK"));
<br/>modelBuilder.Add(rbsSpaK);
<br/>modelBuilder.Add(proteinSpaK);
<br/>modelBuilder.Link(rbsSpaK,proteinSpaK);</td>
</tr>
</tbody>
</table>
<br/>
The repository also provides models for mRNAs which have the constant ‘mRNA’ IDs. However, each time when an mRNA model is requested, the repository returns such a model using different internal IDs and hence mRNA models can be used more than once in the same container model. Moreover, although interactions between CDS parts do not need to be connected explicitly, any interaction involving a promoter also needs to be connected using the ModelBuilder class. For example, the output of a promoter can be connected to the input of such an interaction and the interaction’s output can be connected to an mRNA model. Such connections are also carried out using the Link method of the ModelBuilder class.
<br/>
<h2>Examples</h2>
<h3>Constitutive Gene Expression</h3>
In this example, a simple model for a simple promoter-RBS-CDS model is constructed. The promoter is constitutively expressed.
<br/><br/>
<table class="codingstyletable">
<tbody>
<tr>
<td class="codingstyletablefirsttd">
<div style="overflow: auto; width: 650px; display: block;">
SBMLHandler sbmlHandler = new SBMLHandler();
<br/>SBMLDocument sbmlContainer = sbmlHandler.GetSBMLTemplateModel("SubtilinReceiver");
<br/>ModelBuilder modelBuilder = new ModelBuilder(sbmlContainer);
<br/>PartsHandler partsHandler = new PartsHandler(serverURL);
<br/>Part pspaRK=partsHandler.GetPart("PspaRK");
<br/>SBMLDocument pspaRKModel = partsHandler.GetModel(pspaRK);
<br/>SBMLDocument rbsSpaKModel = partsHandler.GetModel(partsHandler.GetPart("RBS_SpaK"));
<br/>SBMLDocument cdsSpaKModel = partsHandler.GetModel(partsHandler.GetPart("SpaK"));
        
<br/><br/> SBMLDocument mRNAModel = partsHandler.GetModel(partsHandler.GetPart("mRNA"));

<br/><br/>modelBuilder.Add(pspaRKModel);
       
<br/><br/>modelBuilder.Add(mRNAModel);
<br/>modelBuilder.Link(pspaRKModel, mRNAModel);
        
<br/><br/>modelBuilder.Add(rbsSpaKModel );
<br/>modelBuilder.Link(mRNAModel, rbsSpaKModel );
        
<br/><br/>modelBuilder.Add(cdsSpaKModel);
<br/>modelBuilder.Link(rbsSpaKModel, cdsSpaKModel);
        
<br/>String sbmlOutput = modelBuilder.GetModelString();
<br/>System.out.println(sbmlOutput);
<br/>Serializer.WriteToFile(GetBaseDirectory() + "ConstitutiveExpression.xml", sbmlOutput);
        
</div>
</td>
</tr>
</tbody>
</table>
<br/>

<h3>Two-component Systems</h3>
The example below shows the construction of the <a title="subtilin receiver" href="http://bioinformatics.oxfordjournals.org/content/26/7/925" target="blank">subtilin receiver</a> model which can be simulated. Subtilin receiver includes a two component systems comprising the SpaK kinase and the SpaR reponse regulators. Coding sequences of these proteins are located in the same operon and are expressed constitutively by the pspaRK promoter.
<p style="text-align: center;"><a href="http://intbio.ncl.ac.uk/wp-content/uploads/2012/11/SubtilinReceiverSmall.png"><img class="aligncenter  wp-image-268" title="SubtilinReceiver" alt="Subtilin Receiver" src="http://intbio.ncl.ac.uk/wp-content/uploads/2012/11/SubtilinReceiverSmall.png" width="423" height="71" /></a></p>
SpaK is activated by Subtilin via phosphorylation and upon activation phosphorylates SpaR. The activated SpaR acts as a transcription factor to induce the pspaS promoter in order to express green fluorescent protein.
<table class="codingstyletable">
<tbody>
<tr>
<td class="codingstyletablefirsttd">
<div style="overflow: auto; width: 650px; display: block;">SBMLHandler sbmlHandler = new SBMLHandler();
<br/>SBMLDocument sbmlContainer=sbmlHandler.GetSBMLTemplateModel("SubtilinReceiver");
<br/>ModelBuilder modelBuilder = new ModelBuilder(sbmlContainer);
<br/>PartsHandler partsHandler = new PartsHandler(serverURL);
<br/>SBMLDocument pspaRK = partsHandler.GetModel(partsHandler.GetPart("PspaRK"));
<br/>SBMLDocument rbsSpaK=partsHandler.GetModel(partsHandler.GetPart("RBS_SpaK"));
<br/>SBMLDocument rbsSpaR=partsHandler.GetModel(partsHandler.GetPart("RBS_SpaR"));
<br/>Part spaKPart=partsHandler.GetPart("SpaK");
<br/>SBMLDocument proteinSpaK=partsHandler.GetModel(spaKPart);
<br/>SBMLDocument proteinSpaR=partsHandler.GetModel(partsHandler.GetPart("SpaR"));
<br/>SBMLDocument spaK_spaR_interaction= partsHandler.GetInteractionModel(partsHandler.GetInteractions(spaKPart).getInteractions().get(0));
<br/>Part pspaSPart=partsHandler.GetPart("PspaS");
<br/>SBMLDocument pspaS=partsHandler.GetModel(pspaSPart);
<br/>SBMLDocument pspaS_SpaR_interaction= partsHandler.GetInteractionModel(partsHandler.GetInteractions(pspaSPart).getInteractions().get(0));
<br/>SBMLDocument rbsGFP=partsHandler.GetModel(partsHandler.GetPart("RBS_SpaS"));
<br/>SBMLDocument proteinGFP=partsHandler.GetModel(partsHandler.GetPart("GFP_rrnb"));
<br/>//Get mRNAs.mRNA virtual part is a template. Each time it is given with different IDs
<br/>SBMLDocument mRNASpaRK = partsHandler.GetModel(partsHandler.GetPart("mRNA"));
<br/>SBMLDocument mRNAGFP = partsHandler.GetModel(partsHandler.GetPart("mRNA"));
<br/>modelBuilder.Add(pspaRK);
<br/>modelBuilder.Add(mRNASpaRK);
<br/>modelBuilder.Link(pspaRK, mRNASpaRK);
<br/>modelBuilder.Add(rbsSpaK);
<br/>modelBuilder.Link(mRNASpaRK,rbsSpaK);
<br/>modelBuilder.Add(rbsSpaR);
<br/>modelBuilder.Link(mRNASpaRK,rbsSpaR);
<br/>modelBuilder.Add(proteinSpaK);
<br/>modelBuilder.Link(rbsSpaK,proteinSpaK);
<br/>modelBuilder.Add(proteinSpaR);
<br/>modelBuilder.Link(rbsSpaR,proteinSpaR);
<br/>modelBuilder.Add(spaK_spaR_interaction);
<br/>modelBuilder.Add(pspaS);
<br/>modelBuilder.Add(pspaS_SpaR_interaction);
<br/>modelBuilder.Link(pspaS,pspaS_SpaR_interaction);
<br/>modelBuilder.Add(mRNAGFP);
<br/>modelBuilder.Link(pspaS_SpaR_interaction,mRNAGFP);
<br/>modelBuilder.Add(rbsGFP);
<br/>modelBuilder.Link(mRNAGFP,rbsGFP);
<br/>modelBuilder.Add(proteinGFP);
<br/>modelBuilder.Link(rbsGFP,proteinGFP);
<br/>String sbmlOutput = modelBuilder.GetModelString();
<br/>System.out.println(sbmlOutput);
<br/>Serializer.WriteToFile("C:/subtilinreceiver.xml", sbmlOutput);</div></td>
</tr>
</tbody>
</table>
<br/>
The example subtilin receiver device model shows the use of parts only once in order to build a simulateable model. Each modelling entity in a part model contains a unique ID that should also be unique when used to build a model of a system. Therefore, for subsequent use of a model, the model must be cloned with different IDs for modelling entities. ModelBuilder class also provides methods to clone models and interactions. The following example shows the cloning of pspaS1 promoter model as pspaS2, and pspaS_SpaR_interaction1 interaction model as pspaS_SpaR_interaction2:
<table class="codingstyletable">
<tbody>
<tr>
<td class="codingstyletablefirsttd">
<div style="overflow: auto; width: 650px; display: block;">SBMLDocument pspaS2=modelBuilder.Clone(pspaS1, pspaSPart);
<br/>SBMLDocument pspaS_SpaR_interaction2=modelBuilder.CloneInteraction(pspaS_SpaR_interaction1, pspaSInteraction);</div></td>
</tr>
</tbody>
</table>
<br/>
<h3>Negative Feedbacks</h3>
In transcriptional regulatory networks, genetic circuits with negative feedbacks can be constructed by using a promoter that drives the expression of its own repressor.
<br/><br/>
In this example, promoter BO_2685 (<i>abnA</i> promoter) participates in a transcriptioanl repression interaction with the part BO_32077 (AraR transcription factor).
The PoPS output of the promoter is used as an input to the inteaction model that model the repression. The output from this interaction is fed into the mRNA part to produce RiPS which is then use to produce the TF entities in the constructed model.

<br/><br/>
<table class="codingstyletable">
<tbody>
<tr>
<td class="codingstyletablefirsttd">
<div style="overflow: auto; width: 650px; display: block;">
SBMLHandler sbmlHandler = new SBMLHandler();
<br/>SBMLDocument sbmlContainer = sbmlHandler.GetSBMLTemplateModel("SubtilinReceiver");
<br/>ModelBuilder modelBuilder = new ModelBuilder(sbmlContainer);
<br/>PartsHandler partsHandler = new PartsHandler(serverURL);
<br/>Part promoterPart=partsHandler.GetPart("BO_2685");
<br/>SBMLDocument promoterModel = partsHandler.GetModel(promoterPart);
<br/>SBMLDocument rbsModel = partsHandler.GetModel(partsHandler.GetPart("RBS_SpaK"));
<br/>SBMLDocument cdsModel = partsHandler.GetModel(partsHandler.GetPart("BO_32077"));
<br/>SBMLDocument promoterTFInteractionModel = partsHandler.GetInteractionModel(partsHandler.GetInteractions(promoterPart).getInteractions().get(0));
<br/>SBMLDocument mRNAModel = partsHandler.GetModel(partsHandler.GetPart("mRNA"));

<br/><br/>modelBuilder.Add(promoterModel);

<br/><br/>modelBuilder.Add(promoterTFInteractionModel);
<br/>modelBuilder.Link(promoterModel, promoterTFInteractionModel);

<br/><br/>modelBuilder.Add(mRNAModel);
<br/>modelBuilder.Link(promoterTFInteractionModel, mRNAModel);

<br/><br/>modelBuilder.Add(rbsModel);
<br/>modelBuilder.Link(mRNAModel, rbsModel);

<br/>modelBuilder.Add(cdsModel);
<br/>modelBuilder.Link(rbsModel, cdsModel);

<br/><br/>String sbmlOutput = modelBuilder.GetModelString();
<br/>System.out.println(sbmlOutput);
<br/>Serializer.WriteToFile(GetBaseDirectory() + "NegativeFeedBack.xml", sbmlOutput);
</div></td>
</tr>
</tbody>
</table>
<br/>

<h3>Toggle Switches</h3>
In this example, model for a toggle swicth is constructed using two promoters, two operators and the repressor TFs that bind to these operators. These operators are connected to promoters to create repressible promoters.
<br/><br/>
<table class="codingstyletable">
<tbody>
<tr>
<td class="codingstyletablefirsttd">
<div style="overflow: auto; width: 650px; display: block;">
SBMLHandler sbmlHandler = new SBMLHandler();
<br/>SBMLDocument sbmlContainer = sbmlHandler.GetSBMLTemplateModel("ToggleSwitch");
<br/>ModelBuilder modelBuilder = new ModelBuilder(sbmlContainer);
<br/>PartsHandler partsHandler = new PartsHandler(serverURL);
         
<br/><br/>//Get the first binding sequence part and the repressor part for the sequence
<br/>Part repressor1BindingSitePart=partsHandler.GetPart("BO_3964");//PyrR_pyrR, A PyrR binding site      
<br/>Interaction repressor1Binding=partsHandler.GetInteractions(repressor1BindingSitePart).getInteractions().get(0);
<br/>Part repressor1=partsHandler.GetPart(GetTF(repressor1Binding, "BO_3964"));//Get the PyrR part
         
<br/><br/>//Get the second binding sequence part and the repressor part for the sequence          
<br/>Part repressor2BindingSitePart=partsHandler.GetPart("BO_3993");//RocR_rocR, A RocR binding site RocR:BP_32223
<br/>Interaction repressor2Binding=partsHandler.GetInteractions(repressor2BindingSitePart).getInteractions().get(0);
<br/>Part repressor2=partsHandler.GetPart(GetTF(repressor2Binding, "BO_3993"));//Get the RocR Part
                  
<br/><br/>//Get the promoter and RBS parts for the first operon
<br/>Part constPromoter1=partsHandler.GetPart("BO_2770");//fur core promoter     

<br/><br/>//Search for RBSs using the filters
<br/>Filter filter1=new Filter(FilterParameter.TRANSLATION_RATE, FilterOperator.GREATER_THAN_OR_EQUAL_TO, "0.08");
<br/>Filter filter2=new Filter(FilterParameter.TRANSLATION_RATE, FilterOperator.LESS_THAN_OR_EQUAL_TO, "0.2");
<br/>ANDFilter andFilter=new ANDFilter(filter1, filter2);
<br/>Parts rbsParts=partsHandler.GetParts(andFilter);
<br/>Part rbs1=rbsParts.getParts().get(0);
                  
<br/><br/>Part constPromoter2=partsHandler.GetPart("BO_2747");//cysK core promoter
<br/>Part rbs2=partsHandler.GetPart("BO_27785");//Get the mtnE RBS
         
<br/>//Create the models and join them for the first operon
<br/>promoterOperatorConstruct(modelBuilder, constPromoter1, repressor2BindingSitePart, rbs1, repressor1, repressor2Binding);         
<br/>promoterOperatorConstruct(modelBuilder, constPromoter2, repressor1BindingSitePart, rbs2, repressor2, repressor1Binding);
         
<br/>String sbmlOutput = modelBuilder.GetModelString();
<br/>System.out.println(sbmlOutput);
<br/>Serializer.WriteToFile(GetBaseDirectory() + "ToggleSwitch.xml", sbmlOutput);
</div></td>
</tr>
</tbody>
</table>
<br/>
Here is the promoterOperatorConstruct method that creates a model for a promoter-operator-RBS-tfCDS construct.
<br/><br/>
<table class="codingstyletable">
<tbody>
<tr>
<td class="codingstyletablefirsttd">
<div style="overflow: auto; width: 650px; display: block;">
<br/><br/>private void promoterOperatorConstruct(ModelBuilder modelBuilder,Part promoter, Part operator, Part rbs, Part TF, Interaction operatorBinding) throws Exception{
<br/>PartsHandler partsHandler = new PartsHandler(serverURL);
          
<br/><br/>SBMLDocument promoterModel=partsHandler.GetModel(promoter);
<br/>SBMLDocument operatorModel=partsHandler.GetModel(operator);
<br/>SBMLDocument TFModel=partsHandler.GetModel(TF);
<br/>SBMLDocument rbsModel=partsHandler.GetModel(rbs);                  
<br/>SBMLDocument mRNAModel = partsHandler.GetModel(partsHandler.GetPart("mRNA"));
<br/>SBMLDocument operatorBindingModel=partsHandler.GetInteractionModel(operatorBinding);
         
<br/><br/>modelBuilder.Add(promoterModel);
         
<br/><br/>modelBuilder.Add(operatorModel);
<br/>modelBuilder.Link(promoterModel, operatorModel);
         
<br/><br/>modelBuilder.Add(operatorBindingModel);
<br/>modelBuilder.Link(operatorModel, operatorBindingModel);
         
<br/><br/>modelBuilder.Add(mRNAModel);
<br/>modelBuilder.Link(operatorBindingModel, mRNAModel);
         
<br/><br/>modelBuilder.Add(rbsModel );
<br/>modelBuilder.Link(mRNAModel, rbsModel );  
         
<br/><br/>modelBuilder.Add(TFModel);
<br/>modelBuilder.Link(rbsModel, TFModel);
<br/>  }
</div></td>
</tr>
</tbody>
</table>
<br/>

<h3>Two Promoter  Example</h3>
In this example, two promoters are used in the same operon.  The PoPS outputs from two promoters are connected to the same mRNA  part.
<br/><br/>
<table class="codingstyletable">
<tbody>
<tr>
<td class="codingstyletablefirsttd">
<div style="overflow: auto; width: 650px; display: block;">
SBMLHandler sbmlHandler = new SBMLHandler();
<br/>SBMLDocument sbmlContainer = sbmlHandler.GetSBMLTemplateModel("SubtilinReceiver");
<br/>ModelBuilder modelBuilder = new ModelBuilder(sbmlContainer);
<br/>PartsHandler partsHandler = new PartsHandler(serverURL);
<br/>Part pspaRK=partsHandler.GetPart("PspaRK");
<br/>Part pspaS=partsHandler.GetPart("PspaS");      
<br/>Part spaKPart = partsHandler.GetPart("SpaK");                  
        
<br/><br/>SBMLDocument pspaRKModel = partsHandler.GetModel(pspaRK);
<br/>SBMLDocument pspaSModel = partsHandler.GetModel(pspaS);         
<br/>SBMLDocument rbsSpaKModel = partsHandler.GetModel(partsHandler.GetPart("RBS_SpaK"));
<br/>SBMLDocument spaKModel = partsHandler.GetModel(spaKPart);
<br/>SBMLDocument rbsSpaRModel = partsHandler.GetModel(partsHandler.GetPart("RBS_SpaR"));
<br/>SBMLDocument spaRModel = partsHandler.GetModel(partsHandler.GetPart("SpaR"));
        
<br/><br/>SBMLDocument pspaS_SpaRInteractionModel = partsHandler.GetInteractionModel(partsHandler.GetInteractions(pspaS).getInteractions().get(0));
<br/>SBMLDocument mRNAModel = partsHandler.GetModel(partsHandler.GetPart("mRNA"));

<br/><br/>modelBuilder.Add(pspaRKModel);
<br/>modelBuilder.Add(mRNAModel);
        
<br/><br/>modelBuilder.Link(pspaRKModel, mRNAModel);
                 
<br/><br/>modelBuilder.Add(pspaSModel);         
<br/>modelBuilder.Add(pspaS_SpaRInteractionModel);
        
<br/>modelBuilder.Link(pspaSModel, pspaS_SpaRInteractionModel);
        
<br/><br/>modelBuilder.Link(pspaS_SpaRInteractionModel, mRNAModel);
        
<br/><br/>modelBuilder.Add(rbsSpaKModel );
<br/>modelBuilder.Link(mRNAModel, rbsSpaKModel );
        
<br/><br/>modelBuilder.Add(spaKModel);
<br/>modelBuilder.Link(rbsSpaKModel, spaKModel);
        
<br/><br/>modelBuilder.Add(rbsSpaRModel );
<br/>modelBuilder.Link(mRNAModel, rbsSpaRModel );
        
<br/><br/>modelBuilder.Add(spaRModel);
<br/>modelBuilder.Link(rbsSpaRModel, spaRModel);
        
<br/><br/>Interaction spaK_spaR_interactionObject=partsHandler.GetInteractions(spaKPart).getInteractions().get(0);
<br/>SBMLDocument spaK_spaR_interaction=partsHandler.CreateInteractionModel(spaK_spaR_interactionObject);
<br/>modelBuilder.Add(spaK_spaR_interaction);
        
<br/>String sbmlOutput = modelBuilder.GetModelString();
<br/>System.out.println(sbmlOutput);
<br/>Serializer.WriteToFile(GetBaseDirectory() + "TwoPromoters.xml", sbmlOutput);
</div></td>
</tr>
</tbody>
</table>
<br/><br/>



</div>

<div id="sidebar">
<h2>Read more about virtual parts</h2>
	<div class="sidebaritem">         
          <a href="http://intbio.ncl.ac.uk/?projects=standard-virtual-parts" target="blank">Click here</a> to for a short introduction to virtual parts.
     	  <h3>Publications</h3>
     	  <ul><li>M. T. Cooling, V. Rouilly, G. Misirli, J. Lawson, T. Yu, J. Hallinan, and A. Wipat, “<a target="blank" href="http://dx.doi.org/10.1093/bioinformatics/btq063">Standard virtual biological parts</a>: a repository of modular modeling components for synthetic biology,” Bioinformatics, vol. 26, iss. 7, pp. 925-931, 2010
     	  </li></ul>
     </div>
 <br style="clear:both"><br style="clear:both">
 <h2> The JParts API and the JavaDoc</h2>
 <div class="sidebaritem"> 
 		<div class="sbilinks">           
            <ul>
             	<li><a href="https://bitbucket.org/goksel/downloads/downloads/JParts-1.0.2-SNAPSHOT-withDependencies.jar">Download the JParts API</a></li>   
             	<li><a href="/documentation/javadoc/index.html">JavaDoc</a></li>	          
            </ul>
          </div>
 	</div>  
 	
 <br style="clear:both"><br style="clear:both">
 <h2> Previous Versions</h2>
 <div class="sidebaritem"> 
 		<div class="sbilinks">           
            <ul>
             	<li><a href="http://sbol.ncl.ac.uk:8084">Virtual Parts Repository 1.0.1</a></li>   
            </ul>
          </div>
 	</div> 
 	
</div>

 


<jsp:include page ="footer.jsp"/>

</div>

</div>
</body>
</html>


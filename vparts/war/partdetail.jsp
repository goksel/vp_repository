<%@page import="vpart.exchange.Serializer"%>
<%@page import="vpart.exchange.Pigeon"%>
<%@page import="org.sbolstandard.core.SequenceAnnotation"%>
<%@page import="virtualparts.entity.Property"%>
<%@page import="virtualparts.entity.Interaction"%>
<%@page import="virtualparts.entity.Part"%>
<%@page import="vparts.data.InteractionDataHandler"%>
<%@page import="vparts.Util"%>
<%@page import="vparts.entity.VP_ParameterType"%>
<%@page import="vparts.data.PartDataHandler"%>
<%@ page language="java" import="java.util.*"%>
<%-- 
    Document   : index2.jsp
    Created on : 03-Dec-2008, 16:08:19
    Author     : A8901379
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">

<%@page import="vparts.entity.VP_Part"%>
<%@page import="vparts.PartsHandler"%>
<%@page import="vparts.entity.VP_Interaction"%>
<%@page import="vparts.entity.VP_MolecularForm"%><html xmlns="http://www.w3.org/1999/xhtml">
<%
	String partName = request.getParameter("name");

	PartDataHandler partHandler = new PartDataHandler();
	InteractionDataHandler  handler=new InteractionDataHandler();
	Part part = partHandler.GetPart(partName);
	//GMGM 20130109 List<VP_Parameter> parameters = partHandler.GetPartParameters(part);
	List<Interaction> interactions = handler.GetPartInteractions(partName);	
	List<Property> roles = partHandler.GetPartRoles(partName);
	List<SequenceAnnotation> annotations = partHandler.GetAnnotations(partName);
	Set<String> modelTypes=partHandler.GetModelTypes(part.getName());
	//Deleted 20130904 List<VP_MolecularForm> forms= partHandler.GetPartMolecularForms(part);
	
	/*
	//TODO This code is used to clone an existing RBS for RBSs that do noy have kinetic parameters at the moment.	
	//BEGIN
	if ((interactions==null || interactions.size()==0) && part.getType().toLowerCase().equals("rbs"))
	{	
		interactions=handler.CloneInteractionsFrom(part, "RBS_SpaR");
	}
	//END
	*/
%>
<head>
<title>Virtual Parts</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="container"><jsp:include page="header.jsp" />

<div id="container2">

<div id="contentLarge">
<h2><%=part.getName()%></h2>
<h3><%
	if (part.getDisplayName()!=null) out.print(part.getDisplayName());
%></h3>
<p><%
	if (part.getDescription()!=null) out.print(part.getDescription());
%> <br />
<!-- <br /><a href="/xml" target="_blank">View as xml</a></p>-->
<table>
	<tbody>
		<tr>
			<tr>
				<th scope="row">Part Type</th>
				<td><%=part.getType()%></td>
			</tr>
			<%
				if (part.getSequenceURI()!=null && !part.getSequenceURI().equals("")){
			%>
			<tr>
				<th scope="row">SequenceURI</th>
				<td><%=part.getSequenceURI()%></td>
			</tr>
			<%
				}
			%>
			<%
				if (part.getSequence()!=null && !part.getSequence().equals("")){
			%>
			<tr>
				<th scope="row">Sequence</th>
				<td><div class="scroll"><%=part.getSequence().replace(Util.NEWLINE,"<br/>")%></div></td>
			</tr>
			<%
				}
			%>
			<%
				if (part.getStatus()!=null && !part.getStatus().equals("")){
			%>
			<tr>
				<th scope="row">Status</th>
				<td><%=part.getStatus()%></td>
			</tr>
			<%
				}
			%>
			<%
				if (part.getOrganism()!=null && !part.getOrganism().equals("")){
			%>
			<tr>
				<th scope="row">Source</th>
				<td><%=part.getOrganism()%></td>
			</tr>
			<%
				}
			%>
			<%
				if (part.getDesignMethod()!=null && !part.getDesignMethod().equals("")){
			%>
			<tr>
				<th scope="row">Design Method</th>
				<td><%=part.getDesignMethod()%></td>
			</tr>
			<%
				}
			%>
			<tr>
				<th scope="row">SBOL</th>
				<td><a  target="new" href="/part/<%=part.getName()%>/sbol">Click here</a> to download the SBOL file</td>
			</tr>
			
			<% if (modelTypes.contains(part.getName() + "_sbml")){%>
			<tr>
				<th scope="row">SBML</th>
				<td><a target="new" href="/part/<%=part.getName()%>/sbml">Click here</a> to download the SBML file</td>
			</tr>
			<%} %>

			
	</tbody>
</table>



<%
	if (interactions.size() > 0) {
%>
<table>
	<caption>List of internal events</caption>
	<thead>
		<tr>
			<th scope="col">Name</th>
			<th scope="col">Description</th>
			<th scope="col">Type</th>
			<th scope="col">Is Reaction</th>
			<th scope="col">Math</th>
		</tr>
	</thead>
	<tbody>
		<%
			Iterator<Interaction> interactionIterator = interactions.iterator();
				while (interactionIterator.hasNext()) {
					Interaction interaction = (Interaction) interactionIterator.next();
					if (!interaction.getIsInternal())
					{
						continue;
					}
		%>
		<tr>
			<th scope="row"><a
				href="/interaction/<%=interaction.getName()%>"><%=interaction.getName()%></a></th>
			<td><%
				if (interaction.getDescription()!=null)out.print(interaction.getDescription());
			%></td>
			<td><%=interaction.getInteractionType()%></td>
			<td><%=interaction.getIsReaction()%></td>
			<td><%=interaction.getFreeTextMath()%></td>
		</tr>
		<%
			}
		%>

	</tbody>
</table>
<%
	}
%>


<%
	int publicInteractionCount=0;
Iterator<Interaction> interactionIterator = interactions.iterator();
while (interactionIterator.hasNext()) {
	Interaction interaction = (Interaction) interactionIterator.next();
	if (interaction.getIsInternal())
	{
		continue;
	}
	else
	{
		publicInteractionCount++;
	}
}
	if (publicInteractionCount > 0) {
%>
<table>
	<caption>List of interactions</caption>
	<thead>
		<tr>
			<th scope="col">Name</th>
			<th scope="col">Parts</th>
			<th scope="col">Description</th>
			<th scope="col">Type</th>
			<th scope="col">Is Reaction</th>
			<th scope="col">Math</th>
		</tr>
	</thead>
	<tbody>
		<%
			interactionIterator = interactions.iterator();
				while (interactionIterator.hasNext()) {
					Interaction interaction = (Interaction) interactionIterator.next();
					if (interaction.getIsInternal())
					{
						continue;
					}
		%>
		<tr>
			<th scope="row"><a
				href="/interaction/<%=interaction.getName()%>"><%=interaction.getName()%></a></th>
			<td><%=interaction.getParts().toString().replace(",",", ")%></td>
			<td><%=interaction.getDescription()%></td>
			<td><%=interaction.getInteractionType()%></td>
			<td><%=interaction.getIsReaction()%></td>
			<td><%=interaction.getFreeTextMath()%></td>
		</tr>
		<%
			}
		%>

	</tbody>
</table>
<%
	}
%>

<%
	if (roles!=null && roles.size()>0 ){
%>

<table style="width:300px">
	<caption><%=part.getName()%>'s Properties</caption>
	<thead>
		<tr>
			<th scope="col">Name</th>
			<th scope="col">Value</th>
		</tr>
	</thead>
	<tbody>
		<%
			Iterator<Property> roleIterator = roles.iterator();
			while (roleIterator.hasNext()) {
				Property role = roleIterator.next();
		%>
		<tr>
			<td scope="row"><%=role.getName()%></td>
			<td><%if (role.getDescription()!=null && role.getDescription().length()>0) {out.print(role.getDescription());} else {out.print(role.getValue());}%></td>
		</tr>
		<%
			}
		%>
	</tbody>
</table>
<%} %>

<%
	if (annotations!=null && annotations.size()>0 ){
	Pigeon pigeon=new Pigeon();	
	pigeon.GetDataDetailed(partName, Serializer.GetBaseURL(request));
%>

<table style="width:300px">
	<caption><%=part.getName()%>'s Annotations</caption>
	<thead>
		<tr>
			<th scope="col">Start-End</th>
			<th scope="col">Strand</th>
			<th scope="col">Subpart</th>
			
		</tr>
	</thead>
	<tbody>
		<%
			Iterator<SequenceAnnotation> annotationIterator = annotations.iterator();
			while (annotationIterator.hasNext()) {
				SequenceAnnotation annotation = annotationIterator.next();
		%>
		<tr>
			<td><%=annotation.getBioStart() + "-" + annotation.getBioEnd()%></td>
			<td><%if(annotation.getStrand()!=null){out.print(annotation.getStrand());}%></td>
			<td><a href="/part/<%=annotation.getSubComponent().getDisplayId()%>"><%=annotation.getSubComponent().getDisplayId()%></a></td>									
		</tr>
		<%
			}
		%>
	</tbody>
</table>
<div class="scrolllarge"><a href="/imageservlet?part=<%=part.getName()%>"><img src="/imageservlet?part=<%=part.getName()%>" alt="" height="220px" /></a></div>
<%} %>



<!--
<h2>Accessions:</h2>
<div class="links">
<ul>
	<li><a href="NCBI:<%=part.getName()%>" target="_blank">Go to <%=part.getName()%>'s
	page in NCBI</a></li>
</ul>
</div>
</div>

-->

<jsp:include page="footer.jsp" /></div>

</div>
</body>
</html>

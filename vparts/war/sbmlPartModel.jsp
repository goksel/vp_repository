<%@page import="virtualparts.model.ModelOutput"%><%@page import="vparts.model.ModelHandler"%><%@ page language="java"
	import="java.util.*"%><%@page contentType="text/xml"
	pageEncoding="UTF-8"%><%
	String name = request.getParameter("name");
	String modelType = request.getParameter("model");
	String artefactType = request.getParameter("type");
	ModelOutput model = null;
	ModelHandler modelBuilder = new ModelHandler();
	if ("interaction".equals(artefactType))
	{
			
		model = modelBuilder.GetInteractionModel(name, modelType);
	} 	
	else 
	{

		model = modelBuilder.GetPartModel(name, modelType);
	}
%><%if (model!=null)
	{
	
		//response.setContentType("text/plain");
		if ("kappa".equals(modelType))
		{
			response.setContentType("text/plain");
		}
		out.print(model.getMain());
	}
	else
	{
		out.print("");	
	}
	%>
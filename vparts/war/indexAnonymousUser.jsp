<%@page import="vparts.data.PartDataHandler"%>
<%@ page language="java" import="java.util.*" %>
<%-- 
    Document   : index2.jsp
    Created on : 03-Dec-2008, 16:08:19
    Author     : A8901379
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">

<%@page import="vparts.PartsHandler"%>
<%@page import="vparts.security.Auth"%><html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>V parts</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="container">



<div id="container2">

<div id="content">
<h2>Only authorised users!</h2>

<% 
//UserService userService=UserServiceFactory.getUserService();
if (!Auth.IsAuthenticated(request))
{

%>
Please <a href="<%=request.getRequestURI() + "?" + request.getQueryString() %>">sign in</a>  to view this application.
<%}
else
{
	String url=request.getParameter("content");
	if (url!=null)
	{
		response.sendRedirect(url);
	}
	else
	{
		%>
		<a href="<%=request.getRequestURI() + "?" + request.getQueryString() %>">Sign Out</a>.
<%
	}
}
%>
   
</div>


</div>

</div>
</body>
</html>


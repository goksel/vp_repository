<%@page import="vparts.entity.ParameterType"%>
<%@page import="vparts.data.ParameterTypeDataHandler"%>
<%@ page language="java" import="java.util.*" %>
<%-- 
    Document   : index2.jsp
    Created on : 03-Dec-2008, 16:08:19
    Author     : A8901379
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-
transitional.dtd">

<%@page import="vparts.entity.VP_Part"%>
<%@page import="vparts.PartsHandler"%>
<%@page import="vparts.entity.VP_Role"%>
<%@page import="vparts.entity.VP_ParameterType"%><html xmlns="http://www.w3.org/1999/xhtml">
<%
	ParameterTypeDataHandler handler=new ParameterTypeDataHandler();
 List<ParameterType> types= handler.GetTypes();
%>
<head>
<title>V parts</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="container">

<jsp:include page ="header.jsp"/>

<div id="container2">

<div id="content">
<h2>Paramater Types</h2>
<p> This page displays the list of parameter types available. Parameters may belong to parts or to interactions.
<!--<br/><br/><a href="/xml" target="_blank">View as xml</a> -->
</p>


    <table>
        <caption>List of parameter types</caption>
        <thead>
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Accession</th>   
            <th scope="col">Description</th>                    
          </tr>
        </thead>       
        <tbody>
            <%
            	Iterator<ParameterType> iterator= types.iterator();
                                                 while (iterator.hasNext())
                                                 {
                                                	 ParameterType role=(ParameterType)iterator.next();
            %>
          <tr>
              <th scope="row"><%=role.Name%></th>          
            <td><%=role.Accession%></td>
            <td><%=role.Description%></td>            
          </tr>	                     
         <%}%>
         
        </tbody>
      </table>
</div>


<jsp:include page ="footer.jsp"/>

</div>

</div>
</body>
</html>


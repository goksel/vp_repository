package virtualparts.model;

import java.util.ArrayList;
import java.util.List;

import virtualparts.entity.Formula;
import virtualparts.entity.Interaction;
import virtualparts.entity.InteractionPartDetail;
import virtualparts.entity.MolecularFormType;
import virtualparts.entity.Part;

public abstract class ModelBuilder {
	public abstract ModelOutput GetModel(Part part,List<Interaction> partInteractions, List<Part> partsForInternalInteractions, List<Formula> formulas,List<MolecularFormType> molecularFormTypes)  throws Exception; 
	public abstract ModelOutput GetModel(Interaction interaction,List<Part> interactionParts,List<Formula> formulas,List<MolecularFormType> molecularFormTypes )  throws Exception;
	
		
		
	protected List<Part> GetPartsByName(List<InteractionPartDetail> partDetails, List<Part> parts) throws Exception
	{
		List<Part> partsFound=new ArrayList<Part>();
		
			for (InteractionPartDetail partDetail:partDetails)
			{
				String partName=partDetail.getPartName();
				Part partFound=null;
				for (Part part:parts)
				{
					if (part.getName().equals(partName))
					{
						partFound=part;
						break;
					}
				}
				if (partFound==null)
				{
					throw new Exception ("Could not find the part " + partName + " in the list of given parts");
				}
				partsFound.add(partFound);
			}
					
		return partsFound;
	}		
	
	protected MolecularFormType GetType (String name,List<MolecularFormType> types)
	{
		if (types!=null&& types.size()>0)
		{
			for (MolecularFormType type : types)
			{
				if (type.getName().equals(name))
				{
					return type;
				}
			}
		}
		return null;
	}
	
	protected Formula GetFormula(Interaction interaction, List<Formula> formulas)
	{
		return GetFormula (interaction.getMathName(),formulas);
	}

	protected Formula GetFormula(String name,List<Formula> formulas)
	{
		if (formulas!=null&& formulas.size()>0)
		{
			for (Formula formula : formulas)
			{
				if (formula.getName().equals(name))
				{
					return formula;
				}
			}
		}
		return null;
	}

	protected String GetInteractionFormula(Formula formula)
	{
		String interactionFormula=null;
		
		if (formula!=null && formula.getReactionFlux()!=null && formula.getReactionFlux().length()>0){
			interactionFormula=formula.getReactionFlux().replace(" ", "");//Spaces are not allowed in the math in jsbml		
		}
		//If the function is not empty, use the function instead of the reaction flux
		/*if (!vparts.Util.IsEmpty(formula.getFunction()) && !formula.getFunction().contains("\n"))
		{
			interactionFormula=formula.getFunction();
		}*/
		return interactionFormula;
	}
	

	
}

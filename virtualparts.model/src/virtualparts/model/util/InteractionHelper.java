package virtualparts.model.util;

import java.util.List;

import virtualparts.entity.Interaction;
import virtualparts.entity.InteractionPartDetail;
import virtualparts.entity.Parameter;
import virtualparts.entity.Part;

public class InteractionHelper {

public static String INITIAL_ASSIGNMENT="IA";
public static String RELATIVE_VALUE_FROM_GENE_EXPRESSIONS="RVFGE";
public static String RBS_CALCULATOR="RBS_CALCULATOR";


public static String MOLECULAR_FORM_PHOSPHORYLATED="Phosphorylated";
public static String MOLECULAR_FORM_DEFAULT="Default";

public static String ROLE_INPUT="Input";
public static String ROLE_OUTPUT="Output";
public static String ROLE_MODIFIER="Modifier";



public static Interaction GetRegulatedPromoterInteraction(String promoterName, String tfName, double Km, double n, String tfMolecularForm, int tfStoichiometry,String promoterType)
{
    Interaction tfPromoterInteraction=null;
    if (promoterType.equals("InduciblePromoter"))
    {
        tfPromoterInteraction=GetInduciblePromoterInteraction(promoterName,tfName,tfMolecularForm, tfStoichiometry);
    }
    else  if (promoterType.equals("RepressiblePromoter"))
    {
        tfPromoterInteraction=GetRepressiblePromoterInteraction(promoterName,tfName,tfMolecularForm, tfStoichiometry);
    }
    tfPromoterInteraction.setName(tfName + "_" + promoterName);
    tfPromoterInteraction.setIsReaction(false);
    tfPromoterInteraction.AddPart(promoterName);
    tfPromoterInteraction.AddPart(tfName);

    tfPromoterInteraction.AddParameter(new Parameter("Km", "Km", Km,INITIAL_ASSIGNMENT,null));
    tfPromoterInteraction.AddParameter(new Parameter("n", "n", n,INITIAL_ASSIGNMENT,null));
    tfPromoterInteraction.AddParameter(new Parameter("PoPS", "PoPSOutput", null, null, "Output"));
    tfPromoterInteraction.AddParameter(new Parameter("PoPS", "PoPSInput", null, null, "Input"));

     if (promoterType.equals("InduciblePromoter"))
        {
            tfPromoterInteraction.setName(String.format("%s_ac_by_%s", promoterName,tfName));//TODO After making the ontology consistent, use this code. The SPARQL queries should return the ids for CDSs not for the products. GetClassName(interactionClass.getURI());
        }
        else
        {
            tfPromoterInteraction.setName(String.format("%s_in_by_%s", promoterName,tfName));
        }
    return tfPromoterInteraction;
}

public static Interaction GetOperatorInteraction(String operatorName, String tfName, double Km, double n, String tfMolecularForm, int tfStoichiometry,String opeartorType)
{
    Interaction interaction=null;
    if (opeartorType.equals("Inducible"))
    {
        interaction=GetInducibleOperatorInteraction(operatorName,tfName,tfMolecularForm, tfStoichiometry);
    }
    else  if (opeartorType.equals("Repressible"))
    {
        interaction=GetRepressibleOperatorInteraction(operatorName,tfName,tfMolecularForm, tfStoichiometry);
    }
    interaction.setName(tfName + "_bi_to_" + operatorName);
    interaction.setIsReaction(false);
    interaction.AddPart(operatorName);
    interaction.AddPart(tfName);

    interaction.AddParameter(new Parameter("Km", "Km", Km,INITIAL_ASSIGNMENT,null));
    interaction.AddParameter(new Parameter("n", "n", n,INITIAL_ASSIGNMENT,null));
    interaction.AddParameter(new Parameter("PoPS", "Output", null, null, "Output"));
    interaction.AddParameter(new Parameter("PoPS", "Input", null, null, "Input"));
    return interaction;
}

public static Interaction GetPhosphorylationInteraction(String name, String acceptorName, String donorName,Double rate,String rateEvidenceType,boolean isDonorModifier)
{
     Interaction interaction=new Interaction();
     interaction.setName(name);
     interaction.setDescription(String.format("Phosphorylation of %s by %s", acceptorName,donorName));
     if (isDonorModifier)
     {
        interaction.setFreeTextMath(String.format("%s + %s -> %s + %s~P ", donorName,acceptorName,donorName,acceptorName));
        interaction.AddPartDetail(new InteractionPartDetail(donorName, MOLECULAR_FORM_DEFAULT, ROLE_MODIFIER, 1, "EnvironmentConstant"));
        interaction.setMathName("PhosphorylationWithModifier");
     }
     else
     {
        interaction.setFreeTextMath(String.format("%s~P + %s -> %s + %s~P ", donorName,acceptorName,donorName,acceptorName));
        interaction.AddPartDetail(new InteractionPartDetail(donorName, MOLECULAR_FORM_PHOSPHORYLATED, ROLE_INPUT, 1, "Protein1_P"));
        interaction.AddPartDetail(new InteractionPartDetail(donorName, MOLECULAR_FORM_DEFAULT, ROLE_OUTPUT, 1, "Protein1"));
        interaction.setMathName("TwoComponentPhosphorelay");
     }
     interaction.setInteractionType("Phosphorylation");
     interaction.setIsReaction(true);
     interaction.AddParameter(new Parameter("kf", "kf", rate, rateEvidenceType, null));
     interaction.AddPartDetail(new InteractionPartDetail(acceptorName, MOLECULAR_FORM_DEFAULT, ROLE_INPUT, 1, "Protein2"));
     interaction.AddPartDetail(new InteractionPartDetail(acceptorName, MOLECULAR_FORM_PHOSPHORYLATED, ROLE_OUTPUT, 1, "Protein2_P"));
     interaction.AddPart(donorName);
     interaction.AddPart(acceptorName);
     return interaction;
}

private static Interaction GetInduciblePromoterInteraction(String promoterName, String tfName,String tfMolecularForm, int tfStoichiometry)
{
    Interaction tfPromoterInteraction=new Interaction();
    tfPromoterInteraction.setMathName("PromoterInduction");
    tfPromoterInteraction.setInteractionType("Transcriptional activation");
    tfPromoterInteraction.setFreeTextMath(String.format("%s_PoPS * %s / (%s + Km)", promoterName,tfName,tfName));
    tfPromoterInteraction.setDescription(String.format("%s TF activates %s promoter", tfName,promoterName));
    tfPromoterInteraction.AddPartDetail(new InteractionPartDetail(tfName, tfMolecularForm, "Modifier", tfStoichiometry, "Activator"));
    return tfPromoterInteraction;
}

    private static Interaction GetInducibleOperatorInteraction(String name, String tfName,String tfMolecularForm, int tfStoichiometry)
{
    Interaction interaction=new Interaction();
    interaction.setMathName("OperatorInduction");
    interaction.setInteractionType("Transcriptional activation using an operator");
    interaction.setFreeTextMath(String.format("%s / (%s + Km)", tfName,tfName));
    interaction.setDescription(String.format("%s binds to %s operator", tfName,name));
    interaction.AddPartDetail(new InteractionPartDetail(tfName, tfMolecularForm, "Modifier", tfStoichiometry, "Activator"));
    return interaction;
}

 private static Interaction GetRepressibleOperatorInteraction(String name, String tfName,String tfMolecularForm, int tfStoichiometry)
{
    Interaction interaction=new Interaction();
    interaction.setMathName("OperatorRepression");
    interaction.setInteractionType("Transcriptional repression using an operator");
    interaction.setFreeTextMath(String.format("Km / (%s + Km)", tfName));
    interaction.setDescription(String.format("%s binds to %s operator", tfName,name));
    interaction.AddPartDetail(new InteractionPartDetail(tfName, tfMolecularForm, "Modifier", tfStoichiometry, "Repressor"));
    return interaction;
}

 private static Interaction GetRepressiblePromoterInteraction(String promoterName, String tfName,String tfMolecularForm, int tfStoichiometry)
{
    Interaction tfPromoterInteraction=new Interaction();
    tfPromoterInteraction.setMathName("PromoterRepression");
    tfPromoterInteraction.setInteractionType("Transcriptional repression");
    tfPromoterInteraction.setFreeTextMath(String.format("%s_PoPS * Km / (%s + Km)", promoterName,tfName));
    tfPromoterInteraction.setDescription(String.format("%s TF represses %s promoter", tfName,promoterName));
    tfPromoterInteraction.AddPartDetail(new InteractionPartDetail(tfName, tfMolecularForm, "Modifier", tfStoichiometry, "Repressor"));
    return tfPromoterInteraction;
}

public static Interaction GetPoPSProductionInteraction(String promoterName, double transcriptionRate,String evidenceType)
{
 Interaction interaction=new Interaction( );
    interaction.setName(promoterName + "_PoPSProduction");
    interaction.setInteractionType("PoPS Production");
    interaction.setMathName("PoPSProduction");
    interaction.setFreeTextMath(" -> PoPS");
    interaction.setIsReaction(false);
    interaction.setIsInternal(true);
    interaction.AddPart(promoterName);
    interaction.AddParameter(new Parameter("ktr", "ktr", transcriptionRate,evidenceType,null));
    interaction.AddParameter(new Parameter("PoPS", "PoPSOutput", null,null,"Output"));
    return interaction;
}

public static Interaction GetOperatorInternalInteraction(String operatorName)
{
    Interaction interaction=new Interaction( );
    interaction.setName(operatorName + "_PoPSModulation");
    interaction.setInteractionType("OperatorPoPSModulation");
    interaction.setMathName("OperatorPoPSModulation");
    interaction.setFreeTextMath("");
    interaction.setIsReaction(false);
    interaction.setIsInternal(true);
    interaction.AddPart(operatorName);
    interaction.AddParameter(new Parameter("PoPS", "Input", null,null,"Input"));
    interaction.AddParameter(new Parameter("PoPS", "Output", null,null,"Output"));
    return interaction;
}
	
public static Interaction GetProteinProductionInteraction(String tfName)
{
 Interaction interaction=new Interaction( );
    interaction.setName(tfName + "_Production");
    interaction.setDescription(tfName + " Production");
    interaction.setInteractionType("Protein Production");
    interaction.setMathName("ProteinProduction");
    interaction.setFreeTextMath(" -> " + tfName);
    interaction.setIsReaction(true);
    interaction.setIsInternal(true);
    interaction.AddPart(tfName);
    interaction.AddPartDetail(new InteractionPartDetail(tfName, "Default", "Output", 1, "Species"));
    interaction.AddParameter(new Parameter("RiPS", "RiPSInput", null,null,"Input"));
    interaction.AddParameter(new Parameter("volume", "volume", 1.0,null,"Global"));
    return interaction;
}

public static Interaction  GetDegradationInteraction(String name,String molecularForm)
{
    String molecularFormText="";
    if (!molecularForm.equals("Default"))
    {
        molecularFormText=molecularForm + "_";
    }
   
    Interaction interaction=new Interaction();
    interaction.setName(molecularFormText + name + "_Degradation");
    interaction.setDescription(molecularFormText + name + " Degradation");
    interaction.setInteractionType("Degradation");
    interaction.setMathName("Degradation");
    interaction.setFreeTextMath(molecularFormText + name + " -> ");
    interaction.setIsReaction(true);
    interaction.setIsInternal(true);
    interaction.AddPart(name);
    interaction.AddPartDetail(new InteractionPartDetail(name,molecularForm, "Input", 1, "Species"));
    interaction.AddParameter(new Parameter("kd", "kd", 0.0012,INITIAL_ASSIGNMENT,null));
    return interaction;
}

public static Interaction  GetDePhosphorylationInteraction(String name,double rate,String evidenceType)
{
    Interaction interaction=new Interaction( );
    interaction.setName(name + "_P_Dephosphorylation");
    interaction.setDescription(name + "~P Dephosphorylation");
    interaction.setInteractionType("Dephosphorylation");
    interaction.setMathName("Dephosphorylation");
    interaction.setFreeTextMath(name + "~P -> " + name);
    interaction.setIsReaction(true);
    interaction.setIsInternal(true);
    interaction.AddPart(name);
    interaction.AddPartDetail(new InteractionPartDetail(name,MOLECULAR_FORM_PHOSPHORYLATED, "Input", 1, "SpeciesPhosphorylated"));
    interaction.AddPartDetail(new InteractionPartDetail(name,MOLECULAR_FORM_DEFAULT, "Output", 1, "Species"));      
    interaction.AddParameter(new Parameter("kdeP", "kdeP", rate,evidenceType,null));
    return interaction;
}

public static Interaction GetRBSInteraction(String name, double translationRate)
{
    Interaction interaction=new Interaction( );
    interaction.setName(name + "_RiPSProduction");
    interaction.setDescription(name + " RiPS Production");
    interaction.setInteractionType("RiPS Production");
    interaction.setMathName("RiPSProduction");
    interaction.setFreeTextMath("-> RiPS");
    interaction.setIsReaction(false);
    interaction.setIsInternal(true);
    interaction.AddPart(name);
    interaction.AddParameter(new Parameter("ktl","ktl", translationRate, RBS_CALCULATOR,null ));
    interaction.AddParameter(new Parameter("RiPS","RiPSOutput", null, null,"Output"));
    interaction.AddParameter(new Parameter("mRNA","mRNAInput", null, null,"Input"));
    interaction.AddParameter(new Parameter("volume","volume", 1.0, null,"Global" ));
    return interaction;
}

public static Interaction GetMetabolicReactionInteraction(String interactionName, String description, String freeTextMath, List<Part> substrates, List<Part> products,List<Part> enzymes,boolean isReversible)
{	
	Interaction interaction=new Interaction( );
    interaction.setName(interactionName);
    interaction.setDescription(description);
    interaction.setInteractionType("Metabolic Reaction");
    interaction.setMathName("MetabolicReaction");
    interaction.setFreeTextMath(freeTextMath);
    interaction.setIsReaction(true);
    interaction.setIsInternal(false);
    interaction.setIsReversible(isReversible);
    
    for(Part part:substrates)
    {
    	interaction.AddPart(part.getName());
    	interaction.AddPartDetail(new InteractionPartDetail(part.getName(),MOLECULAR_FORM_DEFAULT, "Input", 1, ""));
    }
    for(Part part:products)
    {
    	interaction.AddPart(part.getName());
    	interaction.AddPartDetail(new InteractionPartDetail(part.getName(),MOLECULAR_FORM_DEFAULT, "Output", 1, ""));
    }
    for(Part part:enzymes)
    {
    	if (part==null || part.getName()==null)
    	{
    		String str="";//TODO 
    		str="";
    	}
    	interaction.AddPart(part.getName());
    	interaction.AddPartDetail(new InteractionPartDetail(part.getName(),MOLECULAR_FORM_DEFAULT, "Modifier", 1, ""));
    }   
    return interaction;
}

}

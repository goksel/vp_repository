package virtualparts.model.kappa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.sbml.jsbml.AbstractSBase;
import org.sbml.jsbml.CVTerm;
import org.sbml.jsbml.Model;

import virtualparts.entity.Formula;
import virtualparts.entity.Interaction;
import virtualparts.entity.InteractionPartDetail;
import virtualparts.entity.MolecularFormType;
import virtualparts.entity.Parameter;
import virtualparts.entity.Part;
import virtualparts.model.ModelBuilder;
import virtualparts.model.ModelOutput;
import virtualparts.model.kappa.entity.Agent;
import virtualparts.model.kappa.entity.AgentDefinition;
import virtualparts.model.kappa.entity.ExpressionVariable;
import virtualparts.model.kappa.entity.ReactionSite;
import virtualparts.model.kappa.entity.Rule;
import virtualparts.model.kappa.entity.RuleAgent;
import virtualparts.model.kappa.entity.Site;
import virtualparts.model.kappa.entity.SiteDefinition;
import virtualparts.model.kappa.entity.ValueVariable;
import virtualparts.model.kappa.entity.Variable;
import virtualparts.model.sbml.VpartsSBMLHelper;

public class KappaBuilder extends ModelBuilder {
	public ModelOutput GetModel(Part part,List<Interaction> partInteractions, List<Part> partsForInternalInteractions, List<Formula> formulas,List<MolecularFormType> molecularFormTypes) throws Exception
	{
		ModelOutput modelOutput=new ModelOutput();		
		KappaModel kappaModel=new KappaModel();
		if (partInteractions!=null)
		{
			Map<String,ArrayList<String>> molecularFormsOfParts=GetMolecularForms(partInteractions,part);
			AddAgentDefinitions(kappaModel,molecularFormsOfParts);
			if (part.getName().toLowerCase().equals("mrna"))
			{
				Agent defaultAgent=GetDefaultAgentDefinition(part.getName());
				kappaModel.AddAgent(defaultAgent);
			}
			for(Interaction interaction: partInteractions)
			{
				if (interaction.getIsInternal())
				{
					Formula formula=GetFormula(interaction, formulas);
					AddInteraction(interaction, kappaModel,interaction.getParameters(), interaction.getPartDetails(),formula,molecularFormTypes);
				}
			}			
		}
		String kappaOutput=kappaModel.toString();
		modelOutput.setMain(kappaOutput);
		return modelOutput;
	}
	
	private Map<String,ArrayList<String>> GetMolecularForms(List<Interaction> partInteractions, Part part)
	{
		Map<String, ArrayList<String>> molecularFormsOfParts=new HashMap<String,ArrayList<String>>();
		
		for(Interaction interaction: partInteractions)
		{
			for (InteractionPartDetail partDetail:interaction.getPartDetails())
			{
				if (partDetail.getPartName().equals(part.getName()))
				{
					AddMolecularForm(molecularFormsOfParts, partDetail.getPartForm(), part.getName());
				}
				else if (interaction.getIsInternal())
				{
					AddMolecularForm(molecularFormsOfParts, partDetail.getPartForm(), partDetail.getPartName());
				}
			}
		}
		return molecularFormsOfParts;
	}
	
	private void AddMolecularForm(Map<String,ArrayList<String>> molecularFormsOfParts,String molecularForm,String partName)
	{
		ArrayList<String> molecularForms= molecularFormsOfParts.get(partName);
		if (molecularForms==null)
		{
			molecularForms=new ArrayList<String>();
		}
		if (!molecularForms.contains(molecularForm))
		{
			molecularForms.add(molecularForm);
			molecularFormsOfParts.put(partName, molecularForms);
		}		
	}
	private void AddAgentDefinitions(KappaModel model,Map<String,ArrayList<String>> molecularFormOfParts)
	{
		for (Map.Entry entry :molecularFormOfParts.entrySet())
		{
			String agentName=(String)entry.getKey();
			List<String> molecularForms=(List<String>)entry.getValue();
		
			AgentDefinition agent=new AgentDefinition(agentName);
			SiteDefinition site=new SiteDefinition("Modification");
			for (String form:molecularForms)
			{
				site.AddState(form);			
			}		
			agent.AddSite(site);
			model.AddAgent(agent);
		}		
	}
	
	private AgentDefinition GetDefaultAgentDefinition(String agentName)
	{
		AgentDefinition agent=new AgentDefinition(agentName);
		SiteDefinition site=new SiteDefinition("Modification");		
		site.AddState("Default");							
		agent.AddSite(site);
		return agent;
	}
	
	public ModelOutput GetModel(Interaction interaction,List<Part> interactionParts,List<Formula> formulas,List<MolecularFormType> molecularFormTypes ) throws Exception 
	{
		ModelOutput modelOutput=new ModelOutput();		
		KappaModel kappaModel=new KappaModel();
		
		Formula formula=GetFormula(interaction, formulas);
		String interactionFormula=GetInteractionFormula(formula);
		AddInteraction(interaction, kappaModel,interaction.getParameters(), interaction.getPartDetails(),formula,molecularFormTypes);
		
		String kappaOutput=kappaModel.toString();
		modelOutput.setMain(kappaOutput);
		return modelOutput;
	}
	
	 
	private void AddInteraction(Interaction interaction,KappaModel model,List<Parameter> parameters,List<InteractionPartDetail> partDetails,Formula formula,List<MolecularFormType> molecularFormTypes ) throws Exception
	{
		if (interaction.getIsReaction())
		{					                    
			Boolean variableAdded=false;
			String reactionRate=formula.getReactionRate();
			Variable variable=null;
			for (Parameter parameter:parameters)
			{
				String newParameterName= parameter.getName();
				if (!parameter.getScope().equals("Global"))
				{
					newParameterName=interaction.getName()  +  parameter.getName();
				}
				
				String toReplace=String.format(VpartsSBMLHelper.REGEXWORDSEARCH, parameter.getName());
				reactionRate= reactionRate.replaceAll(toReplace, "'" + newParameterName + "'");
				if (parameter.getValue()!=null)
				{
					
					variable=new ValueVariable(newParameterName, parameter.getValue(), "Parameter");
					model.AddVariable(variable);
					variableAdded=true;
				}				
			}
			//If there is more than one parameter in the rate definition then create an expression variable and use this variable as the reaction rate
			if ((parameters.size()>1) || (parameters.size()==1 && !variableAdded))
			{
				String newParameterName=interaction.getName()  +  "_rate";
				variable=new ExpressionVariable(newParameterName, reactionRate, "Parameter");
				model.AddVariable(variable);
			}
			
			Rule rule=new Rule(variable);
			rule.setReversible(interaction.getIsReversible());
			for (InteractionPartDetail partDetail:partDetails)
			{
				Agent agent=new RuleAgent(partDetail.getPartName());
				Site site=new ReactionSite("Modification", partDetail.getPartForm(),-1);
				agent.AddSite(site);
				//If there is a part included as part of an internal interaction, then this part should be added as an agent, e.g subtilin
				/*if (interaction.getIsInternal() && !partDetail.getPartName().equals(part.getName()))
				{
					AgentDefinition agentDefinition=new AgentDefinition(partDetail.getPartName());
					SiteDefinition siteDefinition=new SiteDefinition("Modification");
					siteDefinition.
					model.AddAgent(agentDefinition);
				}*/
				if (partDetail.getInteractionRole().equals("Input") )
				{
					rule.AddInput(agent);
				}
				else if (partDetail.getInteractionRole().equals("Output") )
				{
					rule.AddOutput(agent);
				}
				else if (partDetail.getInteractionRole().equals("Modifier") )
				{
					rule.AddInput(agent);
					rule.AddOutput(agent);
				}
			}			
			model.AddRule(rule);
			
		}
		else
		{
			Parameter outputParameter=null;
			String reactionRate=formula.getReactionFlux();
			for (Parameter parameter:parameters)
			{
				
				//Use the interaction name as a spefic to make the variable unique
				String newParameterName= parameter.getName();
				if (!"Global".equals(parameter.getScope()))
				{
					newParameterName=interaction.getName()  +  parameter.getName();
				}
				
				//Interaction formula is used to create an expression Kappa variable, if there is one output parameter.
				//Each variable name must be in quotes in such an expression
				String toReplace=String.format(VpartsSBMLHelper.REGEXWORDSEARCH, parameter.getName());
				
				reactionRate= reactionRate.replaceAll(toReplace, "'" + newParameterName + "'");
				//If the parameter has a value, define this parameter in Kappa
				if (parameter.getValue()!=null)
				{					
					Variable parameterVariable=new ValueVariable(newParameterName, parameter.getValue(), "Parameter");
					model.AddVariable(parameterVariable);
				}
				if ("Output".equals(parameter.getScope()))
				{
					outputParameter=parameter;
				}
			}
			
			
			for (InteractionPartDetail partDetail:partDetails)
			{
				String toReplace=String.format(VpartsSBMLHelper.REGEXWORDSEARCH, partDetail.getMathName());
				RuleAgent ruleAgent=new RuleAgent(partDetail.getPartName());
				ReactionSite site=new ReactionSite("Modification", partDetail.getPartForm(), -1);
				ruleAgent.AddSite(site);
				
				Variable variable=new ExpressionVariable(ruleAgent.toString(), ruleAgent.toString(), "Parameter");
				model.AddVariable(variable);
				
				reactionRate= reactionRate.replaceAll(toReplace, "'" + variable.getName() + "'");
			}
			//If there is an output parameter, then this output parameter is represented as an expression variable.
			if (outputParameter!=null)
			{
				String newParameterName=interaction.getName()  +  outputParameter.getName();
				Variable variable=new ExpressionVariable(newParameterName, reactionRate, "Parameter");
				model.AddVariable(variable);
			}
			
		}
	}
}

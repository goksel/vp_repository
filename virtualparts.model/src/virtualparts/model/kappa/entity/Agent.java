package virtualparts.model.kappa.entity;

import java.util.ArrayList;
import java.util.List;

public abstract class Agent {
	protected String name;
	protected List<Site> sites;
	
	public Agent (String name)
	{
		this.name=name;	
	}
	
	public void AddSite(Site site)
	{
		if (sites==null)
		{
			sites=new ArrayList<Site>();
		}
		sites.add(site);
	}
	
	
}

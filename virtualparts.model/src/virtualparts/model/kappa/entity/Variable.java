package virtualparts.model.kappa.entity;

public abstract class Variable {
	protected String name;

	
	public Variable (String name)
	{
		this.name=name;		
	}
	
	public String getName()
	{
		return this.name;
	}
	
	
}

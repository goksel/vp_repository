package virtualparts.model.kappa.entity;

import java.util.ArrayList;
import java.util.List;

public class SiteDefinition extends Site{

	private List<String> states;
	
	public SiteDefinition(String name)
	{
		super(name);
	}
	

	public void AddState(String state)
	{
		if (states==null)
		{
			states=new ArrayList<String>();
		}
		states.add(state);
	}
	
	@Override public  String  toString()
	{
		StringBuilder builder=new StringBuilder();
		for(int i=0;i<states.size();i++)
		{
			String state=states.get(i);
			builder.append(state);
			if (i<states.size()-1)
			{
				builder.append("~");			
			}
		}
		String result=name + "~" + builder.toString();		
		return result;
	}
}

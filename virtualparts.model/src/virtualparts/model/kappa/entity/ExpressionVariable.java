package virtualparts.model.kappa.entity;

public class ExpressionVariable extends Variable {

	private String value;
	private String comment;
	
	public ExpressionVariable (String name, String value, String comment)
	{
		super(name);
		this.value=value;
		this.comment=comment;
	}
	
	
	@Override public  String  toString()
	{
		StringBuilder builder=new StringBuilder();
		String var="%var:\t'" + this.name + "'\t" + this.value;//String.format("%var:\t'%s'\t%d", this.name,this.value);
		builder.append(var);
		if (this.comment!=null)
		{
			builder.append("\t#");
			builder.append(this.comment);			
		}
		return builder.toString();
	}
}

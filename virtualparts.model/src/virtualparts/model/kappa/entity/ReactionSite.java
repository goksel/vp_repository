package virtualparts.model.kappa.entity;

public class ReactionSite extends Site {
	private int boundIndex;
	private String state;
	public ReactionSite(String name,String state, int boundIndex)
	{
		super(name);
		this.boundIndex=boundIndex;
		this.state=state;
	}
	
	@Override public  String  toString()
	{
		StringBuilder builder=new StringBuilder();
		builder.append(name);
		if (state!=null)
		{
			builder.append("~");
			builder.append(state);			
		}		
		if (boundIndex>-1)
		{
			builder.append("!");
			builder.append(boundIndex);			
		}
				
		String result=builder.toString();		
		return result;
	}
	
}

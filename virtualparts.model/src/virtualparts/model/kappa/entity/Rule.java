package virtualparts.model.kappa.entity;

import java.util.ArrayList;
import java.util.List;

public class Rule {
	private List<Agent> inputs;
	private List<Agent> outputs;
	private Variable variable;
	private boolean isReversible=false;

	public boolean isReversible() {
		return isReversible;
	}

	public void setReversible(boolean isReversible) {
		this.isReversible = isReversible;
	}

	public Rule(Variable variable) {
		this.variable = variable;
	}

	public void AddInput(Agent agent) {
		if (inputs == null) {
			inputs = new ArrayList<Agent>();
		}
		inputs.add(agent);
	}

	public void AddOutput(Agent agent) {
		if (outputs == null) {
			outputs = new ArrayList<Agent>();
		}
		outputs.add(agent);
	}

	@Override
	public String toString() {
		
		String inputString=GetAgents(inputs);
		String outputString=GetAgents(outputs);
		//String result=String.format("%s -> %s\t@'%s'", inputString,outputString,variable.getName());	
		String result=inputString;
		if (this.isReversible)
		{
			result=result  + "<->";
		}
		else
		{
			result=result + "->";
		}
		result=result + outputString;
		if (variable!=null)
		{
			result=result +  "\t@'" + variable.getName() + "'";
		}
		return result;
	}
	
	private String GetAgents(List<Agent> agents)
	{
		StringBuilder builder = new StringBuilder();
		if (agents!=null)
		{
			for (int i = 0; i < agents.size(); i++) {
				Agent agent=agents.get(i);
				builder.append(agent.toString());
				if (i<agents.size()-1)
				{
					builder.append(",");			
				}
			}
		}
		return builder.toString();
		
	}
}

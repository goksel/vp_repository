package virtualparts.model.kappa.entity;

public class AgentDefinition extends Agent {

	public AgentDefinition(String name)
	{
		super(name);
	}
	@Override public  String  toString()
	{
		StringBuilder builder=new StringBuilder();
		if (sites!=null)
		{
			for(int i=0;i<sites.size();i++)
			{
				Site site=sites.get(i);
				builder.append(site.toString());
				if (i<sites.size()-1)
				{
					builder.append(",");			
				}
			}
		}
		//String agent=String.format("%agent:\t%s(%s)", this.name,builder.toString());	
		String agent="%agent:\t" + this.name + "(" + builder.toString() + ")";
		return agent;
	}
}

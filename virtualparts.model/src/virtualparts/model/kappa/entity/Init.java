package virtualparts.model.kappa.entity;

public class Init {
	private String name;
	private double value;
	
	public Init (String name, double value)
	{
		this.name=name;
		this.value=value;
	}
	
	@Override public  String  toString()
	{
		//String init=String.format("%init:\t%d\t%s()", this.value,this.name);	
		String init="%init:\t" + String.valueOf(this.value) + "\t" + this.name + "()";
		return init;
	}
}

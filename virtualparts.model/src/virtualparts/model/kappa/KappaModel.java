package virtualparts.model.kappa;

import java.util.ArrayList;
import java.util.List;

import virtualparts.model.kappa.entity.Agent;
import virtualparts.model.kappa.entity.Init;
import virtualparts.model.kappa.entity.Rule;
import virtualparts.model.kappa.entity.Site;
import virtualparts.model.kappa.entity.Variable;

public class KappaModel {
 private List<Variable> variables;
 private List<Agent> agents;
 private List<Rule> rules;
 private List<Init> inits; 
 
	public KappaModel()
 {
	 
 }
	public void AddVariable(Variable variable)
	{
		if (variables==null)
		{
			variables=new ArrayList<Variable>();
		}
		variables.add(variable);
	}
	
	public void AddAgent(Agent agent)
	{
		if (agents==null)
		{
			agents=new ArrayList<Agent>();
		}
		agents.add(agent);
	}
	
	public void AddRule(Rule rule)
	{
		if (rules==null)
		{
			rules=new ArrayList<Rule>();
		}
		rules.add(rule);
	}
	
	public void AddInit(Init init)
	{
		if (inits==null)
		{
			inits=new ArrayList<Init>();
		}
		inits.add(init);
	}
	
	@Override public  String  toString()
	{
		StringBuilder builder=new StringBuilder();
		if (agents!=null)
		{
			for(Agent agent:agents)
			{			
				builder.append(agent.toString());
				builder.append(System.getProperty("line.separator"));						
			}
		}
		if (inits!=null)
		{
			for(Init init:inits)
			{
				builder.append(init.toString());
				builder.append(System.getProperty("line.separator"));						
			}
		}
		if (variables!=null)
		{
			for(Variable variable:variables)
			{
				builder.append(variable.toString());
				builder.append(System.getProperty("line.separator"));						
			}
		}
		if (rules!=null)
		{
			for(Rule rule:rules)
			{
			builder.append(rule.toString());
				builder.append(System.getProperty("line.separator"));						
			}
		}
		
		String model=builder.toString();
		return model;
	}
	
	public List<Agent> GetAgents()
	{
		return this.agents;		
	}
	
	
}

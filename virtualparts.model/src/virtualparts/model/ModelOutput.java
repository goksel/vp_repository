package virtualparts.model;

import java.util.ArrayList;
import java.util.List;

/**
 * ModelOuput is used to store a model and all of its imported models in a single object. Models are represented as strings.
 * @author a8901379
 */
public class ModelOutput {
private String mainModel="";

/**
 * Gets the String representation of the main model
 * @return String representation of the main model
 */
public String getMain() {
	return this.mainModel;
}

/**
 * Sets the string representation of the main model
 * @param mainModel String representation of the main model
 */
public void setMain(String mainModel) {
	this.mainModel = mainModel;
}

/**
 * Gets the list of imported models in the form of strings
 * @return List of imported models in the form of strings
 */
public List<String> getImports() {
	return imports;
}

/**
 * Sets the list of imported models in the form of strings
 * @param imports List of imported models in the form of strings
 */
public void setImports(List<String> imports) {
	this.imports = imports;
}
private List<String> imports;

/**
 * Adds the string representation of a model to list of imported models
 * @param importModel
 */
public void AddImport (String importModel)
{
	if (this.imports==null)
	{
		this.imports=new ArrayList<String>();
	}
	this.imports.add(importModel);
}

/**
 * Appends a string to the main model
 * @param modelElement
 */
public void AddToMain(String modelElement)
{
	mainModel= mainModel + modelElement;
}

}

package virtualparts.model;

import java.util.List;

import virtualparts.entity.Interaction;
import virtualparts.entity.Formula;
import virtualparts.entity.MolecularFormType;
import virtualparts.entity.Part;
import virtualparts.model.kappa.KappaBuilder;
import virtualparts.model.sbml.SBMLBuilder;



/**
 * This class is used to construct SBML models from object representation of parts and interactions
 * @author a8901379
 */
public class ModelFactory {
	

/**
 * Constructs a model for a given biological part.
 * @param part Part object representing a biological part
 * @param modelType Type of the model.E.g.: SBML
 * @param partInteractions List if internal interactions that are specific to the part
 * @param interactionParts List of all parts included in the given interactions. Environment constants are also considered as part and should therefore be added to the list of parts
 * @param formulaList List of mathematical formulas
 * @param molecularFormTypes List of molecular forms
 * @return ModelOuput object representing the model
 * @throws Exception
 */
public ModelOutput GetModel (Part part,String modelType,List<Interaction> partInteractions,List<Part> interactionParts, List<Formula> formulaList,List<MolecularFormType> molecularFormTypes) throws Exception
{
	ModelBuilder builder=GetModelBuilder(modelType);
	return builder.GetModel(part, partInteractions, interactionParts, formulaList, molecularFormTypes);
}

/**
 * Constructs a model for a given biological interaction.
 * @param interaction Interaction object representing a biological interaction
 * @param modelType Type of the model.E.g.: SBML
 * @param interactionParts List of all parts included in the given interaction.
 * @param formulas List of mathematical formulas
 * @param molecularFormTypes List of molecular forms
 * @return ModelOuput object representing the model
 * @throws Exception
 */
public ModelOutput GetModel (Interaction interaction,String modelType, List<Part> interactionParts, List<Formula> formulas,List<MolecularFormType> molecularFormTypes ) throws Exception
{
	ModelBuilder builder=GetModelBuilder(modelType);	
	return builder.GetModel(interaction,interactionParts,formulas,molecularFormTypes );
}

private ModelBuilder GetModelBuilder(String modelType)
{
	ModelBuilder builder=null;
	if (modelType.equals("cellml"))
	{
		return null;
	}
	else if (modelType.equals("kappa"))
	{
		builder=new KappaBuilder();
	}
	else
	{
		builder=new SBMLBuilder();		
	}
	return builder;
}









	
		/*
private String GetFileContent(String file) throws Exception
{
    StringBuilder contents = new StringBuilder();
    try
    {
        BufferedReader input = new BufferedReader(new java.io.FileReader(file));
        try
        {
            String line = null; //not declared within while loop
            while ((line = input.readLine()) != null)
            {
                contents.append(line);
            }
        }
        finally
        {
            input.close();
        }
    }
    catch (IOException ex)
    {
        ex.printStackTrace();
        throw ex;
    }

    return contents.toString();
}
*/
}

package virtualparts.model.sbml;

import java.util.List;

import org.sbml.jsbml.ASTNode;
import org.sbml.jsbml.AssignmentRule;
import org.sbml.jsbml.KineticLaw;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.Reaction;
import org.sbml.jsbml.Species;

// GMGM import com.sun.xml.internal.fastinfoset.stax.events.Util;


import virtualparts.entity.Interaction;
import virtualparts.entity.InteractionPartDetail;
import virtualparts.entity.Formula;
import virtualparts.entity.MolecularFormType;
import virtualparts.entity.Parameter;
import virtualparts.entity.Part;

public class VpartsSBMLHelper extends SBMLHelper {

	public  static String REGEXWORDSEARCH="\\b%s\\b";
	private static String DEFAULT_FORM_NAME="Default";
	
	private String AddReactionSpecies(Reaction reaction, String interactionFormula,List<InteractionPartDetail> partDetails,List<MolecularFormType> molecularFormTypes) throws Exception
	{
		
		for (InteractionPartDetail  partDetail: partDetails)
		{
			MolecularFormType formType=GetType(partDetail.getPartForm(), molecularFormTypes);
			String speciesName=partDetail.getPartName();
			if (!formType.getName().equals("Default"))
			{
				speciesName=speciesName + formType.getName();					
			}
			Species sbmlSpecies=new Species(speciesName, speciesName, LEVEL, VERSION);						
			if (partDetail.getInteractionRole().toLowerCase().equals("input"))
			{
				reaction.createReactant(sbmlSpecies);
			}
			else if (partDetail.getInteractionRole().toLowerCase().equals("output"))
			{
				reaction.createProduct(sbmlSpecies);
			}
			else if (partDetail.getInteractionRole().toLowerCase().equals("modifier"))
			{
				reaction.createModifier(sbmlSpecies);
			}
			if (interactionFormula!=null && interactionFormula.length()>0)
			{
				String toReplace=String.format(REGEXWORDSEARCH, partDetail.getMathName());
				interactionFormula= interactionFormula.replaceAll(toReplace, speciesName);			
			}
		}
		return interactionFormula;
	}
	
	private MolecularFormType GetType (String name,List<MolecularFormType> types)
	{
		if (types!=null&& types.size()>0)
		{
			for (MolecularFormType type : types)
			{
				if (type.getName().equals(name))
				{
					return type;
				}
			}
		}
		return null;
	}
	
	private String AddKineticLawParameters(Model sbmlModel, List<Parameter> parameters, Interaction interaction, KineticLaw kl, String interactionFormula,Boolean localParameters)  throws Exception
	{
		AnnotationDecorator decorator=new AnnotationDecorator();
		for (Parameter  parameter: parameters)
		{		
			String parameterName=GetParameterID(interaction, parameter);			
			org.sbml.jsbml.Parameter sbmlParameter=CreateParameter(parameterName);
			if (parameter.getScope()==null || parameter.getScope().length()==0 || parameter.getScope().toLowerCase().equals("local"))
			{
				sbmlParameter.setValue(parameter.getValue().doubleValue());				
				kl.addParameter(sbmlParameter);	
			}
			else if (parameter.getScope().toLowerCase().equals("input")) //It is an input to the reaction.So it is an assignment rule in the complete model.
			{//We are only adding the parameter  not the rule. Rule needs to be added dynamically to connect the input to something else in the model
				
				sbmlParameter.setConstant(false);
				sbmlModel.addParameter(sbmlParameter);
				decorator.AnnotateParameter(sbmlParameter, parameter, parameter.getScope());
			}
			else if (parameter.getScope().toLowerCase().equals("global")) //It is an input to the reaction.So it is an assignment rule in the complete model.
			{	
				//It is a global parameter, so use the original name that will be shared in the model. Global parameters are normal parameters (not rules)
				parameterName=parameter.getName();//Update the parameterName variable. It is also used to update the formula below
				sbmlParameter=UpdateParameter(sbmlParameter,parameterName);
				sbmlModel.addParameter(sbmlParameter);
				decorator.AnnotateParameter(sbmlParameter, parameter, parameter.getScope());
			}
			String toReplace=String.format(REGEXWORDSEARCH, parameter.getName());
			interactionFormula= interactionFormula.replaceAll(toReplace, parameterName);
		}
		return interactionFormula;
	}
	
	private void SetReactionKineticLaw(Model sbmlModel,Reaction reaction,List<Parameter> parameters, Interaction interaction,  String interactionFormula,Boolean localParameters) throws Exception
	{
		KineticLaw kl = reaction.createKineticLaw();
		
		// Add the reaction parameters and update the formula
		interactionFormula = AddKineticLawParameters(sbmlModel, parameters, interaction,kl, interactionFormula,localParameters);
		//Parse the formula
		ASTNode math = ASTNode.parseFormula(interactionFormula);
		kl.setMath(math);
		reaction.setKineticLaw(kl);
	}
	
	
	private String UpdateRuleFormulaWithParts(String interactionFormula,List<InteractionPartDetail> partDetails,List<MolecularFormType> molecularFormTypes) throws Exception
	{
		
		for (InteractionPartDetail  partDetail: partDetails)
		{
			MolecularFormType formType = GetType(partDetail.getPartForm(), molecularFormTypes);				
			String partName=partDetail.getPartName() + GetVisualFormName(formType);								
			String toReplace=String.format(REGEXWORDSEARCH, partDetail.getMathName());
			interactionFormula= interactionFormula.replaceAll(toReplace, partName);			
		}
		return interactionFormula;
	}
	
	public static String GetVisualFormName(MolecularFormType formType)
	{
		String formName="";
		if (!formType.getName().equals(DEFAULT_FORM_NAME))
		{
			formName=formType.getName();					
		}
		return formName;
	}
	
	/**
	 * Used to add the global parameters for assignment rules. Assignment Rules can not have local parameters and any parameters called by a rule has to be defined globally
	 * @param rule
	 * @param sbmlModel
	 * @param interaction
	 * @param parameters
	 * @param interactionFormula
	 * @return
	 * @throws Exception
	 */
	public String GetParameterID(Interaction interaction, Parameter parameter)
	{
		return interaction.getName().replace("_", "")  +  parameter.getName();
	}
	private String AddGlobalParameters(AssignmentRule rule, Model sbmlModel,Part part, Interaction interaction,Formula formula, List<Parameter> parameters, String interactionFormula) throws Exception
	{
		Boolean outputFound=false;
		String ruleName="";
		AnnotationDecorator decorator=new AnnotationDecorator();
		Parameter outputParameter=null;
		for (Parameter  parameter: parameters)
		{		
			String parameterName=GetParameterID(interaction,parameter);
			org.sbml.jsbml.Parameter sbmlParameter=CreateParameter(sbmlModel, parameterName);
			if ("Output".equals(parameter.getScope()))
			{
				ruleName=parameterName;
				outputFound=true;
				sbmlParameter.setConstant(false);
				outputParameter=parameter;
			}
			else if ("Input".equals(parameter.getScope()))
			{
				sbmlParameter.setConstant(false);
				decorator.AnnotateParameter(sbmlParameter,parameter, parameter.getScope());
			}
			else if ("Global".equals(parameter.getScope()))
			{								
				parameterName=parameter.getName();
				UpdateParameter(sbmlParameter,parameterName);
				decorator.AnnotateParameter(sbmlParameter,parameter, parameter.getScope());
			}	
			else
			{								
				sbmlParameter.setValue(parameter.getValue());
			}		
			//If there is an SBML rule for the parameter, remove the metaid of the relavant parameter.
			if (sbmlParameter.getId().equals(ruleName))
			{
				sbmlParameter.setMetaId(null);
			}
			//20130921 No need to add this anymore :sbmlModel.addParameter(sbmlParameter);
			String toReplace=String.format(VpartsSBMLHelper.REGEXWORDSEARCH, parameter.getName());
			interactionFormula= interactionFormula.replaceAll(toReplace, parameterName);
		}
		if (!outputFound)
		{
			String message=String.format("%s interaction does not have an output parameter specified",interaction.getName());
			throw new Exception (message);
		}
		
		rule.setMetaId(ruleName + "_rule_meta");
		rule.setVariable(ruleName);
		decorator.AnnotateAssignmentRule(rule, part, interaction, formula, outputParameter);
		return interactionFormula;
		
	}
		
	
	public Reaction AddReaction(Model sbmlModel,Interaction interaction,String interactionFormula,List<InteractionPartDetail> partDetails,List<Parameter> parameters,List<MolecularFormType> molecularFormTypes ) throws Exception
	{
		// Create the reaction
		Reaction reaction = CreateReaction(sbmlModel, interaction.getName());
		// Add the reaction species and update the formula
		interactionFormula = AddReactionSpecies(reaction,interactionFormula, partDetails,molecularFormTypes);
		// Set the kinetic law of the reaction
		if (interactionFormula!=null)
		{
			SetReactionKineticLaw(sbmlModel, reaction, parameters, interaction,interactionFormula,true);
		}
		reaction.setReversible(interaction.getIsReversible());	
		
		return reaction;		
	}
	
	public AssignmentRule AddAssignmentRule(Model sbmlModel,Part part, Interaction interaction,Formula formula,String interactionFormula,List<InteractionPartDetail> partDetails,List<Parameter> parameters,List<MolecularFormType> molecularFormTypes ) throws Exception
	{
 		AssignmentRule rule=sbmlModel.createAssignmentRule();
		interactionFormula=AddGlobalParameters(rule,sbmlModel, part, interaction, formula, parameters, interactionFormula);
		interactionFormula=UpdateRuleFormulaWithParts(interactionFormula,partDetails,molecularFormTypes);
		ASTNode math = ASTNode.parseFormula(interactionFormula);
		rule.setMath(math);
		return rule;
	}
	
	public void AddSpecies(Model sbmlModel, List<Part> parts, Interaction interaction,List<InteractionPartDetail> partDetails,List<MolecularFormType> molecularFormTypes) throws Exception
	{
		AnnotationDecorator decorator=new AnnotationDecorator();
		for (int i=0;i<partDetails.size();i++)
		{			
			InteractionPartDetail  partDetail=partDetails.get(i);
			Part part=parts.get(i);
			MolecularFormType formType=GetType(partDetail.getPartForm(), molecularFormTypes);			
			String speciesName=GetSpeciesID(partDetail, formType);
                        Species species=sbmlModel.getSpecies(speciesName);//GM 20130110
                        if (species==null)
                        {
                            species = CreateSpecies(sbmlModel, speciesName);
                            decorator.AnnotateSpecies(species, partDetail,part);
                        }
		}
	}
	
	public String GetSpeciesID(InteractionPartDetail partDetail,MolecularFormType formType) throws Exception
	{
		//GMGM2013 MolecularFormTypeDataHandler formHandler=new MolecularFormTypeDataHandler();				
		//GMGM2013 MolecularFormType formType = formHandler.GetType(partDetail.getPartForm());	
		String speciesName=partDetail.getPartName();
		if (!formType.getName().equals("Default"))
		{
			speciesName=speciesName + formType.getName();					
		}
		return speciesName;
	}
	
}

package virtualparts.model.sbml;

import java.util.List;
import java.util.Random;

import org.sbml.jsbml.AbstractSBase;
import org.sbml.jsbml.CVTerm;
import org.sbml.jsbml.ListOf;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.Reaction;
import org.sbml.jsbml.Rule;
import org.sbml.jsbml.SBMLDocument;

import virtualparts.entity.Formula;
import virtualparts.entity.Interaction;
import virtualparts.entity.InteractionPartDetail;
import virtualparts.entity.InteractionRole;
import virtualparts.entity.MolecularFormType;
import virtualparts.entity.Parameter;
import virtualparts.entity.Part;
import virtualparts.model.ModelBuilder;
import virtualparts.model.ModelOutput;

public class SBMLBuilder extends ModelBuilder {
	public ModelOutput GetModel(Part part,List<Interaction> partInteractions, List<Part> partsForInternalInteractions, List<Formula> formulas,List<MolecularFormType> molecularFormTypes) throws Exception
	{
		ModelOutput modelOutput=new ModelOutput();		
		if (partInteractions!=null && partInteractions.size()>0)
		{
			AnnotationDecorator decorator=new AnnotationDecorator();
			VpartsSBMLHelper sbmlHelper = new VpartsSBMLHelper();
			SBMLDocument sbmlDocument = new SBMLDocument(SBMLHelper.LEVEL,SBMLHelper.VERSION);
			sbmlDocument.addNamespace("mts","xmlns", "http://purl.org/modeltosequence/1.0#"); 
			
			
			Model sbmlModel = sbmlHelper.CreateSBMLModel(sbmlDocument,part.getName() + "_model");
			
			for(Interaction interaction: partInteractions)
			{
				if (interaction.getIsInternal())
				{
					Formula formula=GetFormula(interaction, formulas);
					String interactionFormula=GetInteractionFormula(formula);				
					AddInteraction(interaction, part, sbmlModel,interaction.getParameters(), interaction.getPartDetails(),interactionFormula,formula,molecularFormTypes);
					List<Part> parts= GetPartsByName(interaction.getPartDetails(), partsForInternalInteractions);
					sbmlHelper.AddSpecies(sbmlModel, parts,interaction,interaction.getPartDetails(),molecularFormTypes);	
					
					//Get interaction inputs and outputs, add them to the parts inputs and outputs
					AddModelInterface(decorator, part, interaction, interaction.getPartDetails(), interaction.getParameters(),molecularFormTypes);								

				}
			}
			
			String modelInterfaceAnnotation=decorator.GetModelInterface();
			decorator.Annotate(sbmlDocument, modelInterfaceAnnotation);
			String model = sbmlHelper.GetSBML(sbmlDocument);
			//GMGM 
			/*ClearModel(sbmlModel);
			sbmlDocument.setModel(null);*/
			sbmlDocument=null;
			/*if (part.getName().toLowerCase().equals("mrna"))
			{
				Random randomGenerator = new Random();
				String randomKey=String.valueOf(randomGenerator.nextLong()).replace("-", "");			
				model=model.replace("<mts:SignalType>mRNA</mts:SignalType", "TEMP");
				model=model.replace("mRNA", "mRNA_" + randomKey);
				model=model.replace("TEMP", "<mts:SignalType>mRNA</mts:SignalType");
				
			}	*/
			
			modelOutput.setMain(model);
			//modelOutput.setMain("");
			
			

		}
		
		return modelOutput;
	}
	
	public ModelOutput GetModel(Interaction interaction,List<Part> interactionParts,List<Formula> formulas,List<MolecularFormType> molecularFormTypes ) throws Exception {
		ModelOutput modelOutput = new ModelOutput();
		VpartsSBMLHelper sbmlHelper = new VpartsSBMLHelper();	
		
		SBMLDocument sbmlDocument = new SBMLDocument(SBMLHelper.LEVEL,SBMLHelper.VERSION);
		sbmlDocument.addNamespace("mts","xmlns", "http://purl.org/modeltosequence/1.0#");        
		Model sbmlModel = sbmlHelper.CreateSBMLModel(sbmlDocument,interaction.getName() + "_model");//GM 20130110
		

		Part part=null;
		if (interaction.getIsInternal())
		{
			if (interactionParts.size()==1)
			{
				part=interactionParts.get(0);				
			}
		}
		
		Formula formula=GetFormula(interaction, formulas);
		String interactionFormula=GetInteractionFormula(formula);	
		AddInteraction(interaction, part,sbmlModel,interaction.getParameters(), interaction.getPartDetails(),interactionFormula,formula,molecularFormTypes);
		//Get interaction inputs and outputs, add them to the parts inputs and outputs
		AnnotationDecorator decorator=new AnnotationDecorator();		
		AddModelInterface(decorator, interaction, interaction.getPartDetails(), interaction.getParameters());
		
		String modelInterfaceAnnotation=decorator.GetModelInterface();
		decorator.Annotate(sbmlDocument, modelInterfaceAnnotation);
		
		String model = sbmlHelper.GetSBML(sbmlDocument);
		//GMGM 
		sbmlDocument=null;
		modelOutput.setMain(model);
		return modelOutput;
	}
	
	private void AddModelInterface(AnnotationDecorator decorator, Part part, Interaction interaction, List<InteractionPartDetail> partDetails,List<Parameter> parameters,List<MolecularFormType> molecularFormTypes) throws Exception
	{
		VpartsSBMLHelper sbmlHelper=new VpartsSBMLHelper();
		if (partDetails!=null)
		{
			for (InteractionPartDetail partDetail: partDetails)
			{
				String role=partDetail.getInteractionRole().toLowerCase();
				MolecularFormType formType=GetType(partDetail.getPartForm(), molecularFormTypes);
				String speciesID=sbmlHelper.GetSpeciesID(partDetail,formType);
				if (role.equals(InteractionRole.OUTPUT.toString().toLowerCase()))
				{
					decorator.AddToModelInterface(part.getName(), false, speciesID);
				}
				else if (role.equals(InteractionRole.MODIFIER.toString().toLowerCase()))
				{//modifiers are added as an input. Eg. Subtilin, an environment variable					
					decorator.AddToModelInterface(part.getName(), true, speciesID);
				}
			}
		}
		
		//Add the public parameters to the model interface. These are the assignment rules as public outputs and inputs
		if (parameters!=null)
		{
			for (Parameter parameter: parameters)
			{
				String parameterName=sbmlHelper.GetParameterID(interaction, parameter);
				String scope="";
				if (parameter.getScope()!=null)
				{
					scope=parameter.getScope().toLowerCase();
				}
				if (scope.equals(InteractionRole.OUTPUT.toString().toLowerCase()))
				{
					decorator.AddToModelInterface(part.getName(), false,parameterName );
				}
				else if (scope.equals(InteractionRole.INPUT.toString().toLowerCase()))
				{				
					decorator.AddToModelInterface(part.getName(), true, parameterName);
				}
			}
		}				
	}
	

	
	
	private void AddModelInterface(AnnotationDecorator decorator, Interaction interaction, List<InteractionPartDetail> partDetails,List<Parameter> parameters) throws Exception
	{
		VpartsSBMLHelper sbmlHelper=new VpartsSBMLHelper();
		/* TODO For now species are not considered as inputs and outputs as long as thhis repository is used to create genetic circuits.
		 * if (partDetails!=null)
		{
			for (InteractionPartDetail partDetail: partDetails)
			{
				String role=partDetail.getInteractionRole().toLowerCase();
				String speciesID=sbmlHelper.GetSpeciesID(partDetail);
				if (role.equals(InteractionRole.OUTPUT.toString().toLowerCase()))
				{
					decorator.AddToModelInterface(partDetail.getPartName(), false, speciesID);
				}
				else if (role.equals(InteractionRole.MODIFIER.toString().toLowerCase()))
				{//modifiers are added as an input. Eg. Subtilin, an environment variable					
					decorator.AddToModelInterface(partDetail.getPartName(), true, speciesID);
				}
			}
		}*/
		
		//Add the public parameters to the model interface. These are the assignment rules as public outputs and inputs
		if (parameters!=null)
		{
			for (Parameter parameter: parameters)
			{
				String parameterName=sbmlHelper.GetParameterID(interaction, parameter);
				String scope="";
				if (parameter.getScope()!=null)
				{
					scope=parameter.getScope().toLowerCase();
					
				}
				if (scope.equals(InteractionRole.OUTPUT.toString().toLowerCase()))
				{
					decorator.AddToModelInterface(interaction.getName(), false, parameterName);
				}	
				else if (scope.equals(InteractionRole.INPUT.toString().toLowerCase()))
				{				
					decorator.AddToModelInterface(interaction.getName(), true, parameterName);
				}
			}
		}				
	}

	private void AddInteraction(Interaction interaction,Part part,Model sbmlModel,List<Parameter> parameters,List<InteractionPartDetail> partDetails,String interactionFormula, Formula formula,List<MolecularFormType> molecularFormTypes ) throws Exception
	{
		AnnotationDecorator decorator=new AnnotationDecorator();
		VpartsSBMLHelper sbmlHelper = new VpartsSBMLHelper();
					
		AbstractSBase sbmlElement=null;
		if (interaction.getIsReaction())
		{
			// Create the reaction
			sbmlElement=sbmlModel.getReaction(interaction.getName());//GM 20130110
                        if (sbmlElement==null)
                        {
                            sbmlElement=sbmlHelper.AddReaction(sbmlModel, interaction, interactionFormula,partDetails, parameters,molecularFormTypes);
                            decorator.AnnotateReaction(sbmlElement, part, interaction, formula);
                        }
		}
		else
		{
			sbmlElement=sbmlHelper.AddAssignmentRule(sbmlModel, part, interaction, formula,interactionFormula,partDetails, parameters,molecularFormTypes);
			//Assignment is already annotated
		}
		if (formula.getAccession()!=null && formula.getAccession().length()>0)
		{
			sbmlElement.addCVTerm(new CVTerm(CVTerm.Type.BIOLOGICAL_QUALIFIER,CVTerm.Qualifier.BQB_IS, formula.getAccession()));
		}
	}
	
	private void ClearModel(Model model)
	{
		ListOf<Reaction> reactions=model.getListOfReactions();
		int count=reactions.size();
		for (int i=0;i<count;i++)
		{
			model.removeReaction(0);
		}
		for (Reaction reaction:reactions)
		{
			model.removeReaction(reaction);
		}
		ListOf<Rule> rules=model.getListOfRules();
		count=rules.size();
		for (int i=0;i<count;i++)
		{
			model.removeRule(0);
		}
		ListOf<org.sbml.jsbml.Parameter> parameters=model.getListOfParameters();
		count=parameters.size();
		for (int i=0;i<parameters.size();i++)
		{
			model.removeParameter(0);
		}		
	}

	
}

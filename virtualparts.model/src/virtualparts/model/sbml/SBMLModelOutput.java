package virtualparts.model.sbml;

import java.util.List;

class SBMLModelOutput {
public List<String> species;
public List<String> parameters;
public List<String> reactions;
public List<String> rules;

}

package virtualparts.model.sbml;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.util.Calendar;
import java.util.TimeZone;

import org.sbml.jsbml.AbstractSBase;
import org.sbml.jsbml.Annotation;
import org.sbml.jsbml.SBMLDocument;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFWriter;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.RDF;
// GMGM import com.sun.xml.internal.fastinfoset.stax.events.Util;




import virtualparts.entity.Interaction;
import virtualparts.entity.InteractionPartDetail;
import virtualparts.entity.Formula;
import virtualparts.entity.Parameter;
import virtualparts.entity.Part;
import virtualparts.entity.InteractionRole;
import virtualparts.entity.SignalType;

class AnnotationDecorator {

	private static String MTSSTART="<ModelToSequence xmlns=\"http://purl.org/modeltosequence/1.0#\">";
	private static String MTSEND="</ModelToSequence>";
	private static String MTSNS="http://purl.org/modeltosequence/1.0#";
	private static String MTSNSPREFIX="mts";
	private static String BQBIOLNS="http://biomodels.net/biology-qualifiers#";
	private static String BQBIOLPREFIX="bqbiol";
	private static String MTSTEMP1="http://local1.com/#";
	private static String MTSTEMP2="http://local2.com/";
	
	private static String NON_CIS="Non_cis_Interaction";
	private static String SIGNAL_CARRIER="Signal_Carrier";
	private static String MRNA_SIGNAL="mRNA";
	private static String PROMOTER="Promoter";
	private static String RBS="Shine_Dalgarno_Sequence";	
	private static String CDS="CDS";
	private static String SHIM="Shim";
	private static String OPERATOR="Operator";
	
	//GMGM2013 
	public static String ENVIRONMENT_CONSTANT_META_TYPE="Environment Constant";
	public static String DEFAULT_FORM_NAME="Default";

	
	
	
	public AnnotationDecorator()
	{
		
	}
	private void test()
	{
		 Model m = ModelFactory.createDefaultModel();
		 String nsA = "http://somewhere/else#";
		 String nsB = "http://nowhere/else#";
		 Resource root = m.createResource( nsA + "root" );
		 Property P = m.createProperty( nsA + "P" );
		 Property Q = m.createProperty( nsB + "Q" );
		 Resource x = m.createResource( nsA + "x" );
		 Resource y = m.createResource( nsA + "y" );
		 Resource z = m.createResource( nsA + "z" );
		 m.add( root, P, x ).add( root, P, y ).add( y, Q, z );
		 System.out.println( "# -- no special prefixes defined" );
		 m.write( System.out );
		 System.out.println( "# -- nsA defined" );
		 m.setNsPrefix( "nsA", nsA );
		 m.write( System.out );
		 System.out.println( "# -- nsA and cat defined" );
		 m.setNsPrefix( "cat", nsB );
		 m.write( System.out );
	}
	private String GetAnnotation(Model m) throws Exception
	{
		String annotation=null;
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		
		try
		{
			RDFWriter fasterWriter = m.getWriter("RDF/XML");
			fasterWriter.setProperty("allowBadURIs","true");
			fasterWriter.setProperty("relativeURIs","");
			fasterWriter.setProperty("tab","0");			   
			fasterWriter.write(m,stream,MTSNS);
			annotation = new String(stream.toString());			
					
		}
		catch (Exception exception)
		{
			System.out.println("------------ERROR IN JENA---------------------------------------------");
			exception.printStackTrace();
			throw exception;
		}
		finally
		{						
			if (stream!=null)
			{
				stream.close();
				stream=null;
			}
			
			
		}						
		annotation=GetMoSeCAnnotation(annotation);	
		return annotation;
	}
	
	// TODO Delete 20131001 
	  private String GetAnnotationOld(Model m) throws Exception
	 
	{
		
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		BufferedOutputStream outputStream =new BufferedOutputStream(stream);
		String annotation=null;
		try
		{
		/*	test();
			m.write( System.out,"RDF/XML");	*/		
			m.write( outputStream);
			//annotation = new String(stream.toByteArray());
			annotation = new String(stream.toString());			
			annotation=GetMoSeCAnnotation(annotation);
			//m.write( outputStream,"RDF/XML");						
		}
		catch (Exception exception)
		{
			System.out.println("------------ERROR IN JENA---------------------------------------------");
			exception.printStackTrace();
			throw exception;
		}
		finally
		{
			if (outputStream!=null)
			{
				outputStream.flush();
				outputStream.close();
				outputStream=null;
			}
			
			if (stream!=null)
			{
				stream.close();
				stream=null;
			}
			
		}
		
		return annotation;
	}
	
	  private String GetAnnotationOld2(Model m) throws Exception
		 
	{
		
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		String annotation=null;
		try
		{
		/*	test();
			m.write( System.out,"RDF/XML");	*/		
			m.write( stream);
			//annotation = new String(stream.toByteArray());
			annotation = new String(stream.toString());			
			annotation=GetMoSeCAnnotation(annotation);
			//m.write( outputStream,"RDF/XML");						
		}
		catch (Exception exception)
		{
			System.out.println("------------ERROR IN JENA---------------------------------------------");
			exception.printStackTrace();
			throw exception;
		}
		finally
		{
			
			if (stream!=null)
			{
				stream.close();
				stream=null;
			}
			
		}
		
		return annotation;
	}
	
	  
	private String GetMoSeCAnnotation(String annotation)
	{
		 annotation= annotation.replace(MTSTEMP1, "#");
		 annotation= annotation.replace(MTSTEMP2, "");		 
		 annotation=MTSSTART + annotation + MTSEND;
		 return annotation;
	}
	
	private void AddProperty(Model model,Resource resource, String name, String value)
	{
		Property property = model.createProperty( MTSNS + name );
		model.add(resource, property, value); 
	}
	
	private void AddProperty(Model model,Resource resource, Property property, String value)
	{
		 Resource typeResource=model.createResource(value);	    
		 resource.addProperty (property, typeResource);	   
	}
	
	private void AddType(Model model,Resource resource,String value)
	{
		AddProperty(this.currentModel,this.currentRoot, RDF.type, MTSTEMP2 + "mts:" + value);		
	}
	
	
	Model interfaceModel=null;
	Resource interfaceResource=null;
	/**
	 * Creates the annotation that represents the part's public inputs and outputs
	 * @param part
	 * @param isInput
	 * @param resourceID
	 */
	public void AddToModelInterface(String partName,boolean isInput, String resourceID)
	{
		if (this.interfaceModel==null)
		{
			this.interfaceModel= ModelFactory.createDefaultModel();
		    this.interfaceModel.setNsPrefix( MTSNSPREFIX, MTSNS );	    		   
		    this.interfaceResource = this.interfaceModel.createResource( MTSTEMP1 + partName + "_meta" );
		}
		if (isInput)
		{
			 AddProperty(interfaceModel,interfaceResource,"Input",resourceID);		 
		}
		else
		{
			 AddProperty(interfaceModel,interfaceResource,"Output",resourceID);		 			
		}				
	}
	
	public String GetModelInterface() throws Exception
	{
		if (this.interfaceModel!=null)
		{
			return GetAnnotation(this.interfaceModel);
		}
		else
		{
			return null;
		}
	}
	
	private Boolean IsDNABased(String type)
	{
		boolean result=false;
		if (type!=null && type.length()>0)
		{
			type=type.toLowerCase();
			if (type.equals(PROMOTER.toLowerCase()) || type.equals(CDS.toLowerCase()) || type.equals(RBS.toLowerCase()) ||  type.equals(SHIM.toLowerCase()) ||  type.equals(OPERATOR.toLowerCase()))
			{
				result= true;
			}
		}		
		return result;		
	}
	
	
	
	public void Annotate(SBMLDocument sbmlDocument, String annotationString)
	{
		if (annotationString!=null)
		{
			Annotation annotation=new Annotation(annotationString);
			sbmlDocument.getModel().setAnnotation(annotation);
		}
	}
	
	
	
	
	private Model currentModel=null;
	private Resource currentRoot=null;
	
	private void CreateResource(String metaID){
		currentModel = ModelFactory.createDefaultModel();
		currentModel.setNsPrefix( MTSNSPREFIX, MTSNS );	       
		currentRoot = currentModel.createResource( MTSTEMP1  + metaID );
	}
    
	private void SetAnnotation(AbstractSBase sbmlElement) throws Exception
	{
		String annotationString=GetAnnotation(this.currentModel);
		Annotation annotation=new Annotation(annotationString);
		sbmlElement.setAnnotation(annotation);	
	}
	
	private void AddGenericAnnotation(Part part, Interaction interaction,Formula formula,String metaID,Model model,Resource root)
	{
		if (formula!=null && IsDNABased(formula.getMetaType()))
		{
			 if (part.getSequence()!=null && part.getSequence().length()>0)
			 {
				 String sequence=part.getSequence().replace(" ","").replace(System.getProperty("line.separator"),"");
				 AddProperty(model,root,"Sequence",sequence);
			 }
			 if (part.getSequenceURI()!=null && part.getSequenceURI().length()>0)
			 {
				 AddProperty(model,root,"SequenceURI",part.getSequenceURI());
			 }	
			 AddProperty(model,root,"IsDNABased","true");
			//It represents a part, add the part's name
			AddProperty(model,root,"VisualName",GetPartVisualName(part));
		}		
		else
		{
			//It represents an interaction, add the interaction's name
			AddProperty(model,root,"VisualName",interaction.getName());	
		}
		if (formula!=null)
		{
			AddType(this.currentModel,this.currentRoot, formula.getMetaType());	
		}
	}
	
	private String GetPartVisualName(Part part) {
		String visualName = part.getDisplayName();
		try {
			boolean found = false;
			if (visualName == null || visualName.length() == 0) {
				visualName = part.getName();
			} else {
				if (part.getType().equals("FunctionalPart")) {
					if (visualName.contains("|")) {
						String[] names = visualName.split("\\|\\|");// \\ is used to escapa each of '|'
						// Get the gene name that is not the locus tag
						for (String name : names) {
							if (Character.isLowerCase(name.charAt(0))
									&& !name.toLowerCase().startsWith("bsu") && name.length()<=4) {
								visualName = name;
								found = true;
								break;
							}
						}
						// If the gene name does not exist, get any name that is
						// not the locus tag
						if (!found) {
							for (String name : names) {
								if (!name.toLowerCase().startsWith("bsu") && name.length()<=4) {
									visualName = name;
									found = true;
									break;
								}
							}
						}
						// If still not found, use the first name
						if (!found) {
							visualName = names[0];
							found = true;
						}
					}
					// Convert all uppercase names to lowercase
					if (visualName.toUpperCase().equals(visualName)) {
						visualName = visualName.toLowerCase();
					}
					// If the name still starts with upper case, convert the
					// first uppercase to lowercase
					if (Character.isUpperCase(visualName.charAt(0))
							&& visualName.length() > 1) {
						visualName = Character
								.toLowerCase(visualName.charAt(0))
								+ visualName.substring(1);
					}
				} else if (part.getType().equals("Promoter")) {
					// For promoters
					visualName = visualName.replace("_full", "");
				} else if (part.getType().equals("RBS")) {
					// For promoters
					visualName = visualName.replace("_RBS", "");
				}
			}
		} catch (Exception exception) {
			visualName = part.getName();
		}
		return visualName;
	}
	
	public void AnnotateAssignmentRule(AbstractSBase sbmlElement,Part part,Interaction interaction,Formula formula,Parameter outputParameter) throws Exception
	{
		String metaID=sbmlElement.getMetaId();
		CreateResource(metaID);
		AddGenericAnnotation(part,interaction,formula,metaID,this.currentModel,this.currentRoot);	
		AddProperty(this.currentModel,this.currentRoot ,"InterfaceType","Output");
		AddProperty(this.currentModel,this.currentRoot ,"SignalType",outputParameter.getParameterType());
		SetAnnotation(sbmlElement);
	}		
	
	public void AnnotateParameter(AbstractSBase sbmlElement,Parameter parameter, String scope)  throws Exception
	{
		String metaID=sbmlElement.getMetaId();
		CreateResource(metaID);
		if (scope.equals("Input"))
		{
			AddProperty(this.currentModel,this.currentRoot ,"InterfaceType","Input");
			AddProperty(this.currentModel,this.currentRoot ,"SignalType",parameter.getParameterType());
		}
		else if (scope.equals("Global"))
		{
			AddProperty(this.currentModel,this.currentRoot ,"IsGlobal","true");
			AddProperty(this.currentModel,this.currentRoot ,"SignalType",parameter.getParameterType());			
		}
		SetAnnotation(sbmlElement);
	}
	
	public void AnnotateReaction(AbstractSBase sbmlElement,Part  part,Interaction interaction,Formula formula)  throws Exception
	{
		String metaID=sbmlElement.getMetaId();
		CreateResource(metaID);
		AddGenericAnnotation(part,interaction,formula,metaID,this.currentModel,this.currentRoot);	
		SetAnnotation(sbmlElement);
	}
	
	public void AnnotateSpecies(AbstractSBase sbmlElement,InteractionPartDetail partDetail, Part part)  throws Exception
	{
		String metaID=sbmlElement.getMetaId();
		CreateResource(metaID);
		AddProperty(this.currentModel,this.currentRoot ,"MolecularForm",partDetail.getPartForm());
		AddProperty(this.currentModel,this.currentRoot ,"Species",partDetail.getPartName());
		//The species in the systems are the outputs.
		String interfaceType=InteractionRole.OUTPUT.toString();
		//As a default they are all species type of signal
		String signalType=SignalType.SPECIES.toString();
		
		if (partDetail.getInteractionRole().equals(InteractionRole.MODIFIER.toString())) 
		{
			//Modifiers are interfaced as inputs to the system
			interfaceType=InteractionRole.INPUT.toString();
			// GMGM20130701PartDataHandler handler=new PartDataHandler();
			//GMGM20130701Vpart part=handler.GetPart(partDetail.getPartName());
			if (part.getMetaType().equals(ENVIRONMENT_CONSTANT_META_TYPE))
			{
				//Environment constant is a subclass of species
				signalType=SignalType.ENVIRONMENTCONSTANT.toString();
			}
		}
		if (partDetail.getPartName().toLowerCase().equals("mrna"))
		{		
			signalType=MRNA_SIGNAL;	
		}
		AddProperty(this.currentModel,this.currentRoot ,"SignalType",signalType);		
		AddProperty(this.currentModel,this.currentRoot ,"InterfaceType",interfaceType);
		String molecularForm="";
		if (!partDetail.getPartForm().equals(DEFAULT_FORM_NAME))
		{
			molecularForm=partDetail.getPartForm();
		}
		AddProperty(this.currentModel,this.currentRoot ,"VisualName",partDetail.getPartName() + molecularForm);
		if (partDetail.getPartName().toLowerCase().equals("mrna"))
		{		
			AddType(this.currentModel,this.currentRoot, SIGNAL_CARRIER);	
		}
		else
		{
			AddType(this.currentModel,this.currentRoot, NON_CIS);	
		}
		
		SetAnnotation(sbmlElement);		
	}
}

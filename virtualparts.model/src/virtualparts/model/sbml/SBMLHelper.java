package virtualparts.model.sbml;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;

import org.sbml.jsbml.ASTNode;
import org.sbml.jsbml.AssignmentRule;
import org.sbml.jsbml.Compartment;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.Parameter;
import org.sbml.jsbml.Reaction;
import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.SBMLWriter;
import org.sbml.jsbml.Species;
import org.sbml.jsbml.Unit;
import org.sbml.jsbml.UnitDefinition;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * This class will be merged with the virtual parts.SBML.SBMLHelper class.
 * @author a8901379
 */
class SBMLHelper {
        /**
         * LEVEL=2
         */
	public static int LEVEL=2;
        /**
         * VERSION=4
         */
	public static int VERSION=4;
        /**
         * COMPARTMENT_CELL=cell
         */
	public static String COMPARTMENT_CELL="cell";
	


public Parameter CreateParameter(Model model, String name, Boolean isConstant)
{
        Parameter parameter=model.createParameter(name);
        parameter.setId(name);
        parameter.setMetaId(name + "_meta");
        parameter.setConstant(isConstant);
        return parameter;
}

public Parameter CreateParameter(Model model, String name)
{
        Parameter parameter=model.createParameter(name);
        parameter.setName(name);
        parameter.setId(name);        
        parameter.setMetaId(name + "_meta");
        return parameter;
}

public Parameter CreateParameter(String name)
{
		Parameter parameter=new org.sbml.jsbml.Parameter(name,LEVEL,VERSION);
		parameter.setName(name);
		parameter.setId(name);		
		parameter.setMetaId(name + "_meta");
        return parameter;
}
public Parameter UpdateParameter(Parameter parameter, String name)
{
		parameter.setId(name);
		parameter.setName(name);
		parameter.setMetaId(name + "_meta");
        return parameter;
}

public AssignmentRule CreateAssignmentRule(Model model, String name, ASTNode math)
    {
        AssignmentRule rule=model.createAssignmentRule();
        rule.setVariable(name);
        rule.setMetaId(name + "_rule_meta");
        rule.setMath(math);
        return rule;
}

    /*TODO Delete 20131001 
     public String GetSBML(SBMLDocument doc) throws Exception {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        SBMLWriter writer= new SBMLWriter();
        writer.write(doc, new BufferedOutputStream(stream));       
        //String output = new String(stream.toByteArray());
        String output=stream.toString();        
        stream.close();
        stream=null;
        //SBMLDocument doc2=new SBMLReader().readSBMLFromString(output);
        //output=FixAnnotation(output);
        return output;
    }*/
    public String GetSBML(SBMLDocument doc) throws Exception {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        SBMLWriter writer= SBMLWriterProvider.getSBMLWriter();
        
        writer.write(doc, new BufferedOutputStream(stream));       
        //String output = new String(stream.toByteArray());
        String output=stream.toString();        
        stream.close();
        stream=null;
        //SBMLDocument doc2=new SBMLReader().readSBMLFromString(output);
        //output=FixAnnotation(output);
        return output;
    }
   
    /**
	 * To be used for jsbml bug. (http://sourceforge.net/tracker/?func=detail&aid=3175833&group_id=279608&atid=1186776)
	 * Bug:Annotation tag is closed after the math tag in kinetic laws and assignent rules 
	 * @param sbmlElement
	 */
/*	private String  FixAnnotation(String output)
	{		
		output=output.replace("</math></annotation>", "</math>");
		int startIndex=0;
		//Add annotation close tag after ModelToSequence closing tag
		while (startIndex>-1)
		{
			int  mtsClosingIndex=output.indexOf("</ModelToSequence>",startIndex);
			if (mtsClosingIndex==-1)
			{
				break;
			}
			int  mathOpeninIndex=output.indexOf("<math",mtsClosingIndex);	
			String space=output.substring(mtsClosingIndex, mathOpeninIndex);
			if (space.indexOf("</annotation>")==-1)
			{
				//TODO Add fix if the space includes rdf closing tags. Then annotation should be added after rdf closing tags
				
				output=output.substring(0,mtsClosingIndex) + "</ModelToSequence></annotation>" + output.substring(mathOpeninIndex);
			}			
			startIndex=mtsClosingIndex + "</ModelToSequence>".length();
			
		}
		return output;
	}
*/

    public Model CreateSBMLModel(SBMLDocument sbmlDocument, String name)
    {
    	Model sbmlModel = sbmlDocument.createModel(name);
        sbmlModel.setMetaId(name + "_meta");
        sbmlModel.setName(name);
        return sbmlModel;
    }
    
    public Species CreateSpecies(Model sbmlModel, String name)
    {
    	Species species=sbmlModel.createSpecies(name);    	
    	species.setMetaId(name + "_meta");
    	species.setId(name);
    	species.setName(name);
    	species.setCompartment(COMPARTMENT_CELL);
    	species.setInitialConcentration(0);
        return species;
    }
    
    public Reaction CreateReaction(Model sbmlModel, String name )
    {
		Reaction reaction = sbmlModel.createReaction(name);
		reaction.setMetaId(name + "_meta");		
		reaction.setId(name);
		reaction.setName(name);
		return reaction;
    }
    //Added for the tests
    /**
     * Creates a UnitDefinition object for a given model
     * @param model The SBML model.
     * @param definitionName The name of the unit definition
     * @param kind The kind of the unit
     * @param scale Scale
     * @return UnitDefinition
     */
    public UnitDefinition CreateUnitDefinition(Model model, String definitionName,Unit.Kind kind, int scale)
    {
        UnitDefinition unitDefinition=new UnitDefinition(definitionName,LEVEL,VERSION);
        Unit unit=unitDefinition.createUnit(kind);
        unit.setScale(scale);
        //unitDefinition.addUnit(unit);
        model.addUnitDefinition(unitDefinition);
        return unitDefinition;
    }
    
    /**
     * Creates a compartment for a given SBML model
     * @param model The SBML model.
     * @param name The name of the compartment
     * @return Compartment
     */
    public Compartment CreateCompartment(Model model, String name)
    {
        Compartment compartment=new Compartment(name, name, LEVEL, VERSION);
        compartment.setSize(1);
        compartment.setMetaId(name + "_meta");
        compartment.setId(name);
        model.addCompartment(compartment);
        return compartment;
    }

    public String GetCellAnnotation()
    {
    String annotation="<ModelToSequence xmlns=\"http://purl.org/modeltosequence/1.0#\">" + 
                          "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" xmlns:mts=\"http://purl.org/modeltosequence/1.0#\">" +
                              "<rdf:Description rdf:about=\"#cell_meta\">" +
                              "<rdf:type rdf:resource=\"mts:Chassis\" />" +
                              "<mts:TerminatorSequence>taaaggacgccgccaagccagcttaaacccagctcaatgagctgggttttttgtttgttaa</mts:TerminatorSequence>" +
                              "</rdf:Description>" +
                          "</rdf:RDF>" +
                      "</ModelToSequence>";
        return annotation;
    }
}

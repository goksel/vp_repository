package virtualparts.model.sbml;

import org.sbml.jsbml.SBMLWriter;

public class SBMLWriterProvider {
	 private static SBMLWriter writer = new SBMLWriter( );
	   	   
	   private SBMLWriterProvider(){ }
	   	   
	   public static SBMLWriter getSBMLWriter( ) {
		   return writer;
	   }	  
}
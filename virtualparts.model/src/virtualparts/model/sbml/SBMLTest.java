package virtualparts.model.sbml;

import static org.junit.Assert.*;

import java.awt.event.FocusAdapter;
import java.io.ObjectInputStream.GetField;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.sbml.jsbml.Annotation;
import org.sbml.jsbml.Compartment;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.Parameter;
import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.Unit.Kind;

import virtualparts.entity.Formula;
import virtualparts.entity.Interaction;
import virtualparts.entity.InteractionPartDetail;
import virtualparts.entity.MolecularFormType;
import virtualparts.entity.Part;
import virtualparts.model.ModelFactory;
import virtualparts.model.ModelOutput;



public class SBMLTest {

	public SBMLDocument GetSBMLTemplateModel(String modelName)
    {
        SBMLHelper sbmlHelper=new SBMLHelper();
        SBMLDocument sbmlDocument = new SBMLDocument(SBMLHelper.LEVEL,SBMLHelper.VERSION);
        sbmlDocument.addNamespace("mts","xmlns", "http://purl.org/modeltosequence/1.0#");
        Model model = sbmlHelper.CreateSBMLModel(sbmlDocument,modelName);
        Parameter parameter=sbmlHelper.CreateParameter(model, "volume");
        parameter.setValue(1);
        sbmlHelper.CreateUnitDefinition(model, "volume", Kind.LITRE, -15);
        sbmlHelper.CreateUnitDefinition(model, "substance", Kind.MOLE, -9);
        Compartment compartment=sbmlHelper.CreateCompartment(model, "cell");
        compartment.setAnnotation(new Annotation(sbmlHelper.GetCellAnnotation()));
        return sbmlDocument;
    }
	
	@Test
	public void testSBML() {
		SBMLDocument sbmlDocument=GetSBMLTemplateModel("testmodel");
		assert(sbmlDocument!=null);
		assert(sbmlDocument.getModel()!=null);	
		assert(sbmlDocument.getModel().getParameter("volume")!=null);	
		assert(sbmlDocument.getModel().getParameter("volumedoesnotexist")==null);					
	}
	
	@Test
	public void testModel() throws Exception {
		ModelFactory modelFactory=new ModelFactory();
		Part part=GetPart();
		List<Interaction> interactions=GetPartInteractions();
		List<Part> parts=new ArrayList<Part>();
		parts.add(part);
		List<Formula> formulas=GetMaths();
		List<MolecularFormType> forms=GetMolecularForms();
		ModelOutput output= modelFactory.GetModel(part, "SBML", interactions, parts, formulas, forms);
		System.out.print(output.getMain());
		assert(output!=null);
	}
	
	private Part GetPart()
	{
		Part part=new Part();
		part.setName("PspaRK");
		part.setType("Promoter");
		part.setDescription("Constitutive sigmaH type promoter");
		part.setMetaType("Part");
		part.setSequence("gcatgaaataaattcaggggtattgatgatggtttttggtataatatgtacaatcattg");
		part.setOrganism("Bacillus subtilis ATCC6633");
		part.setStatus("Works");
		part.setDesignMethod("Manual");
		return part;
	}
	
	private List<Interaction> GetPartInteractions()
	{
		List<Interaction> interactions=new ArrayList<Interaction>();
		Interaction interaction=new Interaction();
		interaction.setName("PspaRK_PoPSProduction");
		interaction.setInteractionType("PoPS Production");
		interaction.setMathName("PoPSProduction");
		interaction.setFreeTextMath(" -> PoPS");
		interaction.setIsReaction(false);
		interaction.setIsInternal(true);
		interaction.AddPart("PspaRK");
		virtualparts.entity.Parameter parameter1=new virtualparts.entity.Parameter("ktr", "ktr", 0.04);
		virtualparts.entity.Parameter parameter2=new virtualparts.entity.Parameter();
		parameter2.setName("PoPSOutput");
		parameter2.setParameterType("PoPS");
		parameter2.setScope("Output");		
		interaction.AddParameter(parameter1);
		interaction.AddParameter(parameter2);	
		List<InteractionPartDetail> partDetails=new ArrayList<InteractionPartDetail>();
		interaction.setPartDetails(partDetails);
		interactions.add(interaction);
		return interactions;
	}
	
	private List<Formula> GetMaths()
	{
		List<Formula> formulas=new ArrayList<Formula>();
		Formula formula=new Formula();
		formula.setName("PoPSProduction");
		formula.setReactionFlux("1 * ktr");
		formula.setMetaType("Promoter");
		formulas.add(formula);
		return formulas;
		
	}
	
	private List<MolecularFormType> GetMolecularForms()
	{
		List<MolecularFormType> forms=new ArrayList<MolecularFormType>();
		MolecularFormType form=new MolecularFormType();
		form.setName("Default");	
		forms.add(form);
		return forms;
		
	}		

}
